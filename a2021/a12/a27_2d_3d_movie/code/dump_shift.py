import argparse
import os
import glob
import tqdm
import shutil

from a2021.a12.a24_3d_to_2d.thylacine.file_name import (
    parse_epoch,
    parse_file_name,
    get_header_path
)

def parse_command_line_args():
    parser = argparse.ArgumentParser(
        description=(
            'Renames PROMPI data files in given directory by decrease the'
            'number part by given number `shift`. '
            ''
            'Example: '
            'Files in direcotry start with ccptwo.2D.04494. prefix'
            'If shift is -1000, then all files will be renamed'
            'such that the first file has ccptwo.2D.03494 prefix.'
        )
    )

    parser.add_argument(
        'input_dir',
        help='Directory containing PROMPI files to rename'
    )

    parser.add_argument(
        'output_dir',
        help='Directory containing PROMPI files to rename'
    )

    parser.add_argument(
        'shift',
        type=int,
        help='Number by which the numeric part of file names will be shifted'
    )

    return parser.parse_args()


def all_bindata_files(intput_dir):
    pattern = os.path.join(intput_dir, '*.bindata')
    return sorted(list(glob.glob(pattern)))


def calc_dest_name(input_file, new_epoch):
    parsed = parse_file_name(input_file)
    number_length = len(parsed['number'])
    new_number = str(new_epoch).rjust(number_length, '0')
    return f"{parsed['prefix']}.{new_number}.{parsed['extension']}"


def rename_file(input_file, output_dir, shift):
    old_epoch = parse_epoch(input_file)
    new_epoch = old_epoch + shift
    if new_epoch < 1: return None  # Ignore negative file names
    dest_name = calc_dest_name(input_file=input_file, new_epoch=new_epoch)
    dest_path = os.path.join(output_dir, dest_name)
    shutil.copyfile(input_file, dest_path)


def rename_bindata_and_header(bindata_file, output_dir, shift):
    rename_file(input_file=bindata_file, output_dir=output_dir, shift=shift)
    header_file = get_header_path(bindata_file)
    rename_file(input_file=header_file, output_dir=output_dir, shift=shift)


def rename_files(intput_dir, output_dir, shift):
    os.makedirs(output_dir, exist_ok=True)
    bindata_files = all_bindata_files(intput_dir)

    for bindata_file in tqdm.tqdm(bindata_files):
        rename_bindata_and_header(
            bindata_file=bindata_file, output_dir=output_dir, shift=shift)


def lets_goooooo():
    args = parse_command_line_args()
    rename_files(intput_dir=args.input_dir, output_dir=args.output_dir, shift=args.shift)
    print(f"Success: renamed files in '{args.output_dir}'")


if __name__ == '__main__':
    lets_goooooo()
    print('We are done!')
