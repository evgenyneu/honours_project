import os
import tqdm
import subprocess
import multiprocessing
from shutil import copyfile
from datetime import datetime
import matplotlib.pyplot as plt
from multiprocessing import Pool

from a2021.a12.a27_2d_3d_movie.code.compare_times import check_same_times

from a2021.a12.a27_2d_3d_movie.code.plot import (
    set_plot_style,
    make_plot_for_real
)


def make_movie(plot_dir, movie_dir, movie_name, frame_rate):
    """
    Creates a movie .mp4 file from individual images.

    Parameters
    ----------
    plot_dir : str
        Path to directory containing individual images for the movie.
    movie_name: str
        Name of the output movie file.
    movie_dir : str
        The output directory of the movie
    frame_rate : int
        The frame rate of the movie, in frames per second.
    """

    status, _ = subprocess.getstatusoutput('which ffmpeg')

    if status != 0:
        raise Exception("ffmpeg command not found, please install ffmpeg.")

    src_movie = os.path.join(plot_dir, movie_name)

    if os.path.exists(src_movie):
        os.remove(src_movie)

    command = (f"ffmpeg -framerate {frame_rate} -pattern_type glob -i '*.png' "
               f"-c:v libx264 -pix_fmt yuv420p {movie_name}")

    subprocess.call(command, cwd=plot_dir, shell=True)

    # Copy movie to output directory
    # ----------

    dest_movie = os.path.join(movie_dir, movie_name)
    os.makedirs(movie_dir, exist_ok=True)

    if os.path.exists(dest_movie):
        os.remove(dest_movie)

    copyfile(src_movie, dest_movie)


def output_image_path(epoch, dest_dir):
    filename = f'{epoch:05d}.png'
    return os.path.join(dest_dir, filename)


def make_and_save_plot(data_name1, data_name2, epoch, dest_dir):
    set_plot_style()

    fig, _ = make_plot_for_real(
        epoch=epoch, variable='0002', zindex=0,
        left_plot=data_name1, right_plot=data_name2,
        perturbation=False,
        colormap='turbo', percentile=100, stretch=0.65, full3d=True
    )


    dest_path = output_image_path(epoch=epoch, dest_dir=dest_dir)
    os.makedirs(dest_dir, exist_ok=True)

    # Manual fiddling to achieve 1080 pixels height to get 1080p video
    # and avoid resizing on youtube
    fig.savefig(dest_path, dpi=148, bbox_inches='tight', pad_inches=0.108)
    plt.close(fig)


def make_and_save_plot_with_params(params):
    make_and_save_plot(**params)


def make_plots_in_parallel(data_name1, data_name2, dest_dir, epoch_start, epoch_end):
    params = [
        {
            'epoch': epoch,
            'dest_dir': dest_dir,
            'data_name1': data_name1,
            'data_name2': data_name2
        }
        for epoch in range(epoch_start, epoch_end + 1)
    ]

    processes = multiprocessing.cpu_count() - 2 # Leave two CPU cores free
    if processes < 0: processes = 1

    with Pool(processes=processes) as p:
        list(
            tqdm.tqdm(
                p.imap(make_and_save_plot_with_params, params),
                total=len(params)
            )
        )


def lets_goooooo():
    data_name1='2D_264_for_movie'
    data_name2='3D_264_for_movie'

    check_same_times(data_name1=data_name1,
                     data_name2=data_name2,
                     tolerance=0.5)

    frames_dir = '/Users/evgenii/Downloads/movies'

    make_plots_in_parallel(data_name1=data_name1,
                           data_name2=data_name2,
                           dest_dir=frames_dir,
                           epoch_start=300, epoch_end=2305)

    movie_name = f'{datetime.now():%Y-%m-%d_%H-%M-%S}_stellar_convection_simulation_2d_vs_3d.mp4'

    make_movie(plot_dir=frames_dir,
               movie_dir='/Users/evgenii/Downloads/movie_out',
               movie_name=movie_name,
               frame_rate=30)


if __name__ == '__main__':
    lets_goooooo( )
    print("We are done!")
