from mpl_toolkits.axes_grid1.inset_locator import InsetPosition
from astropy.visualization import (ImageNormalize, PercentileInterval, PowerStretch)
import seaborn as sns
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import numpy as np
from a2021.a11.a17_plot_lum_0_01.code.data import data_info

from a2021.a11.a17_plot_lum_0_01.code.load_data import (
    load_data,
    data_time,
    calc_gradients_2d,
    calc_gradients_3d,
    CUSTOM_VARIABLES
)

from a2021.a11.a12_perturbation_single_frame.code.perturbation import calc_perturbation

def set_plot_style():
    """Set global plotting style"""

    sns.set(style="ticks")

    # Text sizes
    # -------
    mpl.rcParams['figure.titlesize'] = 20
    mpl.rcParams['axes.titlesize'] = 16
    mpl.rcParams['font.size'] = 11
    mpl.rcParams['axes.labelsize'] = 13
    mpl.rcParams['xtick.labelsize'] = 11
    mpl.rcParams['ytick.labelsize'] = 11


    # Color
    # -------

    TEXT_COLOR = '#8888cc'
    mpl.rcParams['figure.facecolor'] = '#000033'
    mpl.rcParams['axes.edgecolor'] = TEXT_COLOR
    mpl.rcParams['text.color'] = TEXT_COLOR
    mpl.rcParams['axes.labelcolor'] = TEXT_COLOR
    mpl.rcParams['xtick.color'] = TEXT_COLOR
    mpl.rcParams['ytick.color'] = TEXT_COLOR


def load_data_and_gradients(epoch, zindex, variable, data_name,
                              custom_variables, perturbation, full3d):

    data = load_data(epoch=epoch, variable=variable,
                     info=data_info(data_name),
                     custom_variables=custom_variables,
                     full3d=full3d)

    if perturbation: data = calc_perturbation(data)

    if len(data.shape) == 3: # 3D
        gradients = calc_gradients_3d(data)
        data_2d = data[:, :, zindex]
    else:
        data_2d = data
        data = data_2d
        gradients = calc_gradients_2d(data)

    return {
        'data_2d': data_2d,
        'data': data,
        'gradients': gradients
    }


def load_all_data(epoch, variable, compare, zindex, custom_variables,
                  perturbation, full3d):

    data1 = load_data_and_gradients(
        epoch=epoch, zindex=zindex, variable=variable,
        data_name=compare[0], custom_variables=custom_variables,
        perturbation=perturbation,
        full3d=full3d
    )

    data2 = load_data_and_gradients(
        epoch=epoch, zindex=zindex, variable=variable,
        data_name=compare[1], custom_variables=custom_variables,
        perturbation=perturbation,
        full3d=full3d
    )

    return data1, data2


def set_plot_axes(axes, compare):
    # Left top image
    axes[0, 0].set_title('2D')
    axes[0, 0].tick_params(bottom=False, labelbottom=False)
    axes[0, 0].set_yticks([0, 0.2, 0.4, 0.6])
    axes[0, 0].invert_yaxis()

    # Right top image
    axes[0, 1].set_title('3D')
    axes[0, 1].tick_params(left=False, labelleft=False, bottom=False, labelbottom=False)
    axes[0, 1].invert_yaxis()
    axes[0, 1].sharey(axes[0, 0])
    axes[0, 1].set_ylim(0, 0.65)

    # Top right colorbar
    axes[0, 2].set_yticks(np.arange(0.1, 0.5, 0.1))
    axes[0, 2].set_ylabel('Concentration gradient')

    # Left bottom image
    axes[1, 0].set_yticks([0, 0.2, 0.4, 0.6])
    axes[1, 0].set_xticks(axes[0, 0].get_xticks()[0:-1])
    axes[1, 0].invert_yaxis()

    # Right bottom image
    axes[1, 1].tick_params(left=False, labelleft=False)
    axes[1, 1].invert_yaxis()
    axes[1, 1].sharey(axes[1, 0])
    axes[1, 1].set_ylim(0, 0.65)

    # Bottom right colorbar
    axes[1, 2].set_ylabel('Concentration')


def make_image_plots(fig, axes, data1, data2, colormap, percentile, stretch, vmax):
    # Map data to colors
    combined_data = np.concatenate([data1.flatten(), data2.flatten()])
    interval = PercentileInterval(percentile)
    stretch_obj = PowerStretch(stretch)
    normalizer = ImageNormalize(combined_data, interval=interval, stretch=stretch_obj, vmax=vmax)
    im = cm.ScalarMappable(norm=normalizer, cmap=colormap)

    # Makes images a bit sharper than default interpolation
    interpolation = 'spline16'

    # Show data
    axes[0].imshow(data1, cmap=colormap, norm=normalizer, extent=(0, 1, 1, 0), interpolation=interpolation)
    axes[1].imshow(data2, cmap=colormap, norm=normalizer, extent=(0, 1, 1, 0), interpolation=interpolation)

    # Show colorbar
    ip = InsetPosition(axes[1], [1.014, 0, 0.04, 1])
    axes[2].set_axes_locator(ip)
    fig.colorbar(im, cax=axes[2])


def get_gradient_img(data, data_name):
    if len(data['gradients'].shape) == 3: # 3D
        # Ignore z component of 3D gradient on the plot
        return calc_gradients_2d(data['data_2d'])

    return data['gradients']


def load_and_show(
    fig, axes, epoch, compare, variable, colormap,
    show_gradient, zindex, custom_variables, perturbation,
    percentile, stretch, full3d, vmax):

    data1, data2 = load_all_data(
        epoch=epoch, variable=variable, compare=compare, zindex=zindex,
        custom_variables=custom_variables, perturbation=perturbation,
        full3d=full3d
    )

    img_data1 = data1['data_2d']
    img_data2 = data2['data_2d']

    if show_gradient:
        img_data1 = get_gradient_img(data1, compare[0])
        img_data2 = get_gradient_img(data2, compare[1])

    make_image_plots(fig=fig, axes=axes,
                     data1=img_data1,
                     data2=img_data2,
                     colormap=colormap,
                     percentile=percentile,
                     stretch=stretch,
                     vmax=vmax)


def configure_figure(fig):
    fig.suptitle("Simulation of stellar convection", x=0.48, y=0.93)


def show_time(fig, epoch, data_name):
    time = data_time(epoch=epoch, info=data_info(data_name))
    fig.text(x=0.124, y=0.887, s=f'Time: {time:.1f} s')


def make_plot(epoch, compare, variable, colormap,
               zindex, custom_variables, perturbation,
               percentile, stretch, full3d):

    fig, axes = plt.subplots(2, 3,
        figsize=(14, 8.32),
        gridspec_kw={
            'width_ratios': [1, 1, 0.2],
            'wspace': 0.02,
            'hspace': 0.02
        },
    )

    load_and_show(fig=fig, axes=axes[0,:], epoch=epoch, compare=compare,
        variable=variable, colormap=colormap,
        show_gradient=True, zindex=zindex,
        custom_variables=custom_variables, perturbation=perturbation,
        percentile=percentile, stretch=stretch, full3d=full3d,
        vmax=0.45
    )

    load_and_show(fig=fig, axes=axes[1,:], epoch=epoch, compare=compare,
        variable=variable, colormap=colormap,
        show_gradient=False, zindex=zindex,
        custom_variables=custom_variables, perturbation=perturbation,
        percentile=percentile, stretch=stretch, full3d=full3d,
        vmax=1
    )

    set_plot_axes(axes, compare=compare)
    show_time(fig=fig, epoch=epoch, data_name=compare[0])
    configure_figure(fig=fig)

    return fig, axes


def make_plot_for_real(epoch, variable, zindex,
                left_plot, right_plot, perturbation, colormap,
                percentile, stretch, full3d):

    fig, axes = make_plot(epoch=epoch,
                     compare=[left_plot, right_plot],
                     variable=variable,
                     colormap=colormap,
                     zindex=zindex,
                     custom_variables=CUSTOM_VARIABLES,
                     perturbation=perturbation,
                     percentile=percentile,
                     stretch=stretch,
                     full3d=full3d)

    return fig, axes


