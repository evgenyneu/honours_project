from a2021.a11.a17_plot_lum_0_01.code.data import data_info
from a2021.a12.a24_3d_to_2d.thylacine.reader import get_time

from a2021.a11.a17_plot_lum_0_01.code.load_data import (
    bindata_path,
    locate_epochs
)

def min_max_epochs(data_name):
    info = data_info(data_name)
    epochs = locate_epochs(info['dir'])
    min_epoch = min(epochs)
    max_epoch = max(epochs)

    if max_epoch - min_epoch != len(epochs) - 1:
        raise Exception(f"Some epochs are missing")

    return min_epoch, max_epoch


def get_frame_time(epoch, data_name):
    info = data_info(data_name)

    data_path = bindata_path(epoch=epoch,
                             data_dir=info['dir'],
                             data_filename=info['file_name'])

    return get_time(data_path)


def compare_times(epoch, compare, tolerance):
    time1 = get_frame_time(epoch, compare[0])
    time2 = get_frame_time(epoch, compare[1])

    if abs(time1 - time2) > tolerance:
        raise Exception(f"Frame times do not match {time1} != {time2}, epoch {epoch}")


def check_same_times(data_name1, data_name2, tolerance):
    """
    Compare the timstamps for all dumps from two different simulations
    to make sure they roughly match (within `tolerance`).
    """

    epoch_start1, epoch_end1 = min_max_epochs(data_name=data_name1)
    epoch_start2, epoch_end2 = min_max_epochs(data_name=data_name2)
    min_epoch = min(epoch_end1, epoch_end2)

    if epoch_start1 != epoch_start2:
        raise Exception('Different number of snapshots')

    for epoch in range(epoch_start1, min_epoch + 1):
        compare_times(epoch, [data_name1, data_name2], tolerance)

if __name__ == '__main__':
    lets_goooooo( )
    print("We are done!")
