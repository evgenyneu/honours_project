## Animate 2D/3D

## Find start dump for 2D

### Data

* 2D data: data/a2021/a12/a22/2d_for_movie_0_823_percent_lum_with_0_25_sec_interval
* 3D data: data/a2021/a12/a25/3d_for_movie_1_23_percent_lum_with_0_25_sec_interval

### Dumps

2D dump: 5942, time: 0.14954050E+04
3D dump: 300, time: 0.14954195E+04


### Rename 2D files

I want to rename 2D files to make the numbers match 3D. The numbers in both should start from 300.

```
python -m a2021.a12.a27_2d_3d_movie.code.dump_shift '/Users/evgenii/Documents/honours_project/data/a2021/a12/a22/2d_for_movie_0_823_percent_lum_with_0_25_sec_interval' '/Users/evgenii/Documents/honours_project/data/a2021/a12/a27/2d_for_movie_0_823_percent_lum_with_0_25_sec_interval' -5642
```

### VSCode task

```
{
  "label": "Run dump shift",
  "type": "shell",
  "command": ". .venv/bin/activate && PYTHONPATH=. && python -m a2021.a12.a27_2d_3d_movie.code.dump_shift '/Users/evgenii/Downloads/2d_for_movie_0_823_percent_lum_with_0_25_sec_interval' '/Users/evgenii/Downloads/2d_renamed' -5642",
  "problemMatcher": [],
  "group": {
    "kind": "build",
    "isDefault": true
  }
}
```

## TODO: compress 3D 1032^2 file for movie


```
sbatch slurm_jobs/2021/12/13/gz_copy.sh /scratch/ew6/evgenyneu/PROMPI/setups/ccp_two_layers/2d /group/ew6/evgenyneu/prompi_output/2021_12_17_2d_1032x1032_for_movie
```
