## Main Result

* 2D/3D boundary profiles look similar.

* Density/Pressure profiles seam similar.

* 2/3 luminosity rule.

* Rate of boundary advance depends on input luminosity.


## Todo

Line plots for the x/y velocities and composition.


## Movie

40 sec


##  Ask Emily

About 2 PM Mon, Two, Thu for Simon.


### Compress and copy files


```
sbatch slurm_jobs/2021/09/26/gz_copy.sh /scratch/ew6/evgenyneu/PROMPI/setups/ccp_two_layers/3d /group/ew6/evgenyneu/prompi_output/2021_12_08_lum_1_2345_percent_3d_from_204
```

Job ID: 5315768


## Download

```
mkdir -p /Users/evgenii/Documents/honours_project/data/a2021/a12/a08/lum_1_2345_percent_3d_from_204

rsync -Pvhtr --bwlimit=4000 pawsey:/group/ew6/evgenyneu/prompi_output/2021_12_08_lum_1_2345_percent_3d_from_204/ /Users/evgenii/Documents/honours_project/data/a2021/a12/a08/lum_1_2345_percent_3d_from_204
```


