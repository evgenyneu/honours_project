## Extract copied files

```
cd /Users/evgenii/Documents/honours_project/logbook/a2021/a12/a10_plot_1.2_percent_3d_from_204/code

python extract_bindata.py /Users/evgenii/Documents/honours_project/data/a2021/a12/a13/lum_1_2345_percent_3d_from_379
```


## Continue 3d 1.2% lum from frame 551

```
cd /scratch/ew6/evgenyneu/PROMPI/setups/ccp_two_layers/3d
cp ccptwo.3D.00551* ./data/
```

### Balance before

```
Compute Information
-------------------
          Project ID     Allocation          Usage     % used
          ----------     ----------          -----     ------
                 ew6         875000         117033       13.4
         --evgenyneu                        117033       13.4
```


### Run

```
Job: `. slurm_jobs/2021/12/14/lum_1.2345_percent_continue_from_551.sh`
Job ids: 7285840 7285841
```

