### Compress and copy files using multiprocessing


```
sbatch slurm_jobs/2021/12/13/gz_copy.sh /scratch/ew6/evgenyneu/PROMPI/setups/ccp_two_layers/2d /group/ew6/evgenyneu/prompi_output/2021_12_18_2d_1032x1032
```

Job ID:


## Download

```
mkdir -p /Users/evgenii/Documents/honours_project/data/a2021/a12/a19/2d_1032x1032

rsync -Pvhtr --bwlimit=4000 pawsey:/group/ew6/evgenyneu/prompi_output/2021_12_18_2d_1032x1032 /Users/evgenii/Documents/honours_project/data/a2021/a12/a19/2d_1032x1032
```



