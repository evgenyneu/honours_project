## Restarts 2d 1032^2 from 509

Made a mistake yesterday, it did not run, repeat

### Run

```
Job: . slurm_jobs/2021/12/20/resume_509_2d_1032x1032_lum_0.823_percent.sh
Job ids: 7309371 7309372
```


Failed: [lob_failed.txt](a2021/a12/a21_restart_1032x1032_from_509/lob_failed.txt)


## Restart 264 2d and 3d from frame 300 to frame 400

Using 0.25 sec interval between frames, to be used for a movie.

### Copy data files to Pawsey

#### 2D

```
scp /Users/evgenii/Documents/honours_project/data/a2021/a12/a02/0_823_percent_lum_0_sec/ccptwo.2D.00300* pawsey:/scratch/ew6/evgenyneu/prompi_movie/setups/ccp_two_layers/2d/data/
```
#### 3D

```
scp /Users/evgenii/Documents/honours_project/data/a2021/a12/a06/lum_1_2345_percent_3d/ccptwo.3D.00300* pawsey:/scratch/ew6/evgenyneu/prompi_movie/setups/ccp_two_layers/3d/data/
```

### Slurm jobs

pawsey_home_backup/slurm_jobs/2021/12/21
