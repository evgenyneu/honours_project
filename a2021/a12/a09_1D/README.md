## Run 1D locally on Mac

Added 1d settings to PROMPI repository on branch `evgenii_honours_run_on_mac` commit da3b01d.

To run, follow [these instructions](a2021/a08/a03_run_prompi_make_movies/README.md).

## Plot 1D vs 0.8% lum 2D

Velocity, energy are very weird in 1D.

![frame.jpg](a2021/a12/a09_1D/plots/frame.jpg)
