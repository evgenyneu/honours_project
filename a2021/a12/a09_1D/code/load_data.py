import os
import numpy as np

from a2020.a10.a13_singularity.code.make_movies.thylacine.reader import (
    get_variable_data,
    get_variable_data_2d,
    get_variable_data_1d,
    get_variable_names
)

CUSTOM_VARIABLES = ['y_momentum', 'speed', 'kinetic_energy', 'log_kinetic_energy', 'abs_vely', 'abs_velx']

def variable_names(data_dir, data_filename, custom_variables):
    data_path = bindata_path(epoch=1, data_dir=data_dir,
                             data_filename=data_filename)

    data_variables = get_variable_names(data_path)
    return data_variables + custom_variables


def bindata_path(epoch, data_dir, data_filename):
    epoch_str = f"{epoch:05d}"
    path = os.path.join(data_dir, data_filename)
    return f"{path}.{epoch_str}.bindata"


def calc_gradients_1d(data):
    max_value = data.max()
    if max_value > 1: data = data / max_value
    return np.absolute(np.gradient(data))


def calc_gradients_2d(data):
    max_value = data.max()
    if max_value > 1: data = data / max_value
    gradients = np.gradient(data)
    return np.sqrt(gradients[0]**2 + gradients[1]**2)


def calc_gradients_3d(data):
    max_value = data.max()
    if max_value > 1: data = data / max_value
    gradients = np.gradient(data)
    return np.sqrt(gradients[0]**2 + gradients[1]**2 + gradients[2]**2)


def calc_y_momentum(epoch, info, custom_variables, full3d):
    vely = load_data(epoch=epoch, variable='vely', info=info,
                     custom_variables=custom_variables, full3d=full3d)

    density = load_data(epoch=epoch, variable='density', info=info,
                        custom_variables=custom_variables, full3d=full3d)
    return vely * density


def calc_kinetic_energy(epoch, info, custom_variables, full3d):
    speed_squared = calc_speed_squared(epoch=epoch, info=info,
                                       custom_variables=custom_variables, full3d=full3d)

    density = load_data(epoch=epoch, variable='density', info=info,
                        custom_variables=custom_variables, full3d=full3d)

    return density * speed_squared


def calc_log_kinetic_energy(epoch, info, custom_variables, full3d):
    energy = calc_kinetic_energy(epoch=epoch, info=info,
                                 custom_variables=custom_variables, full3d=full3d)

    return np.log10(energy)


def calc_speed_squared(epoch, info, custom_variables, full3d):
    x = load_data(epoch=epoch, variable='velx', info=info,
                  custom_variables=custom_variables, full3d=full3d)

    if info['dimensions'] == 1: # 1D
        return x**2
    elif info['dimensions'] == 2:
        y = load_data(epoch=epoch, variable='vely', info=info,
                  custom_variables=custom_variables, full3d=full3d)

        return x**2 + y**2
    else:
        y = load_data(epoch=epoch, variable='vely', info=info,
                  custom_variables=custom_variables, full3d=full3d)

        z = load_data(epoch=epoch, variable='velz', info=info,
                      custom_variables=custom_variables, full3d=full3d)

        return x**2 + y**2 + z**2




def calc_speed(epoch, info, custom_variables, full3d):
    speed_squared = calc_speed_squared(epoch=epoch, info=info,
                                       custom_variables=custom_variables, full3d=full3d)

    return np.sqrt(speed_squared)


def load_custom_data(epoch, variable, info, custom_variables, full3d):
    if variable == 'y_momentum':
        return calc_y_momentum(epoch=epoch, info=info,
                               custom_variables=custom_variables, full3d=full3d)

    if variable == 'speed':
        return calc_speed(epoch=epoch, info=info,
                          custom_variables=custom_variables, full3d=full3d)

    if variable == 'abs_vely':
        return np.absolute(
            load_data(epoch=epoch, variable='vely', info=info,
                      custom_variables=custom_variables, full3d=full3d)
        )

    if variable == 'abs_velx':
        return np.absolute(
            load_data(epoch=epoch, variable='velx',info=info,
                      custom_variables=custom_variables, full3d=full3d)
        )

    if variable == 'kinetic_energy':
        return calc_kinetic_energy(epoch=epoch, info=info,
                                   custom_variables=custom_variables,
                                   full3d=full3d)

    if variable == 'log_kinetic_energy':
        return calc_log_kinetic_energy(epoch=epoch, info=info,
                                       custom_variables=custom_variables,
                                       full3d=full3d)

    raise Exception(f'Unknown custom variable {variable}')


def load_data(epoch, variable, info, custom_variables, full3d):
    if variable in custom_variables:
        return load_custom_data(epoch=epoch, variable=variable,
                                info=info, custom_variables=custom_variables,
                                full3d=full3d)

    data_path = bindata_path(epoch=epoch,
                             data_dir=info['dir'],
                             data_filename=info['file_name'])

    if info['dimensions'] == 3:
        if full3d:
            return get_variable_data(data_path=data_path, variable=variable)
        else:
            return get_variable_data_2d(
                data_path=data_path, variable=variable, zindex=0
            )
    elif info['dimensions'] == 2:
        return get_variable_data_2d(
            data_path=data_path, variable=variable, zindex=0
        )
    else:
        return get_variable_data_1d(data_path=data_path, variable=variable)
