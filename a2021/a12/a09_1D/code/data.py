import os

ALL_DATA = {
    '1D': {
        'dir': 'a2021/a12/a09/1d',
        'dimensions': 1,
        'img_title': '1D',
        'gradient_title': '1D'
    },
    '2D': {
        'dir': 'a2021/a11/a11/2d_3000s',
        'dimensions': 2,
        'img_title': '2D',
        'gradient_title': '2D'
    },
    '2D_0.823_percent_lum_0_s': {
        'dir': 'a2021/a12/a02/0_823_percent_lum_0_sec',
        'dimensions': 2,
        'img_title': '2D 0.82%',
        'gradient_title': '2D 0.82%'
    },
    '2D_1_percent_lum_80_s': {
        'dir': 'a2021/a11/a17/1_percent_lum_80_sec',
        'dimensions': 2,
        'img_title': '2D 1% 80s',
        'gradient_title': '2D 1% 80s'
    },
    '2D_10_percent_lum_80_s': {
        'dir': 'a2021/a11/a22/10_percent_lum_80_sec',
        'dimensions': 2,
        'img_title': '2D 10% 80s',
        'gradient_title': '2D 10% 80s'
    },
    '2D_50_percent_lum_80_s': {
        'dir': 'a2021/a11/a23/50_percent_lum_80_sec',
        'dimensions': 2,
        'img_title': '2D 50% 80s',
        'gradient_title': '2D 50% 80s'
    },
    '2D_67_percent_lum_80_s': {
        'dir': 'a2021/a11/a24/67_percent_lum_80_sec',
        'dimensions': 2,
        'img_title': '2D 67% 80s',
        'gradient_title': '2D 67% 80s'
    },
    '2D_67_percent_lum_0_s': {
        'dir': 'a2021/a11/a26/67_percent_lum_0_sec',
        'dimensions': 2,
        'img_title': '2D 67% 0s',
        'gradient_title': '2D 67% 0s'
    },
    '3D': {
        'dir': 'a2021/a11/a11/3d_3000s',
        'dimensions': 3,
        'img_title': '3D',
        'gradient_title': '3D'
    },
    '3D_1.23345_percent_lum': {
        'dir': 'a2021/a12/a06/lum_1_2345_percent_3d',
        'dimensions': 3,
        'img_title': '3D 1.2%',
        'gradient_title': '3D 1.2%'
    },
}


def data_info(name):
    info = ALL_DATA[name]
    dimensions = info['dimensions']
    file_name = 'ccptwo.1D'
    if dimensions == 2: file_name = 'ccptwo.2D'
    if dimensions == 3: file_name = 'ccptwo.3D'
    root_dir = '/Users/evgenii/Documents/honours_project/data'
    full_dir = os.path.join(root_dir, info['dir'])

    return {
        'dir': full_dir,
        'file_name': file_name,
        'plot_title': info['img_title'],
        'gradient_title': info['gradient_title'],
        'dimensions': dimensions
    }


