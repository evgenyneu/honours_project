## Luminosity 0.823 percent

### Balance before

```
Compute Information
-------------------
          Project ID     Allocation          Usage     % used
          ----------     ----------          -----     ------
                 ew6         875000          54720        6.3
         --evgenyneu                         54720        6.3
```


### Run

```
Job: `. slurm_jobs/2021/12/02/lum_0.823_percent.sh`
Job ids: 7240343 7240344
```

## Download

```
mkdir -p /Users/evgenii/Documents/honours_project/data/a2021/a12/a02/

rsync -Pvht pawsey:/group/ew6/evgenyneu/prompi_output/2021_12_02_15_25_17_lum_0_823_percent_after_0s.tar.gz /Users/evgenii/Documents/honours_project/data/a2021/a12/a02/
```

## Plot 1% vs 0.823%

Boundary raises faster with 10% than 1%, but still significantly slower than 3D.

![evolution.jpg](a2021/a12/a02_run_0.8_percent_2d/plots/evolution.jpg)

![one_frame.jpg](a2021/a12/a02_run_0.8_percent_2d/plots/one_frame.jpg)
