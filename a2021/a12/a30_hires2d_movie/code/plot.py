from astropy.visualization import (ImageNormalize, PercentileInterval, PowerStretch)
import seaborn as sns
import matplotlib as mpl
import matplotlib.pyplot as plt
from a2021.a11.a17_plot_lum_0_01.code.data import data_info

from a2021.a11.a17_plot_lum_0_01.code.load_data import (
    load_data,
    data_time,
    calc_gradients_2d,
    calc_gradients_3d,
    CUSTOM_VARIABLES
)

from a2021.a11.a12_perturbation_single_frame.code.perturbation import calc_perturbation

def set_plot_style():
    """Set global plotting style"""

    sns.set(style="ticks")

    # Text sizes
    # -------
    mpl.rcParams['figure.titlesize'] = 20
    mpl.rcParams['axes.titlesize'] = 14
    mpl.rcParams['font.size'] = 11
    mpl.rcParams['axes.labelsize'] = 13
    mpl.rcParams['xtick.labelsize'] = 11
    mpl.rcParams['ytick.labelsize'] = 11


    # Color
    # -------

    TEXT_COLOR = '#8888cc'
    mpl.rcParams['figure.facecolor'] = '#000000'
    mpl.rcParams['axes.edgecolor'] = TEXT_COLOR
    mpl.rcParams['text.color'] = TEXT_COLOR
    mpl.rcParams['axes.labelcolor'] = TEXT_COLOR
    mpl.rcParams['xtick.color'] = TEXT_COLOR
    mpl.rcParams['ytick.color'] = TEXT_COLOR


def load_data_and_gradients(epoch, variable, data_name,
                              custom_variables, perturbation):

    data = load_data(epoch=epoch, variable=variable,
                     info=data_info(data_name),
                     custom_variables=custom_variables,
                     full3d=True)

    if perturbation: data = calc_perturbation(data)

    if len(data.shape) == 3: # 3D
        gradients = calc_gradients_3d(data)
        data_2d = data[:, :, 0]
    else:
        data_2d = data
        data = data_2d
        gradients = calc_gradients_2d(data)

    return {
        'data_2d': data_2d,
        'data': data,
        'gradients': gradients
    }


def set_plot_axes(ax):
    # Image
    ax.set_yticks([0, 0.2, 0.4, 0.6])
    ax.set_ylim(0, 0.65)
    ax.tick_params(left=False, labelleft=False, bottom=False, labelbottom=False)


def make_image_plots(fig, ax, data, colormap, percentile, stretch):
    # Map data to colors
    interval = PercentileInterval(percentile)
    stretch_obj = PowerStretch(stretch)
    normalizer = ImageNormalize(data.flatten(), interval=interval, stretch=stretch_obj)

    # Makes images a bit sharper than default interpolation
    interpolation = 'spline16'

    # Show data
    ax.imshow(data, cmap=colormap, norm=normalizer, extent=(0, 1, 1, 0), interpolation=interpolation)

    for spine in ax.spines.values():
        spine.set_visible(False)


def get_gradient_img(data, data_name):
    if len(data['gradients'].shape) == 3: # 3D
        # Ignore z component of 3D gradient on the plot
        return calc_gradients_2d(data['data_2d'])

    return data['gradients']


def load_and_show(
    fig, ax, epoch, data_name, variable, colormap,
    show_gradient, custom_variables, perturbation,
    percentile, stretch):

    data = load_data_and_gradients(
        epoch=epoch, variable=variable,
        data_name=data_name, custom_variables=custom_variables,
        perturbation=perturbation
    )

    img_data = data['data_2d']

    if show_gradient:
        img_data = get_gradient_img(data, data_name)

    make_image_plots(fig=fig, ax=ax,
                     data=img_data,
                     colormap=colormap,
                     percentile=percentile,
                     stretch=stretch)


def show_time(fig, epoch, data_name):
    time = data_time(epoch=epoch, info=data_info(data_name))
    fig.text(x=0.06, y=0.965, s=f'Time: {time:.1f} s', horizontalalignment='left')


def make_plot(epoch, data_name, variable, colormap,
              custom_variables, perturbation,
              percentile, stretch):

    fig, ax = plt.subplots(1, 1, figsize=(12, 7))
    ax.set_position((0, 0, 1, 1))

    load_and_show(fig=fig, ax=ax,
        epoch=epoch,
        data_name=data_name,
        variable=variable, colormap=colormap,
        show_gradient=True,
        custom_variables=custom_variables, perturbation=perturbation,
        percentile=percentile, stretch=stretch
    )

    show_time(fig=fig, epoch=epoch, data_name=data_name)

    set_plot_axes(ax)

    return fig, ax


def make_plot_for_real(epoch, variable, data_name,
                       perturbation, colormap, percentile, stretch):

    fig, ax = make_plot(epoch=epoch,
                     data_name=data_name,
                     variable=variable,
                     colormap=colormap,
                     custom_variables=CUSTOM_VARIABLES,
                     perturbation=perturbation,
                     percentile=percentile,
                     stretch=stretch)

    return fig, ax
