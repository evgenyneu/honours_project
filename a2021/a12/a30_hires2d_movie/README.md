## Make video for 2d hi-res full screen


Code: [movie.py](a2021/a12/a30_hires2d_movie/code/movie.py)


## Add audio

### Download audio from youtube

```
brew install yt-dlp/taps/yt-dlp
yt-dlp https://www.youtube.com/watch?v=aJCilj7qMGg -x --audio-format wav
```

### Trim audio

```
ffmpeg -i audio.wav -ss 00:45:00 -to 00:49:00 -c copy audio_trimmed.wav
```

### Add audio to video

```
ffmpeg -i movie.mp4 -i audio_trimmed.wav -map 0:v -map 1:a -c:v copy -shortest movie_with_audio.mp4
```

### Upload to youtube

https://www.youtube.com/watch?v=WCaFXkMT_O0

### Backup

[movie_with_audio.mp4](a2021/a12/a30_hires2d_movie/movie_with_audio.mp4)
