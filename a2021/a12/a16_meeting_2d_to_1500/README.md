## 1D simulation

* Initial velocity wave in 1D is the same as 2d/3d

* There is no convection in 1D.

* Idea for the future: turn off convection in 2d/3d.


## Suggestions

* Run 2D to 1500s and see if it reaches steady state.

* Do 2D in 1024 px.

* Run the 3D one further, to 1500 dumps also, to match the 2D.


## Extract copied files

```
cd /Users/evgenii/Documents/honours_project/logbook/a2021/a12/a10_plot_1.2_percent_3d_from_204/code

python extract_bindata.py /Users/evgenii/Documents/honours_project/data/a2021/a12/a15/lum_1_2345_percent_3d_from_551
```


## Run 2D 0.823% to frame 1500

### Balance before

```
Compute Information
-------------------
          Project ID     Allocation          Usage     % used
          ----------     ----------          -----     ------
                 ew6         875000         194756       22.3
         --evgenyneu                        122113       14.0
         --scampbell                         72643        8.3
```

### Run

```
Job: . slurm_jobs/2021/12/16/lum_0.823_percent_1500_dumps.sh
Job ids: 7294014 7294015
```
