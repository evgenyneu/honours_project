### Compress and copy files


```
sbatch slurm_jobs/2021/09/26/gz_copy.sh /scratch/ew6/evgenyneu/PROMPI/setups/ccp_two_layers/3d /group/ew6/evgenyneu/prompi_output/2021_12_10_lum_1_2345_percent_from_379
```

Job ID: 5330122

Elapsed time: 01:54:09

### Return the number of CPUs

```
sbatch slurm_jobs/2021/12/13/cpu_count.sh
```

Job ID: 5330245

Output: `Number of CPUs: 32`


### Compress and copy files using multiprocessing


```
sbatch slurm_jobs/2021/12/13/gz_copy.sh /scratch/ew6/evgenyneu/PROMPI/setups/ccp_two_layers/3d /group/ew6/evgenyneu/prompi_output/2021_12_10_lum_1_2345_percent_from_379_multiprocess
```

Job ID: 5330529


## Download

```
mkdir -p /Users/evgenii/Documents/honours_project/data/a2021/a12/a13/lum_1_2345_percent_3d_from_379

rsync -Pvhtr --bwlimit=5000 pawsey:/group/ew6/evgenyneu/prompi_output/2021_12_10_lum_1_2345_percent_from_379/ /Users/evgenii/Documents/honours_project/data/a2021/a12/a13/lum_1_2345_percent_3d_from_379
```

## Backup to ssd

```
rsync -Pvhtr /Users/evgenii/Documents/honours_project/data/ /Volumes/wd_blue_2tb/honours/data
```
