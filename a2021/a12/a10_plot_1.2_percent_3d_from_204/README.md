## Extract copied files

```
cd /Users/evgenii/Documents/honours_project/logbook/a2021/a12/a10_plot_1.2_percent_3d_from_204/code

python extract_bindata.py /Users/evgenii/Documents/honours_project/data/a2021/a12/a08/lum_1_2345_percent_3d_from_204
```

## Plot 3D 1.2% vs 2D 0.8% luminosity

2D moves a bit faster:

![single_frame.jpg](a2021/a12/a10_plot_1.2_percent_3d_from_204/plots/single_frame.jpg)

![evolution.jpg](a2021/a12/a10_plot_1.2_percent_3d_from_204/plots/evolution.jpg)


## Continue 3d from frame 00379

```
cd /scratch/ew6/evgenyneu/PROMPI/setups/ccp_two_layers/3d
cp ccptwo.3D.00379* ./data/
```

### Balance before

```
Compute Information
-------------------
          Project ID     Allocation          Usage     % used
          ----------     ----------          -----     ------
                 ew6         875000          96294       11.0
         --evgenyneu                         96294       11.0
```


### Run

```
Job: `. slurm_jobs/2021/12/10/lum_1.2345_percent_continue_from_379.sh`
Job ids: 7271453 7271454
```

