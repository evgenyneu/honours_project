#
# Extracts *.bindata.gz files in the given directory.
#
# Usage example
# ----------
#
# python extract_bindata.py /path_to_your_dir
#

import argparse
import subprocess
import multiprocessing
import os
from multiprocessing import Pool
import tqdm
# from a2021.a11.a30_new_mac_benchmark.code.parallel import square_with_sleep

def parse_command_line_args():
    parser = argparse.ArgumentParser(
        description=(
            'Extracts *.bindata.gz files in the given directory'
        )
    )

    parser.add_argument(
        'src_dir',
        help='Directory containing *.bindata.gz files.'
    )

    return parser.parse_args()


def uncompress(file_path):
    cmd = f"gzip -d '{file_path}'"
    subprocess.run(cmd, shell=True, check=True)


def get_file_paths(suffix, src_dir):
    file_names = [
        f.name for f in os.scandir(src_dir)
        if f.name.endswith(suffix)
    ]

    file_names.sort()
    return [os.path.join(src_dir, file_name) for file_name in file_names]


def uncompress_files_with_suffix(paths):
    processes = multiprocessing.cpu_count() - 2
    if processes < 1: processes = 1

    with Pool(processes=processes) as p:
        list(tqdm.tqdm(p.imap(uncompress, paths), total=len(paths)))


def extract_files(src_dir):
    file_paths = get_file_paths(suffix='bindata.gz', src_dir=src_dir)
    uncompress_files_with_suffix(file_paths)


def lets_goooooo():
    args = parse_command_line_args()
    extract_files(args.src_dir)
    print(f"Success: extracted files in {args.src_dir}")


if __name__ == '__main__':
    lets_goooooo()
    print('We are done!')
