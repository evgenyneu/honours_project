## Convert 3D to 2D

```
sbatch slurm_jobs/2021/12/24/a3d_to_2d.sh /scratch/ew6/evgenyneu/PROMPI/setups/ccp_two_layers/3d /group/ew6/evgenyneu/prompi_output/2021_12_25_3d_to_2d_264_for_animation 70 1
```

Holy crap it works!

## Download

```
mkdir -p /Users/evgenii/Documents/honours_project/data/a2021/a12/a25/

rsync -Pvhtr --bwlimit=4000 pawsey:/group/ew6/evgenyneu/prompi_output/2021_12_25_3d_to_2d_264_for_animation /Users/evgenii/Documents/honours_project/data/a2021/a12/a25/
```

## Run 2D 1032^2 for animation


### Balance before

```
Compute Information
-------------------
          Project ID     Allocation          Usage     % used
          ----------     ----------          -----     ------
                 ew6         875000         495566       56.6
         --scampbell                        353859       40.4
         --evgenyneu                        141707       16.2
```
### Run

```
Job: . slurm_jobs/2021/12/25/2d_1032x1032_lum_0.823_percent_for_movie.sh
Job ids: 7315284 7315285
```



