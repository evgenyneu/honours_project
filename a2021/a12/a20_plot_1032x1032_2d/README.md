## Extract copied files

```
cd /Users/evgenii/Documents/honours_project/logbook/a2021/a12/a10_plot_1.2_percent_3d_from_204/code

python extract_bindata.py /Users/evgenii/Documents/honours_project/data/a2021/a12/a19/2d_1032x1032_0_823_percent_lum
```

## Plot 264 vs 1032 2D

Boundary advances a bit faster in higher resolution simulation.

![evolution.jpg](a2021/a12/a20_plot_1032x1032_2d/plots/evolution.jpg)

![signle_frame.jpg](a2021/a12/a20_plot_1032x1032_2d/plots/signle_frame.jpg)


## Fixing resume 2D in PROMPI locally on my Mac

Resume does not work in PROMPI in 2D. Running a small 64x64 simulation locally on my Mac and trying to resume.

Actually, restart worked!


## Continue 2D 1032x1032 from dump 509

```
cd /scratch/ew6/evgenyneu/PROMPI/setups/ccp_two_layers/2d
cp ccptwo.2D.00509* ./data/
```

### Balance before

```
Compute Information
-------------------
          Project ID     Allocation          Usage     % used
          ----------     ----------          -----     ------
                 ew6         875000         416205       47.6
         --scampbell                        290428       33.2
         --evgenyneu                        125777       14.4
```


### Run

```
Job: . slurm_jobs/2021/12/20/resume_509_2d_1032x1032_lum_0.823_percent.sh
Job ids: 7306598 7306599
```

