## Download

```
mkdir /Users/evgenii/Documents/honours_project/data/a2021/a12/a17

rsync -Pvht --bwlimit=5000 pawsey:/group/ew6/evgenyneu/prompi_output/2021_12_16_17_31_56_lum_0_823_percent_1500_dumps.tar.gz /Users/evgenii/Documents/honours_project/data/a2021/a12/a17
```

## Plots 1500 dumps

Boundary continues to advance:

![1500_dumps.jpg](a2021/a12/a17_download_2d_1500_dumps/plots/1500_dumps.jpg)

## Hi-Res 2D

Use [calc_tiles.py](a2021/a09/a24_3D_264_with_fewer_nodes/code/calc_tiles.py)

Resolution: 1032^2

6 nodes 12 by 12 tiles


### Balance before

```
Compute Information
-------------------
          Project ID     Allocation          Usage     % used
          ----------     ----------          -----     ------
                 ew6         875000         252724       28.9
         --scampbell                        130405       14.9
         --evgenyneu                        122318       14.0
```

### Run

```
Job: . slurm_jobs/2021/12/17/2d_1032x1032_lum_0.823_percent.sh
Job ids: 7298539 7298540
```



