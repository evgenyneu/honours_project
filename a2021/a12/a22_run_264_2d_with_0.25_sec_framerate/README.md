## Run 264^2 with 0.25 sec frame rate


### Copy data files for 3D simulation

```
cd /scratch/ew6/evgenyneu
mv prompi_movie/setups/ccp_two_layers/3d/data/ccptwo* PROMPI/setups/ccp_two_layers/3d/data/
```

### Balance before

```
Compute Information
-------------------
          Project ID     Allocation          Usage     % used
          ----------     ----------          -----     ------
                 ew6         875000         416212       47.6
         --scampbell                        290428       33.2
         --evgenyneu                        125784       14.4

```
### Run

```
Job: . slurm_jobs/2021/12/22/2d_lum_0.823_percent_with_0.25_sec_interval.sh
Job ids: 7310486 7310487
```

### Download output

```
mkdir -p /Users/evgenii/Documents/honours_project/data/a2021/a12/a22/

rsync -Pvht pawsey:/group/ew6/evgenyneu/prompi_output/2021_12_22_05_00_43_2d_lum_0_823_percent_with_0_25_sec_interval.tar.gz /Users/evgenii/Documents/honours_project/data/a2021/a12/a22/
```


## Meeting

Kelvin Helmholtz instability: boundary mixing.




## Run 3D 264^3 with 0.25 sec frame rate


### Copy data files for 3D simulation

```
cd /scratch/ew6/evgenyneu
mv prompi_movie/setups/ccp_two_layers/3d/data/ccptwo* PROMPI/setups/ccp_two_layers/3d/data/
```

### Balance before

```
Compute Information
-------------------
          Project ID     Allocation          Usage     % used
          ----------     ----------          -----     ------
                 ew6         875000         416273       47.6
         --scampbell                        290428       33.2
         --evgenyneu                        125844       14.4

```
### Run

```
Job: . slurm_jobs/2021/12/22/0.25_sec_interval_1.2345_percent_lum_continue_from_300.sh
Job ids: 7310703 7310704
```

