## Copy result to group

```
cd /scratch/ew6/evgenyneu/PROMPI/setups/ccp_two_layers/3d
salloc --partition=copyq --cluster=zeus --time 10:00:00 --nodes=1 --ntasks=1
cp *.gz /group/ew6/evgenyneu/prompi_output/2021_12_05_lum_1_2345_percent_3d/
cp *.header /group/ew6/evgenyneu/prompi_output/2021_12_05_lum_1_2345_percent_3d/
```

Failed with [memory error](a2021/a12/a06_download_3d_files/files/memory_error.txt)

```
scp -r /scratch/ew6/evgenyneu/PROMPI/setups/ccp_two_layers/3d/*.gz /group/ew6/evgenyneu/prompi_output/2021_12_05_lum_1_2345_percent_3d/
```

### Download to local SSD

```
scp -r pawsey:/scratch/ew6/evgenyneu/PROMPI/setups/ccp_two_layers/3d/\*.gz /Users/evgenii/Documents/honours_project/data/a2021/a12/a06/lum_1_2345_percent_3d
```

Continue:

```
rsync -Pvhtr --ignore-existing --bwlimit=3000 pawsey:/scratch/ew6/evgenyneu/PROMPI/setups/ccp_two_layers/3d/\*.gz /Users/evgenii/Documents/honours_project/data/a2021/a12/a06/lum_1_2345_percent_3d
```


## Memory error solved by ashley


By default on copyq, it only allocates 2GB per cpu asked if memory requirement is not defined. So in your case it's using for more than 2GB per cpu.

Add the "--mem" option ie something like this/

salloc --partition=copyq --cluster=zeus --time 10:00:00 --nodes=1 --ntasks=1 --mem=8GB


## Copy result to group (attempt #3)

```
cd /scratch/ew6/evgenyneu/PROMPI/setups/ccp_two_layers/3d
salloc --partition=copyq --cluster=zeus --time 10:00:00 --nodes=1 --ntasks=1 --mem=8GB
cp *.gz /group/ew6/evgenyneu/prompi_output/2021_12_05_lum_1_2345_percent_3d/
cp *.header /group/ew6/evgenyneu/prompi_output/2021_12_05_lum_1_2345_percent_3d/
```

### Download to local SSD ( uncompressed files)

```
rsync -Pvhtr --ignore-existing pawsey:/scratch/ew6/evgenyneu/PROMPI/setups/ccp_two_layers/3d/ccptwo.3D.0001\*.bindata /Users/evgenii/Documents/honours_project/data/a2021/a12/a06/lum_1_2345_percent_3d

rsync -Pvhtr --ignore-existing pawsey:/scratch/ew6/evgenyneu/PROMPI/setups/ccp_two_layers/3d/ccptwo.3D.0000\*.bindata /Users/evgenii/Documents/honours_project/data/a2021/a12/a06/lum_1_2345_percent_3d

rsync -Pvhtr --ignore-existing pawsey:/scratch/ew6/evgenyneu/PROMPI/setups/ccp_two_layers/3d/\*.header /Users/evgenii/Documents/honours_project/data/a2021/a12/a06/lum_1_2345_percent_3d
```

