## Run 3D 16^3 locally

Need small 3D files for unit tests.

Cores: 8
Tiles: 2x2x2.

```
cd setups/ccp_two_layers/3d/src
make prompi
mpiexec -n 8 ./prompi.x
```

## Convert 3D to 2D index

Conver 3D to 2D at z-index 70.


## Test 3D to 2D

Copy files to test

```
cp /scratch/ew6/evgenyneu/PROMPI/setups/ccp_two_layers/3d/ccptwo.3D.0030* /scratch/ew6/evgenyneu/prompi_output/2021_12_24_3d_to_2d_test/
```

```
sbatch slurm_jobs/2021/12/24/a3d_to_2d.sh /scratch/ew6/evgenyneu/prompi_output/2021_12_24_3d_to_2d_test /group/ew6/evgenyneu/prompi_output/2021_12_24_3d_to_2d_test_output 70 1
```

Holy crap it works!

## Download

```
mkdir -p /Users/evgenii/Documents/honours_project/data/a2021/a12/a24/

rsync -Pvhtr pawsey:/group/ew6/evgenyneu/prompi_output/2021_12_24_3d_to_2d_test_output /Users/evgenii/Documents/honours_project/data/a2021/a12/a24/
```




