import re
import os

def get_header_path(data_path):
    """Return path to the header file.

    Parameters
    ----------
    data_path : str
        Path to PROMPI data file.

    Returns
    -------
    str
        Path to header file

    """

    return data_path.replace(".bindata", ".header")


def parse_epoch(bindata_path):
    """Returns the 'epoch' from the path to PROMPI data file.

    Example
    -------

    Given /dir//dir/ccptwo.2D.00231.bindata, return 231.

    Parameters
    ----------
    data_path : str
        Path to PROMPI data file, a .bindata or .header

    Returns
    -------
    int
        Epoch number

    """
    parsed = parse_file_name(bindata_path)
    return int(parsed['number'])


def parse_file_name(file_path):
    """Parses the PROMPI output file file name and returns
    three comonent: prefix, number and extension.


    Example
    ---------

    Given /dir/ccptwo.2D.00231.bindata, it returns

        * 'ccptwo.2D' prefix,
        * '00231' number and
        * 'bindata' extension.

    Parameters
    ----------
    file_path : str
        Path to PROMPI output file, .bindata or .header.

    Returns
    -------
    dict
        Keys:
            * name
            * number
            * extension
    """

    file_name = os.path.basename(file_path)
    result = re.search(r"(.+)\.(\d+)\.(bindata|header)", file_name)

    if result is None: raise ValueError(f"Unknown file name format: '{file_name}'")

    return {
        'prefix': result.group(1),
        'number': result.group(2),
        'extension': result.group(3)
    }
