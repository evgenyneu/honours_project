from file_name import parse_file_name, parse_epoch
import pytest

def test_parse_file_name():
    result = parse_file_name('/dir/ccptwo.2D.00233.bindata')

    assert result == {
        'prefix': 'ccptwo.2D',
        'number': '00233',
        'extension': 'bindata'
    }

def test_parse_file_name_incorrect_format():
    with pytest.raises(ValueError) as error:
        parse_file_name('/dir/incorrect')

    assert str(error.value) == "Unknown file name format: 'incorrect'"


def test_parse_epoch():
    result = parse_epoch('/dir/ccptwo.2D.00233.bindata')
    assert result == 233
