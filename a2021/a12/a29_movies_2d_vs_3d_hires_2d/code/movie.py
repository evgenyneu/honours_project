import os
import tqdm
import subprocess
import multiprocessing
from shutil import copyfile
from datetime import datetime
import matplotlib.pyplot as plt
from multiprocessing import Pool

from a2021.a12.a29_movies_2d_vs_3d_hires_2d.code.plot import (
    set_plot_style,
    make_plot_for_real
)

from a2021.a12.a27_2d_3d_movie.code.movie import (make_movie, output_image_path)


def make_and_save_plot(data_name1, data_name2, epoch, dest_dir):
    set_plot_style()

    fig, _ = make_plot_for_real(
        epoch=epoch, variable='0002', zindex=0,
        left_plot=data_name1, right_plot=data_name2,
        perturbation=False,
        colormap='turbo', percentile=100, stretch=0.60, full3d=True
    )

    dest_path = output_image_path(epoch=epoch, dest_dir=dest_dir)
    os.makedirs(dest_dir, exist_ok=True)

    # Manual fiddling to achieve 1440 pixels height to get 1440p video
    # and avoid resizing on youtube
    fig.savefig(dest_path, dpi=197, bbox_inches='tight', pad_inches=0.11)
    plt.close(fig)


def make_and_save_plot_with_params(params):
    make_and_save_plot(**params)


def make_plots_in_parallel(data_name1, data_name2, dest_dir, epoch_start, epoch_end):
    params = [
        {
            'epoch': epoch,
            'dest_dir': dest_dir,
            'data_name1': data_name1,
            'data_name2': data_name2
        }
        for epoch in range(epoch_start, epoch_end + 1)
    ]

    processes = multiprocessing.cpu_count() - 2 # Leave two CPU cores free
    if processes < 0: processes = 1

    with Pool(processes=processes) as p:
        list(
            tqdm.tqdm(
                p.imap(make_and_save_plot_with_params, params),
                total=len(params)
            )
        )


def lets_goooooo():
    data_name1='2D_264_for_movie_from_time_zero'
    data_name2='2D_1032_for_movie_from_time_zero'

    frames_dir = '/Users/evgenii/Downloads/movies'

    make_plots_in_parallel(data_name1=data_name1,
                           data_name2=data_name2,
                           dest_dir=frames_dir,
                           epoch_start=500, epoch_end=7947)

    movie_name = f'{datetime.now():%Y-%m-%d_%H-%M-%S}_2d_vs_2d_hires.mp4'

    make_movie(plot_dir=frames_dir,
               movie_dir='/Users/evgenii/Downloads/movie_out',
               movie_name=movie_name,
               frame_rate=30)


if __name__ == '__main__':
    lets_goooooo( )
    print("We are done!")
