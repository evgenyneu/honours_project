from a2021.a11.a17_plot_lum_0_01.code.data import data_info
from a2021.a12.a27_2d_3d_movie.code.compare_times import get_frame_time
from a2021.a11.a17_plot_lum_0_01.code.load_data import locate_epochs

def epochs(data_name):
    info = data_info(data_name)
    return locate_epochs(info['dir'])

def epochs_times(data_name):
    return [
        {
            'epoch': epoch,
            'time': get_frame_time(epoch, data_name)
        }
        for epoch in epochs(data_name)
    ]


def locate_epoch_time(at_time, times):
    best_time = None
    time_diff = 1000000

    for time in times:
        new_time_diff = abs(time['time'] - at_time)

        if new_time_diff < time_diff:
            time_diff = new_time_diff
            best_time = time
        else:
            break

    return best_time, time_diff


def find_closest_epoch_in_time(epoch, data_name1, data_name2):
    time1 = get_frame_time(epoch, data_name1)
    times2 = epochs_times(data_name2)
    best_time, time_diff = locate_epoch_time(time1, times2)
    return best_time['epoch'], time_diff
