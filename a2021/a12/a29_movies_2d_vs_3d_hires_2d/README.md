## Make video for 2d vs 3d


Code: [movie.py](a2021/a12/a27_2d_3d_movie/code/movie.py)


## Add audio

### Download audio from youtube

```
brew install yt-dlp/taps/yt-dlp
yt-dlp https://www.youtube.com/watch?v=5VV07ipOFF0 -x --audio-format wav
```

### Trim audio

```
ffmpeg -i audio.wav -ss 00:45:00 -to 00:47:00 -c copy audio_trimmed.wav
```

### Add audio to video

```
ffmpeg -i movie.mp4 -i audio_trimmed.wav -map 0:v -map 1:a -c:v copy -shortest movie_with_audio.mp4
```

### Upload to youtube

https://www.youtube.com/watch?v=6Lyg7AQox20

### Backup

[movie_with_audio.mp4](a2021/a12/a29_movies_2d_vs_3d_hires_2d/movie_with_audio.mp4)



## Make Hires 2D movie

### Extract data files

```
cd /Users/evgenii/Documents/honours_project/logbook/a2021/a12/a10_plot_1.2_percent_3d_from_204/code

python extract_bindata.py /Users/evgenii/Documents/honours_project/data/a2021/a12/a28/hires_2d_for_movie_0_823_percent_lum_with_0_25_sec_interval
```

### Make video for 2d vs 2d hires


Code: [movie.py](a2021/a12/a29_movies_2d_vs_3d_hires_2d/code/movie.py)


## Add audio

### Download audio from youtube

```
brew install yt-dlp/taps/yt-dlp
yt-dlp https://www.youtube.com/watch?v=D5GQ2_5hWU0 -x --audio-format wav
```


### Trim audio

```
ffmpeg -i audio.wav -ss 00:04:30 -to 00:09:30 -c copy audio_trimmed.wav
```

### Add audio to video

```
ffmpeg -i movie.mp4 -i audio_trimmed.wav -map 0:v -map 1:a -c:v copy -shortest movie_with_audio.mp4
```


### Upload to youtube

https://www.youtube.com/watch?v=5zW5bmXpYp0


### Backup

[movie_with_audio_2d_vs_2d_hires.mp4](a2021/a12/a29_movies_2d_vs_3d_hires_2d/movie_with_audio_2d_vs_2d_hires.mp4)
