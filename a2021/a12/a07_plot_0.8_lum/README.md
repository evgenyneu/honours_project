## Extract copied files

```
cd /Users/evgenii/Documents/honours_project/logbook/pawsey_home_backup/slurm_jobs/2021/09/26

python extract_bindata.py /Users/evgenii/Documents/honours_project/data/a2021/a12/a06/lum_1_2345_percent_3d
```

## Plot 2d vs 30 0.8% lum

![evolution.jpg](a2021/a12/a07_plot_0.8_lum/plots/evolution.jpg)


## Continue 3d from frame 204

```
cd /scratch/ew6/evgenyneu/PROMPI/setups/ccp_two_layers/3d
cp ccptwo.3D.00204* ./data/
```



### Balance before

```
Compute Information
-------------------
          Project ID     Allocation          Usage     % used
          ----------     ----------          -----     ------
                 ew6         875000          75557        8.6
         --evgenyneu                         75557        8.6
```


### Run

```
Job: `. slurm_jobs/2021/12/07/lum_1.2345_percent_continue_from_204.sh`
Job ids: 7259378 7259379
```

