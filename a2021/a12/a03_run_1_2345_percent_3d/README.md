## Luminosity 1.12345 percent 3D

### Balance before

```
Compute Information
-------------------
          Project ID     Allocation          Usage     % used
          ----------     ----------          -----     ------
                 ew6         875000          54812        6.3
         --evgenyneu                         54812        6.3
```


### Run

```
Job: `. slurm_jobs/2021/12/03/lum_1.2345_percent.sh`
Job ids: 7243394 7243395
```

Failed, syntax error,


### Run #2

```
Job: `. slurm_jobs/2021/12/03/lum_1.2345_percent.sh`
Job ids: 7243461 7243462
```
