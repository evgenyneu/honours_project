### Compress and copy files using multiprocessing


```
sbatch slurm_jobs/2021/12/13/gz_copy.sh /scratch/ew6/evgenyneu/PROMPI/setups/ccp_two_layers/3d /group/ew6/evgenyneu/prompi_output/2021_12_15_lum_1_2345_percent_from_551
```

Job ID: 5335244


## Download

```
mkdir -p /Users/evgenii/Documents/honours_project/data/a2021/a12/a15/lum_1_2345_percent_3d_from_551

rsync -Pvhtr --bwlimit=5000 pawsey:/group/ew6/evgenyneu/prompi_output/2021_12_15_lum_1_2345_percent_from_551 /Users/evgenii/Documents/honours_project/data/a2021/a12/a15/lum_1_2345_percent_3d_from_551
```
