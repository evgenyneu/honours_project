## Perturbation evolution

![perturbation_pressure_evolution.jpg](a2021/a11/a13_evolution_with_perturbation/plots/perturbation_pressure_evolution.jpg)


## Setup heating locally

```
git checkout 9e5f9f2
cd /Users/evgenii/Documents/honours_project/PROMPI/setups/ccp_two_layers/2d/src
make prompi
cd ..
mpiexec -n 1 ./prompi.x
```

## Increased Luminosity 10x after 80 sec for a test

Change in BURN/volheat.f90

```
CCP LUMINOSITY update 7/March/2020
      lum = 1.2082151d-4
      if (time .gt. 80) lum = 10.0d0 * lum!
```

Data stored in /Volumes/wd_blue_2tb/honours/data/a2021/a11/a15

## Next

Compare 1x and 10x luminocity
