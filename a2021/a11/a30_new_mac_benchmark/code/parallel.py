import time

def func(x):
    time.sleep(x)
    return x + 2


def square_with_sleep(my_number):
   square = my_number * my_number
   time.sleep(1)
   return square
