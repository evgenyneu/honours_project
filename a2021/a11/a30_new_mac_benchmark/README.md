## Measure execution time

* New mac with external SSD: 263 s.

* New mac with internal SDD: 71 s (3.7x faster).

* Old mac with external SDD: 646 s (2.4x slower).
