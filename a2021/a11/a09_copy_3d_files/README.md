## Copy result of run 1 to scratch

```
cd /scratch/ew6/evgenyneu/PROMPI/setups/ccp_two_layers/3d
salloc --partition=copyq --cluster=zeus --time 10:00:00 --nodes=1 --ntasks=1
cp *.gz /group/ew6/evgenyneu/prompi_output/2021_11_08_3d_from_dump400/
cp *.header /group/ew6/evgenyneu/prompi_output/2021_11_08_3d_from_dump400/
```

### Download to local SSD

```
rsync -Pvhtr --ignore-existing pawsey:/group/ew6/evgenyneu/prompi_output/2021_11_08_3d_from_dump400/ /Volumes/wd_blue_2tb/honours/data/a2021/a11/a08/2021_11_08_3d_from_dump400
```
