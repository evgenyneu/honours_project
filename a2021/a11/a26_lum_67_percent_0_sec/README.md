## Luminosity 67 percent from the start

### Balance before

```
Compute Information
-------------------
          Project ID     Allocation          Usage     % used
          ----------     ----------          -----     ------
                 ew6         875000          54630        6.2
         --evgenyneu                         54630        6.2
```


### Run

```
Job: `. slurm_jobs/2021/11/26/lum_67_percent_after_0s.sh`
Job ids: 7211132, 7211133
```

## Download

```
mkdir /Volumes/wd_blue_2tb/honours/data/a2021/a11/a26/

rsync -Pvht pawsey:/group/ew6/evgenyneu/prompi_output/2021_11_26_15_35_10_lum_67_percent_after_0s.tar.gz /Volumes/wd_blue_2tb/honours/data/a2021/a11/a26/
```

## Plot 67% 80s vs 9s

Very similar:

![67_percent_80s_vs_0s](a2021/a11/a26_lum_67_percent_0_sec/plots/67_percent_80s_vs_0s)

