## Balance before

```
$ balance
Compute Information
-------------------
          Project ID     Allocation          Usage     % used
          ----------     ----------          -----     ------
                 ew6         875000          34671        4.0
         --evgenyneu                         34671        4.0
```


### 2D

### Copy dump to Pawsey

```
scp /Volumes/wd_blue_2tb/honours/data/a2021/a09/a25/2021_09_25_2D_264_2000s/ccptwo.2D.00401.bindata pawsey:/scratch/ew6/evgenyneu/PROMPI/setups/ccp_two_layers/2d/data/

scp /Volumes/wd_blue_2tb/honours/data/a2021/a09/a25/2021_09_25_2D_264_2000s/ccptwo.2D.00401.header pawsey:/scratch/ew6/evgenyneu/PROMPI/setups/ccp_two_layers/2d/data/
```

### Run

```
Job: `. slurm_jobs/2021/11/05/2d_264_3nodes_start_dump_401.sh`
Job ids: 7120163 7120164
```

Error running: [fail_to_run2d.txt](a2021/a11/a05_run_3d_from_frame_401/logs/fail_to_run2d.txt)

### Try again

```
Job: `. slurm_jobs/2021/11/05/2d_264_3nodes_start_dump_401.sh`
Job ids: 7120266 7120267
```

Same error.

### Try restarting from dump 400: copy dump to Pawsey

```
scp /Volumes/wd_blue_2tb/honours/data/a2021/a09/a25/2021_09_25_2D_264_2000s/ccptwo.2D.00400.bindata pawsey:/scratch/ew6/evgenyneu/PROMPI/setups/ccp_two_layers/2d/data/

scp /Volumes/wd_blue_2tb/honours/data/a2021/a09/a25/2021_09_25_2D_264_2000s/ccptwo.2D.00400.header pawsey:/scratch/ew6/evgenyneu/PROMPI/setups/ccp_two_layers/2d/data/
```

### Try again

```
Job: `. slurm_jobs/2021/11/05/2d_264_3nodes_start_dump_400.sh`
Job ids: 7120300 7120301
```

Same error.


### 3D

### Copy dump to Pawsey

```
cp /scratch/ew6/evgenyneu/prompi_output/2021_10_03_13_40_22_264x264x264_2000s_run3_from_250/ccptwo.3D.00400* /scratch/ew6/evgenyneu/PROMPI/setups/ccp_two_layers/3d/data/
```

### Run

```
Job: `. slurm_jobs/2021/11/05/3d_264_36nodes_start_dump_400.sh`
Job ids: 7120432 7120433
```
