## Luminosity 10 percent

### Balance before

```
Compute Information
-------------------
          Project ID     Allocation          Usage     % used
          ----------     ----------          -----     ------
                 ew6         875000          54363        6.2
         --evgenyneu                         54363        6.2
```


### Run

```
Job: `. slurm_jobs/2021/11/22/lum_10_percent_after_80s.sh`
Job ids: 7193676, 7193677
```
## Download

```
mkdir /Volumes/wd_blue_2tb/honours/data/a2021/a11/a22/

rsync -Pvht pawsey:/group/ew6/evgenyneu/prompi_output/2021_11_22_13_47_16_lum_10_percent_after_80s.tar.gz /Volumes/wd_blue_2tb/honours/data/a2021/a11/a22/
```

## Plot 1% vs 10%

Boundary raises faster with 10% than 1%, but still significantly slower than 3D.

![10_percent_one_frame.jpg](a2021/a11/a22_lum_10_percent/plots/10_percent_one_frame.jpg)

![10_percent_one_frame.jpg](a2021/a11/a22_lum_10_percent/plots/10_percent_one_frame.jpg)
