## Extract copied files

```
cd /Users/evgenii/Documents/honours_project/logbook/pawsey_home_backup/slurm_jobs/2021/09/26

python extract_bindata.py /Volumes/wd_blue_2tb/honours/data/a2021/a11/a08/2021_11_08_3d_from_dump400
```


## Meeting

Pressure plot evolution, plot perturbation

## Run 2D full for 3000s

## Balance before

```
$ balance
Compute Information
-------------------
          Project ID     Allocation          Usage     % used
          ----------     ----------          -----     ------
                 ew6         875000          54149        6.2
         --evgenyneu
```


## Run

```
Job: `. slurm_jobs/2021/11/10/2d_264_3nodes_3000s.sh`
Job ids: 7137245 7137246
```
