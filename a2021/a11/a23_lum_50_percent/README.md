## Luminosity 50 percent from 80 sec

### Balance before

```
Compute Information
-------------------
          Project ID     Allocation          Usage     % used
          ----------     ----------          -----     ------
                 ew6         875000          54448        6.2
         --evgenyneu                         54448        6.2
```


### Run

```
Job: `. slurm_jobs/2021/11/23/lum_50_percent_after_80s.sh`
Job ids: 7197580, 7197581
```

## Download

```
mkdir /Volumes/wd_blue_2tb/honours/data/a2021/a11/a23/

rsync -Pvht pawsey:/group/ew6/evgenyneu/prompi_output/2021_11_23_14_17_32_lum_50_percent_after_80s.tar.gz /Volumes/wd_blue_2tb/honours/data/a2021/a11/a23/
```

## Plot 3D vs 2D 50%

![one_frame.jpg](a2021/a11/a23_lum_50_percent/plots/one_frame.jpg)

![evolution.jpg](a2021/a11/a23_lum_50_percent/plots/evolution.jpg)
