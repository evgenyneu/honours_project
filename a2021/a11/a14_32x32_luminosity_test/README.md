## Plot lum 1 vs lum 10

Luminosity is increased from dump 17, as expected.

![enuc.jpg](a2021/a11/a14_32x32_luminosity_test/plots/enuc.jpg)

![enuc_evolution.jpg](a2021/a11/a14_32x32_luminosity_test/plots/enuc_evolution.jpg)


## Next: run 2D with decreased luminosity

Change in BURN/volheat.f90

```
CCP LUMINOSITY update 7/March/2020
      lum = 1.2082151d-4
      if (time .gt. 80) lum = 10.0d0 * lum
```

## Run on Magnus

### Balance before

```
$ balance
Compute Information
-------------------
          Project ID     Allocation          Usage     % used
          ----------     ----------          -----     ------
                 ew6         875000          54284        6.2
         --evgenyneu                         54284        6.2
```


### Run

```
Job: `. slurm_jobs/2021/11/16/lum_0.01_after_80s.sh`
Job ids: 7163666 7163667
```

