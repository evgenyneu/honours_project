import os

ALL_DATA = {
    '2D': {
        'dir': 'a2021/a11/a11/2d_3000s',
        '3d': False,
        'img_title': '2D',
        'gradient_title': '2D'
    },
    '2D_lum_1': {
        'dir': 'a2021/a11/a15/32x32_luminosity_1x',
        '3d': False,
        'img_title': 'lum 1',
        'gradient_title': 'lum 1'
    },
    '2D_lum_10': {
        'dir': 'a2021/a11/a15/32x32_luminosity_10x_after_80s',
        '3d': False,
        'img_title': 'lum 10',
        'gradient_title': 'lum 10'
    },
    '3D': {
        'dir': 'a2021/a11/a11/3d_3000s',
        '3d': True,
        'img_title': '3D',
        'gradient_title': '3D'
    },
}


def data_info(name):
    info = ALL_DATA[name]
    is3d = info['3d']
    file_name = 'ccptwo.3D' if is3d else 'ccptwo.2D'
    root_dir = '/Volumes/wd_blue_2tb/honours/data/'
    full_dir = os.path.join(root_dir, info['dir'])

    return {
        'dir': full_dir,
        'file_name': file_name,
        'plot_title': info['img_title'],
        'gradient_title': info['gradient_title'],
        '3d': is3d
    }


