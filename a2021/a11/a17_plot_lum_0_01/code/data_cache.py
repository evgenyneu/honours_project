import os
import pickle

def cached_data(path):
    if os.path.isfile(path):
        with open(path, 'rb') as f:
            return pickle.load(f)

    return None

def cache_data(path, data):
    os.makedirs(os.path.dirname(path), exist_ok=True)

    with open(path, 'wb') as f:
        pickle.dump(data, f)
