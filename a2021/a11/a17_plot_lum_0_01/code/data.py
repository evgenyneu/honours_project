import os

ALL_DATA = {
    '2D': {
        'dir': 'a2021/a11/a11/2d_3000s',
        '3d': False,
        'img_title': '2D',
        'gradient_title': '2D'
    },
    '2D_0.823_percent_lum_0_s': {
        'dir': 'a2021/a12/a02/0_823_percent_lum_0_sec',
        '3d': False,
        'img_title': '2D 0.82%',
        'gradient_title': '2D 0.82%'
    },
    '2D_0.823_percent_lum_1032x1032': {
        'dir': 'a2021/a12/a19/2d_1032x1032_0_823_percent_lum',
        '3d': False,
        'img_title': '2D 0.82% hires',
        'gradient_title': '2D 0.82% hires'
    },
    '2D_1_percent_lum_80_s': {
        'dir': 'a2021/a11/a17/1_percent_lum_80_sec',
        '3d': False,
        'img_title': '2D 1% 80s',
        'gradient_title': '2D 1% 80s'
    },
    '2D_10_percent_lum_80_s': {
        'dir': 'a2021/a11/a22/10_percent_lum_80_sec',
        '3d': False,
        'img_title': '2D 10% 80s',
        'gradient_title': '2D 10% 80s'
    },
    '2D_50_percent_lum_80_s': {
        'dir': 'a2021/a11/a23/50_percent_lum_80_sec',
        '3d': False,
        'img_title': '2D 50% 80s',
        'gradient_title': '2D 50% 80s'
    },
    '2D_67_percent_lum_80_s': {
        'dir': 'a2021/a11/a24/67_percent_lum_80_sec',
        '3d': False,
        'img_title': '2D 67% 80s',
        'gradient_title': '2D 67% 80s'
    },
    '2D_67_percent_lum_0_s': {
        'dir': 'a2021/a11/a26/67_percent_lum_0_sec',
        '3d': False,
        'img_title': '2D 67% 0s',
        'gradient_title': '2D 67% 0s'
    },
    '2D_64x64': {
        'dir': 'a2021/a12/a20/2d_64x64',
        '3d': False,
        'img_title': '2D 64x64',
        'gradient_title': '2D 64x64'
    },
    '2D_64x64_from_dump_38': {
        'dir': 'a2021/a12/a20/2s_64x64_from_dump_38',
        '3d': False,
        'img_title': '2D 64x64 from 38',
        'gradient_title': '2D 64x64 from 38'
    },
    '2D_264_for_movie': {
        'dir': 'a2021/a12/a27/2d_for_movie_0_823_percent_lum_with_0_25_sec_interval',
        '3d': False,
        'img_title': '2D 264 movie',
        'gradient_title': '2D 264 movie'
    },
    '2D_264_for_movie_from_time_zero': {
        'dir': 'a2021/a12/a22/2d_for_movie_0_823_percent_lum_with_0_25_sec_interval',
        '3d': False,
        'img_title': '2D 264 movie',
        'gradient_title': '2D 264 movie'
    },
    '2D_1032_for_movie_from_time_zero': {
        'dir': 'a2021/a12/a28/hires_2d_for_movie_0_823_percent_lum_with_0_25_sec_interval',
        '3d': False,
        'img_title': '2D 1032 movie',
        'gradient_title': '2D 1032 movie'
    },
    '3D': {
        'dir': 'a2021/a11/a11/3d_3000s',
        '3d': True,
        'img_title': '3D',
        'gradient_title': '3D'
    },
    '3D_1.23345_percent_lum': {
        'dir': 'a2021/a12/a06/lum_1_2345_percent_3d',
        '3d': True,
        'img_title': '3D 1.2%',
        'gradient_title': '3D 1.2%'
    },
    '3D_264_for_movie': {
        'dir': 'a2021/a12/a25/3d_for_movie_1_23_percent_lum_with_0_25_sec_interval',
        '3d': False,
        'img_title': '3D movie',
        'gradient_title': '3D movie'
    },
}


def data_info(name):
    info = ALL_DATA[name]
    is3d = info['3d']
    file_name = 'ccptwo.3D' if is3d else 'ccptwo.2D'
    root_dir = '/Users/evgenii/Documents/honours_project/data'
    full_dir = os.path.join(root_dir, info['dir'])

    return {
        'dir': full_dir,
        'file_name': file_name,
        'plot_title': info['img_title'],
        'gradient_title': info['gradient_title'],
        '3d': is3d
    }


