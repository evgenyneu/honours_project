import os
import numpy as np
from tqdm import tqdm

from a2020.a10.a13_singularity.code.make_movies.thylacine.reader import (
    get_variable_data,
    get_variable_data_2d,
    get_variable_names,
    locate_epochs,
    get_time
)

from a2021.a11.a17_plot_lum_0_01.code.data_cache import (
    cached_data,
    cache_data
)

from a2021.a11.a17_plot_lum_0_01.code.data import data_info
from a2021.a11.a12_perturbation_single_frame.code.perturbation import calc_perturbation

CUSTOM_VARIABLES = ['y_momentum', 'speed', 'kinetic_energy', 'log_kinetic_energy', 'abs_vely', 'abs_velx']

def variable_names(data_dir, data_filename, custom_variables):
    data_path = bindata_path(epoch=1, data_dir=data_dir,
                             data_filename=data_filename)

    data_variables = get_variable_names(data_path)
    return data_variables + custom_variables


def bindata_path(epoch, data_dir, data_filename):
    epoch_str = f"{epoch:05d}"
    path = os.path.join(data_dir, data_filename)
    return f"{path}.{epoch_str}.bindata"


def data_time(epoch, info):
    data_path = bindata_path(epoch=epoch,
                             data_dir=info['dir'],
                             data_filename=info['file_name'])

    return get_time(data_path)


def calc_gradients_2d(data):
    max_value = data.max()
    if max_value > 1: data = data / max_value
    gradients = np.gradient(data)
    return np.sqrt(gradients[0]**2 + gradients[1]**2)


def calc_gradients_3d(data):
    max_value = data.max()
    if max_value > 1: data = data / max_value
    gradients = np.gradient(data)
    return np.sqrt(gradients[0]**2 + gradients[1]**2 + gradients[2]**2)


def calc_y_momentum(epoch, info, custom_variables, full3d):
    vely = load_data(epoch=epoch, variable='vely', info=info,
                     custom_variables=custom_variables, full3d=full3d)

    density = load_data(epoch=epoch, variable='density', info=info,
                        custom_variables=custom_variables, full3d=full3d)
    return vely * density


def calc_kinetic_energy(epoch, info, custom_variables, full3d):
    speed_squared = calc_speed_squared(epoch=epoch, info=info,
                                       custom_variables=custom_variables, full3d=full3d)

    density = load_data(epoch=epoch, variable='density', info=info,
                        custom_variables=custom_variables, full3d=full3d)

    return density * speed_squared


def calc_log_kinetic_energy(epoch, info, custom_variables, full3d):
    energy = calc_kinetic_energy(epoch=epoch, info=info,
                                 custom_variables=custom_variables, full3d=full3d)

    return np.log10(energy)


def calc_speed_squared(epoch, info, custom_variables, full3d):
    x = load_data(epoch=epoch, variable='velx', info=info,
                  custom_variables=custom_variables, full3d=full3d)

    y = load_data(epoch=epoch, variable='vely', info=info,
                  custom_variables=custom_variables, full3d=full3d)

    if info['3d']: # 3D
        z = load_data(epoch=epoch, variable='velz', info=info,
                      custom_variables=custom_variables, full3d=full3d)

        return x**2 + y**2 + z**2
    else: # 2D
        return x**2 + y**2



def calc_speed(epoch, info, custom_variables, full3d):
    speed_squared = calc_speed_squared(epoch=epoch, info=info,
                                       custom_variables=custom_variables, full3d=full3d)

    return np.sqrt(speed_squared)


def load_custom_data(epoch, variable, info, custom_variables, full3d):
    if variable == 'y_momentum':
        return calc_y_momentum(epoch=epoch, info=info,
                               custom_variables=custom_variables, full3d=full3d)

    if variable == 'speed':
        return calc_speed(epoch=epoch, info=info,
                          custom_variables=custom_variables, full3d=full3d)

    if variable == 'abs_vely':
        return np.absolute(
            load_data(epoch=epoch, variable='vely', info=info,
                      custom_variables=custom_variables, full3d=full3d)
        )

    if variable == 'abs_velx':
        return np.absolute(
            load_data(epoch=epoch, variable='velx',info=info,
                      custom_variables=custom_variables, full3d=full3d)
        )

    if variable == 'kinetic_energy':
        return calc_kinetic_energy(epoch=epoch, info=info,
                                   custom_variables=custom_variables,
                                   full3d=full3d)

    if variable == 'log_kinetic_energy':
        return calc_log_kinetic_energy(epoch=epoch, info=info,
                                       custom_variables=custom_variables,
                                       full3d=full3d)

    raise Exception(f'Unknown custom variable {variable}')


def min_max_epochs(data_name):
    info = data_info(data_name)
    epochs = locate_epochs(info['dir'])

    if max(epochs) != len(epochs):
        raise Exception(f"Some epochs are missing")

    return min(epochs), max(epochs)


def calc_data_sum(data):
    nx = data.shape[0]
    data_sum = data.sum(axis=1) / nx

    if len(data.shape) == 3: # 3D
        data_sum = data_sum.sum(axis=1) / nx

    return data_sum


def load_and_calc_data(epoch, variable, show_gradient, perturbation,
                       data_name, custom_variables, full3d):

    data = load_data(epoch=epoch, variable=variable,
                     info=data_info(data_name),
                     custom_variables=custom_variables,
                     full3d=full3d)

    if perturbation: data = calc_perturbation(data)

    if len(data.shape) == 3: # 3D:
        if show_gradient: data = calc_gradients_3d(data)
    else:
        if show_gradient: data = calc_gradients_2d(data)

    return calc_data_sum(data)


def load_from_data_or_cache_from_dict(params):
    load_from_data_or_cache(**params)


def load_from_data_or_cache(
        variable, show_gradient, perturbation,
        data_name, cache_path, custom_variables,show_progress, full3d):

    data = cached_data(cache_path)
    if data is not None: return data

    epoch_start, epoch_end = min_max_epochs(data_name=data_name)

    if show_progress:
        print(f"Loading {variable} {'gradient ' if show_gradient else ''}data for {data_name}...")


    enum = range(epoch_start, epoch_end + 1)
    if show_progress: enum = tqdm(enum)

    for epoch in enum:
        loaded = load_and_calc_data(
            epoch=epoch, variable=variable,
            show_gradient=show_gradient,
            perturbation=perturbation,
            data_name=data_name,
            custom_variables=custom_variables,
            full3d=full3d)

        if data is None:
            data = np.empty((len(loaded), epoch_end))

        data[:, epoch - 1] = loaded

    cache_data(cache_path, data)

    return data


def load_data(epoch, variable, info, custom_variables, full3d):
    if variable in custom_variables:
        return load_custom_data(epoch=epoch, variable=variable,
                                info=info, custom_variables=custom_variables,
                                full3d=full3d)

    data_path = bindata_path(epoch=epoch,
                             data_dir=info['dir'],
                             data_filename=info['file_name'])

    if info['3d'] and full3d:
        return get_variable_data(data_path=data_path, variable=variable)
    else:
        return get_variable_data_2d(
            data_path=data_path, variable=variable, zindex=0
        )
