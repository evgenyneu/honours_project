## Download

```
rsync -Pvht pawsey:/group/ew6/evgenyneu/prompi_output/2021_11_16_15_39_22_lum_0.01_after_80s.tar.gz /Volumes/wd_blue_2tb/honours/data/a2021/a11/a17/
```

## Plot

![plots/evolution.jpg](a2021/a11/a17_plot_lum_0_01/plots/evolution.jpg)

![one_frame.jpg](a2021/a11/a17_plot_lum_0_01/plots/one_frame.jpg)
