def calc_perturbation(data):
    if len(data.shape) == 3: # 3D
        mean = data.mean(axis=(1,2))
        mean = mean[:, None, None]
    else:
        mean = data.mean(axis=1)
        mean = mean[:, None]

    return data - mean
