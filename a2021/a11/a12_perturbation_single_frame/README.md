## Plot 3000s evolution

* 3D boundary continues to raise.

![evolution_3000s.jpg](a2021/a11/a12_plot_3000s_evolution/plots/evolution_3000s.jpg)


## Calculate perturbation

![perturbation.jpg](a2021/a11/a12_perturbation_single_frame/plots/perturbation.jpg)
