## Download

```
rsync -Pvh pawsey:/group/ew6/evgenyneu/prompi_output/2021_11_10_16_56_11_264x264_3000s.tar.gz /Volumes/wd_blue_2tb/honours/data/a2021/a11/a11/2021_11_11_2d_3000s
```

## Missing a file

Missing `ccptwo.3D.00576.bindata`

## Copy 575 dump file

```
cd /group/ew6/evgenyneu/prompi_output/2021_11_08_3d_from_dump400
cp ccptwo.3D.00575.* /scratch/ew6/evgenyneu/PROMPI/setups/ccp_two_layers/3d/data/
```

Extract:

```
cd /scratch/ew6/evgenyneu/PROMPI/setups/ccp_two_layers/3d/data
gzip -d ccptwo.3D.00575.bindata.gz
```

## Run 3D from 575

```

Job: `. slurm_jobs/2021/11/11/3d_from_575.sh`
Job ids: 7139673 7139674
```

### Compress and copy files

```
sbatch slurm_jobs/2021/09/26/gz_copy.sh /scratch/ew6/evgenyneu/PROMPI/setups/ccp_two_layers/3d /group/ew6/evgenyneu/prompi_output/2021_11_11_3d_from_dump_575
```

Job ID:

## Oops

The missing is found. Copy it:

```
cd /scratch/ew6/evgenyneu/PROMPI/setups/ccp_two_layers/3d
cp ccptwo.3D.00576.bindata /group/ew6/evgenyneu/prompi_output/2021_11_08_3d_from_dump400
```

### Compress

```
cd /group/ew6/evgenyneu/prompi_output/2021_11_08_3d_from_dump400
gzip ccptwo.3D.00576.bindata
```


### Download

```
rsync -Pvh pawsey:/group/ew6/evgenyneu/prompi_output/2021_11_08_3d_from_dump400/ccptwo.3D.00576.bindata.gz /Volumes/wd_blue_2tb/honours/data/a2021/a11/a08/2021_11_08_3d_from_dump400
```

