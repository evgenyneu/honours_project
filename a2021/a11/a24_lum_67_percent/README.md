## Luminosity 67 percent from 80 sec

### Balance before

```
Compute Information
-------------------
          Project ID     Allocation          Usage     % used
          ----------     ----------          -----     ------
                 ew6         875000          54542        6.2
         --evgenyneu                         54542        6.2
```


### Run

```
Job: `. slurm_jobs/2021/11/24/lum_67_percent_after_80s.sh`
Job ids: 7203280, 7203281
```

## Download

```
mkdir /Volumes/wd_blue_2tb/honours/data/a2021/a11/a24/

rsync -Pvht pawsey:/group/ew6/evgenyneu/prompi_output/2021_11_24_15_25_30_lum_67_percent_after_80s.tar.gz /Volumes/wd_blue_2tb/honours/data/a2021/a11/a24/
```

## Plot 3D vs 2D 67%

![single_frame.jpg](a2021/a11/a24_lum_67_percent/plots/single_frame.jpg)

![evolution.jpg](a2021/a11/a24_lum_67_percent/plots/evolution.jpg)
