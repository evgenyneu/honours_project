END_OF_LIST = -10_000_000

def sum_upper_index(values, probability):
    """
    A helper function for `credible_interval`.

    Parameters
    ----------
    values: list of float
        List of numbers that sums to 1.
    probability: float
        Probability, value betwee 0 and 1. Default value of 0.6826 corresponds
        to a 1-sigma (68.26%) credible interval.

    Returns
    -------
    Largest upper index of `values` list, such that sum of
    values[0:upper_index] is less than `probablity`.

    Returns END_OF_LIST if end of list is reached.
    """
    total = 0
    right_zeros = 0

    for i, a in enumerate(values):
        total += a

        if total > (probability + 1e-6):
            if i == 0: return 0
            return i - 1 - right_zeros
        else:
            if a < 1e-10:
                right_zeros += 1
            else:
                right_zeros = 0

    return END_OF_LIST


def credible_interval(values, probability=0.6826):
    """
    Returns the narrowest interval that includes the given `probability`,
    or the fraction of the area under the curve for the values (y axis) against
    indices (x axis).

    Parameters
    ----------
    values: list of float
        List of numbers.
    probability: float
        Probability, value between 0 and 1. Default value of 0.6826 corresponds
        to a 1-sigma (68.26%) credible interval.

    Returns
    -------
    Tuple containing lower and upper boundaries of the interval, which
    are indices of `values` list. Both indices are inclusive.
    Returns None if all values are zero
    """
    total = values.sum()

    if total > 1e-50:
        normalized = values / values.sum()
    else:
        return None

    prev_width = len(values)
    interval = (0, len(values) - 1)

    for i in range(len(normalized)):
        index = sum_upper_index(normalized[i:], probability=probability)
        if index == END_OF_LIST: break
        if index < 0: continue
        width = index - i

        if width < prev_width:
            prev_width = width
            interval = (i, i + index)

    return interval
