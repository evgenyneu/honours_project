from credible_interval import credible_interval
import numpy as np

def test_credible_interval1():
    values = np.array([0.25, 0.5, 0.25])
    result = credible_interval(values=values, probability=0.5)
    assert result == (1, 1)

def test_credible_interval2():
    values = np.array([1, 1, 1, 1])
    result = credible_interval(values=values, probability=0.5)
    assert result == (1, 2)

def test_credible_interval3():
    values = np.array([3, 0, 0, 1, 1, 1])
    result = credible_interval(values=values, probability=0.5)
    assert result == (0, 0)

def test_credible_interval4():
    values = np.array([1, 0, 2, 1, 1, 1])
    result = credible_interval(values=values, probability=0.5)
    assert result == (2, 3)

def test_credible_interval4():
    values = np.array([1, 1, 0, 1, 2, 1, 0, 1, 1])
    result = credible_interval(values=values, probability=0.5)
    assert result == (3, 5)

def test_credible_interval5():
    values = np.array([0, 0, 0])
    result = credible_interval(values=values, probability=0.5)
    assert result == None

def test_credible_interval6():
    values = np.array([0, 1, 0])
    result = credible_interval(values=values, probability=0.1)
    assert result == (1, 1)

def test_credible_interval7():
    values = np.array([1, 1, 1, 1])
    result = credible_interval(values=values, probability=1)
    assert result == (0, 3)

