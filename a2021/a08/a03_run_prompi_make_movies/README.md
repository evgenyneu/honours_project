# ... continued

Pawsey is under maintenance.

## Compile Pawsey locally on MacOS

```
brew install open-mpi
```

Add to .zshrc

```
# PROMPI (hydrodynamic simulation https://github.com/evgenyneu/PROMPI)
export SITE=evgenii.mac
export PROMPI_SRC=/Users/evgenii/Documents/honours_project/PROMPI/root/src
```

Add [makefile.local](a2020/a09/a11_compiling_prompi/local/makefile.local) to `root/sites/evgenii.mac` directory.

In `setups/ccp_two_layers/2d` change `dimen.inc` and `inlist_prompi` from [here](a2020/a09/a11_compiling_prompi/local/)


### Complile

```
cd setups/ccp_two_layers/2d
make prompi
```


### Run

```
mpiexec -n 1 ./prompi.x
```

Error:

```
At line 113 of file /Users/evgenii/Documents/honours_project/PROMPI/root/src/IO/write_binary_bindata.f90 (unit = 65, file = 'ccptwo.2D.r512.dev.00001.bindata')
Fortran runtime error: Write exceeds length of DIRECT access record
```

### Fixing the error

In `setups/ccp_two_layers/2d/src/MAIN/input_nml.f90`:

```
irecl_float   = 4
```

Make and run:

```
make prompi
mpiexec -n 1 ./prompi.x
```

Worked!

