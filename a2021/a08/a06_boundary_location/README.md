## X, Y and full gradients

Use full gradients, better visualize the boundary.

![gradient](a2021/a08/a06_boundary_location/plots/plot_boundary_00050_gradient.png)

Code [dx_dy_full_gradients/plot_boundary.py](a2021/a08/a06_boundary_location/code/dx_dy_full_gradients/plot_boundary.py)

## Plot gradient evolution

Plot evolution of gradients at different epochs.

Code: [gradient_evolution/plot_boundary.py](a2021/a08/a06_boundary_location/code/gradient_evolution/plot_boundary.py)


## Make gradient movie

Video: [https://youtu.be/hvytiwdFMN4](https://youtu.be/hvytiwdFMN4)

Code: [gradient_evolution_movie/plot_boundary.py](a2021/a08/a06_boundary_location/code/gradient_evolution_movie/plot_boundary.py)
