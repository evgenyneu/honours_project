import matplotlib.pyplot as plt
import numpy as np
import os
import re
from tqdm import tqdm
import inspect
import pathlib
import shutil
import subprocess
from shutil import copyfile
from tqdm.contrib.concurrent import process_map

from a2020.a10.a13_singularity.code.make_movies.thylacine.reader import (
    get_variable_data,
    get_time
)

from code_dope.python.plot.plot import save_plot


def create_dir(dir):
    """Creates a directory if it does not exist.

    Parameters
    ----------
    dir : str
        Directory path, can be nested directories.

    """

    if not os.path.exists(dir):
        os.makedirs(dir)

def data_files(data_dir):
    """Returns the list of paths to PROMPI data time in the given directory.

    Parameters
    ----------
    data_dir : str
        Path to directory.

    Returns
    -------
    list of str
        Paths to PROMPI data files.

    """

    paths = [str(i) for i in pathlib.Path(data_dir).glob('*.bindata')]
    return sorted(paths)


def this_dir():
    """
    Returns
    -------
    str
        Directory of the current code file

    """
    frame = inspect.stack()[1]
    module = inspect.getmodule(frame[0])
    codefile = module.__file__
    return os.path.dirname(codefile)


def plot_data(data, title, file_suffix, data_path, plot_dir,
              epoch, vmin=None, vmax=None):
    fig, ax = plt.subplots(1, 1)
    ax.set_title(title)
    pos = ax.imshow(data, cmap='Greys', vmin=vmin, vmax=vmax)
    fig.colorbar(pos, ax=ax)
    ax.set_xlabel('Cell index in y direction, $i_y$')
    ax.set_ylabel('Cell index in x direction, $i_x$')
    ax.invert_yaxis()

    # Show time
    # -------

    time = get_time(data_path)

    ax.text(
        0.03, 0.06,
        f'epoch={epoch:05d}, time={int(time)} s',
        horizontalalignment='left',
        verticalalignment='top',
        transform=ax.transAxes,
        fontsize='small',
        bbox=dict(facecolor='white', alpha=0.8, edgecolor='0.7', boxstyle='round'))

    # Save plot
    # -------

    fig.tight_layout()
    save_plot(fig, subdir=plot_dir, suffix=f"{epoch:05d}_{file_suffix}")
    plt.close(fig)


def plot_gradients(data, data_path, plot_dir, epoch):
    gradients = np.gradient(data)
    gradient = np.sqrt(gradients[0]**2 + gradients[1]**2)

    title = (
        f"Gradient of mass fraction of first element, "
        r"$\nabla X_1$"
    )

    plot_data(
        data=gradient,
        title=title,
        file_suffix=f"gradient",
        data_path=data_path,
        plot_dir=plot_dir,
        epoch=epoch
    )


def get_epoch_from_data_path(path):
    pattern = '.(\d+)\.bindata$'
    match = re.search(pattern, path)
    return int(match[1])


def plot(data_path, plot_dir, variable):
    epoch = get_epoch_from_data_path(data_path)
    data = get_variable_data(data_path=data_path, variable=variable)
    data = data[:, :, 0]  # Convert to 2D array
    plot_gradients(data, epoch=epoch, plot_dir=plot_dir, data_path=data_path)


def make_movie(plot_dir, movie_dir, movie_name, frame_rate):
    """
    Creates a movie .mp4 file from individual images.

    Parameters
    ----------
    plot_dir : str
        Path to directory containing individual images for the movie.
    movie_dir : str
        The output directory of the movie
    frame_rate : int
        The frame rate of the movie, in frames per second.

    """

    src_movie = os.path.join(plot_dir, movie_name)

    if os.path.exists(src_movie):
        os.remove(src_movie)

    command = (f"ffmpeg -framerate {frame_rate} -pattern_type glob -i '*.png' "
               f"-c:v libx264 -pix_fmt yuv420p {movie_name}")

    subprocess.call(command, cwd=plot_dir, shell=True)

    # Copy movie to output directory
    # ----------

    dest_movie = os.path.join(movie_dir, movie_name)
    create_dir(movie_dir)

    if os.path.exists(dest_movie):
        os.remove(dest_movie)

    copyfile(src_movie, dest_movie)


def plot_with_args(args):
    plot(**args)


def make_plots(data_paths, plot_dir, variable, silent):
    cpu_workers = os.cpu_count()

    if not silent:
        print(f"Creating plot images using {cpu_workers} CPU workers...")

    args = [
        {
            'data_path': path,
            'plot_dir': plot_dir,
            'variable': variable
        }
        for path in data_paths
    ]

    # Use all CPU cores to make plots in parallel
    process_map(plot_with_args, args, max_workers=cpu_workers)


def plot_and_make_movie(data_paths, variable, movie_dir, movie_name,
                        frame_rate, silent):
    """Make plots and create a movie from them

    Parameters
    ----------
    data_paths : list of str
        Paths to PROMPI data files.
    variable : str
        Variable to be plotted, for example: 'density', 'energy'.
    movie_dir : str
        Directory where the movie will be saved.
    movie_name : str
        Name of the movie file.
    frame_rate : int
        The frame rate of the movie, in frames per second.
    silent : bool
        If True, do not show any output (used in unit tests).

    """
    plot_dir = os.path.join(this_dir(), 'plots_tmp')

    make_plots(data_paths=data_paths, plot_dir=plot_dir, variable=variable,
               silent=silent)

    if not silent:
        print("Making a movie from images...")

    make_movie(plot_dir=plot_dir, movie_dir=movie_dir, movie_name=movie_name,
               frame_rate=frame_rate)

    shutil.rmtree(plot_dir)


def reead_data_and_make_movie(
    data_dir, movie_dir, movie_name,
    max_frames, frame_rate, skip_epochs=0, silent=False):
    """Make plots for all data files and create a movie.

    Parameters
    ----------
    data_dir : str
        Directory containing PROMPI data files.
    movie_dir : str
        Directory where the movie will be saved
    movie_name : str
        Name of the output movie file.
    max_frames : int
        Maximum number of frames in the movie. Each frame is a plot of one
        data file.
    frame_rate : int
        The frame rate of the movie, in frames per second.
    skip_epochs : int
        Number of epochs to skip between frames. If skip_epochs=0, then use
        all frames all.
    silent : bool
        If True, do not show any output (used in unit tests).

    """

    data_paths = data_files(data_dir)
    data_paths = data_paths[::skip_epochs+1]  # skip epochs
    data_paths = data_paths[:max_frames]

    plot_and_make_movie(data_paths=data_paths, variable='0001',
                        movie_dir=movie_dir, movie_name=movie_name,
                        frame_rate=frame_rate,
                        silent=silent)

    movie_path = os.path.join(movie_dir, movie_name)

    if not silent:
        print('Saved movie to')
        print(movie_path)


def lets_gooooo():
    movie_dir = os.path.join(this_dir(), 'movies_tmp')

    data_dir = (
        'data/a2020/a09/a17/'
        '2020_09_17_09_59_26_192x192x1_2000s_time_1.00_luminosity')

    reead_data_and_make_movie(
        data_dir=data_dir, movie_dir=movie_dir,
        movie_name='mass_fraction_gradient.mp4',
        skip_epochs=0, max_frames=10_000, frame_rate=5)


if __name__ == '__main__':
    lets_gooooo()
    print('We are done')
