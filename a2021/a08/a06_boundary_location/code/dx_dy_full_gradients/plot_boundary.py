import matplotlib.pyplot as plt
import numpy as np

from a2020.a10.a13_singularity.code.make_movies.thylacine.reader import (
    get_variable_data,
    get_time
)

from code_dope.python.plot.plot import save_plot


def plot_data(data, title, file_suffix, data_path, epoch, vmin=None, vmax=None):
    fig, ax = plt.subplots(1, 1)
    ax.set_title(title)
    pos = ax.imshow(data, cmap='Greys', vmin=vmin, vmax=vmax)
    fig.colorbar(pos, ax=ax)
    ax.set_xlabel('Cell index in y direction, $i_y$')
    ax.set_ylabel('Cell index in x direction, $i_x$')
    ax.invert_yaxis()

    # Show time
    # -------

    time = get_time(data_path)

    ax.text(
        0.03, 0.06,
        f'epoch={epoch:05d}, time={int(time)} s',
        horizontalalignment='left',
        verticalalignment='top',
        transform=ax.transAxes,
        fontsize='small',
        bbox=dict(facecolor='white', alpha=0.8, edgecolor='0.7', boxstyle='round'))

    # Save plot
    # -------

    fig.tight_layout()
    save_plot(fig, subdir='plots_tmp', suffix=f"{epoch:05d}_{file_suffix}")
    plt.close(fig)


def plot_composition(data, epoch, data_path):
    plot_data(data=data,
        title="Mass fraction of the first element, $X_1$",
        file_suffix="composition",
        vmin=0,
        vmax=1,
        data_path=data_path,
        epoch=epoch
    )



def plot_gradients(data, data_path, epoch):
    # X gradient
    # --------

    gradient_x = np.gradient(data, axis=0)
    gradient = np.abs(gradient_x)

    title = (
        f"X-gradient of mass fraction of first element, "
        f"$d(X_1)/dx$"
    )

    plot_data(
        data=gradient,
        title=title,
        file_suffix=f"gradient_x",
        data_path=data_path,
        epoch=epoch
    )

    # Y gradient
    # --------

    gradient_y = np.gradient(data, axis=1)
    gradient = np.abs(gradient_y)

    title = (
        f"Y-gradient of mass fraction of first element, "
        f"$d(X_1)/dy$"
    )

    plot_data(
        data=gradient,
        title=title,
        file_suffix=f"gradient_y",
        data_path=data_path,
        epoch=epoch
    )

    # Full gradient
    # --------

    gradient = np.sqrt(gradient_y**2 + gradient_x**2)

    title = (
        f"Gradient of mass fraction of first element, "
        r"$\nabla X_1$"
    )

    plot_data(
        data=gradient,
        title=title,
        file_suffix=f"gradient",
        data_path=data_path,
        epoch=epoch
    )


def plot_epoch(epoch):
    epoch_str = f"{epoch:05d}"

    data_path = (
        'data/a2020/a09/a17/'
        '2020_09_17_09_59_26_192x192x1_2000s_time_1.00_luminosity/'
        f'ccptwo.2D.{epoch_str}.bindata'
    )

    variable = '0001'  # Mass fraction of first element

    data = get_variable_data(data_path=data_path, variable=variable)
    data = data[:, :, 0]  # Convert to 2D array

    plot_composition(data, epoch=epoch, data_path=data_path)
    plot_gradients(data, epoch=epoch, data_path=data_path)


def make_plots():
    plot_epoch(50)


if __name__ == '__main__':
    make_plots()
    print('We are done')
