## Mode and area under the curve

Use mode as a point estimate of boundary location, and area under the curve as
estimate of its width.

![Area under the curve](a2021/a08/a20_mode_and_area_under_curve/plots/boundary.png)

