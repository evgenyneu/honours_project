# Run PROMPI on Pawsey

Yesterday Pawsey was on maintenance, now it's working, so I try to compile and run PROMPI on Pawsey.


## Compile PROMPI

```
ssh pawsey
cd /scratch/ew6/evgenyneu/PROMPI/setups/ccp_two_layers/2d/src
```

### Compile

Allocate interactive job:

```
salloc --partition=workq --time 00:30:00 --nodes=1 --ntasks=1
```

Compile:

```
export PROMPI_SRC=/scratch/ew6/evgenyneu/PROMPI/root/src
export SITE=magnus
module swap PrgEnv-cray PrgEnv-intel
make clean
make prompi
```

Worked!

### Run PROMPI

```
salloc --partition=workq --time 00:30:00 --nodes=2 --ntasks-per-node=24
```

```
cd /scratch/ew6/evgenyneu/PROMPI/setups/ccp_two_layers/2d
module swap PrgEnv-cray PrgEnv-intel
srun ./prompi.x
```

Works now! Does not work without `module swap PrgEnv-cray PrgEnv-intel`.


### Compile and run PROMPI with scheduled job

```
$ . slurm_jobs/2020/09/17/jobs/010_2000s_run_1x_luminosity.sh
Submitted PROMPI run job 6794969
View job's status: sacct -j 6794969
```

Compiled successfully. Now running the simululation...
