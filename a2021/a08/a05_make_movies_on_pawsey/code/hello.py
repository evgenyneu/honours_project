from a2020.a10.a13_singularity.code.make_movies.thylacine.reader import get_variable_data

def hello():
    print('hello world')
    return 123

if __name__ == '__main__':
    hello()
    print('We are done')
