import matplotlib.pyplot as plt
import numpy as np

from a2020.a10.a13_singularity.code.make_movies.thylacine.reader import (
    get_variable_data,
    get_time
)

from code_dope.python.plot.plot import save_plot


def plot_data(data, title, file_suffix, vmin=None, vmax=None):
    fig, ax = plt.subplots(1, 1)
    ax.set_title(title)
    pos = ax.imshow(data, cmap='Greys', vmin=vmin, vmax=vmax)
    fig.colorbar(pos, ax=ax)
    ax.set_xlabel('Cell index in y direction, $i_y$')
    ax.set_ylabel('Cell index in x direction, $i_x$')
    ax.invert_yaxis()

    # Show time
    # -------

    time = get_time(data_path)

    ax.text(
        0.05, 0.95,
        f't = {int(time)} s',
        horizontalalignment='left',
        verticalalignment='top',
        transform=ax.transAxes,
        bbox=dict(facecolor='white', alpha=0.8, edgecolor='0.7', boxstyle='round'))

    # Save plot
    # -------

    fig.tight_layout()
    save_plot(fig, suffix=file_suffix)
    plt.close(fig)

def plot_composition(data):
    plot_data(data=data,
        title="Mass fraction of the first element, $X_1$",
        file_suffix=None,
        vmin=0,
        vmax=1
    )


def plot_gradient(data):
    gradient = np.gradient(data, axis=0)

    plot_data(
        data=gradient,
        title="X-gradient of mass fraction of first element, $d(X_1)/dx$",
        file_suffix="gradient"
    )


def lets_goooo(data_path):
    variable = '0001'  # Mass fraction of first element
    data = get_variable_data(data_path=data_path, variable=variable)
    data = data[:, :, 0]  # Convert to 2D array

    plot_composition(data)
    plot_gradient(data)


if __name__ == '__main__':
    data_path = 'data/a2020/a09/a17/2020_09_17_09_59_26_192x192x1_2000s_time_1.00_luminosity/ccptwo.2D.00020.bindata'
    lets_goooo(data_path=data_path)

    print('We are done')
