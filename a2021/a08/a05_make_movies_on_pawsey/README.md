# Make movies

Create [Singularity image on Pawsey](a2020/a10/a13_singularity/README.md).

```
ssh pawsey
cd /scratch/ew6/evgenyneu/logbook/a2020/a10/a13_singularity/code/make_movies
```

* Run `make_movie.py` inside Singularity container:

```
salloc --partition=workq --nodes=1 --ntasks=1 --time=00:05:00
module load singularity
singularity exec -B /scratch/ew6/evgenyneu/prompi_output/2021_08_04_15_18_51_192x192x1_2000s_time_1.00_luminosity:/data /scratch/ew6/evgenyneu/singularity/hello_pawsey_1.1.sif python make_movie.py
exit
```

Success:

> Saved movie to /data/movies/sim_2021-08-05_13-45-29.mp4

scp pawsey:/scratch/ew6/evgenyneu/prompi_output/2021_08_04_15_18_51_192x192x1_2000s_time_1.00_luminosity/movies/sim_2021-08-05_13-45-29.mp4 ./


## Plot mass fraction and its x-gradient

[Code](a2021/a08/a05_make_movies_on_pawsey/code/plot_boundary.py)

![Mass fraction](a2021/a08/a05_make_movies_on_pawsey/code/plots/plot_boundary.png)

![Mass fraction](a2021/a08/a05_make_movies_on_pawsey/code/plots/plot_boundary.pdf)

![Gradient](a2021/a08/a05_make_movies_on_pawsey/code/plots/plot_boundary_gradient.png)
