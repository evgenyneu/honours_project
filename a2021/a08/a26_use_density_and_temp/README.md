Tried to use density and temperature variables instead of composition to locate the boundary. The signal in gradient is similar to composition, but much weaker. Conclusion: composition is the best.

![a2021/a08/a26_use_density_and_temp/plots/gradient_of_other_variables.jpg](a2021/a08/a26_use_density_and_temp/plots/gradient_of_other_variables.jpg)
