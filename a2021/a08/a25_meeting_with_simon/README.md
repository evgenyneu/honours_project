## Meeting with Simon

* Consider temperature and density for locating boundary.

* Develop method of locating boundary

* Test in 2D vs 3D.

* Run 2D tweaking the energy.

* We are interested in region where boundary is stabilized.

* Plot kinetic energy near the boundary


## Example paper

https://ui.adsabs.harvard.edu/abs/1997A%26A...324L..81H/abstract

https://ui.adsabs.harvard.edu/abs/1996A%26A...313..497F/abstract
