## Comparing with Meaking 2007 boundary/width

Calculated boundary using Meaking 2007 method (average of max concentration gradient):

![a2021/a08/a31_max_gradient_method/plots/boundary_evolution_meakin_vs_mine.jpg](a2021/a08/a31_max_gradient_method/plots/boundary_evolution_meakin_vs_mine.jpg)

![a2021/a08/a31_max_gradient_method/plots/boundary_width_evolution_meakin_vs_mine.jpg](a2021/a08/a31_max_gradient_method/plots/boundary_width_evolution_meakin_vs_mine.jpg)

![a2021/a08/a31_max_gradient_method/plots/meakin_vs_my_boundary.jpg](a2021/a08/a31_max_gradient_method/plots/meakin_vs_my_boundary.jpg)


### Thoughts

* Locations and width look very similar.

* Meakin's 'width' estimate is a bit more variable.
