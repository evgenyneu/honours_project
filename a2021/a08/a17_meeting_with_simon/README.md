## Plan

* Reading sessions.
* Writing sessions.
* Read broader papers, old 2D simulations of stellar convection.
* Find papers from references.

## Layout interactive widgets horizontally

![Layout interactive widgets horizontally](a2021/a08/a17_meeting_with_simon/plots/horizontal_widget_layout.png)

## Plot gradient sum

![Plot gradient sum](a2021/a08/a17_meeting_with_simon/plots/gradient_sum.png)

## Next

Calculate mode and 68.3% credible interval.
