import matplotlib.pyplot as plt
import numpy as np

from a2020.a10.a13_singularity.code.make_movies.thylacine.reader import (
    get_variable_data,
    get_time
)

from code_dope.python.plot.plot import save_plot


def plot_data(data, title, file_suffix, data_path, epoch, vmin=None, vmax=None):
    fig, ax = plt.subplots(1, 1)
    ax.set_title(title)
    pos = ax.imshow(data, cmap='Greys', vmin=vmin, vmax=vmax)
    fig.colorbar(pos, ax=ax)
    ax.set_xlabel('Cell index in y direction, $i_y$')
    ax.set_ylabel('Cell index in x direction, $i_x$')
    ax.invert_yaxis()

    # Show time
    # -------

    time = get_time(data_path)

    ax.text(
        0.03, 0.06,
        f'epoch={epoch:05d}, time={int(time)} s',
        horizontalalignment='left',
        verticalalignment='top',
        transform=ax.transAxes,
        fontsize='small',
        bbox=dict(facecolor='white', alpha=0.8, edgecolor='0.7', boxstyle='round'))

    # Save plot
    # -------

    fig.tight_layout()
    save_plot(fig, subdir='plots_tmp', suffix=f"{epoch:05d}_{file_suffix}")
    plt.close(fig)


def plot_composition(data, epoch, data_path):
    plot_data(data=data,
        title="Mass fraction of the first element, $X_1$",
        file_suffix="composition",
        vmin=0,
        vmax=1,
        data_path=data_path,
        epoch=epoch
    )



def plot_gradients(gradients, data_path, epoch):
    title = (
        f"Gradient of mass fraction of first element, "
        r"$\nabla X_1$"
    )

    plot_data(
        data=gradients,
        title=title,
        file_suffix=f"gradient",
        data_path=data_path,
        epoch=epoch
    )


def plot_boundary_distance_distribution(data, data_path, epoch, y):
    title = (
        f"Distribution of X-distances weighted by mass fraction gradient"
    )

    file_suffix = "distance_distr"
    fig, ax = plt.subplots(1, 1)
    ax.set_title(title)
    ax.plot(data[:, y], range(data.shape[0]))
    ax.set_ylim(0, data.shape[0])

    x = (
        f"Gradient of mass fraction of first element, "
        r"$\nabla X_1$"
    )

    ax.set_xlabel(x)
    ax.set_ylabel('Cell index in x direction, $i_x$')
    ax.grid(color='#dddddd')

    # Show time
    # -------

    time = get_time(data_path)

    ax.text(
        0.98, 0.06,
        f'y={y} epoch={epoch:05d}, time={int(time)} s',
        horizontalalignment='right',
        verticalalignment='top',
        transform=ax.transAxes,
        fontsize='small',
        bbox=dict(facecolor='white', alpha=0.8, edgecolor='0.7', boxstyle='round'))

    # Save plot
    # -------

    fig.tight_layout()
    save_plot(fig, subdir='plots_tmp', suffix=f"{epoch:05d}_{file_suffix}_{y:04d}")
    plt.close(fig)


def plot_boundary_distance_distributions(data, data_path, epoch, y_values):
    for y in y_values:
        plot_boundary_distance_distribution(
            data=data,
            data_path=data_path,
            epoch=epoch,
            y=y
        )


def calc_gradients(data):
    gradients = np.gradient(data)
    return np.sqrt(gradients[0]**2 + gradients[1]**2)


def plot_epoch(epoch):
    epoch_str = f"{epoch:05d}"

    data_path = (
        'data/a2020/a09/a17/'
        '2020_09_17_09_59_26_192x192x1_2000s_time_1.00_luminosity/'
        f'ccptwo.2D.{epoch_str}.bindata'
    )

    variable = '0001'  # Mass fraction of first element

    data = get_variable_data(data_path=data_path, variable=variable)
    data = data[:, :, 0]  # Convert to 2D array

    gradients = calc_gradients(data)
    plot_gradients(gradients=gradients, epoch=epoch, data_path=data_path)

    plot_boundary_distance_distributions(
        data=gradients, epoch=epoch, data_path=data_path,
        y_values=[0, 60, 85, 125])


def make_plots():
    epochs = [1, 50, 100, 150, 200, 250, 300, 350, 400]
    # epochs = [150]

    for epoch in epochs:
        plot_epoch(epoch)


if __name__ == '__main__':
    make_plots()
    print('We are done')
