# Plot mass fraction, its gradient and its slice along y-axis

[plot.ipynb](a2021/a08/a13_boundary_distance_distribution/code/gradient_evolution/plot.ipynb)

![Mass fraction and its sum](a2021/a08/a13_boundary_distance_distribution/plots/plot_mass_density_and_its_sum.jpg)
