# Run PROMPI and make movies

I was to recall how to run PROMPI and make movies.


### Download PROMPI

[Download PROMPI](a2020/a09/a10_download_prompi):

Use this branch:

```
cd $MYSCRATCH
git clone git@github.com:evgenyneu/PROMPI.git
cd PROMPI
git checkout evgenii_honours
```


### Checkout correct commit

```
git checkout 2e965dd
```

### Compute balance

```
$ balance
Compute Information
-------------------
          Project ID     Allocation          Usage     % used
          ----------     ----------          -----     ------
                 ew6         875000              2        0.0
         --evgenyneu                             2        0.0
```

### Compile and run PROMPI

```
$ . slurm_jobs/2020/09/17/jobs/010_2000s_run_1x_luminosity.sh
Submitted PROMPI run job 6792150
View job's status: sacct -j 6792150
```

Error:

```
$ cat /scratch/ew6/evgenyneu/jobs_output/2021_08_02_14_47_32_cc2_2D_test_6792481.log
/scratch/ew6/evgenyneu/PROMPI/setups/ccp_two_layers/2d/./prompi.x: error while loading shared libraries: libintlc.so.5: cannot open shared object file: No such file or directory
```
