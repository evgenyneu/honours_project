Radial velocity fluctuates both in 2D and smaller in 3D.

![velx_fluctuations.jpg](a2021/a10/a29_velx_fluctuations/plots/velx_fluctuations.jpg)

Horizontal velocity, once directional streams above the boundary both in 2D and 3D:

![vely.jpg](a2021/a10/a29_velx_fluctuations/plots/vely.jpg)
