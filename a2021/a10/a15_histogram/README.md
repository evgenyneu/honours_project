* Plotted histogram of data values to see how they are distributed.

## Element mass density

In 2D bottom becomes diluted faster (mixing is faster)

![concentration_histogram.jpg](a2021/a10/a15_histogram/plots/concentration_histogram.jpg)


## Velocity Y

* Variation in velocity becomes much larger in 2D, with longer tails.

* Variation grows in 2D while in 3D it stays relatively stable.

![velocity_y_histogram.jpg](a2021/a10/a15_histogram/plots/velocity_y_histogram.jpg)
