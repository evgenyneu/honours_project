## Copy results to mygroup


```
sbatch slurm_jobs/2021/09/26/gz_copy.sh /scratch/ew6/evgenyneu/PROMPI/setups/ccp_two_layers/3d /group/ew6/evgenyneu/prompi_output/2021_10_01_3d_2000s_part2
```

JobID: 5197898

Copied so far:

/group/ew6/evgenyneu/prompi_output/2021_09_26_3d_2000s_part1
/group/ew6/evgenyneu/prompi_output/2021_10_01_3d_2000s_part2


