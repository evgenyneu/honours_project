## Run 3 (from dump 250) completed

Run time: 19:24:35

Output: /group/ew6/evgenyneu/prompi_output/2021_10_03_13_40_22_264x264x264_2000s_run3_from_250.tar.gz

### Download to local SSD

```
rsync -Pvh pawsey:/group/ew6/evgenyneu/prompi_output/2021_10_03_13_40_22_264x264x264_2000s_run3_from_250.tar.gz /Volumes/wd_blue_2tb/honours/data/a2021/a10/a03/2021_10_03_3d_2000s_part3
```

The file looks corrupted.



### Copy run 3 files


```
sbatch slurm_jobs/2021/09/26/gz_copy.sh /scratch/ew6/evgenyneu/prompi_output/2021_10_03_13_40_22_264x264x264_2000s_run3_from_250 /group/ew6/evgenyneu/prompi_output/2021_10_03_3d_2000s_part3
```


### Download to local SSD (second try)

```
rsync -Pvhtr --ignore-existing pawsey:/group/ew6/evgenyneu/prompi_output/2021_10_03_3d_2000s_part3/ /Volumes/wd_blue_2tb/honours/data/a2021/a10/a03/2021_10_03_3d_2000s_part3
```
