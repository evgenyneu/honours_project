* Temperature profiles are different between 2d/3d
* Higher gradients near the boundary, probably higher pressure (fix warnings).
* Could be a problem with 2D.
* Density profile: average over time in 2D and compare to 3D.
* Run the model reducing the heating, to 1% after 80 sec.
