## Todo

* Plot time volution of one z-slice for 3D.

* Check upper boundary energy flux: reflective (both bottom and upper, from code comparison paper 2021).

* Plot composition profiles at each time step.

* Plot Kinetic energy and TKE vs time.

* Run 3D for further 200 dumps.
