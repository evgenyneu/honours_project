import os
import numpy as np

from a2020.a10.a13_singularity.code.make_movies.thylacine.reader import (
    get_variable_data,
    get_variable_data_2d,
    get_variable_names
)

from a2021.a10.a14_full_2d_3d_first_plot.code.data import data_info

CUSTOM_VARIABLES = ['y_momentum', 'speed', 'kinetic_energy', 'log_kinetic_energy', 'abs_vely', 'abs_velx']

def variable_names(data_dir, data_filename, custom_variables):
    data_path = bindata_path(epoch=1, data_dir=data_dir,
                             data_filename=data_filename)

    data_variables = get_variable_names(data_path)
    return data_variables + custom_variables


def bindata_path(epoch, data_dir, data_filename):
    epoch_str = f"{epoch:05d}"
    path = os.path.join(data_dir, data_filename)
    return f"{path}.{epoch_str}.bindata"


def calc_gradients_2d(data):
    max_value = data.max()
    if max_value > 1: data = data / max_value
    gradients = np.gradient(data)
    return np.sqrt(gradients[0]**2 + gradients[1]**2)


def calc_gradients_3d(data):
    max_value = data.max()
    if max_value > 1: data = data / max_value
    gradients = np.gradient(data)
    return np.sqrt(gradients[0]**2 + gradients[1]**2 + gradients[2]**2)


def calc_y_momentum(epoch, data_name, custom_variables):
    vely = load_data(epoch=epoch, variable='vely', data_name=data_name, custom_variables=custom_variables)
    density = load_data(epoch=epoch, variable='density', data_name=data_name, custom_variables=custom_variables)
    return vely * density


def calc_kinetic_energy(epoch, data_name, custom_variables):
    speed_squared = calc_speed_squared(epoch=epoch, data_name=data_name, custom_variables=custom_variables)
    density = load_data(epoch=epoch, variable='density', data_name=data_name, custom_variables=custom_variables)
    return density * speed_squared


def calc_log_kinetic_energy(epoch, data_name, custom_variables):
    energy = calc_kinetic_energy(epoch=epoch, data_name=data_name, custom_variables=custom_variables)
    return np.log10(energy)


def calc_speed_squared(epoch, data_name, custom_variables):
    x = load_data(epoch=epoch, variable='velx', data_name=data_name, custom_variables=custom_variables)
    y = load_data(epoch=epoch, variable='vely', data_name=data_name, custom_variables=custom_variables)

    if len(y.shape) == 2: # 2D
        return x**2 + y**2
    else: # 3D
        z = load_data(epoch=epoch, variable='velz', data_name=data_name, custom_variables=custom_variables)
        return x**2 + y**2 + z**2


def calc_speed(epoch, data_name, custom_variables):
    speed_squared = calc_speed_squared(epoch=epoch, data_name=data_name, custom_variables=custom_variables)
    return np.sqrt(speed_squared)


def load_custom_data(epoch, variable, data_name, custom_variables):
    if variable == 'y_momentum':
        return calc_y_momentum(epoch=epoch, data_name=data_name, custom_variables=custom_variables)

    if variable == 'speed':
        return calc_speed(epoch=epoch, data_name=data_name, custom_variables=custom_variables)

    if variable == 'abs_vely':
        return np.absolute(
            load_data(epoch=epoch, variable='vely', data_name=data_name, custom_variables=custom_variables)
        )

    if variable == 'abs_velx':
        return np.absolute(
            load_data(epoch=epoch, variable='velx',data_name=data_name, custom_variables=custom_variables)
        )

    if variable == 'kinetic_energy':
        return calc_kinetic_energy(epoch=epoch, data_name=data_name, custom_variables=custom_variables)

    if variable == 'log_kinetic_energy':
        return calc_log_kinetic_energy(epoch=epoch, data_name=data_name, custom_variables=custom_variables)

    raise Exception(f'Unknown custom variable {variable}')


def load_data(epoch, variable, data_name, custom_variables):
    if variable in custom_variables:
        return load_custom_data(epoch=epoch, variable=variable,
                                data_name=data_name, custom_variables=custom_variables)

    info = data_info(data_name)

    data_path = bindata_path(epoch=epoch,
                             data_dir=info['dir'],
                             data_filename=info['file_name'])

    if info['3d']:
        return get_variable_data(data_path=data_path, variable=variable)
    else:
        return get_variable_data_2d(
            data_path=data_path, variable=variable, zindex=0
        )
