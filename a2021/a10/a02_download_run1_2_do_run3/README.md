## Download frame 38 from second run

I want to download the frame 38 and compare it withframe from the first run:


```
rsync -Pvh pawsey:/group/ew6/evgenyneu/prompi_output/2021_10_01_3d_2000s_part2/ccptwo.3D.00038.bindata.gz /Users/evgenii/Documents/honours_project/data/a2021/a10/a02/2021_10_01_3d_2000s_part2_one_frame

rsync -Pvh pawsey:/group/ew6/evgenyneu/prompi_output/2021_10_01_3d_2000s_part2/ccptwo.3D.00038.header /Users/evgenii/Documents/honours_project/data/a2021/a10/a02/2021_10_01_3d_2000s_part2_one_frame
```

## Compare frame 38 between runs 1 and 2

[part1_part2_frame38.ipynb](a2021/a10/a02_download_run1_2_do_run3/part1_part2_frame38.ipynb)

Result: identical, all good.


## Balance before

```
Project ID     Allocation          Usage     % used
          ----------     ----------          -----     ------
                 ew6         875000          17900        2.0
         --evgenyneu                         17900        2.0
```

## Run 3: restart from frame 250


```
cd /scratch/ew6/evgenyneu/PROMPI/setups/ccp_two_layers/3d
mv ccptwo.3D.00250* data/
rm ccp*
```

### Run

```
Job: `. slurm_jobs/2021/10/02/3d_264_36nodes_start_dump_250.sh`
Job ids: 6960465 6960466
```


### Copy run 1 to local SSD

Copy locally:

```
rsync -Pvhr pawsey:/group/ew6/evgenyneu/prompi_output/2021_09_26_3d_2000s_part1/ /Volumes/wd_blue_2tb/honours/data/a2021/a10/a02/2021_09_26_3d_2000s_part1
```


### Copy run 2 to local SSD

Copy locally:

```
rsync -Pvhr --ignore-existing pawsey:/group/ew6/evgenyneu/prompi_output/2021_10_01_3d_2000s_part2/ /Volumes/wd_blue_2tb/honours/data/a2021/a10/a02/2021_10_01_3d_2000s_part2
```
