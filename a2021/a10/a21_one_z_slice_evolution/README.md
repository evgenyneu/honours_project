Plotted gradient over time, BUT this time the 3D plots shows sub of gradients from only one z-slice:

![one_z_slice_gradient_over_time.jpg](a2021/a10/a21_one_z_slice_evolution/plots/one_z_slice_gradient_over_time.jpg)

* The 3D boundary is still thin, a bit more variation than in full 3D gradient, but still very different from 2D boundary.

