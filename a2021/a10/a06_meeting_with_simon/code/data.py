ALL_DATA = {
    '2D_192': {
        'dir': (
            './data/a2020/a09/a17/'
            '2020_09_17_09_59_26_192x192x1_2000s_time_1.00_luminosity/'
        ),
        '3d': False,
        'img_title': '2D 192^2',
        'gradient_title': '192^2'
    },
    '2D_264_200s': {
        'dir': './data/a2021/a09/a16/2021_09_16_2D_264/',
        '3d': False,
        'img_title': '2D 200s',
        'gradient_title': '2D 200s'
    },
    '2D_264_2000s': {
        'dir': './data/a2021/a09/a25/2021_09_25_2D_264_2000s/',
        '3d': False,
        'img_title': '2D 2000s',
        'gradient_title': '2D 2000s'
    },
    '2D_32x32': {
        'dir': './data/a2021/a09/a23/2d_32x32/',
        '3d': False,
        'img_title': 'Normal',
        'gradient_title': 'Normal'
    },
    '3D_264_part_1_frame_38': {
        'dir': './data/a2021/a09/a27/2021_09_26_3d_2000s_part1_one_frame/',
        '3d': True,
        'img_title': '3D part1',
        'gradient_title': '3D part1'
    },
    '3D_264_part_2_frame_38': {
        'dir': './data/a2021/a10/a02/2021_10_01_3d_2000s_part2_one_frame/',
        '3d': True,
        'img_title': '3D part2',
        'gradient_title': '3D part2'
    },
    '3D_192': {
        'dir': (
            './data/a2021/a09/a09/'
            '2021_09_09_05_16_192x192x192_200s_time_1.00_luminosity/'
        ),
        '3d': True,
        'img_title': '3D 192^3',
        'gradient_title': '192^3'
    },
    '3D_264': {
        'dir': './data/a2021/a09/a18/2021_09_18_08_59_04_264x264x264_200s/',
        '3d': True,
        'img_title': '3D 264^3',
        'gradient_title': '264^3'
    },
    '3D_264_242_nodes': {
        'dir': './data/a2021/a09/a21/2021_09_21_242_nodes_3D_264/',
        '3d': True,
        'img_title': '3D 264^3 242 nodes',
        'gradient_title': '264^3'
    },
}

def data_info(name):
    info = ALL_DATA[name]
    is3d = info['3d']
    file_name = 'ccptwo.3D' if is3d else 'ccptwo.2D'

    return {
        'dir': info['dir'],
        'file_name': file_name,
        'plot_title': info['img_title'],
        'gradient_title': info['gradient_title'],
        '3d': is3d
    }


