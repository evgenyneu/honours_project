## Simulation speed (36 nodes)

* Run 1: 38 dumps in 24 hours
* Run 2: 215 dumps in 24 hours (5.7 times faster)
* Run 3: 152 dumps in 19 hours 24 min (4.8 times faster)


## Previous (16 nodes)

41 dumps - 8h 45m (525 min).
112      - 24h (1440 min).

1.9 times slower than 36 nodes simulation, good scaling.
