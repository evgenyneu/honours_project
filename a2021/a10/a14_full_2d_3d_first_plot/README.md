* Plotted full 2000s 2D versus 3D.

* The plot of gradient sum is bell-shaped in 3D, slightly skewed.

* In 2D the gradient sum is NOT bell shaped, sometime bimodal or complex.

* The boundary in 2D advances faster.

![full_2d_3d.jpg](a2021/a10/a14_full_2d_3d_first_plot/plots/full_2d_3d.jpg)

TODO: plot histogram of mass density and compare between 2D and 3D.
