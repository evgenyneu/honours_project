# I'm combining several videos and add sound


## Concatenate mp4 files

```
$ cat mylist.txt
file '/path/to/file1'
file '/path/to/file2'
file '/path/to/file3'

$ ffmpeg -f concat -safe 0 -i mylist.txt -c copy output.mp4
```


## Change frame rate

```
ffmpeg -i 2dsim.mp4 -filter:v fps=30 30fps_5.mp4
```


## Add audio to video


```
ffmpeg -i 30fps_5.mp4 -i audio.mp3 -map 0:v -map 1:a -c:v copy -shortest 30fps_with_audio.mp4
```
