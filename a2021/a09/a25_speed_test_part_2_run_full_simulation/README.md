Yesterday I experimented with different compilation option to find out which one is the fastest. Today I want to redo the measurements in random order, to minimize effect of network congestion that could have happened yesterday, since the data center was very busy.

## Balance before

Project ID     Allocation          Usage     % used
          ----------     ----------          -----     ------
                 ew6         875000         681425       77.9
         --scampbell                        669264       76.5
         --evgenyneu                         12161        1.4


## Test results

Results: [speed_test.csv](a2021/a09/a25_speed_test_part_2/speed_test.csv)

No evidence that original configuration (`ftn -dynamic -mcmodel medium -O2 -xCORE-AVX2`) is slower than others. Differences could be caused by network/io congestion.


## Run 2D 2000s simulation


Job: `. slurm_jobs/2021/09/25/2d_264_3nodes.sh`
Job ids: 6934076 6934077

### Copy

```
rsync -Pvh pawsey:/group/ew6/evgenyneu/prompi_output/2021_09_25_14_07_36_264x264_2000s.tar.gz /Users/evgenii/Documents/honours_project/data/a2021/a09/a25
```

## Run 3D 2000s simulation

### Belance before

```
Project ID     Allocation          Usage     % used
          ----------     ----------          -----     ------
                 ew6         875000         681425       77.9
         --scampbell                        669264       76.5
         --evgenyneu                         12161        1.4
```

Job: `. slurm_jobs/2021/09/25/3d_264_36nodes.sh`
Job ids: 6934104 6934105
