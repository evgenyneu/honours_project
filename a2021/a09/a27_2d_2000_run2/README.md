## Copy result of run 1 to scratch

```
mkdir -p  /scratch/ew6/evgenyneu/prompi_output/2021_09_26_3d_2000s_part1
cd /scratch/ew6/evgenyneu/prompi_output/2021_09_26_3d_2000s_part1
salloc --partition=copyq --cluster=zeus --time 01:00:00 --nodes=1 --ntasks=1
cp /scratch/ew6/evgenyneu/PROMPI/setups/ccp_two_layers/3d/ccptwo.3D.* ./
```


## Copy result to mygroup


```
sbatch slurm_jobs/2021/09/26/gz_copy.sh /scratch/ew6/evgenyneu/PROMPI/setups/ccp_two_layers/3d /group/ew6/evgenyneu/prompi_output/2021_09_26_3d_2000s_part1
```

Success: copied PROMPI output to /group/ew6/evgenyneu/prompi_output/2021_09_26_3d_2000s_part1

Download single frame locally:

```
rsync -Pvh pawsey:/group/ew6/evgenyneu/prompi_output/2021_09_26_3d_2000s_part1/ccptwo.3D.00038.bindata.gz /Users/evgenii/Documents/honours_project/data/a2021/a09/a27/2021_09_26_3d_2000s_part1_one_frame
```

## Compare frame 38

[3D_264_part1_test.ipynb](a2021/a09/a27_2d_2000_run2/3D_264_part1_test.ipynb)

Looks very similar.

## Balance before

```
Compute Information
-------------------
          Project ID     Allocation          Usage     % used
          ----------     ----------          -----     ------
                 ew6         875000         756093       86.4
         --scampbell                        721938       82.5
         --evgenyneu                         34155        3.9
```


## Restart simulation from frame 37

```
cd /scratch/ew6/evgenyneu/PROMPI/setups/ccp_two_layers/3d
mv ccptwo.3D.00037* data/
rm ccp*
```

### Run

```
Job: `. slurm_jobs/2021/09/27/3d_264_36nodes_start_dump_37.sh`
Job ids: 6939850 6939851
```
