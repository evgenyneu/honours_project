* Calculated gradient of perturbation of a variable. Perturbation is subtracting the average for each x position (radial distance), which is the effect of removing local background.

* Temperature and density show clear boundary similar to mass fraction.

* However, the mass fraction (without perturbation) still makes a better boundary detector because for temp/density:

  * Boundary is not present at the start.

  * There is more noise with high values away from the boundary.


![Temp perturbation](a2021/a09/a06_temp_perturbation/plots/temp_perturbation.jpg)
