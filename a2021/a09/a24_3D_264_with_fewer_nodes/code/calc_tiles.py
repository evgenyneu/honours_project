from functools import cmp_to_key
import numpy as np

def compare(a, b):
    return abs(np.std(a)) - abs(np.std(b))


def too_many_tiles(dim, tiles, max_tile_size):
    """Return True if the tile size is smaller than `max_tile_size`"""
    return dim / tiles < max_tile_size


def get_tiles_2D(dim, cores_per_node, max_nodes, max_tile_size):
    tiles = []

    for tile in range(1, round(dim / 2 + 1)):
        if dim % tile == 0:
            for tile2 in range(tile, round(dim / 2 + 1)):
                if dim % tile2 == 0:
                    cores = tile * tile2
                    nodes = cores / cores_per_node
                    
                    if too_many_tiles(dim, tile, max_tile_size) or \
                        too_many_tiles(dim, tile2, max_tile_size):
                            
                        continue

                    if nodes == round(nodes) and nodes == max_nodes:
                        tiles.append([tile, tile2])


    tiles.sort(key=cmp_to_key(compare))

    if len(tiles) > 0:
        print(f"\n--------------Nodes={max_nodes}--------------")

    for tile in tiles:
        nodes = round(tile[0] * tile[1] / cores_per_node)
        print(f"{tile[0]}x{tile[1]}")


def get_tiles_3D(dim, cores_per_node, max_nodes, max_tile_size):
    tiles = []

    for tile in range(1, round(dim / 2 + 1)):
        if dim % tile == 0:
            for tile2 in range(tile, round(dim / 2 + 1)):
                if dim % tile2 == 0:
                    for tile3 in range(tile2, round(dim / 2 + 1)):
                        if dim % tile3 == 0:
                            cores = tile * tile2 * tile3
                            nodes = cores / cores_per_node
                            
                            if too_many_tiles(dim, tile, max_tile_size) or \
                               too_many_tiles(dim, tile2, max_tile_size) or \
                               too_many_tiles(dim, tile3, max_tile_size):
                            
                                continue

                            if nodes == round(nodes) and nodes == max_nodes:
                                tiles.append([tile, tile2, tile3])


    tiles.sort(key=cmp_to_key(compare))

    if len(tiles) > 0:
        print(f"\n\n--------------Nodes={max_nodes}--------------")

    for tile in tiles:
        nodes = round(tile[0] * tile[1] * tile[2] / cores_per_node)
        print(f"{tile[0]}x{tile[1]}x{tile[2]}")


if __name__ == '__main__':
    dim = 264
    max_tile_size = 20
    cores_per_node = 24

    print('\n\n\n================2D {dim}x{dim}================\n')
    
    for max_nodes in range(1, round(20)):
        get_tiles_2D(dim=dim, cores_per_node=cores_per_node,
                     max_nodes=max_nodes,
                     max_tile_size=max_tile_size)


    print('\n\n\n================3D {dim}x{dim}x{dim}================\n')

    for max_nodes in range(16, round(120)):
        get_tiles_3D(dim=dim, cores_per_node=cores_per_node,
                     max_nodes=max_nodes,
                     max_tile_size=max_tile_size)

    print('We are done')
