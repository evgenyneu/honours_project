[Previously](a2021/a09/a21_run_3D_264_more_nodes/README.md) I used 242 nodes for 3D 264^3 simulation. That's too many, interprocess communication becomes the bottleneck. Simon told me that one tile should not be smaller than 20 pixels on each side. 


## Calculate tiles and nodes

Code: [calc_tiles.py](a2021/a09/a24_3D_264_with_fewer_nodes/code/calc_tiles.py)

### 2D

Nodes: 6
Tiles: 12x12


### 3D

Nodes: 48
Tiles: 12x12x8


## Balance before

```
$ balance
Compute Information
-------------------
          Project ID     Allocation          Usage     % used
          ----------     ----------          -----     ------
                 ew6         875000         641724       73.3
         --scampbell                        631696       72.2
         --evgenyneu                         10027        1.1
```


## Speed test

I want to experiment with compilation flags in order to improve simulation speed. I will run 3D 264^3 simulation for 10 min and see how many dumps it will create.


## Ideas from Pawsey compiling [documentation](https://support.pawsey.org.au/documentation/display/US/Compiling+Introduction)


* xHost: `icc -O3 -xHost -c main.c`

* Inlining -ipo:  `icc -O3 -ipo -c main.c`

* -shared-intel: `icc -mcmodel=large -shared-intel -c main.c`

* `-fast`: A combination of -Ofast, -ipo, -static (for static linking),  and -xHost ([source](https://www.bu.edu/tech/support/research/software-and-programming/programming/compilers/intel-compiler-flags/))

### test1 (previous used flags)

Flags: `ftn -dynamic -mcmodel medium -O2 -xCORE-AVX2`
Job: `. slurm_jobs/2021/09/22/test1.sh`
Job IDs: 6928411 6928412


Error: [2021_09_24_09_05_53_264x264x264_test1_6928412.log.zip](a2021/a09/a24_3D_264_with_fewer_nodes/pawsey_logs/2021_09_24_09_05_53_264x264x264_test1_6928412.log.zip)


### Try different tiles

### 2D

Nodes: 4
Tiles: 12x8


### 3D

Nodes: 44
Tiles: 12x8x11


### Test 1 (with reduce 44 nodes)


Flags: `ftn -dynamic -mcmodel medium -O2 -xCORE-AVX2`
Job: `. slurm_jobs/2021/09/22/test1_2.sh`
Job IDs: 6929388 6929389

Build time: 04:17

Run failed with errors: [2021_09_24_11_24_55_264x264x264_test1_6929389.log.zip](a2021/a09/a24_3D_264_with_fewer_nodes/pawsey_logs/2021_09_24_11_24_55_264x264x264_test1_6929389.log.zip)


### Test 1 (reverting dumpdata change)

Not sure what's causing the failure. Reverting the dumpdata change.


Flags: `ftn -dynamic -mcmodel medium -O2 -xCORE-AVX2`
Job: `. slurm_jobs/2021/09/22/test1_3_revert_dumpdata.sh`
Job IDs: 6929846 6929847

Failed with same error.


### Try 6x8x8 16 nodes

Out of ideas. Reverting to previous configuration that worked before to see if it still works.

Job: `. slurm_jobs/2021/09/22/test1_4_revert_to_6x8x8_16_nodes.sh`
Job IDs: 6929860 6929861

Works!


### Decrease tout from 5 to 0.1 sec


Job: `. slurm_jobs/2021/09/22/test1_5_decrease_tout.sh`
Job IDs: 6929873 6929874
 
Worked!


### Try 36 nodes

Tiles: 6x12x12

Job: `. slurm_jobs/2021/09/22/test1_5_6x12x12_36_nodes.sh`
Job IDs: 6929886 6929887

Worked! Holy!


### Test 2: remove `-dynamic`

Flags: `ftn -mcmodel medium -O2 -xCORE-AVX2`
Job: `. slurm_jobs/2021/09/22/test2_remove_dynamic.sh`
Job IDs: 6930217 6930218

Build error: [2021_09_24_13_21_28_build_prompi_6930217.log](a2021/a09/a24_3D_264_with_fewer_nodes/pawsey_logs/2021_09_24_13_21_28_build_prompi_6930217.log)

### Test 3: add `-shared-intel`

Flags: `ftn -dynamic -mcmodel medium -shared-intel -O2 -xHost -xCORE-AVX2`
Job: `. slurm_jobs/2021/09/22/test3_add_shared-intel.sh`
Job IDs: 6930222 6930223


### Test 4: add `-xHost`

Flags: `ftn -dynamic -mcmodel medium -shared-intel -O2 -xHost -xCORE-AVX2`
Job: `. slurm_jobs/2021/09/22/test4_add_xhost.sh`
Job IDs: 6930233 6930234



### Test 5: use `-O3`

Flags: `ftn -dynamic -mcmodel medium -shared-intel -O3 -xCORE-AVX2`
Job: `. slurm_jobs/2021/09/22/test5_o3.sh`
Job IDs: 6930721 6930722 

Build time: 6:17 (two times slower than -O2).



### Test 6: use `-ipo`

Flags: `ftn -dynamic -mcmodel medium -shared-intel -O3 -ipo -xCORE-AVX2`
Job: `. slurm_jobs/2021/09/22/test6_ipo.sh`
Job IDs: 6930954 6930955

Build time: 11:30


### Test 7: don't use mcmodel

Flags: `ftn -dynamic -O3 -ipo -xCORE-AVX2`
Job: `. slurm_jobs/2021/09/22/test7_no_mcmodel.sh`
Job IDs: 6931027 6931028

Build time: 12:33


### Test 8: remove dynamic

Flags: `ftn -O3 -ipo -xCORE-AVX2`
Job: `. slurm_jobs/2021/09/22/test8_no_dynamic.sh`
Job IDs: 6931272 6931273

Build time: 08:00


### Test 9: Use xHost instead of xCORE-AVX2

Flags: `ftn -O3 -ipo -xHost`
Job: `. slurm_jobs/2021/09/22/test9_xhost.sh`
Job IDs: 6931286 6931287

Build time: 08:00


### Result

Fastest flags: `ftn -O3 -ipo -xHost`

[speed_test.csv](a2021/a09/a24_3D_264_with_fewer_nodes/speed_test.csv)


## Fix restart dump index

Job: `. slurm_jobs/2021/09/22/fix_restart_dump_index.sh`
Job IDs: 6931326 6931327 compiled but did not report run job
Job IDs: 6931386 6931387







