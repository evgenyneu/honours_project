I want to move output of PROMPI from scratch to group location, compressing individual .bindata files. Previously I compressed all files, but it's not ideal here since it will be very big (hundreds of GB). It will be easier and faster to download many smaller files with rsync than on 100 GB file.

Source: [gz_move.sh](pawsey_home_backup/slurm_jobs/2021/09/26/gz_move.sh)

gz_move.sh

## Check Python version available on copyq

```
salloc --partition=workq --time 00:05:00 --nodes=1 --ntasks=1
module avail -S ython
```

Output:

```
------------------------------------------ /opt/modulefiles ------------------------------------------
cray-python/2.7.13.1(default) cray-python/3.6.1.1
cray-python/2.7.15.3          cray-python/3.7.3.2

--------------------------------- /pawsey/cle60up07/modulefiles/apps ---------------------------------
python/3.6.3(default)

------------------------------- /pawsey/cle60up07/modulefiles/bio-apps -------------------------------
biopython/1.70(default)

-------------------------------- /pawsey/cle60up07/modulefiles/python --------------------------------
cython/0.27.3(default)          ipython/5.5.0(default)          python-casacore/3.3.1
cython/0.29.21                  ipython_genutils/0.2.0(default) python-dateutil/2.6.1(default)
```


```
salloc --cluster=zeus --partition=copyq --time 00:05:00 --nodes=1 --ntasks=1
module avail -S ython
```

## Run test job

Job: `sbatch slurm_jobs/2021/09/26/gz_copy.sh mysource mydest`

## Run a copy job

```
sbatch slurm_jobs/2021/09/26/gz_copy.sh /scratch/ew6/evgenyneu/prompi_output/2021_09_16_17_05_53_lovely_simulation /group/ew6/evgenyneu/prompi_output/2021_09_26_copy_test2
```

Copied to /group/ew6/evgenyneu/prompi_output/2021_09_26_copy_test2

Copy locally:

```
rsync -Pvhr pawsey:/group/ew6/evgenyneu/prompi_output/2021_09_26_copy_test2/ /Users/evgenii/Documents/honours_project/data/a2021/a09/a26/2021_09_26_copy_test2
```

## Extract copied files

```
python extract_bindata.py /Users/evgenii/Documents/honours_project/data/a2021/a09/a26/2021_09_26_copy_test2
```




