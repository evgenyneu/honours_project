DATA_PATHS = {
    '2D': {
        'dir': (
            './data/a2020/a09/a17/'
            '2020_09_17_09_59_26_192x192x1_2000s_time_1.00_luminosity/'
        ),
        'file_name': 'ccptwo.2D'
    },
    '3D': {
        'dir': (
            './data/a2021/a09/a09/'
            '2021_09_09_05_16_192x192x192_200s_time_1.00_luminosity/'
        ),
        'file_name': 'ccptwo.3D'
    }
}

def all_colormaps():
    perceptually_uniform_sequential = [
        'viridis', 'plasma', 'inferno', 'magma', 'cividis']

    sequential = [
        'Greys', 'Purples', 'Blues', 'Greens', 'Oranges', 'Reds',
        'YlOrBr', 'YlOrRd', 'OrRd', 'PuRd', 'RdPu', 'BuPu',
        'GnBu', 'PuBu', 'YlGnBu', 'PuBuGn', 'BuGn', 'YlGn']

    sequential2 = ['binary', 'gist_yarg', 'gist_gray', 'gray', 'bone', 'pink',
            'spring', 'summer', 'autumn', 'winter', 'cool', 'Wistia',
            'hot', 'afmhot', 'gist_heat', 'copper']

    diverging = [
        'PiYG', 'PRGn', 'BrBG', 'PuOr', 'RdGy', 'RdBu',
        'RdYlBu', 'RdYlBu_r', 'RdYlGn', 'Spectral', 'coolwarm', 'bwr', 'seismic']

    cyclic = ['twilight', 'twilight_shifted', 'hsv']

    qualitative = ['Pastel1', 'Pastel2', 'Paired', 'Accent',
        'Dark2', 'Set1', 'Set2', 'Set3',
        'tab10', 'tab20', 'tab20b', 'tab20c']

    misc =  [
        'flag', 'prism', 'ocean', 'gist_earth', 'terrain', 'gist_stern',
        'gnuplot', 'gnuplot2', 'CMRmap', 'cubehelix', 'brg',
        'gist_rainbow', 'rainbow', 'jet', 'turbo', 'nipy_spectral',
        'gist_ncar']

    return (
        perceptually_uniform_sequential +
        sequential + sequential2 + diverging + cyclic + qualitative + misc
    )

