## Simulation ran, but it did not compress the output

Fixing the copy file script and reruning (comminting out actual simulation):

```
$ cd
$ . slurm_jobs/2021/09/17/jobs/264x264x264_200s.sh
View job's status: sacct -j 6913063
View job's status: sacct -j 6913064
```

Copied to

/group/ew6/evgenyneu/prompi_output/2021_09_18_08_59_04_264x264x264_200s.tar.gz

Size: 14 GB.


### Copy to local computer

```
rsync -Pvh pawsey:/group/ew6/evgenyneu/prompi_output/2021_09_18_08_59_04_264x264x264_200s.tar.gz /Users/evgenii/Documents/honours_project/data/a2021/a09/a18
```
