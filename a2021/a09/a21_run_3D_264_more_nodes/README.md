## Running 264 grid 3D with more nodes

[Previously](a2021/a09/a17_2D_192_vs_264/README.md) I ran 264 3D job on 16 nodes and it took 8 hours 45 min to complete. Job was 200 s simulation, which is 10% of what's needed. The full simulation will take 87 hours 30 min. This is above the maximum running time of 24 hours (see [limits](https://support.pawsey.org.au/documentation/display/US/Queue+Policies+and+Limits)).

I need to increase the number of nodes at least by the factor of four, so the full simulation is within 24 hr.

User [calc_tiles.py](a2021/a09/a21_run_3D_264_more_nodes/code/calc_tiles.py) to calculate tiles and nodes.

## 2D

Nodes: 11
Tiles (X*Y*Z): 12x22

## 3D

Nodes: 242
Tiles (X*Y*Z): 12x22x22


## Run 3D sim

### Balance before

```
$ ssh pawsey
$ balance
Compute Information
-------------------
          Project ID     Allocation          Usage     % used
          ----------     ----------          -----     ------
                 ew6         875000         334203       38.2
         --scampbell                        329993       37.7
         --evgenyneu                          4210        0.5
```


### Run (failed)

```
$ cd
$ . slurm_jobs/2021/09/21/jobs/264x264x264_20s.sh
View job's status: sacct -j 6918689
View job's status: sacct -j 6918690
```

Forgot to uncomment the run command in slurm job. :)


### Run (again)

```
$ cd
$ . slurm_jobs/2021/09/21/jobs/264x264x264_20s.sh
View job's status: sacct -j 6918699
View job's status: sacct -j 6918700
```

### Balance before

```
Project ID     Allocation          Usage     % used
          ----------     ----------          -----     ------
                 ew6         875000         347136       39.7
         --scampbell                        337108       38.5
         --evgenyneu                         10027        1.1
```

### Results

Created 23 snapshopts in 60 min. This means it will take 60*401/23 = 17.4 hours to run entire simulation.

It consumed 0.6% of balance. Mean full simulation will take 11%.

### Move files

```
cd slurm_jobs/2021/09/16/run
sbatch move_files.sh /scratch/ew6/evgenyneu/PROMPI/setups/ccp_two_layers/3d/src "242 node test run" 3D_264

sacct -j 5184256
```

Failed with error because rans and completion files did not exist. Try again


```
sbatch move_files.sh /scratch/ew6/evgenyneu/PROMPI/setups/ccp_two_layers/3d/src "242 node test run" 3D_264
sacct -j 5184302
```

Complete successfully:

```
/group/ew6/evgenyneu/prompi_output/2021_09_21_16_36_43_3D_264.tar.gz
```

### Copy to local computer

```
rsync -Pvh pawsey:/group/ew6/evgenyneu/prompi_output/2021_09_21_16_36_43_3D_264.tar.gz /Users/evgenii/Documents/honours_project/data/a2021/a09/a21
```



