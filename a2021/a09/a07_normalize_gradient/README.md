* Normalized gradient for each y column before summations.

* Not much difference from original.

* Reduced the values far from boundary between 0 and 100 epochs.

* The gradient point estimate hardly changed.

![Normalized](a2021/a09/a07_normalize_gradient/plots/normalized.jpg)

![Normalized vs original](a2021/a09/a07_normalize_gradient/plots/original_vs_normalized.jpg)
