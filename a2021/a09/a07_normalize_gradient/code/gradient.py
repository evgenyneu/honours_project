def get_normalized_gradient_sum(gradients):
    column_sum = gradients.sum(axis=0)
    normalized = gradients / column_sum
    return normalized.sum(axis=1)


def clip_gradient_sum(gradients_sum, min_clip):
    min_value = gradients_sum.max() * min_clip
    gradients_sum[gradients_sum < min_value] = 0
    return gradients_sum
