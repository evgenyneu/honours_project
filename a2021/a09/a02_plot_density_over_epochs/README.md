## Plotting gradient vs epoch

![Gradient vs epoch](a2021/a09/a02_plot_density_over_epochs/plot/gradient_vs_epoch_color_maps.jpg)

Opinion: better than just [plotting point estimates](a2021/a08/a24_plot_boundary_evolution) (mode and uncertainty), because you can see details.

## Meeting with Simon

* Make 3D simulation

* Normalize area under the curve for each gradient before summing

* Calculate temperature perturbation and then take gradient of that

* Calculate pressure perturbation.

* Send Simon links to the code.

## Experimented with colormaps

Good ones for mass fraction gradient: 'turbo', 'Pink', 'BrBG'
