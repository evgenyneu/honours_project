DATA_PATHS = {
    '2D': {
        'dir': (
            './data/a2020/a09/a17/'
            '2020_09_17_09_59_26_192x192x1_2000s_time_1.00_luminosity/'
        ),
        'file_name': 'ccptwo.2D'
    },
    '2D_264': {
        'dir': './data/a2021/a09/a16/2021_09_16_2D_264/',
        'file_name': 'ccptwo.2D'
    },
    '3D': {
        'dir': (
            './data/a2021/a09/a09/'
            '2021_09_09_05_16_192x192x192_200s_time_1.00_luminosity/'
        ),
        'file_name': 'ccptwo.3D'
    },
    '3D_264': {
        'dir': './data/a2021/a09/a18/2021_09_18_08_59_04_264x264x264_200s/',
        'file_name': 'ccptwo.3D'
    },
}
