[Yesterday](a2021/a09/a21_run_3D_264_more_nodes/README.md) I ran short 3D 264^3 simulation. I want compare it with 16 node simulation to see if they differ. I want to make sure the tile configuration does not affect the results.

## Results

The values produces by the two simulation are not identical. They are similar at epoch 9, but slowly start to diverge after.

![epoch_09.jpg](a2021/a09/a22_compare_242_vs_16_nodes_3d_264/plots/epoch_09.jpg)

![epoch_13.jpg](a2021/a09/a22_compare_242_vs_16_nodes_3d_264/plots/epoch_13.jpg)


## Next

Learn how to resume simulation using 192^3 3D setup from [before](a2021/a09/a08_short_test_3d_simulation/README.md).
