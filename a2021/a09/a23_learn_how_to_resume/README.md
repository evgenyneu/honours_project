# Learn how to resume PROMPI simulation

### Test locally

```
git checkout evgenii_honours_run_on_mac
cd setups/ccp_two_layers/2d/src
make prompi
mpiexec -n 1 ./prompi.x
```


### How to restart PROMPI

* Put one set of .bindata and .header files into `setups/../data` directory. These are the file you want to restart from.

* In `setups/inlist_prompi` set `irstrt` variable to the number of the dump file. For example, if you put `sim.00020.bindata` into `data`, then use `irstrt = 20`.

* Fix the bug in `dumpdata.f90`. In line 116, replace `ndump   = irstrt + 1` with `ndump   = irstrt`. This bug caused incorrect dump file numbers. In the example above, when we want to restart from `sim.00020.bindata`, the first dump after restart would be named `sim.00021.bindata` but it was identical to `ccptwo.2D.00020.bindata`. The second dump would be named `sim.00022.bindata`, which should be `sim.00021.bindata`.


### Comparing normal with restarted

I ran a small 2D 32x32 simulation from 1 to 30 dump. Then ran it again, restarting from dump 20 (`irstrt = 20`). Next, I compared the results. THe numbers are very close, starting to diverge slightly after about three dumps (15 sec). This probably happens because the dumps used for restart only store single precision numbers, while simulation code words in double precision.

![Compare normal with restarted](a2021/a09/a23_learn_how_to_resume/plots/compare_restarted.jpg)
