ALL_DATA = {
    '2D_192': {
        'dir': (
            './data/a2020/a09/a17/'
            '2020_09_17_09_59_26_192x192x1_2000s_time_1.00_luminosity/'
        ),
        '3d': False,
        'img_title': '2D 192^2',
        'gradient_title': '192^2'
    },
    '2D_264': {
        'dir': './data/a2021/a09/a16/2021_09_16_2D_264/',
        '3d': False,
        'img_title': '2D 264^2',
        'gradient_title': '264^2'
    },
    '2D_32x32': {
        'dir': './data/a2021/a09/a23/2d_32x32/',
        '3d': False,
        'img_title': 'Normal',
        'gradient_title': 'Normal'
    },
    '2D_32x32_restarted': {
        'dir': './data/a2021/a09/a23/2d_32x32_restarted_from_00020/',
        '3d': False,
        'img_title': 'Restarted',
        'gradient_title': 'Restarted'
    },
    '3D_192': {
        'dir': (
            './data/a2021/a09/a09/'
            '2021_09_09_05_16_192x192x192_200s_time_1.00_luminosity/'
        ),
        '3d': True,
        'img_title': '3D 192^3',
        'gradient_title': '192^3'
    },
    '3D_264': {
        'dir': './data/a2021/a09/a18/2021_09_18_08_59_04_264x264x264_200s/',
        '3d': True,
        'img_title': '3D 264^3',
        'gradient_title': '264^3'
    },
    '3D_264_242_nodes': {
        'dir': './data/a2021/a09/a21/2021_09_21_242_nodes_3D_264/',
        '3d': True,
        'img_title': '3D 264^3 242 nodes',
        'gradient_title': '264^3'
    },
}

def data_info(name):
    info = ALL_DATA[name]
    is3d = info['3d']
    file_name = 'ccptwo.3D' if is3d else 'ccptwo.2D'

    return {
        'dir': info['dir'],
        'file_name': file_name,
        'plot_title': info['img_title'],
        'gradient_title': info['gradient_title'],
        '3d': is3d
    }


