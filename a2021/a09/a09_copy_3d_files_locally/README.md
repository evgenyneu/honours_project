## What's happening?

* Yesterday the test 192x192x192 3d simulation ran for two hours, for 34 epochs.

* Consumed 784 credits (~ 2 hours * 16 nodes * 24 cores/node).

* I want to copy files locally to my computer, so I can plot the data.


## Moving files

### Schedule job

```
cd /scratch/ew6/evgenyneu/PROMPI/setups/ccp_two_layers/3d
salloc --partition=copyq --cluster=zeus --time 00:30:00 --nodes=1 --ntasks=1
```

### Move files to a separate directory

```
suffix="192x192x192_200s_time_1.00_luminosity"
date=$(date '+%Y_%m_%d_%H_%M_%S')
output_dir="${MYSCRATCH}/prompi_output/${date}${suffix}"
mkdir -p $output_dir
mv *.bindata *.header $output_dir
```

Moved to directory: /scratch/ew6/evgenyneu/prompi_output/2021_09_09_05_16_15192x192x192_200s_time_1.00_luminosity

Directory size: 12G

### Compress

```
cd /scratch/ew6/evgenyneu/prompi_output/
tar -zcvf 2021_09_09_05_16_15192x192x192_200s_time_1.00_luminosity.tar.gz -C 2021_09_09_05_16_15192x192x192_200s_time_1.00_luminosity .
```

File: /scratch/ew6/evgenyneu/prompi_output/2021_09_09_05_16_15192x192x192_200s_time_1.00_luminosity.tar.gz
Size: 6G



### Copy to local computer

```
rsync -Pvh pawsey:/scratch/ew6/evgenyneu/prompi_output/2021_09_09_05_16_15192x192x192_200s_time_1.00_luminosity.tar.gz /Users/evgenii/Documents/honours_project/data/a2021/a09/a09
```


