## Run 3D on Pawsey

### Update PROMPI source code

* Use branch `evgenii_honours`

* Make 3D same as 2D.

* Difference: in 3D use 6 * 8 * 8 zones (instead of 6 * 8 * 1).

* 2D used 2 nodes (CPUs), each having 24 cores on Magnus.

* In 3D we need to use 8 times more nodes: 16 nodes in total.

* Restrict max time to 200 sec, just for a first test run, commit `17c4a55`.


## Compile PROMPI

### Check balance

```
$ ssh pawsey
$ balance
Compute Information
-------------------
          Project ID     Allocation          Usage     % used
          ----------     ----------          -----     ------
                 ew6         875000             50        0.0
         --evgenyneu                            50        0.0
```

### Change directory

```
cd $MYSCRATCH
git clone git@github.com:evgenyneu/PROMPI.git
cd PROMPI
git checkout evgenii_honours
```

### Compile interactively

Allocate interactive job:

```
salloc --partition=workq --time 00:30:00 --nodes=1 --ntasks=1
```

Compile:

```
cd setups/ccp_two_layers/3d/src
export PROMPI_SRC=/scratch/ew6/evgenyneu/PROMPI/root/src
export SITE=magnus
module swap PrgEnv-cray PrgEnv-intel
make clean
make prompi
```

### Run PROMPI interactively

Allocate job:

```
salloc --partition=workq --time 00:30:00 --nodes=16 --ntasks-per-node=24
```

Run:

```
cd /scratch/ew6/evgenyneu/PROMPI/setups/ccp_two_layers/3d
module swap PrgEnv-cray PrgEnv-intel
srun ./prompi.x
```

### Compile and run PROMPI with scheduled job

```
$ cd
$ . slurm_jobs/2021/09/08/jobs/010_200s_run_1x_luminosity.sh
Submitted PROMPI compilation job 6885950
View job's status: sacct -j 6885950
Submitted PROMPI run job 6885951
View job's status: sacct -j 6885951
```

Compiled successfully. Now running the simululation...



