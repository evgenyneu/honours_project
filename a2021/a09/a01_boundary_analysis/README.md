## Meakin vs this work boundary comparison

My boundary is higher, because Meakin average of max density points, and their boundary is lowered by few outliers. Mine is more robust. See epoch 46:

![Meaking vs This work](a2021/a09/a01_boundary_analysis/plots/meaking_this_work_46.jpg)

### Boundary location

Meakins boundary: average of max density points. Does not represent distance where most change is happening. Imagine bimodal distribution, with big dip of zero gradient in the middle. The average will be in this dip where no change is happening. Less sensitive to changing boundary because average smooths out variation.

My boundary: represents distance from the center. If you traverse entire volume at this distance you will get the total change in concentration will be the highest. More sensitive to the change in boundary.


### Boundary width

Meakins boundary width: variation of max gradient values. Does not physically represet region where the concentration is changing. Example, consider Epoch 1, standard deviation will be zero, however, there is a non zero region of changing concentration. Highly influenced by outliers: for example turbulent regions inside that are far away from the boundary are still included.

My boundary width: region where most change in concentration is happening. Turbulent regions deep inside are excluded, since they contribute little to the total density, at some points inside change of concentration is maximum.
