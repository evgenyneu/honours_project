## Comparing 192x192 with 264x264 simulations

![192 vs 264 2D](a2021/a09/a17_2D_192_vs_264/plots/2d_192_vs_264.jpg)

Look very similar, can see more detail in higher resolution.


## Run 3D 256x256x256 200s simulation

#### Balance before

```
pawseyAccountBalance -p ew6 -users
Compute Information
-------------------
          Project ID     Allocation          Usage     % used
          ----------     ----------          -----     ------
                 ew6         875000          11448        1.3
         --scampbell                         10606        1.2
         --evgenyneu                           842        0.1
```

#### Run

```
$ cd
$ . slurm_jobs/2021/09/17/jobs/264x264x264_200s.sh
View job's status: sacct -j 6911156
View job's status: sacct -j 6911157
```

Success.

Run time: 8h 45m

Data size: 37 GB


#### Balance after

```
pawseyAccountBalance -p ew6 -users
Compute Information
-------------------
          Project ID     Allocation          Usage     % used
          ----------     ----------          -----     ------
                 ew6         875000          52919        6.0
         --scampbell                         48711        5.6
         --evgenyneu                          4208        0.5
```

Used 0.4% of CPU credits.
