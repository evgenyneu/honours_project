## Fix color glitch
* Fixing glitch that showed different colors for gam1 between 2d and 3d


### Run 256x256x1 200s simulation

```
$ cd
$ . slurm_jobs/2021/09/16/jobs/256x256x1_200s.sh
View job's status: sacct -j 6908901
View job's status: sacct -j 6908902
```

Compiled successfully. Now running the simululation...

Number of cores 72 did nto equal the number of tiles 8*8=64.

### Calculate

Calculate number of tiles: [calc_tiles.py](a2021/a09/a16_color_glitch_256_2D_sim/code/calc_tiles.py)

```
------2D 264x264------
6x8 nodes: 2
4x12 nodes: 2
2x24 nodes: 2
------3D 264x264x264------
6x8x8 nodes: 16
4x8x12 nodes: 16
2x8x24 nodes: 16
4x4x24 nodes: 16
```

I'll use 264 resolution and the following number of tiles:

For 2D: 6x8, 2 nodes

For 3D: 6x8x8 16 nodes


## Run 264x264x1 simulation


```
$ cd
$ . slurm_jobs/2021/09/16/jobs/264x264x1_200s.sh
View job's status: sacct -j 6908987
View job's status: sacct -j 6908988
```

Success!

## Changing move_files script

Add compression to move script:

[move_files.sh](pawsey_home_backup/slurm_jobs/2021/09/16/run/move_files.sh)

```
sbatch move_files.sh /scratch/ew6/evgenyneu/PROMPI/setups/ccp_two_layers/2d/src "This is a simulation of my lovely horse" lovely_simulation
```

sacct -j 5180660

## Download data

Downloaded into:

data/a2021/a09/a16/2021_09_16_17_05_53_lovely_simulation.tar.gz
