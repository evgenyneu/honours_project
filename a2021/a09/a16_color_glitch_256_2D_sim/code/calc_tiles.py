from functools import cmp_to_key
import numpy as np

def compare(a, b):
    return abs(np.std(a)) - abs(np.std(b))

def get_tiles_2D(dim, cores_per_node, max_nodes):
    tiles = []

    for tile in range(1, round(dim / 2 + 1)):
        if dim % tile == 0:
            for tile2 in range(tile, round(dim / 2 + 1)):
                if dim % tile2 == 0:
                    cores = tile * tile2
                    nodes = cores / cores_per_node

                    if nodes == round(nodes) and nodes == max_nodes:
                        tiles.append([tile, tile2])


    tiles.sort(key=cmp_to_key(compare))

    for tile in tiles:
        nodes = round(tile[0] * tile[1] / cores_per_node)
        print(f"{tile[0]}x{tile[1]} nodes: {nodes}")


def get_tiles_3D(dim, cores_per_node, max_nodes):
    tiles = []

    for tile in range(1, round(dim / 2 + 1)):
        if dim % tile == 0:
            for tile2 in range(tile, round(dim / 2 + 1)):
                if dim % tile2 == 0:
                    for tile3 in range(tile2, round(dim / 2 + 1)):
                        if dim % tile3 == 0:
                            cores = tile * tile2 * tile3
                            nodes = cores / cores_per_node

                            if nodes == round(nodes) and nodes == max_nodes:
                                tiles.append([tile, tile2, tile3])


    tiles.sort(key=cmp_to_key(compare))

    for tile in tiles:
        nodes = round(tile[0] * tile[1] * tile[2] / cores_per_node)
        print(f"{tile[0]}x{tile[1]}x{tile[2]} nodes: {nodes}")


if __name__ == '__main__':
    dim = 264

    print(f'------2D {dim}x{dim}------')
    get_tiles_2D(dim=dim, cores_per_node=24, max_nodes=2)

    print(f'------3D {dim}x{dim}x{dim}------')
    get_tiles_3D(dim=dim, cores_per_node=24, max_nodes=16)

    print('We are done')
