# Honours project logbook

This is a logbook for my Honours project. The logs for individual days are in `Year/Month/Day` directories, for example:

[a2021/a09/a02_plot_density_over_epochs](a2021/a09/a02_plot_density_over_epochs).

Clone the repository:

```
git clone --recurse-submodules https://gitlab.com/evgenyneu/honours_project.git
```


## File structure

* **a2020/a2021/a2022**: Contains daily logbook files in YEAH/MONTH/DAY format. Directories starts with letter 'a' to make them available as Python modules.

* **[my_notes](my_notes)**: General notes I make on important topics.

* **[papers](papers)**: literature I attempted to read with some comments.

* **[report](report)**: LaTeX code for project report.


## Setup Python environment

[See Python setup instructions](python_setup.md).


## Run Python code example


Here is how I ran Python code in this project. Open file in VSCode and press F9, or:

```
PYTHONPATH=. python a2021/a08/a05_make_movies_on_pawsey/code/hello.py
```


### Run Jupyter noteboook

Run in VSCode with [Python VSCode extension](https://github.com/Microsoft/vscode-python).


Alternatively:

```
jupyter-lab a2021/a09/a02_plot_density_over_epochs/plot_boundary.ipynb
```

## Using project data_dir

Data used by the project is stored in a separate repository, so it can be kept in a separate location if it grows large. Link the data into the `data` directory in honours_project:

```
cd honours_project
ln -s <your path to honours_data directory> data
```

## Project links

[my_notes/links.md](my_notes/links.md).
