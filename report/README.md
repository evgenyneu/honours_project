# Honours project report

The document contains references to code used in my 2022 honours project report.

## Code for the plots

Here is the list of source files for the plots used in the project report:

* Figure 1: Stellar model grid [[plot](report/images/cell_stellar_model.pdf) | [source](report/images/cell_stellar_model.sketch)].

* Figure 2: Single grid cell [[plot](report/images/single_cell.pdf) | [source](report/images/single_cell.sketch)].

* Figure 3: Kinetic energy spectra, taken from Figure 6 in [Andrassy et al. (2021)](https://arxiv.org/abs/2111.01165).

* Figure 4: Initial setup of 2D and 3D stellar models containing two fluids [[plot](report/images/010_initial_state.pdf) | [source](a2022/a01/a31_intial_model_plot/initial_model.ipynb)].

* Figure 5: Change of the convective boundary over time [[plot](report/images/040_evolution.pdf) | [source](a2022/a04/a12_evolution_plot/evolution.ipynb)].

* Figure 6: Convective boundary for 2D and 3D simulations at 2300 seconds [[plot](report/images/050_single_frame.pdf) | [source](a2022/a04/a19_one_frame/one_frame.ipynb)].

* Figures 7, 8: Horizontally averaged kinetic energy density [[plot](report/images/090_kinetic_energy_averaged.pdf) [plot](report/images/091_kinetic_energy_averaged_zoomed.pdf) | [source](a2022/a05/a06_kinetic_energy_range/kinetic_energy_range.ipynb)].

* Figures 9, 10: Horizontally averaged speeds [[plot](report/images/100_velocity_horizontal_radial.pdf) [plot](report/images/101_velocity_horizontal_radial_zoomed.pdf) | [source](a2022/a05/a16_velocity_horizontal_radial/velocity_horizontal_radial.ipynb)].

* Figures 11: Comparison of radial and horizontal speeds [[plot](report/images/104_velocity_compare_horizontal_radial_zoomed.pdf) | [source](a2022/a05/a18_compare_velocity_horizontal_radial/compare_velocity_horizontal_radia.ipynb)].

* Figures 12: Composition profile [[plot](report/images/110_composition_profile.pdf) | [source](a2022/a05/a23_composition_profile/composition_profile.ipynb)].

* Figures 13: Temperature profile [[plot](report/images/120_temperature_profile.pdf) | [source](a2022/a05/a24_temperature_profile/temperature_profile.ipynb)].

## Code for the tables

* Table 2: Turnover times [[source](a2022/a05/a09_velocity/turnover_time.ipynb)].

Data for the other tables was calculates using the code from the corresponding plots (see above).

## PROMPI model setup

The PROMPI model setup code I used for 2D and 3D simulations is located in [a2022/a01/a04_prompi_settings_example/prompi](a2022/a01/a04_prompi_settings_example/prompi) directory. I can not share the full PROMPI code because it is not open source. To get access to PROMPI code please contact the authors of [Meakin, C. A. and Arnett, D. (2007). Turbulent Convection in Stellar Interiors. I. Hydrodynamic Simulation. ApJ, 667(1):448–475](https://ui.adsabs.harvard.edu/abs/2007ApJ...667..448M/abstract).

## Running PROMPI on Pawsey

On [Magnus computer](https://pawsey.org.au/systems/magnus/) clone this repository into your home directory:

```
cd
git clone https://gitlab.com/evgenyneu/pawsey_home_backup.git .
```


The [slurm_jobs](https://gitlab.com/evgenyneu/pawsey_home_backup/-/tree/master/slurm_jobs) directory contains the scripts I used to run all my simulations, for example [slurm_jobs/2021/12/03/lum_1.2345_percent.sh](https://gitlab.com/evgenyneu/pawsey_home_backup/-/blob/master/slurm_jobs/2021/12/03/lum_1.2345_percent.sh).

The PROMPI code was located in `/scratch/ew6/evgenyneu/PROMPI` directory.

### Example of running a simulation

```
cd
. slurm_jobs/2021/12/22/0.25_sec_interval_1.2345_percent_lum_continue_from_300.sh
```



## Latex setup

Here is how I setup LaTeX on MacBook to render honours project report.

* Install [LaTeX workshop](https://marketplace.visualstudio.com/items?itemName=James-Yu.latex-workshop) VS Code extension.

* Install Pygments, needed for the `minted` LaTeX package that prints out programming code:

```
brew install pygments
```

and add [.latexmkrc](report/.latexmkrc) file.


