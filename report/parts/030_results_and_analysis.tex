\section{Results}

\subsection{Grid of models}
\label{sec_list_of_simulations}

The list of all completed simulations is shown in \Cref{table_simulations}, consisting of ten 2D and three 3D runs. Most simulations had resolution of 264 zones along each spatial direction, except for the two high-resolution 2D runs with 1032 zones. In order to decrease computation time the PROMPI program runs calculations in parallel on multiple CPU cores using Message Passing Interface (MPI) protocol. For 3D simulation with $264^3$ grid I used 864 CPU cores (36 nodes\footnote{A \emph{compute node} is a single hardware module, similar to a desktop computer, that consists of a motherboard, CPU and memory (RAM). Magnus computer includes \num{1488} nodes that communitate via network and allow to run massively parallel computations.}), while $264^2$ 2D simulation was run on 72 cores (3 nodes).

\begin{table}[!ht]
  \centering
  \caption{List of all simulations. \emph{Resolution:} number of grid cells along each spatial direction. \emph{Luminosity:} modification of the power input relative to \SI{4.534e+38}{\W}. \emph{Luminosity time:} the start time when the power input modification was applied. \emph{Duration:} length of the simulated stellar time period. \emph{Interval:} time interval between model data outputs.}
  \def\arraystretch{1.2}
  \begin{tabularx}{\textwidth}{ c c c c Y Y c}
      \hline

      \textbf{No.} & \textbf{Type} & \textbf{Resolution} & \textbf{Luminosity} & \textbf{Luminosity time [s]} & \textbf{Duration [s]} & \textbf{Interval [s]} \\

      \hline

      1   & 2D & 264  & 0.823\%   & 0     & 3000  & 5 \\
      2   & 2D & 264  & 0.823\%   & 0     & 2000  & 0.25 \\
      3   & 2D & 264  & 1\%       & 80    & 3000  & 5 \\
      4   & 2D & 264  & 10\%      & 80    & 3000  & 5 \\
      5   & 2D & 264  & 50\%      & 80    & 3000  & 5 \\
      6   & 2D & 264  & 67\%      & 80    & 3000  & 5 \\
      7   & 2D & 264  & 67\%      & 0     & 3000  & 5 \\
      8   & 2D & 264  & 100\%     & 0     & 3000  & 5 \\
      9   & 2D & 1032 & 0.823\%   & 0     & 2500  & 5 \\
      10  & 2D & 1032 & 0.823\%   & 0     & 2000  & 0.25 \\
      11  & 3D & 264  & 1.2345\%  & 0     & 3000  & 5 \\
      12  & 3D & 264  & 1.2345\%  & 0     & 500   & 0.25 \\
      13  & 3D & 264  & 100\%     & 0     & 3000  & 5 \\

      \hline
  \end{tabularx}
  \label{table_simulations}
\end{table}

The choice of the resolution was influenced by the number of CPU cores in each node (24 CPU cores per node on Magnus). For example, a resolution of $264^3$ in 3D simulations was chosen because the grid box could be divided into $12 \times 12 \times 6 = 864$ smaller sub-boxes. Each sub-box represents a smaller area of space in which simulation is handled by a single CPU core. The total number of CPU cores (864 in this case) needs to be evenly divided by the number of cores per node (24 on Magnus) in order to make all nodes fully utilized. In addition, these requirements needed to be met for the $264^2$ 2D grid as well, in order to compare 2D and 3D models of the same 264-zone resolution.

The duration of simulated stellar time was \SI{2000}{\s} in two runs, which is the same as in \cite{andrassy_2021_code_comparison}. The duration was increased to \SI{3000}{\s} in nine of the runs in order to see if the convective boundary eventually stopped advancing. It took three days to compute each of the $\SI{3000}{\s}$ 3D simulations and one hour for the 2D runs.

Three of the simulations used the highest power input of \SI{4.534e+38}{\W}, which corresponds to $100\%$ in ``Luminosity'' column of \Cref{table_simulations}. The lowest power input of $0.823\%$ (\Cref{sec_model_setup}) was used in runs 1, 2, 9 and 10. In the remaining simulations the power was chosen arbitrarily between $0.082\%$ and $100\%$ in order to equalize the location of the convective boundary between 2D and 3D simulations.

``Luminosity Time'' column contains the time after which the power input was decrased. In runs 3, 4, 5 and 6 the full $100\%$ power input was suppllied initially in order to start the convection and then was decreased after a turnover time of $\SI{80}{\s}$ (\Cref{sec_turnover_time}). After comparing runs 6 and 7, I found that decreasing the luminosity from the start instead of after $\SI{80}{\s}$ resulted in the same speed of advance of the convective boundary. Hence I concluded that this delay of $\SI{80}{\s}$ was not needed in the remaining run, and instead the power level was set from the start.

Lastly, the snapshots of the model that included parameters such as density, temperature and pressure were periodically saved to disk for later analysis. The time intervals between the snapshots are shown in the ``Interval'' column of \Cref{table_simulations}. In all simulations used for data analysis, the time interval between snapshots was five seconds, the same as in \cite{andrassy_2021_code_comparison}. For the remaning three simulations I chose a shorter time interval of \SI{0.25}{\s} in order to make movies of the convective flow with smoother transitions between the frames.

The size of a single snapshot was about \SI{1}{GB} for a 3D run, with 600 total snapshots and combined size of \SI{600}{GB} for one \SI{3000}{\s} simulation. The total size of the snapshot data produced by all simulations listed in \Cref{table_simulations} was \SI{1.7}{TB}.

\subsection{Methods of data analysis}

The binary data files produced by PROMPI were read with a Python code that is based on ideas shared by Miroslav Mocak via private communication. Analysis was then performed on the data collected from 2100 to 2500 seconds, since it was the period at the end of the code runs, which allowed to better see the differences in the locations of the convective boundaries between different simulations. The duration of the data analysis period was chosen to be 400 seconds because it is two times larger than the highest turnover time (\Cref{sec_turnover_time}).

The point estimates for the distribution of values, such as speeds shown in \Cref{table_speeds}, were calculated by taking the middle value of HPDI (highest posterior density interval) with $0.6827$ probability and the uncertainties were taken to be the half of the HPDI width. The uncertainties of the ratios of the point estimates, such as speed ratios, were calculated with the \texttt{\small{uncertainties}} Python package \citep{uncertainties_python_package}.

The spatial gradients, such as mass fraction gradient shown on \Cref{fig_gradient_evolution}, was calculated using the \texttt{\small{gradient}} function from Numpy Python library \citep{numpy}. The location of the convective boundary was estimated by first calculating the horizontal averages of the absolute value of the mass fraction gradient of the first element $|\nabla X_1|$, which gave the distribution of the gradients over the radial positions. The mode of this distribution was then used as an estimate of the boundary location.


\subsection{Location of convective boundary over time}

The goal of this work was to compare physical conditions near convective boundaries between 2D and 3D simulations, which first requires to locate the boundaries. The strongest indicator of the boundary location that I found was the gradient of the mass fraction $|\nabla X_1|$. Its horizontal average over time is shown in \Cref{fig_gradient_evolution}, with higher color-coded values corresponding to the radial positions where the concentration of the first element changes most rapidly. Locations where this change is most pronounced I define to be the convective boundary, and it is visualized with the rising bright curve. The curve separates the top radiative region occupied by the second fluid from the convective zone at the bottom.

The convective boundary is advancing upwards, and the rate of the advance increases with the higher power input, indicated as percentage in the subplot labels. When the same $100\%$ power input (\Cref{sec_model_setup}) is used in 2D and 3D simulations, shown in subplots (8) and (13), the boundary advances faster in 2D, which agrees with \cite{meakin_arnett_2007}. Lowering the power input of 2D model to $67\%$ (subplot 7) makes the boundary location similar to the $100\%$ 3D simulation (subplot 13). In other words, the ratio of power inputs between 2D and 3D simulations that equalized the boundary locations  was 2/3. Interestingly this is the same as the ratio of the number of dimensions - 2D/3D. A similar result was observed at the lower power levels shown in subplots (1) and (11), with power inputs of $0.82\%$ and $1.23\%$ in 2D and 3D respectively. It would be worth verifying this 2/3 ratio observation using different stellar model setups in order to see whether it was a coincidence or if it is a robust result that does not depend on the model.


\subsection{Morphology of the flow and boundary}

The values of the mass fraction gradient at \SI{2300}{s} is shown in \Cref{fig_single_frame}. The shapes of the features in the convective regions are different: in 3D (subplots 11 and 13) we see multiple small elongated randomly located filaments while 2D structure is dominated by two large rolls that become more pronounced with larger power input. Similar observations were also reported by  \cite{muthsam_1995}, \cite{van_der_poel_stevens_lohse_2013} and \cite{pratt_2020}. The boundary becomes more disturbed with the increasing power input. For the same $100\%$ power input the boundary in 2D is thicker than in 3D (subplots 8 and 13), which agrees with \cite{meakin_arnett_2007}.


\pagebreak

\newgeometry{top=0.5cm, left=0.5cm, right=0.5cm, bottom=0cm}

\begin{figure}[H]
  \centering
  \includegraphics[width=0.92\textwidth]{images/040_evolution.pdf}
  \caption{Time evolution of the convective boundary, visualized by plotting the horizontal average of the magnitude of mass fraction gradient $|\nabla X_1|$. The first number in the subplot label corresponds to the simulation in \Cref{table_simulations}. The percentage is the power input, where $100\%$ is \SI{4.534e+38}{\W}. It can be seen that higher power input corresponds to the higher thickness and faster advance of the boundary. The speed of advance of the boundary is similar between 2D and 3D simulation when the 2D luminosity is 2/3 of that in the 3D model, illustrated by subplot pairs (1), (11) and (7), (13).}
  \label{fig_gradient_evolution}
\end{figure}

\begin{figure}[H]
  \centering
  \includegraphics[width=0.67\textwidth]{images/050_single_frame.pdf}
  \caption{Convective boundary at \SI{2300}{seconds}, visualized by plotting the magnitude of mass fraction gradient $|\nabla X_1|$. The subplot labels are the same as in \Cref{fig_gradient_evolution}. Subplot pairs (1), (11) and (7), (13) show similar boundary locations between 2D and 3D simulations.}
  \label{fig_single_frame}
\end{figure}

\restoregeometry

\subsection{Turnover times}
\label{sec_turnover_time}

The turnover times are shown in \Cref{table_turnover_times}, they were calculated by taking two times the size of the convective region and dividing it by the speed inside this region. All turnover times agree within uncertainties.

\begin{table}[!ht]
  \centering
  \caption{Turnover times for time range from 2100 to 2500 seconds. The ``No.'' column contains the simulation numbers from \Cref{table_simulations}.}
  \def\arraystretch{1.2}
  \begin{tabular}{ |c|c|c| }
    \hline

    \textbf{No.} &
    \textbf{Simulation} &
    \textbf{Turnover time [s]}

    \\

    \hline

    1 & 2D 0.82\% & \num{77(32)} \\

    \hline

    9 & 2D 0.82\% hi-res & \num{110(60)} \\

    \hline

    11 & 3D 1.23\% & \num{190(80)} \\

    \hline
  \end{tabular}
  \label{table_turnover_times}
\end{table}


\subsection{Kinetic energy}

\Cref{fig_kinetic_energy} compares kinetic energy between 2D, high-resolution 2D and 3D simulations. Corresponding numerical summary is shown in \Cref{table_kinetic_energy}. Kinetic energies are at their global maxima at the bottom of the convection zone at \SI{4e6}{\m}, with the 2D value \num{8(3)} times higher than 3D. The energies decline as they approach the boundaries. In 3D the decline is almost monotonic, while in 2D there is a local minimum at around \SI{5e6}{\m}, which conicides with centres of the two rolls visible in \Cref{fig_single_frame}.

In the convective zone 2D has \num{6(3)} times higher energy than 3D, while the high resolution 2D is \num{3(2)} times higher than 3D. These results are similar to \cite{meakin_arnett_2007}, who reported kinetic energies three time times larger in 2D compared to 3D (see their Fig. 5), however the resolution of their 2D model was different from 3D. \cite{meakin_arnett_2007} also reported smaller kinetic energy for the higher resolution 2D model when compared to the lower resolution 2D. It can be seen from \Cref{fig_kinetic_energy} that the kinetic energy curves differ qualitatively between hi-res 2D and 2D simulations, which means that the results depend on the resolution.

In the radiative region kinetic energies of 2D and hi-res 2D simulations agree within uncertainties. Kinetic energy in the radiative region is \num{140(100)} times lower than in the convective zone in both 2D and 2D hi-res models. In 3D however, this ratio is \num{340(230)}. Consequently, the kinetic energy of the radiative region in 2D is \num{14(12)} times higher than in 3D.

\begin{figure}[!ht]
  \centering
  \includegraphics[width=0.7\textwidth]{images/090_kinetic_energy_averaged.pdf}
  \caption{Horizontally averaged kinetic energy density for 2D (red solid line), 2D high resolution (pink dotted line) and 3D (blue dashed line) simulations, which are labeled 1, 9 and 11 \Cref{table_simulations} respectively. The thinner curved lines are the data from 80 individual snapshots separated by five second intervals with simulation times ranging from 2100 to 2500 seconds. The thicker lines are the time averages. The vertical lines indicate the time averaged locations of the convective boundaries, with the shaded vertical bars indicating the range of boundary locations over the time period. The percentage in the legend is the power input, where $100\%$ is \SI{4.534e+38}{\W}.}
  \label{fig_kinetic_energy}
\end{figure}

\begin{table}[!ht]
  \centering
  \caption{Numerical summary of kinetic energy that supplements Figures \ref{fig_kinetic_energy} and \ref{fig_kinetic_energy_zoomed}. Bottom: at \SI{4e6}{\m} radial position. Convection:in the convective region. Boundary: at the convective boundary. Radiation: in the radiative region. The data are for the time range from 2100 to 2500 seconds. The ``No.'' column contains the simulation numbers from \Cref{table_simulations}. }
  \def\arraystretch{1.2}
  \resizebox{\textwidth}{!}{%
    \begin{tabular}{ |c|l|c|c|c|c|  }
      \hline

      \multirow{2}{*}{\textbf{No.}} &
      \multirow{2}{*}{\textbf{Simulation}} &
      \multicolumn{4}{c|}{\textbf{Kinetic energy density [$\text{J} / \text{m}^3$]}} \\ \cline{3-6}

      & & \textbf{Bottom ($\times 10^{19}$)} & \textbf{Convection ($\times 10^{18}$)} & \textbf{Boundary ($\times 10^{17}$)} & \textbf{Radiation ($\times 10^{16}$)}

        \\

      \hline

      1 & 2D 0.82\% & \num{4.11(83)} & \num{15.9(64)} & \num{2.5(12)} & \num{11.5(67)} \\

      \hline

      9 & 2D 0.82\% hi-res & \num{2.30(40)} & \num{8.7(52)} & \num{2.12(59)} & \num{6.1(34)} \\

      \hline

      11 & 3D 1.23\% & \num{0.50(13)} & \num{2.8(10)} & \num{0.80(48)} & \num{0.82(47)} \\

      \hline
    \end{tabular}
  }
  \label{table_kinetic_energy}
\end{table}

The zoomed-in view of the region near the boundary is shown in \Cref{fig_kinetic_energy_zoomed}. Inside the convection region (to the left from the vertical lines) kinetic energies decline as radial position increases. The rate of the decline is higher in 2D and 2D hi-res initially and becomes more similar to 3D near the boundaries. At the boundary the kinetic energy in 2D and hi-res 2D simulations is \num{3.1(23)} and \num{2.6(17)} times higher than in 3D respectively. The energies at the boundary in 2D is \num{1.2(6)} higher than in 2D hi-res. At the boundaries 2D and 3D kinetic energies nearly agree within uncertainties.

\begin{figure}[!ht]
  \centering
  \includegraphics[width=0.7\textwidth]{images/091_kinetic_energy_averaged_zoomed.pdf}
  \caption{Horizontally averaged kinetic energy density. Same as \Cref{fig_kinetic_energy} but zoomed into the region near the convective boundaries marked with the vertical lines. }
  \label{fig_kinetic_energy_zoomed}
\end{figure}


\FloatBarrier

\subsection{Comparing fluid speeds between simulations}

Comparison of fluid speeds between the simulations is presented in \Cref{fig_velocity_compared} with the corresponding numerical summary shown in \Cref{table_speeds}. Inside the convection region radial speeds are higher near the center and decline at the boundaries. As we have seen for the kinetic energy, horizontal speeds are largest at the bottom (\SI{4e6}{\m}). Horizontal speeds in the convection zone vary between 2D and 3D simulations: in 3D there is little change except for the increase near both boundaries, while in 2D we see larger variation and a local minimum at around \SI{5e6}{\m}. In the convection zone radial and horizontal 2D speeds speeds are \num{2.7(11)} and \num{2.4(11)} times larger than 3D speeds respectively. Radial and horizontal speeds in hi-res 2D are \num{1.9(8)} and \num{1.7(6)} larger than 3D speeds respectively. Thus in the convective zone radial speeds in hi-res 2D and 3D agree within uncertainties and their horizontal speeds almost agree.

In the radiative region radial speeds are \num{4.3(18)} times higher in 2D than in 3D, and hi-res 2D speeds are \num{2.8(11)} times higher than 3D. Horizontal speeds are \num{3.4(17)} times higher in 2D than in 3D, and hi-res 2D speeds are \num{2.4(11)} times higher than 3D. Qualitatively, horizontal speeds in the radiative region differ between 2D and 3D: in 2D see see larger variation with prediodic spikes and troughs.

\begin{figure}[!ht]
  \centering
  \includegraphics[width=0.7\textwidth]{images/100_velocity_horizontal_radial.pdf}
  \caption{Horizontally averaged speeds for 2D (red solid line), 2D high resolution (pink dotted line) and 3D (blue dashed line) simulations, which are labeled 1, 9 and 11 in \Cref{table_simulations} respectively. The top and bottom subplots show radial and horizontal speeds respectively. The thinner curved lines are the data from 80 individual snapshots separated by five second intervals with simulation times ranging from 2100 to 2500 seconds. The thicker lines are the time averages. The vertical lines indicate the time averaged locations of the convective boundaries, with the shaded vertical bars indicating the range of boundary locations over the time period. The percentage in the legend is the power input, where $100\%$ is \SI{4.534e+38}{\W}. }
  \label{fig_velocity_compared}
\end{figure}

\begin{table}[!ht]
  \centering
  \caption{Numerical summary of speeds that supplements \Crefrange{fig_velocity_compared}{fig_velocity_vs_horizontal_radial}.  Convection: in the convective region. Boundary: at the convective boundary. Radiation: in the radiative region. The data are for the time range from 2100 to 2500 seconds. The ``No.'' column contains the simulation numbers from \Cref{table_simulations}. }
  \def\arraystretch{1.2}
  \resizebox{\textwidth}{!}{%
    \begin{tabular}{ |c|l|c|c|c|c|  }
      \hline

      \multirow{2}{*}{\textbf{No.}} &
      \multirow{2}{*}{\textbf{Simulation}} &
      \multirow{2}{*}{\textbf{Direction}} &
      \multicolumn{3}{c|}{\textbf{Speed [$\times 10^{4}$ m/s]}} \\ \cline{4-6}

      & & & \textbf{Convection} & \textbf{Boundary} & \textbf{Radiation}

      \\

      \hline

      1 & 2D 0.82\% & Radial & \num{8.5(26)} & \num{.500(87)} & \num{0.69(21)} \\

      \hline

      1 & 2D 0.82\% & Horizontal & \num{7.2(28)} & \num{1.78(48)} & \num{2.15(86)} \\

      \hline

      9 & 2D 0.82\% hi-res & Radial & \num{6.0(21)} & \num{.458(52)} & \num{0.45(11)} \\

      \hline

      9 & 2D 0.82\% hi-res & Horizontal & \num{5.2(16)} & \num{1.70(27)} & \num{1.53(50)} \\

      \hline

      11 & 3D 1.23\% & Radial & \num{3.20(88)} & \num{0.146(24)} & \num{0.160(46)} \\

      \hline

      11 & 3D 1.23\% & Horizontal & \num{2.99(57)} & \num{1.06(32)} & \num{0.63(19)} \\

      \hline
    \end{tabular}
  }
  \label{table_speeds}
\end{table}


The speeds near the convective boundaries are shown in \Cref{fig_velocity_compared_zoomed}. At the convective boundaries, radial 2D speeds are \num{3.4(8)} times larger than 3D, while hi-res 2D speeds are \num{3.1(6)} times larger than 3D. At the boundaries, horizontal speeds of all simulations agree within uncertainties, except for the 2D hi-res and 3D horizontal speeds that nearly agree with each other.


\begin{figure}[!ht]
  \centering
  \includegraphics[width=0.7\textwidth]{images/101_velocity_horizontal_radial_zoomed.pdf}
  \caption{Horizontally averaged speeds. Same as \Cref{fig_velocity_compared} but zoomed into the region near the convective boundaries marked with the vertical lines.}
  \label{fig_velocity_compared_zoomed}
\end{figure}


\FloatBarrier

\subsection{Horizontal vs radial speeds}

Next, we plot the same speed data in \Cref{fig_velocity_vs_horizontal_radial} but show horizontal and radial components on the same subplots for easier comparison. At the convective boundaries horizontal speeds are \num{3.6(11)} and \num{3.7(7)} times higher than radial for 2D and 2D hi-res simulations respectively, while for 3D the horizontal speeds are \num{7.3(25)} times higher than radial.

\begin{figure}[!ht]
  \centering
  \includegraphics[width=0.68\textwidth]{images/104_velocity_compare_horizontal_radial_zoomed.pdf}
  \caption{Comparison of radial (solid line) and horizontal (dash-dotted line) speeds near convective boundaries for 2D (top), 2D high resolution (middle) and 3D (bottom) simulations, which are labeled 1, 9 and 11 in \Cref{table_simulations} respectively. The thinner curved lines are the horizontal averages from 80 individual snapshots separated by five second intervals with simulation times ranging from 2100 to 2500 seconds. The thicker lines are the time averages. The vertical lines indicate the time averaged locations of the convective boundaries, with the shaded vertical bars indicating the range of boundary locations over the time period. The percentage next to 2D/3D text is the power input, where $100\%$ is \SI{4.534e+38}{\W}.}
  \label{fig_velocity_vs_horizontal_radial}
\end{figure}



\FloatBarrier

\subsection{Composition profile}

Comparison of composition profiles between the simulations is shown in \Cref{fig_composition_profile} with the data summarized in \Cref{table_composition}. The profiles are ``s''-shaped curves. From the left plot we can see that the radial position of the boundary in 2D is larger than in 3D. However, the boundary's position in high resolution 2D is smaller than in 3D. We can compare the shapes of the curves using the right plot: 2D hi-res is more similar to 3D than 2D, especially in the right part of the curve where 2D hi-res and 3D appear identical. The 2D curve, on the other hand, is less steep, particularly on the left side.

The data in \Cref{table_composition} tells us that the mass fraction of the first fluid is exactly zero in the radiative region. This means that no mixing of the two fluids is taking place despite the fact the matter is moving in the radial direction with speeds from \num{1.60(46)e3} to \num{6.9(21)e3} m/s in the radiative zone (\Cref{table_speeds}). This implies if we consider the radial component of velocity in the radiative region, the fluid altenates its movement back and forward periodically instead of circulating continuously.

\begin{figure}[!ht]
  \centering
  \includegraphics[width=1\textwidth]{images/110_composition_profile.pdf}
  \caption{Composition profile for 2D (red solid line), 2D high resolution (pink dotted line) and 3D (blue dashed line) simulations, which are labeled 1, 9 and 11 in \Cref{table_simulations} respectively. The original data are shown on the left, while on the right the 2D and 2D hi-res profiles are shifted for easier comparison by \SI{-.91e06}{m} and \SI{.62e06}{m} respectively. The composition is calculated by taking horizontal averages of the mass fraction of the first fluid X$_1$. The thinner curved lines are the data from 80 individual snapshots separated by five second intervals with simulation times ranging from 2100 to 2500 seconds. The thicker lines are the time averages. The vertical lines at the top of the left plot indicate the time averaged locations of the convective boundaries, with the shaded vertical bars indicating the range of boundary locations over the time period. The percentage in the legend is the power input, where $100\%$ is \SI{4.534e+38}{\W}.}
  \label{fig_composition_profile}
\end{figure}

\begin{table}[!ht]
  \centering
  \caption{Numerical summary for the mass fraction, supplementing \Cref{fig_composition_profile}. Bottom: at \SI{4e6}{\m} radial position. Convection: in the convective region. Radiation: in the radiative region. The data are for the time range from 2100 to 2500 seconds. The ``No.'' column contains the simulation numbers from \Cref{table_simulations}. }
  \def\arraystretch{1.2}
  \begin{tabular}{ |c|l|c|c|c|c|  }
    \hline

    \multirow{2}{*}{\textbf{No.}} &
    \multirow{2}{*}{\textbf{Simulation}} &
    \multicolumn{3}{c|}{\textbf{Mass fraction X$_1$}} \\ \cline{3-5}

    & & \textbf{Bottom} & \textbf{Convection} & \textbf{Radiation}

      \\

    \hline

    1 & 2D 0.82\% & \num{0.967(3)} & \num{0.965(2)} & \text{exactly zero} \\

    \hline

    9 & 2D 0.82\% hi-res & \num{0.982(4)} & \num{0.981(3)} & \text{exactly zero} \\

    \hline

    11 & 3D 1.23\% & \num{0.975(0)} & \num{0.975(1)} & \text{exactly zero} \\

    \hline
  \end{tabular}
  \label{table_composition}
\end{table}

\FloatBarrier

\subsection{Temperature profile}

Comparison of temperature and temperature gradients between simulations is shown in \Cref{fig_composition_profile}, and numerical data summares are given in \Cref{table_temperature_difference}. The temperature profiles have similar shapes, and the 3D temperatures are between \SI{3e6}{K} and \SI{4e6}{K} larger than 2D and 2D hi-res in the convective and radiative regions. The temperature gradients of all three simulations agree within uncertainties.

Near the convective boundaries, the shapes of the temperature gradient curves differ between the simulations. In both 3D and 2D hi-res there is a local minimum before the boundary that in missing in the 2D. We can see that the temperature gradients change  considerably near the boundary. However, the 2D and 3D curves are not smooth due to low number of grid points at this zoom level, thus the temperature gradient variation may not be captured accurately due to low resolution of the 2D and 3D simulations.

\begin{figure}[!ht]
  \centering
  \includegraphics[width=0.9\textwidth]{images/120_temperature_profile.pdf}
  \caption{Temperature (top) and its gradient (bottom) in 2D (red solid line), 2D high resolution (pink dotted line) and 3D (blue dashed line) simulations, which are labeled 1, 9 and 11 in \Cref{table_simulations} respectively. The temperature is calculated by taking horizontal averages. The thinner curved lines are the data from 80 individual snapshots separated by five second intervals with simulation times ranging from 2100 to 2500 seconds. The thicker lines are the time averages. The percentage in the legend is the power input, where $100\%$ is \SI{4.534e+38}{\W}.}
  \label{fig_temperature_profile}
\end{figure}

\begin{table}[!ht]
  \centering
  \caption{Difference of temperatures and temperature gradients in convective and radiative regions between 2D, 2D high-resolution and 3D simulations, which are labeled 1, 9 and 11 in \Cref{table_simulations} respectively. Supplements \Cref{fig_temperature_profile}.}
  \def\arraystretch{1.2}
  \begin{tabular}{ |c|c|c|c|c| }
    \hline

    \multirow{2}{*}{\textbf{Simulations}} &
    \multicolumn{2}{c|}{\textbf{Temperature difference [$\times 10^{6}$ K]}} &
    \multicolumn{2}{c|}{\textbf{Temp. grad. difference [K/m]}} \\ \cline{2-5}
    & \textbf{Convection} &  \textbf{Radiation} & \textbf{Convection} &  \textbf{Radiation}

    \\

    \hline

    3D - 2D & \num{3.90(76)} & \num{3.1(17)} & \num{-0.6(13)} &  \num{-0.4(72)} \\

    \hline

    3D - 2D hi-res & \num{3.9(17)} & \num{3.7(19)} & \num{0.4(18)} & \num{3(13)} \\

    \hline
  \end{tabular}
  \label{table_temperature_difference}
\end{table}


% \FloatBarrier

% \subsection{Density profile}

% Comparison of density and density gradient profiles between 2D and 3D simulations is shown in \Cref{fig_composition_profile}.

% \begin{figure}[!ht]
%   \centering
%   \includegraphics[width=0.9\textwidth]{images/130_density_profile.pdf}
%   \caption{Density (bottom) and its gradient (top) profiles for 2D and 3D simulations at the region near convective boundary. The density is calculated by taking horizontal averages. The thinner curved lines are the data from 80 individual snapshots separated by five second intervals with simulation times ranging from 2300 to 2700 seconds. The thicker lines are the time averages. The percentage in the legend is the power input, where $100\%$ is \SI{4.534e+38}{\W}. The pressure gradients both outside and at the boundaries are the same in 2D and 3D.}
%   \label{fig_density_profile}
% \end{figure}

% \FloatBarrier


% \subsection{Resolution test}

% Comparison of high and low resolution 2D simulations are shown in \Cref{fig_evolution_resolution_test} and \Cref{fig_single_frame_resolution_test}.


% \begin{figure}[!ht]
%   \centering
%   \includegraphics[width=1\textwidth]{images/060_evolution_resolution_test.pdf}
%   \caption{Change of the convective boundary over time, comparing two 2D simulations of low and high resolution with $264^2$ and $1032^2$ grids respectively. Both simulation use $0.82\%$ power input, where $100\%$ corresponds to \SI{4.534e+38}{\W}. The first number in the subplot label corresponds to the simulation in \Cref{table_simulations}. The high resolution simulation has a slower advance of convective boundary.}
%   \label{fig_evolution_resolution_test}
% \end{figure}

% \begin{figure}[!ht]
%   \centering
%   \includegraphics[width=1\textwidth]{images/070_single_frame_resolution_test.pdf}
%   \caption{Convective boundary at 2500 second, comparing two 2D simulations of low and high resolution with $264^2$ and $1032^2$ grids respectively. Both simulation use $0.82\%$ power input, where $100\%$ corresponds to \SI{4.534e+38}{\W}. The first number in the subplot label corresponds to the simulation in \Cref{table_simulations}. The boundary is lower and thinner in the higher resolution simulation. In addition, higher resolution simulation has larger number of eddies.}
%   \label{fig_single_frame_resolution_test}
% \end{figure}

\FloatBarrier


