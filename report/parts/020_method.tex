\section{Method}

\subsection{PROMPI}

To simulate stellar convection I used the PROMPI hydrodynamics code that solves \Crefrange{eq_continuity}{eq_of_state} and is written in Fortran \citep{meakin_arnett_2007}. PROMPI is based on PROMETHEUS code \citep{1991ApJ...367..619F} that solves fluid dynamics equations using the numerical techniques described in \Cref{solving_2d_code_in_codes}, including the Piecewise Parabolic Method (PPM) \citep{1984JCoPh..54..174C} for flux reconstruction. Calculations are done in parallel on multiple CPU cores by breaking computation grid into smaller sub-grids (called tiles) and using Message Passing Interface (MPI). PROMPI has been used in various 3D studies of stellar interiors \citep{2019MNRAS.484.4645C, 2018MNRAS.481.2918M, 2018arXiv181004659A, 2017IAUS..329..237C} and compared with other 3D codes in \cite{andrassy_2021_code_comparison}. The code can simulate artificial friction but this feature was switched off. Effects of magnetic fields are not supported by this software, and rotation was ignored for simplicity. PROMPI was suitable for the current project because of its ability to track concentrations of multiple fluids using \Cref{eq_track_fluids}. My model included two fluids (\Cref{sec_model_setup}) and the ability to track them was useful for estimating the location of the convective boundary. I ran the code\footnote{The code for the PROMPI model, instructions on how to run it and code for all the plots and tables in this report can be found at \url{https://gitlab.com/evgenyneu/honours_project/-/tree/master/report}} on the Magnus machine located at Pawsey supercomputing center in Perth, WA \citep{magnus_supercomputer}.

\subsection{Model setup}
\label{sec_model_setup}

In this work I compute 2D and 3D stellar models with the setup from \cite{andrassy_2021_code_comparison}. The models describe a box inside a 25 solar mass star, with a side length of $\SI{8e6}{\m}$. The box contains two fluids with mean molecular weights $\mu_1 = 1.848$ and $\mu_2 = 1.802$ that are modelled as ideal monoatomic gasses with specific heat ratio of $\gamma = 5/3$. Initially, the first fluid occupies the lower half of the box with a thin boundary in the middle where concentration gradually changes from first fluid to the second, as shown on the left in \Cref{fig_initial_setup}.

The model parameters, such as density and pressure shown on the right in \Cref{fig_initial_setup}, are based on a 1D model computed by \cite{2017MNRAS.465.2991J} using the MESA stellar evolution code \citep{2013ApJS..208....4P}. The lower half of the model interior corresponds to the convective oxygen burning shell in the original 1D model, while the upper half is a stable radiative region containing oxygen, neon, magnesium and traces of silicon.

\begin{figure}[!ht]
  \centering
  \includegraphics[width=1.0\textwidth]{images/010_initial_state.pdf}
  \caption{Initial setup of 2D and 3D stellar models containing two fluids based on the output of a 1D stellar model of a giant 25 solar mass star. \textbf{Left:} mass fraction of the first fluid occupying the bottom half of the box and marked with dark red color. The bottom half region corresponds to the convective oxygen-burning shell in the original 1D model. \textbf{Right:} density and pressure at different radial positions. The rightmost plot shows the location of unchanging power input that mimiced nuclear fusion, with the total luminosity of \SI{4.534e+38}{\W}.}
  \label{fig_initial_setup}
\end{figure}

For simplicity, nuclear fusion was not modelled. Instead, an unchanging amount of energy was supplied into a thin region near the bottom of the box, as shown in the rightmost plot in \Cref{fig_initial_setup}. The amount of power input varied between different models. The highest power was \SI{4.534e+38}{\W} (marked as 100\% luminosity in \Cref{table_simulations}), the same as in \cite{andrassy_2021_code_comparison}, and it is 22.5 times larger than in the original 1D stellar model, which also had 5.4 times more mass in the convection zone. I divided $100\%$  by the product of these two numbers, resulting in the $0.823\%$ power input that the lowest ones used in my models. The goal was to slow down the advance of the convective boundary with the hope of achieving a steady state at  later times. In order to speed up the onset of convection a density perturbation was applied. Specifically, the initial density values were changed periodically by up to \SI{3.642e5}{\kg / \m^3} ($0.02\%$ of the maximum density) in the heating region at the bottom of the box.










