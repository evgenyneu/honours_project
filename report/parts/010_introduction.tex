\section{Introduction}

\subsection{Studying stars}

Almost all sources of light we see in the sky are stars. Stars are formed when free-flowing atoms of mostly hydrogen and helium gravitate towards each other and gather into spherical clumps. When these objects collect enough material, their gravity brings atomic nuclei in their cores so close to each other that they merge and transform into heavier nuclei in the process known as nuclear fusion. Nuclear fusion produces most of the chemical elements in the Universe in addition to hydrogen and helium nuclei that were made within several minutes after the Big Bang. One of the by-products of nucleosynthesis is electromagnetic radiation. We observe some of this radiation as visible light, which comes out of the Sun and supplies almost all of the Earth's energy. We are not only made of star stuff, the material produced in stars, but they also power us. Thus, living organisms are one of the many outcomes of stars.

Since we owe so much of our existence to stars, it is no surprise that we are naturally curious about them. Stars are studied with observations and modelling. There are different kinds of stellar observations. Firstly, photometry allows to estimate star's surface temperature, luminosity\footnote{Luminosity is power generated by a star, it can be estimated with photometry when distance to the star is known.} and infer radius. Secondly, with spectroscopy we can analyze the electromagnetic radiation spectrum and estimate both surface temperature and chemical abundances. Finally, asteroseismology gives us a way to peek inside a star and get an estimate of its structure by observing variations in its brightness and size. However, in order to get detailed picture of a stellar interior and its changes over time we have to use modelling.

\emph{Stellar modelling} relies on equations that represent properties of a real star. These equations are often based on fundamental physical laws, such as Newton's laws of motion and universal gravitation, nuclear physics and thermodynamics, and are used to estimate density, pressure, velocity of material and its chemical composition at any point inside the modelled star. Unfortunately, it is almost never possible to find solutions to a stellar model's equations analytically. In this case, we can use a computer program to find approximate solutions. Such a program is called a \emph{stellar code} or \emph{numerical simulation}, and the most popular type is called a \emph{1D model}.

A 1D model represents a star in space using one dimension instead of three, which becomes possible if we adopt an assumption of \emph{spherical symmetry}. This means that all points inside a star that are separated from the center by the same distance $r$ have identical properties, such as pressure, temperature, chemical composition and others. Therefore, distance $r$ from the center is the only spatial coordinate we need.

\subsection{Equations of stellar structure}

Most 1D modelling techniques in use today are based on four equations of stellar structure. I'll briefly state these equations here, an overview can be found in \cite{basu_2007}, and a detailed description in \cite{kippenhahn2012stellar}.

The first equation is called \emph{the equation of hydrostatic equilibrium}. It is applicable when a star has inner material at rest. This happens when gravitational force is balanced by pressure $P$ for all regions inside the star:
\begin{equation}
  \frac{dP(r)}{dr} = - G \frac{M(r) \rho(r)}{r^2}.
\label{eq_hydrostatic_equilibrium}
\end{equation}
Here $G$ is the gravitational constant, $r$ is radius (distance to stellar center), $\rho(r)$ is density at radius $r$, and $M(r)$ is mass inside a sphere of radius $r$.

The second equation is \emph{the mass equation}, which is derived from the definition of density:
\begin{equation}
  \frac{dM(r)}{dr} = 4 \pi r^2 \rho(r).
\end{equation}
It states that the mass of a thin spherical shell $dM(r)$ at radius $r$ is equal to its density $\rho(r)$ multiplied by shell's volume ($4 \pi r^2 dr$), where $dr$ is the radial size of the shell.

Next is \emph{the luminosity equation} which computes luminosity $dL(r)$ produced by a spherical shell via nuclear fusion:
\begin{equation}
  \frac{dL(r)}{dr} = 4 \pi r^2 \rho(r) \varepsilon,
\end{equation}
where $\varepsilon$ is power generated by unit mass of stellar material.

Finally, \emph{the radiative diffusion equation} gives temperature gradient $dT/dr$ for regions inside a star where energy is transported by electromagnetic radiation:
\begin{equation}
\frac{dT}{dr} = - \frac{3}{16 \pi a c} \frac{\kappa}{r^2} \frac{\rho L}{T^3}.
\label{eq_diffusion}
\end{equation}
Here $T$ is temperature, $\kappa$ is opacity\footnote{Materials with higher opacity block more more electromagnitic radiation.}, $a$ is the radiation density constant, and $c$ is the speed of light in vacuum. \Crefrange{eq_hydrostatic_equilibrium}{eq_diffusion} are solved together numerically to find values of $\rho$, $P$ and $T$ at different distances from the center $r$. These values describe the state of a stellar model at one moment in time.


\subsection{Temperature difference leads to convection}
\label{sec_convection}

\Crefrange{eq_hydrostatic_equilibrium}{eq_diffusion} are based on the assumption of spherical symmetry. According to this assumption, temperature is the same for all neighboring elements that are separated from the stellar center by the same distance. However, real stars are not ideal and we can expect these temperatures to differ.

Assume that the temperature of one small element gets slightly larger than the surrounding temperature. The hotter element will expand to equalize pressure with the environment, keeping pressure uniform. According to the ideal gas law equation,
\begin{equation}
P \propto \frac{\rho T}{\mu},
\end{equation}
where $\mu$ is the mean molecular weight of material. Thus, increase in temperature will result in decrease in density of the element, which creates upward buoyancy force with magnitude
\begin{equation}
F = g (\rho - \rho_e) V_e,
\end{equation}
where $g$ is gravitational acceleration, $\rho_e$ and $\rho$ and are densities of the element and surrounding material respectively, and $V_e$ is the element's volume. According to Newton's second law, $F = ma$, the buoyancy force will make the element accelerate and move upwards. As the element moves towards the stellar surface, the density of the surrounding material decreases. If, however, the density of the element decreases faster, then the element will continue to accelerate upwards, which will result in the flow of the material.

This flow is called \emph{convection}. Inside a star it will move hotter elements away from the stellar center, tranporting both the energy and chemical elements that were created inside the core. Consequently, stellar convection changes the internal structure and chemical composition of a star. Both of these internal changes can be indirectly observed with spectroscopy and asteroseismology. If convection delivers chemical elements to the stellar surface, these elements can be detected in the spectum of the star's electromagnetic radiation. In addition, asteroseismology allows to infer the presence of convection in a stellar core and estimate its size \citep{asteroseismology_measure_convective_core_2016A&A...589A..93D}.


\subsection{Locations of convective regions}

In order to model convection we first need to determine the location of convective regions in the stellar interior. Using the same idea of rising convective elements (also called bubbles or plumes) described in \Cref{sec_convection}, one can derive a condition that describes regions that are stable (non-convective), with the energy transported entirely by electromagnetic radiation \citep[page 51]{kippenhahn2012stellar}:
\begin{equation}
  \nabla_{\text{rad}} < \nabla_{\text{ad}} + \frac{\varphi}{\delta} \nabla_{\mu}.
  \label{eq_ledoux}
\end{equation}
Such regions are called \emph{radiative}, and locations outside these regions are considered convective\footnote{Conduction also needs to be considered when density is high, which happens in stellar cores, wide dwarfs and neutron stars.}.  The terms in \Cref{eq_ledoux} are:
\begin{itemize}
  \item $\nabla_{\text{rad}} = \frac{d \ln T}{d \ln P}$: temperature gradient in a radiative region. From \Cref{eq_diffusion},
  \begin{equation}
    \nabla_{\text{rad}} = \frac{3 \kappa L P}{16 \pi a c G M T^4}.
    \label{eq_temperature_gradient_rad}
  \end{equation}
  The temperature and pressure values for different locations in stellar interior are usually calculated by a stellar code. In this case this temperature gradient $\nabla_{\text{rad}}$ is computed directly from these values instead of using \Cref{eq_temperature_gradient_rad}.
  \item $\nabla_{\text{ad}} = \frac{d \ln T}{d \ln P}$: same temperature gradient but calculated inside a rising bubble that expands adiabatically. Here we assume that a bubble rises so quickly that it has no time to exchange its internal energy with surrounding material. From the adiabatic relation for ideal gas $PT^{\gamma / (1 - \gamma)} = \text{const}$, where $\gamma = C_P / C_V$ is the specific heat ratio, one can show that for monoatomic gas,
  \begin{equation}
    \nabla_{\text{ad}} = 2 / 5.
  \end{equation}
  \item $\varphi = \frac{\partial \ln \rho}{\partial \ln \mu}$, $\delta = - \frac{\partial \ln \rho}{\partial \ln T}$: density gradients.
  \item $\nabla_{\mu} = \frac{d \ln \mu}{d \ln P}$: mean molecular weight gradient.
\end{itemize}

\Cref{eq_ledoux} is also known as \emph{the Ledoux criterion}, it is widely used in 1D models to calculate locations of convective and radiative regions. For models that have uniform chemical composition the $\nabla_{\mu}$ term is zero, and the condition simplifies to
\begin{equation}
  \nabla_{\text{rad}} < \nabla_{\text{ad}},
  \label{eq_schwarzschild}
\end{equation}
which is known as \emph{the Schwarzschild criterion}. Both Schwarzschild and Ledoux creteria are the most popular methods of locating convective regions in 1D models today.


\subsection{Energy transport in convective region}

Once the boundaries of a convective region has been established with the Schwarzschild or Ledoux criterion a 1D simulation needs calculate the energy flux\footnote{Energy flux is amount of energy transported though unit area per unit time.} in that region. The most widely used method of estimating this energy flux is called the Mixing Length Theory (MLT), that was initially proposed by Prandtl (1925) and further developed in B\"ohm-Vitense (1958) \citep{Arnett_2015_beyond_mixing_length}.

The line of reasoning behind MLT is based on the same idea of rising hot bubbles described in \Cref{sec_convection}. A bubble travels distance $\ell_\text{m}$, called the \emph{mixing length}, and then dissolves into surrounding material releasing its excess internal energy. The mixing length is defined as
\begin{equation}
  \ell_\text{m} \equiv \alpha H_\text{P},
\label{eq_mixing_length}
\end{equation}
where $\alpha$ is a free parameter chosen to fit a model to observations and $H_\text{P}$ is the \emph{pressure scale height}, which is the distance over which the pressure decreases by $e = 2.718$.

The MLT then gives the relation for convective flux $F_\text{con}$:
\begin{equation}
  F_\text{con} = \rho C_\text{P} T \sqrt{g \delta} \frac{\ell^2_\text{m}}{4 \sqrt{2}} H^{-3/2}_\text{P} ( \nabla - \nabla_\text{e})^{3/2},
\end{equation}
where $C_\text{P}$ is specific heat at constant pressure. The terms $\nabla_\text{e}$ and $\nabla$ are temperature gradients $\frac{d \ln T}{d \ln P}$ of a convective element and its surrounding material respectively, they can be found after solving equations described on page 65 of \cite{kippenhahn2012stellar}.


\subsection{Problems with MLT}
\label{sec_mlt_problem}

The MLT is a remarkably successful theory and it is used in most 1D stellar codes today. However, the predictive value of MLT suffers from the fact that it is not based entirely on first physical principles and instead includes a simplified treatment of convection. In particular, the MLT is based on the concept of a convective flow made of hot rising bubbles. However, this phenomenon has never been observed in the real world convection or its numerical simulations \citep[page 49]{kupka_muthsam_2017}.

Moreover, predictions of MLT models depend on the choice of free parameter $\alpha$. This parameter is usually set to be around $\alpha=1.5$ by fitting model's prediction to Solar observations, such as radius and luminosity. However, this presence of free parameter $\alpha$ has been shown to reduce universality of the MLT models because they can not accurately describe other types of stars, or the same star at different stages of its evolution without changing the value of $\alpha$.

Specifically, \cite{mlt_alpha_callibration_ludwig_1999A&A...346..111L} used $\alpha=1.3$ to fit observations from F-dwarf stars while hooter K-subgiants required a large value of $\alpha=1.75$ (see their Fig. 5). \cite{pre_mainsequence_mlt_alpha_1994ApJS...90..467D} found that changing $\alpha$ from $1$ to $2$ resulted in $25\%$ variation in predicted effective temperature and an order of magnitude difference in Lithium depletion in pre-main-sequence stellar models. \cite{rgb_stars_lower_mtl_alpha_2011A&A...526A.100P} showed that $\alpha$ with a lower than Solar value was needed to accurately model stars at early Red Giant Branch stage of evolution. Finally, \cite{meakin_arnett_2007} demonstrated that $\alpha$ is not constant throughout a convection zone and changes significantly near its boundaries (see their Fig. 18).


\subsection{Equations of fluid dynamics}

A more physically rigorous method of stellar modeling that mitigates some of the drawbacks of MLT is based on \emph{fluid dynamics}. It involves solving a set of partial differential equations numerically in order to find values of velocity, pressure, density and others. Unlike 1D models, this method allows to calculate these properties in two or three dimensions of space, and the corresponding stellar codes are often called 2D and 3D models. Thus, the assumption of spherical symmetry required for 1D models is no longer needed. Another advantage of 2D and 3D models is that they don't require additional treatment of convection because it is automatically handled by the physical laws these models are based on. Thus, fluid dynamics does not include an observation-dependent parameter $\alpha$ from MLT, nor does it rely on a fictional concept of rising bubbles.

I'll briefly describe equations of fluid dynamics here in the form given by \cite{andrassy_2021_code_comparison}. The hydrodynamics code PROMPI \citep{meakin_arnett_2007} I used in this work for stellar modelling is also based on this form of equations. A more detailed overview can by found in Chapter 1 of \cite{toro2009riemann}.

The first equation describes conservation of mass and is called \emph{continuity equation}. It states that the change of mass of a small parcel of fluid is equal to the net flow of the mass through its boundary:
\begin{equation}
  \frac{\partial \rho}{\partial t} + \nabla \cdot (\rho \bm{u}) = 0,
\label{eq_continuity}
\end{equation}
where $\bm{u}$ is velocity and $t$ is time.

The second is the \emph{Euler equation} of motion, it is based on Newton's second law, which states that the rate of change of momentum is equal to the net force:
\begin{equation}
  \frac{\partial (\rho \bm{u})}{\partial t} + \nabla \cdot (\rho \bm{u} \bm{u} + P) = \rho \bm{g},
\label{eq_euler}
\end{equation}
where $\bm{g}$ is gravitational acceleration. In this case, the momentum of a small parcel of fluid $\frac{\partial (\rho \bm{u})}{\partial t} + \nabla \cdot \rho \bm{u} \bm{u}$ changes due to the force of pressure $-\nabla \cdot P$ from neighboring parcels and the force of gravity $\rho \bm{g}$. A parcel of fluid also experiences frictional forces from the contact with surrounding material. However, friction is ignored here for simplicity. A version of this equation with a frictional force term is known as \emph{Navier-Stokes}.

The third equation describes \emph{conservation of energy}:
\begin{equation}
  \frac{\partial (\rho E)}{\partial t} + \nabla \cdot [(\rho E + P) \bm{u}] = \rho \bm{u} \cdot \bm{g} + \dot{q}.
\label{eq_energy}
\end{equation}
Here $E = e + 0.5 \|\bm{u}\|^2$ is total specific energy\footnote{Specific energy is energy per unit mass.}, where $e$ and $0.5 \|\bm{u}\|^2$ are specific internal and kinetic energies respectively. The term $\dot{q}$ is energy generated by nuclear reactions per unit volume. The system of \Crefrange{eq_continuity}{eq_energy} is commonly know as \emph{Euler equations}. When frictional term is added to \autoref{eq_euler}, the system is referred as \emph{Navier-Stokes equations}.

The next equation is used to track changes in chemical composition of the star:
\begin{equation}
  \frac{\partial (\rho X_i)}{\partial t} + \nabla \cdot (\rho X_i \bm{u} ) = 0,
  \label{eq_track_fluids}
\end{equation}
where $X_i$ is the mass fraction of the i-th chemical element.

Lastly, \Cref{eq_continuity,eq_euler,eq_energy} are closed with the ideal gas formula:
\begin{equation}
  P = (\gamma - 1) \rho e,
\label{eq_of_state}
\end{equation}
where $\gamma = C_P / C_V$ is the specific heat ratio equal to $\gamma = 5/3$ for monoatomic gas.

Given the initial values of velocity $\bm{u}$, pressure $P$, density $\rho$, internal energy $e$ and chemical composition $X$, \Crefrange{eq_continuity}{eq_of_state} allow to find values of these variables at any moment of time. These calculations are always done numerically because finding an analytical solution to 3D Euler/Navier-Stokes equations is a famous unsolved problem in mathematics. The initial values of the variables are often supplied by a 1D code that solves \Crefrange{eq_hydrostatic_equilibrium}{eq_diffusion}.


\subsection{Solving 2D/3D model equations in codes}
\label{solving_2d_code_in_codes}

The first step in solving \Crefrange{eq_continuity}{eq_of_state} numerically is to setup a grid containing a finite number of spatial coordinates. An example of a spherical grid is shown in \Cref{cell_stellar_model}, containing 432 coordinates, also called \emph{cells} or \emph{zones}. This grid has 6, 6 and 12 zones in each of the three directions, often referred as a $6 \times 6 \times 12$ grid. Alternatively, one can use a rectangular grid with cubical cells if the model represents smaller region inside a star, such a model is called ``box-in-a-star''.

\begin{figure}[!ht]
  \centering
  \includegraphics[width=0.75\textwidth]{images/cell_stellar_model.pdf}
  \caption{A grid of cells for modeling the interior volume of the entire star. The grid is setup in spherical coordinates with six cells along radial and polar directions and twelve cells along the azimuthal direction. This amounts to $6 \times 6 \times 12 = 432$ total cells. Adapted from \cite{spherical_coordinate_system}.}
  \label{cell_stellar_model}
\end{figure}

Next, for each cell, we supply the $P$, $\rho$, $e$ and $X$ variables at initial moment of time using a model calculated by a 1D code that solved \Crefrange{eq_hydrostatic_equilibrium}{eq_diffusion}. The goal now is to calculate these variables at the next moment of time after a time interval $\Delta t$, also called the \emph{time step}. If the material is moving with speed $u$, the time step needs to smaller than
\begin{equation}
  \Delta t \le C \frac{\Delta x}{u},
\label{eq_courant}
\end{equation}
where $\Delta x$ is the size of a cell. Otherwise the changes will propagate well beyond the cell's boundary in a single time step and the approximated solution will be inaccurate. Relation \ref{eq_courant} is called \emph{Courant–Friedrichs–Lewy condition}, and $C$ is a constant called the \emph{Courant number}.

Each of the \Cref{eq_continuity,eq_euler,eq_energy} is given in conservation form
\begin{equation}
  \frac{\partial Q}{\partial t} + \nabla \cdot F = S,
\label{eq_conservation}
\end{equation}
where $Q$, $F$ and $S$ are called state, flux and source terms respectively. For example, in \Cref{eq_euler} $Q = \rho \bm{u}$, $F=(\rho E + P) \bm{u}$ and $S=\rho \bm{g}$. The meaning of this equation is better explained with a diagram in \Cref{single_cell} that shows a single cell in one dimensional grid at position $x_i$. As material moves through the cell, the conservation requires that the change of quantity $Q$ in the cell after time $\Delta t$ is equal to flow $F$ of this quantity through the two cell walls\footnote{The wall is imaginary, there are no physical bondaries between grid cells.}, plus the amount generated by the source S.
\begin{figure}[!ht]
  \centering
  \includegraphics[width=0.50\textwidth]{images/single_cell.pdf}
  \caption{Single grid cell in one dimension at position $x_i$. Convection moves material through cell's walls, resulting in fluxes $F_{i-1/2}$ and $F_{i+1/2}$ through the left and right walls respectively.}
  \label{single_cell}
\end{figure}
\Cref{eq_conservation} can be written in discrete form that is used in code:
\begin{equation}
  Q^{t + \Delta t} = Q^{t} - \Delta t \frac{F_{i+1/2} - F_{i-1/2}}{\Delta x} + \Delta t S ,
\label{eq_conservation_discrete}
\end{equation}
where $Q^{t}$ is the current state value of the cell at time $t$, $Q^{t + \Delta t}$ is its new value after time step $\Delta t$. Quantities $F_{i-1/2}$ and $F_{i+1/2}$ are fluxes trough left and right cell walls respectively. Since the cell walls are located between the grid points, the fluxes are unknown and need to be constructed. This can be done by using an interpolation method, such as PPM (piecewise parabolic method) \citep{1984JCoPh..54..174C}.

Finally, calculating the right-hand-side of \Cref{eq_conservation_discrete} gives  the values for all variables $\bm{u}$, $P$, $\rho$, $e$ and $X$ at new moment in time. Repeating this operation allows to evolve the model and predict its future state.


\subsection{The main limitation of 3D codes}

3D codes allow to model stellar convection more realistically than 1D codes. However, technological limitations prevent us from modelling stars exclusively in 3D without relying on 1D simulations. The main problem with 3D simulations is that they are expensive to run because they require large capacity of computer memory (RAM) and long calculation times.

3D simulations require large memory size because of the big difference between two scales: the large size of the stars and the small scale of energy dissipation in stellar convection. Stellar convection is turbulent with material moving chaotically and forming eddies, as opposed to moving straight (also called ``laminar flow''). Large eddies break into smaller ones, which break into even smaller ones. The process continues until eddies reach a size where friction turns kinetic energy into internal energy\footnote{Internal energy is kinetic energy of individual particles moving in random directions and is proportional to temperature in the ideal gas approximation.} in the process called \emph{energy dissipation}. This dissipation size is called the ``Kolmogorov scale'' and it is about $1$ cm in the solar convection zone \citep[p. 18]{kupka_muthsam_2017}. This transfer of kinetic energy from large to smaller scales is called the \emph{turbulence energy cascade}.

The large memory requirement arises because 3D codes need to simulate both the small-scale dissipation and the stellar-size processes, such as large  convection eddies, at the same time. The means that the model's grid cells need to be at least $1$ cm in size, while the number of cells should be large enough to cover significant volume of the stellar interior, ideally the entire star. Each grid cell has associated values of $\bm{u}$, $P$, $\rho$, $e$ and $X$ variables which are stored in memory to perform calculations. Therefore, increasing the number of cells requires larger amount of memory. For example, to model the Sun's full interior with the resolution of $1$ cm one needs a $10^{11} \times 10^{11} \times 10^{11}$ grid, which requires at least $10^{35}$ bytes of memory \citep[page 19]{kupka_muthsam_2017}. This is about $10^{20}$ times higher than memory available on Fugaku, one of the world's largest supercomputers \citep{fugaku_supercomputer}.

This inconceivably large memory requirement limits the current 3D stellar codes, which have to use fewer grid cells or model smaller regions of the stellar interior. For example, state of the art 3D stellar models today have only up to $1000$ grid cells in each of the three spatial directions. If such a grid is used to model the full interior of the Sun, then one cell needs to be $1000$ km long. Such models are also called Large Eddy Simulations, or LES. Since the cells in an LES are much larger than the Kolmogorov scale these codes can not model friction and energy dissipation based on fundamental physical laws.

The second technical limitation of 3D codes is due to long time scales of stellar processes and the large number of iterations required to simulate them. This happens because the time step $\Delta t$ between successive iterations is limited by the Courant-Friedrichs-Lewy condition (\Cref{eq_courant}). Therefore, one can not set $\Delta t$ to be arbitrarily large in order to reduce the number of iterations. For example, the time step needs to be smaller than $1$ second for an LES near the surface of stellar convection zone \cite[page 19]{kupka_muthsam_2017}. However, the most advanced 3D simulations today compute no more than $10^{8}$ iterations, which only covers a few years of stellar lifetime \citep[page 35]{kupka_muthsam_2017}. A ten-billion-year lifetime of the Sun would therefore require $10^9$ times more iterations than currently possible.

These two requirements --- memory and computer time --- will prevent us from running full 3D stellar simulations for at least half a century, even if we assume that available computer resources double every two years. Therefore, 1D models will remain necessary for studying stellar interiors in the coming decades.

\subsection{Validating LES with Kolmogorov's power law}

Despite inability of LES\footnote{The type of models I'm describing here is called ILES, or Implicit Large Eddy Simulations, but these details are out the scope for this work.} to model small-scale dissipation from first physical principles, there is some evidence that these models can accurately model the distribution of kinetic energy over the resolved scales. This evidence comes from comparison of LES energy spectra with Kolmogorov's energy cascade, illustrated in \Cref{energy_spectra}. The figure shows energy spectra that measure kinetic energy of the turbulent flow (y-axis) for each wavenumber $k$ (x-axis). The wavenumber is inversely proportional to the length scale, therefore a smaller $k$ value corresponds to the larger eddies in a turbulent flow. The curved lines describe the output of six different 3D stellar codes, including PROMPI, which used the same stellar model setup as in this work (\Cref{sec_model_setup}). The straight dotted line is the theoretical model from \cite{1941DoSSR..32...16K, 1941DoSSR..30..301K}, also known as the \emph{Kolmogorov's $-5/3$ power law}:
\begin{equation}
  E(k) = \alpha \epsilon^{2/3} k^{-5/3},
  \label{eq_kolmogorov_power_law}
\end{equation}
where $E(k)$ is kinetic energy distribution, $\alpha \approx 1.5$ is a constant, $\epsilon$ is the mean energy dissipation rate and $k$ is the wave number. \Cref{eq_kolmogorov_power_law} has been experimentally validated \citep{kolmogorov_power_law, 4c6e766fb75f46bbac455437516dd36e}  and is generally assumed to be a universal relation that describes energy spectra of turbulent flows within limited range of $k$ values. The key result illustrated in \Cref{energy_spectra} is that the slope of the energy spectra of LES is very similar to the Kolmogorov's law at the large scales. This suggests that these LES codes are sufficient for modelling distribution of kinetic energy in large eddies, even though these codes do not resolve smaller scales, nor do they model friction and energy dissipation.

\begin{figure}[!ht]
  \centering
  \includegraphics[width=1\textwidth]{images/energy_spectrum.png}
  \caption{Kinetic energy spectra of convective flow from six hydrodynamics codes compared to Kolmogorov's $-5/3$ power law (straight dotted line). The $y$-axis is kinetic energy of turbulent flow, and the $x$-axis is the wave number $k$, which is inversely proportional to eddie size. The numbers $128^3$, $256^3$ and $512^3$ in the legends correspond to the sizes of computational grids. There is a good agreement between the codes and Kolmogorov's relation for large eddies (small $k$ values). This agreement is improved for smaller eddies when the grid size is increased. Figure source: \cite{andrassy_2021_code_comparison}, their Figure 6.}
  \label{energy_spectra}
\end{figure}

\subsection{The science question}

We do not yet have the technology to run full 3D stellar simulations, but we can still use partial ones. To run them on current computers, we can lower the resolution, use smaller regions inside a star instead of its full interior and simulate shorter stellar lifetimes. Another compromise that will make a stellar model more computable is to reduce the number of dimensions from three to two, which will let us either achieve a higher resolution or keep the same resolution but evolve the model for a more extended time. For example, consider a 3D model with $1000 \times 1000 \times 1000$ grid. The model contains $10^9$ grid points in total, and therefore a two-dimensional $31622 \times 31622$ model will use the same amount of memory but will have $32$ times higher resolution. Alternatively, we can keep the resolution unchanged with a $1000 \times  1000$ 2D grid, but since there are $1000$ times fewer numbers to calculate, the model will run faster. Thus the remaining computer time can be used to simulate a more extended period of stellar life (e.g. $1000$ days instead of one day).

The goal of this work is to run 2D and 3D simulations of stellar convection using identical model setup and compare the results. Specifically, I want to take a close look at the boundary between convective and radiative regions, also called the \emph{convective boundary}, by comparing model parameters (such as fluid mass fraction, temperature, pressure) near the boundary, as well as its shape and location between 2D and 3D models. Understanding of the convective boundaries is necessary because they affect the physical structure (density, pressure and temperature) of stellar interiors, which in turn changes the stellar evolution and its observed outcomes, such as stellar size, surface temperature and luminosity. Moreover, a convective boundary is a place where material from a stable region is mixed into a convection zone. Thus, the boundary location can affect the transport and distribution of chemical elements in the stellar interior and the chemical composition of both the stellar surface and interstellar space.

The science question I want to answer is whether 2D simulations are good enough for modelling a convective boundary. If the answer is yes, then convective boundary modelling can be done with 2D models instead of 3D ones, which will make computations 1000 times more affordable. The results from 2D models can then be used to improve 1D stellar evolution codes, which currently use simplified treatment of convection and its boundaries (\Cref{sec_mlt_problem}).


