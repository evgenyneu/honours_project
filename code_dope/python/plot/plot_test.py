import matplotlib.pyplot as plt
import os
import pytest
from pytest import approx
from plot import save_plot, kde_with_uncerts


def test_save_plot():
    pltdir = 'code_dope/python/plot/plots'
    path_to_file = os.path.join(pltdir, 'plot_test_mytest.png')

    if os.path.exists(path_to_file):
        os.remove(path_to_file)

    plt.plot([1, 2, 3], [4, 5, 6])
    save_plot(plt=plt, suffix='mytest')

    assert os.path.exists(path_to_file)
    os.remove(path_to_file)
    os.rmdir(pltdir)


def test_save_plot__no_suffix():
    pltdir = 'code_dope/python/plot/plots'
    path_to_file = os.path.join(pltdir, 'plot_test.png')

    if os.path.exists(path_to_file):
        os.remove(path_to_file)

    plt.plot([1, 2, 3], [4, 5, 6])
    save_plot(plt=plt)

    assert os.path.exists(path_to_file)
    os.remove(path_to_file)
    os.rmdir(pltdir)


def test_kde_with_uncerts():
    result = kde_with_uncerts([-10.1, 9.8], [-10, 10], [1.4, 1.8])

    assert result.shape[0] == 2
    assert result[0] == approx(0.14211638124953146, rel=1e-15)
    assert result[1] == approx(0.11013534965419111, rel=1e-15)


def test_kde_with_uncerts_unequal_data():
    with pytest.raises(ValueError):
        kde_with_uncerts([-10.1, 9.8], [-10, 10], [1.4, 1.8, 4.3])


def test_kde_with_uncerts_empty_arrays():
    result = kde_with_uncerts([-10.1, 9.8], [], [])

    assert result.shape[0] == 0
