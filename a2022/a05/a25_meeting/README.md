## Horizontal vs radial speeds plots

* Near the boundary horizontal is dominating.

* In 2D horizontal speed decreases slower than in 3D.

* Plot speeds for 3D vs high resolution 2D. Same for composition profile.
