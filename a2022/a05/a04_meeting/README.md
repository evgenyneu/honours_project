* Average kinetic energy.

* Composition profile plot (averaged over time). Zoomed in, and shifted.

* Temperature, density profile and gradient (averaged).
