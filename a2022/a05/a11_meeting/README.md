* Horizontal/radial velocity on same plot (rotate the plot)
* Used time-average velocity.
* 2D and 3D radial/horizontal velocities behave same way near boundaries (even though magnitudes are different)
