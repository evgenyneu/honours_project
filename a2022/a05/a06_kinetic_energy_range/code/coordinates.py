from a2022.a04.a12_evolution_plot.code.data import data_info
from a2022.a04.a12_evolution_plot.code.load_data import bindata_path
from a2021.a12.a24_3d_to_2d.thylacine.reader import get_coordinates
from a2021.a12.a24_3d_to_2d.thylacine.reader import get_grid_size

def get_space_coordinates(simulation, epoch):
    """
    Return the first and last value for space coordinates in meters
    for each of the x,y,z dimensions.

    Parameters
    ----------
    simulation : str
        Name of the simulation.

    epoch : int
        The epoch number.

    Returns
    -------
    dict
        Key: coordinate name, for example 'x', 'y' or 'z'.

        Value: array with containing first and last values for the coordinate,
               in meters.
    """

    info = data_info(simulation)

    data_path = bindata_path(epoch=epoch,
                             data_dir=info['dir'],
                             data_filename=info['file_name'])

    coordinates = get_coordinates(data_path)

    # Convert coordinates from cm to meters
    return  {
        coordinate: [
            coordinates[coordinate][0] / 100,
            coordinates[coordinate][1] / 100
        ]
        for coordinate in coordinates
    }


def convert_index_to_radial_coordinates(index, max_index, simulation):
    """
    Converts the array index into radial coordinate, in meter.
    """

    space_coordinates = get_space_coordinates(simulation=simulation, epoch=1)
    r_from = space_coordinates['x'][0]
    r_to = space_coordinates['x'][1]
    return (index / max_index) * (r_to - r_from) + r_from


def get_grid_size_for_simulation(simulation, epoch):
    """
    Return the number of grid points in x, y and z directions.

    Parameters
    ----------
    simulation : str
        Name of the simultion.

    Returns
    -------
    (int, int, int)
        Number of grid points in each direction(x, y, z).
    """

    info = data_info(simulation)

    data_path = bindata_path(epoch=epoch,
                             data_dir=info['dir'],
                             data_filename=info['file_name'])

    return get_grid_size(data_path)


def calc_cell_size_meters(simulation, direction='y'):
    """
    Calculate the size in meters of single grid cell.

    Parameters
    ----------
    simulation : str
        Name of the simultion.

    direction : str
        Name of direction: 'x' (radial), 'y' or 'z'.

    Returns
    -------
    float
       Size of one grid cell in meters.
    """

    coordinates = get_space_coordinates(simulation=simulation, epoch=1)
    range_meters = coordinates[direction]
    total_range = range_meters[1] - range_meters[0]
    grid_size = get_grid_size_for_simulation(simulation, epoch=1)

    if direction == 'x':
        index = 0
    elif direction == 'y':
        index = 1
    else:
        index = 2

    cells_horizontal = grid_size[index]
    return total_range / cells_horizontal
