from a2022.a04.a12_evolution_plot.code.data import data_info
from a2022.a04.a12_evolution_plot.code.load_data import bindata_path
from a2021.a12.a24_3d_to_2d.thylacine.reader import get_time

def get_frame_time(epoch, data_name):
    info = data_info(data_name)

    data_path = bindata_path(epoch=epoch,
                             data_dir=info['dir'],
                             data_filename=info['file_name'])

    return get_time(data_path)


def get_frame_times(epoch, compare):
    return [
       get_frame_time(epoch, data_name)
       for data_name in compare
    ]


def print_times(epoch_range, plots_info):
    simulations = plots_info.keys()

    time_start = get_frame_times(epoch_range[0], simulations)
    time_end = get_frame_times(epoch_range[-1], simulations)

    print(f"Time: from {time_start} to {time_end} s")
