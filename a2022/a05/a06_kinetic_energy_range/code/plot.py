import numpy as np
from a2022.a05.a06_kinetic_energy_range.code.coordinates import get_space_coordinates


def plot_one_simulation(ax, simulation, info, data):
    space_coordinates = get_space_coordinates(simulation=simulation, epoch=1)
    r_from = space_coordinates['x'][0]
    r_to = space_coordinates['x'][1]
    locations = np.linspace(r_from, r_to, len(data[0, :]))

    # Plot data for individual epochs
    for i in range(data.shape[0]):
        data_one_epoch = data[i, :]

        ax.plot(locations, data_one_epoch,
            linewidth=info['line_width'],
            color=info['line_color'],
            alpha=info['line_alpha'],
            linestyle=info['line_style'])

    # Plot time average
    # ---------

    time_average_data = data.mean(axis=0)

    ax.plot(locations, time_average_data,
            linewidth=info['average_line_width'],
            color=info['line_color'],
            alpha=info['average_line_alpha'],
            linestyle=info['average_line_style'],
            label=info['title'])


def plot_boundaries(ax, data, info):
    # Full the range of the boundaries
    # ---------

    data = data[:, 0]  # Get boundary position values
    locations = data.flatten()

    ax.axvspan(locations.min(), locations.max(),
               ymin=info['boundary_fill_ymin'],
               facecolor=info['boundary_fill_color'],
               alpha=info['boundary_fill_alpha'])


    # Plot time average
    # ---------

    time_averaged_location = data.mean()

    ax.axvline(
        x=time_averaged_location,
        ymin=info.get('average_boundary_ymin', 0),
        ymax=info.get('average_boundary_ymax', 1),
        color=info['line_color'],
        linestyle=info['average_boundary_line_style'],
        alpha=info['average_boundary_line_alpha'],
        linewidth=info['average_boundary_line_width']
    )
