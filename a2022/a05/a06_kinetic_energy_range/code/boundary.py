import numpy as np
from a2021.a08.a19_plot_boundary.code.credible_interval import credible_interval
from a2022.a05.a06_kinetic_energy_range.code.coordinates import convert_index_to_radial_coordinates
from a2022.a05.a06_kinetic_energy_range.code.data import load_data_and_gradients

def locate_boundary(gradients,  simulation):
    """
    Finds boundary location and its width.

    Parameters
    ----------
    gradients: 2D or 3D numpy array
        Gradients of mass fraction.

    simulation: string
        Name of the simulation

    Returns
    -------
    Tuple: (mode, interval, mode_index, inteval_index)
        mode: float
            Location of the peak gradient.

        mode_index: float
            Array index corresponding to the peak gradient.

    """

    # Calculate horizontal average of the gradient
    nx = gradients.shape[0]
    x = gradients.sum(axis=1) / nx

    if len(gradients.shape) == 3: # 3D
        x = x.sum(axis=1) / nx

    mode_index = x.argmax()

    mode_position = convert_index_to_radial_coordinates(
        mode_index, max_index=nx - 1, simulation=simulation)

    return (mode_position, mode_index)


def find_boundary(simulation, epoch, variable,
    custom_variables, full3d, zindex):

    data = load_data_and_gradients(
            epoch=epoch, variable=variable,
            data_name=simulation,
            custom_variables=custom_variables, full3d=full3d,
            zindex=zindex,
            perturbation=False
        )

    (location, location_index) = locate_boundary(
        gradients=data['gradients'],
        simulation=simulation
    )

    return [location, location_index]


def find_boundaries_multiple_epochs(
    simulation, epoch_range, variable, custom_variables,
    full3d, zindex):

    return np.array([
        find_boundary(
            epoch=epoch, variable=variable,
            simulation=simulation,
            custom_variables=custom_variables, full3d=full3d,
            zindex=zindex)

        for epoch in range(*epoch_range)
    ])


def find_boundaries_multiple_epochs_and_simulations(
    epoch_range, simulations, variable, custom_variables, full3d, zindex):

    return {
        simulation: find_boundaries_multiple_epochs(
            simulation=simulation, epoch_range=epoch_range,
            variable=variable,
            custom_variables=custom_variables, full3d=full3d,
            zindex=zindex)
        for simulation in simulations
    }
