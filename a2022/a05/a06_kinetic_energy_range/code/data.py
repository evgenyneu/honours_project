import numpy as np
from a2022.a04.a12_evolution_plot.code.data import data_info
from a2021.a11.a12_perturbation_single_frame.code.perturbation import calc_perturbation

from a2022.a04.a12_evolution_plot.code.load_data import (
    load_data,
    calc_gradients_2d,
    calc_gradients_3d
)

def load_data_and_gradients(epoch, zindex, variable, data_name,
                            custom_variables, perturbation, full3d):
    data = load_data(epoch=epoch, variable=variable,
                     info=data_info(data_name),
                     custom_variables=custom_variables,
                     full3d=full3d)

    if perturbation: data = calc_perturbation(data)

    if len(data.shape) == 3: # 3D
        gradients = calc_gradients_3d(data)
        data_2d = data[:, :, zindex]
    else:
        data_2d = data
        data = data_2d
        gradients = calc_gradients_2d(data)

    return {
        'data_2d': data_2d,
        'data': data,
        'gradients': gradients
    }


def load_data_multiple_epochs(
    simulation, epoch_range, variable, custom_variables,
    full3d):

    return np.array([
        load_data(epoch=epoch, variable=variable, info=data_info(simulation),
            custom_variables=custom_variables, full3d=full3d)

        for epoch in range(*epoch_range)
    ])


def load_data_multiple_epochs_and_simulations(
    epoch_range, simulations, variable, custom_variables,
    full3d):

    return {
        simulation: load_data_multiple_epochs(
            simulation=simulation, epoch_range=epoch_range,
            variable=variable,
            custom_variables=custom_variables, full3d=full3d)
        for simulation in simulations
    }


def calc_horizontal_averages(epochs_data):
    axes = tuple(range(2, len(epochs_data.shape)))
    return epochs_data.mean(axis=axes)
