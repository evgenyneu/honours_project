
import numpy as np
from a2022.a05.a06_kinetic_energy_range.code.coordinates import calc_cell_size_meters
from a2022.a05.a09_velocity.code.stats import calc_value_and_uncertainty


def calc_average_boundary_index(boundaries):
    boundary_indices = boundaries[:, 1]
    boundary_index = boundary_indices.mean()
    return round(boundary_index)


def calc_shift_from_boundary(data_size, shift_fraction, simulation):
    """Returns the shift from convective boundary given"""
    shift_index = int(data_size * shift_fraction)
    cell_size_meters = calc_cell_size_meters(simulation)
    shift_meters = shift_index * cell_size_meters

    return (shift_index, shift_meters)


def calc_values_at_boundaries(data_epochs, boundary_indices):
    """Return array of values at the boundaries"""

    return [
        data_epochs[epoch, boundary_indices[epoch]]
        for epoch in range(data_epochs.shape[0])
    ]


def calc_value_at_boundary(data_epochs, boundary_indices):
    """Calculates the value and uncertainty at the boundary"""
    values = calc_values_at_boundaries(data_epochs=data_epochs, boundary_indices=boundary_indices)
    return calc_value_and_uncertainty(values)


def convective_or_radiative_values(
    data_epochs, simulation, boundary_indices, shift_fraction, convective):
    """
    Returns values in radiative or convective region.

    Parameters
    ----------
    data_epochs: 2D Numpy array
        Array containing data (second index) for epochs (first index).

    simulation: string
        Name of the simulation

    boundary_indices: Array
        List of boundary indices for epochs. Size: number of epochs.

    shift_fraction: float
        Fraction of the domain size to shift from the boundary.
        Number between 0 (no shift) and 1. Example: 0.1 means shift by 10%
        of the radial size.

    convective: boolean
        If True calculate in convective region, othersize in radiative region.

    Returns
    ---------
    Array of values in radiative or convective region.

    """

    shift_index, _ = calc_shift_from_boundary(
        data_epochs.shape[1], shift_fraction=shift_fraction, simulation=simulation
    )

    if convective:
        # Convective zone
        data_slices = [
            data_epochs[epoch, 0: boundary_indices[epoch] - shift_index]
            for epoch in range(data_epochs.shape[0])
        ]
    else:
        # Radiative zone
        data_slices = [
            data_epochs[epoch, boundary_indices[epoch] + shift_index:]
            for epoch in range(data_epochs.shape[0])
        ]

    return [item for sub_list in data_slices for item in sub_list]


def calc_convective_value(data_epochs, simulation, boundary_indices, shift_fraction=0.125):
    """Value and uncertainty in convective region."""
    values = convective_or_radiative_values(
        data_epochs=data_epochs, simulation=simulation,
        boundary_indices=boundary_indices,
        shift_fraction=shift_fraction,
        convective=True)

    return calc_value_and_uncertainty(values)


def calc_radiative_value(data_epochs, simulation, boundary_indices, shift_fraction=0.125):
    """Value and uncertainty in radiative region."""

    values = convective_or_radiative_values(
        data_epochs=data_epochs, simulation=simulation,
        boundary_indices=boundary_indices,
        shift_fraction=shift_fraction,
        convective=False)

    return calc_value_and_uncertainty(values)
