!..include file bdimen.inc
!..reaction network size
!
!
!..network grid (see notes below)
!
      integer*4 nnuc, nreac, nrtype, ndim
!
      parameter( nnuc   = 2      )
      parameter( nreac  = 3000   )
      parameter( nrtype = 15     )
!
!..----------------------------------------------------------------
!..NOTES
!..----------------------------------------------------------------
!..nnuc   =  number of nuclei + n + p + alpha
!..nreac  = max dimension for reaction links
!..nrtype = number of reaction types(do not change without recoding)
!..----------------------------------------------------------------
