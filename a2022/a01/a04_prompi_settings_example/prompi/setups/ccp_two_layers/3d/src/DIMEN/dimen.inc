!..include file dimen.inc
!..MUST precede dimensioned arrays
!
!
!..spatial grid
      integer*4      qqq, qq, qx, qy, qz
      integer*4      qqx, qqy, qqz

      parameter( qqq  = 274   )
      parameter( qqx  = 264   )
      parameter( qqy  = 264   )
      parameter( qqz  = 264   )

      parameter( qq   =  54  )
      parameter( qx   =  44  )
      parameter( qy   =  22  )
      parameter( qz   =  22  )


!..composition array
      integer*4 qn
      parameter( qn     = 2  )

!..some io stuff
      integer*4 qdat

      parameter( qdat = 13 + qn )
