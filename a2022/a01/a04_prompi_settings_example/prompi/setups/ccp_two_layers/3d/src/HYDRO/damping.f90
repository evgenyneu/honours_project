!     
!     
!     DAMP VELOCITES
!     
      subroutine damping
!..   
      implicit none
!
      include 'dimen.inc'
      include 'vnew.inc'
      include 'vold.inc'
      include 'mesh.inc'
      include 'float.inc'
      include 'intgrs.inc'
      include 'grd.inc'
      include 'shareplane.inc'
      include 'mpicom.inc'
      include 'mpif.h'
!
!
      integer*4 i, j, k
      real*8 damp0,damp1,vel1,vel2
      real*8 vdampx, vdampy, vdampz, edamp
      real*8 xxxx
!
!..   COPY VELOCITIES TO TEMP ARRAYS
!..   AND GET VELOCITES AT DOMAIN
!..   BOUNDARIES
!..   
      do i=1,qx
         do j=1,qy
            do k=1,qz
               vxold(i,j,k) = velx(i,j,k)
               if(nsdim.ge.2) vyold(i,j,k) = vely(i,j,k)
               if(nsdim.eq.3) vzold(i,j,k) = velz(i,j,k)
            enddo
         enddo
      enddo         
!
!     print*,'DBG(damping):call shareplane3d(4)'
!      call shareplane3d(4)
!     print*,'DBG(damping):call shareplane3d(5)'
!      if(nsdim.ge.2) call shareplane3d(5)
!     print*,'DBG(damping):call shareplane3d(6)'
!      if(nsdim.eq.3) call shareplane3d(6)
!
!
      !damp0  = 5.d-2            !critical damping coefficient
      damp0 = 5.d-3
      !damp0 = 0.01d0 
      !
      xmin = 4.d8  ! xmin for damping 

      !
      do i=1,qx
!
!     damp outter region using exponential falloff from bounday
!     with scale height: xdamp, if idamp=2.
!     
         if(idamp.eq.2) then
            xxxx = (xmax - xzn(i))/xdamp
            damp1 = damp0*exp(-xxxx)            
            !print*,'DBG(damp) i,xxxx,xzn(i),damp1:',i,xxxx,xzn(i),xmax,damp1
            
!
!     damp inner region using exponential falloff from boundary
!     with scale height: xdamp, if idamp=3.
!     
         else if(idamp.eq.3) then
            xxxx = (xzn(i) - xmin)/xdamp
            damp1 = damp0*exp(-xxxx)
            !print*,'DBG(damp):',xxxx,i,xzn(i),xmin,damp1
!
!     damp everywhere uniformly if (idamp!=0,2,3)     
         else                
            damp1 = damp0
         endif
!
         do j=1,1
            do k=1,1
!
               vdampx = 0.d0
               vdampy = 0.d0
               vdampz = 0.d0

!...  velx
               if(i.eq.1) then
                  vel1 = yzdat_min(j,k,4)
                  vel2 = vxold(i+1,j,k)
               else if(i.eq.qx) then
                  vel1 = vxold(i-1,j,k)
                  vel2 = yzdat_max(j,k,4)
               else
                  vel1 = vxold(i-1,j,k)
                  vel2 = vxold(i+1,j,k)
               endif
               vdampx  = damp1*(2.d0*velx(i,j,k)-vel1-vel2)
               !print*,i,j,k,velx(i,j,k),vdampx
               
               !...  vely
               if(nsdim.ge.2) then
                  if(j.eq.1) then
                     vel1 = xzdat_min(i,k,5)
                     vel2 = vyold(i,j+1,k)
                  else if(j.eq.qy) then
                     vel1 = vyold(i,j-1,k)
                     vel1 = xzdat_max(i,k,5)
                  else
                     vel1 = vyold(i,j-1,k)
                     vel2 = vyold(i,j+1,k)
                  endif
                  vdampy  = damp1*(2.d0*vely(i,j,k)-vel1-vel2)
               endif
               
               !...  velz
               if(nsdim.eq.3) then
                  if(k.eq.1) then
                     vel1 = xydat_min(i,j,6)
                     vel2 = vzold(i,j,k+1)
                  else if(k.eq.qz) then
                     vel1 = vzold(i,j,k-1)
                     vel2 = xydat_max(i,j,6)
                  else
                     vel1 = vzold(i,j,k-1)
                     vel2 = vzold(i,j,k+1)
                  endif
                  vdampz  = damp1*(2.d0*velx(i,j,k)-vel1-vel2)
               endif
               !
               velx(i,j,k) = velx(i,j,k) - vdampx
               vely(i,j,k) = vely(i,j,k) - vdampy
               velz(i,j,k) = velz(i,j,k) - vdampz
!               print*,'[damping] velx,vely,velz',i,j,k,velx(i,j,k),vely(i,j,k),velz(i,j,k)    
!               print*,'[damping] vdax,vday,vdaz',i,j,k,vdampx,vdampy,vdampz
               
               edamp = (vdampx**2.d0 + vdampy**2.d0 + vdampz**2.d0)/2.d0
               energy(i,j,k) = energy(i,j,k) + edamp
               !
            enddo               !z-loop
         enddo                  !y-loop
      enddo                     !x-loop
      !
      !..   
      !..   
      !..   update pressure and temperature
      call eos3d(1)
      !
      !
      !     SUCCESS
      return
      !
      !     
    end subroutine damping
