!
!
!
subroutine bndry_ends_bot(j,k)
  
  implicit none
  
  include 'dimen.inc'
  include 'vold.inc'
  include 'vnew.inc'
  include 'mesh.inc'
  include 'float.inc'
  include 'intgrs.inc'
  include 'comp.inc'
  include 'hydro.inc'
  include 'grd.inc'
  include 'physcs.inc'
  include 'core.inc'
  include 'gravity.inc'
  include 'bndry.inc' 
  include 'constants.inc'
  include 'mpif.h'
  include 'mpicom.inc'
  
  integer*4 i, j, k, n
  integer*4 ipp, jpp, kpp
  integer*4 nzn54
  integer*4 ntop, nbot, nlft, nrgt
  integer*4 ibndmn, ibndmx, ibndbt
  integer*4 ibndtp, ibndlt, ibndrt 
  
  real*8 yrbot, ylbot, xrbot, xlbot
  real*8 yltop, yrtop, xltop, xrtop
  real*8 zrlft, zllft, yrlft, yllft
  real*8 zlrgt, zrrgt, ylrgt, yrrgt
  
  integer*4 inputz, errorkey
  real*8 cs2
  real*8 eint,pp,dd,tt,cv,cp,gme,gmc
  real*8 XXcomp,YYcomp,ZZcomp,imu,abar,zbar
  real*8 enx, delta
  real*8 tguess
  
  real*8 y_int,dy_i,xps
  real*8 yh(4,6+qn)
  real*8 xh(4),yhd(4),scrch(4),xhbndry(4),yhbndry(4),xpos(4)
  integer*4 ip,itmp
  
  real*8 delp, delx
  real*8 five_fourth,y
  
  nbot = 1
  nlft = 1
  if(xyzswp.eq.1) ntop = ny
  if(xyzswp.eq.2) ntop = nx
  if(xyzswp.eq.3) ntop = nx
  if(xyzswp.eq.1) nrgt = nz
  if(xyzswp.eq.2) nrgt = nz
  if(xyzswp.eq.3) nrgt = ny
  ibndmn = idnint(bndmin)
  ibndmx = idnint(bndmax)
  ibndbt = idnint(bndbot)
  ibndtp = idnint(bndtop)
  ibndlt = idnint(bndlft)
  ibndrt = idnint(bndrgt)
  
  go to (410, 420, 430, 440, 450, 460, 470, 480), ibndmn
  

  !     REFLECT
410 continue
  
  do i = 1, 4
     rho  (i) =  rho  (9-i)
     u    (i) = -u    (9-i)
     !         if(xyzswp.eq.1.and.igrav.eq.1) then
     !            u(i) = u(i) + 0.5d0*gravcon*dt
     !         endif
     ut   (i) =  ut   (9-i)
     utt  (i) =  utt  (9-i)
     utbt (i) =  utbt (9-i)
     uttp (i) =  uttp (9-i)
     utlt (i) =  utlt (9-i)
     utrt (i) =  utrt (9-i)
     e    (i) =  e    (9-i)
     gamc (i) =  gamc (9-i)
     game (i) =  game (9-i)
     p    (i) =  p    (9-i)
     dx   (i) =  dx   (9-i)
     ugrid(i) = -ugrid(9-i)
  enddo
  !
  do n = 1, qn
     do i = 1, 4
        xn(i,n) = xn(9-i,n)
     enddo
  enddo

  go to 500

!     FLOW OUT
420   continue
  
  do i = 1, 4
     rho  (i) = rho  (5)
     u    (i) = u    (5)
     ut   (i) = ut   (5)
     utt  (i) = utt  (5)
     utbt (i) = utbt (5)
     uttp (i) = uttp (5)
     utlt (i) = utlt (5)
     utrt (i) = utrt (5)
     e    (i) = e    (5)
     gamc (i) = gamc (5)
     game (i) = game (5)
     p    (i) = p    (5)
     dx   (i) = dx   (5)
     ugrid(i) = ugrid(5)
  enddo
!
  do n = 1, qn
     do i = 1, 4
        xn(i,n) = xn(5,n)
     enddo
  enddo

  go to 500
  
  
  !     FLOW IN
430 continue

  do i = 1, 4
     rho  (i) = rhoin
     u    (i) = uin
     ut   (i) = utin
     utt  (i) = uttin
     utbt (i) = utin
     uttp (i) = utin
     utlt (i) = uttin
     utrt (i) = uttin
     e    (i) = ein
     p    (i) = pin
     gamc (i) = gamcin
     game (i) = gamein
     dx   (i) = dx(5)
     ugrid(i) = 0.d00
  enddo
!
  do n = 1, qn
     do i = 1, 4
        xn(i,n) = xnin(n)
     enddo
  enddo

  go to 500


  !     PERIODIC
440 continue
  !
  do i = 1, 4
     rho   (i) = rho  (i+nzn)
     u     (i) = u    (i+nzn)
     ut    (i) = ut   (i+nzn)
     utt   (i) = utt  (i+nzn)
     utbt  (i) = utbt (i+nzn)
     uttp  (i) = uttp (i+nzn)
     utlt  (i) = utlt (i+nzn)
     utrt  (i) = utrt (i+nzn)
     e     (i) = e    (i+nzn)
     gamc  (i) = gamc (i+nzn)
     game  (i) = game (i+nzn)
     p     (i) = p    (i+nzn)
     dx    (i) = dx   (i+nzn)
     ugrid (i) = 0.d00
  enddo
  !
  do n = 1, qn
     do i = 1, 4
        xn(i,n) = xn(i+nzn,n)
     enddo
  enddo

  go to 500


  !     SPECIAL
450 continue
  !
  do i = 1, 4
     rho  (i) =  rhoco(i)
     u    (i) =  uco(i)
     ut   (i) =  utco(i)
     utt  (i) =  uttco(i)
     utbt (i) =  utbtco(i)
     uttp (i) =  uttpco(i)
     utlt (i) =  utltco(i)
     utrt (i) =  utrtco(i)
     e    (i) =  eco(i)
     gamc (i) =  gamcco(i)
     game (i) =  gameco(i)
     p    (i) =  pco(i)
     dx   (i) =  dxco(i)
     !..grid velocity taken constant................
     ugrid(i) =  ugrid(5)
  enddo
  !
  do n = 1, qn
     do i = 1, 4
        xn(i,n) = xnco(i,n)
     enddo
  enddo

  goto 500



!     INTERNAL BOUNDARY: DUE TO DOMAIN DECOMPOSITION
460 continue
  !
  if(xyzswp.eq.1) then
     do i=1,4
        ipp = i
        ugrid(i) = 0.d00            
        rho  (i) = rho_mnx  (ipp, j, k)
        u    (i) = u_mnx    (ipp, j, k)
        ut   (i) = ut_mnx   (ipp, j, k)
        utt  (i) = utt_mnx  (ipp, j, k)
        e    (i) = e_mnx    (ipp, j, k)
        p    (i) = p_mnx    (ipp, j, k)
        gamc (i) = gamc_mnx (ipp, j, k)
        game (i) = game_mnx (ipp, j, k)
        dx   (i) = dx_mnx   (ipp      )
        do n = 1, qn
           xn(i,n) = xn_mnx(ipp, j, k, n)
        enddo
     enddo

  elseif(xyzswp.eq.2) then
     do i=1,4
        ipp = i
        ugrid(i) = 0.d00            
        rho  (i) = rho_mny  (j, ipp, k)
        u    (i) = u_mny    (j, ipp, k)
        ut   (i) = ut_mny   (j, ipp, k)
        utt  (i) = utt_mny  (j, ipp, k)
        e    (i) = e_mny    (j, ipp, k)
        p    (i) = p_mny    (j, ipp, k)
        gamc (i) = gamc_mny (j, ipp, k)
        game (i) = game_mny (j, ipp, k)
        dx   (i) = dx_mny   (ipp      )
        do n = 1, qn
           xn(i,n) = xn_mny(j, ipp, k, n)
        enddo
     enddo
     
  elseif(xyzswp.eq.3) then
     do i=1,4
        ipp = i
        ugrid(i) = 0.d00            
        rho  (i) = rho_mnz  (j, k, ipp)
        u    (i) = u_mnz    (j, k, ipp)
        ut   (i) = ut_mnz   (j, k, ipp)
        utt  (i) = utt_mnz  (j, k, ipp)
        e    (i) = e_mnz    (j, k, ipp)
        p    (i) = p_mnz    (j, k, ipp)
        gamc (i) = gamc_mnz (j, k, ipp)
        game (i) = game_mnz (j, k, ipp)
        dx   (i) = dx_mnz   (ipp      )
        do n = 1, qn
           xn(i,n) = xn_mnz(j, k, ipp, n)
        enddo
     enddo
  endif

  goto 500

  
  
  ! MIRO Extrapolation
470 continue
  !     REFLECT (with extrapolation to "ghostzones")
  !     Author: Miroslav Mocak (last update 07/19/2012)
  !     get data for extrapolation
  xh(1) = xzn(4)  
  xh(2) = xzn(3) 
  xh(3) = xzn(2)  
  xh(4) = xzn(1)
  !     get data for extrapolation
  do i = 1,4 
     yh(i,1) = rho  (9-i) 
     yh(i,2) = u    (9-i) 
     yh(i,3) = e    (9-i)
     yh(i,4) = gamc (9-i)
     yh(i,5) = game (9-i)
     yh(i,6) = p    (9-i)
     do n = 1, qn
        yh(i,n+6) = xn(9-i,n)
     enddo
  enddo
  !     get position of "ghostzones" in the inner boundary
  do i = 4,1,-1 
     xhbndry(i) = i+4
     yhbndry(i) = xzn(i)
  enddo
  do i = 4,1,-1 
     xps = i
     call polint (xhbndry,yhbndry,3,xps,y_int,dy_i)
     xpos(i) = y_int
  enddo
  !     extrapolate data to "ghostzones"
  do ip = 1,6+qn
     do i = 1,4
        yhd(i) = yh(i,ip) 
     enddo
     do i = 1,4
        xps = xpos(i)
        call polint (xh,yhd,3,xps,y_int,dy_i)             
        scrch(i) =  y_int
     enddo
     do i = 1,4
        yh(i,ip) = scrch(i)        
     enddo
  enddo
  !     map extrapolated data to the inner boundary
  do i = 1,4 
     rho(i)  =  yh(i,1)
     u  (i)  = -yh(i,2)      ! reflective
     e    (i) = yh(i,3)
     gamc (i) = yh(i,4)
     game (i) = yh(i,5)
     p    (i) = yh(i,6)  
  enddo
  !     leave transverse velocities untouched       
  do i = 1, 4
     ut   (i) =  ut   (9-i)
     utt  (i) =  utt  (9-i)
     utbt (i) =  utbt (9-i)
     uttp (i) =  uttp (9-i)
     utlt (i) =  utlt (9-i)
     utrt (i) =  utrt (9-i)
     dx   (i) =  dx   (9-i)
     ugrid(i) = -ugrid(9-i)
  enddo
  !     map extrapolated composition to the inner boundary 
  do n = 1, qn
     do i = 1, 4
        xn(i,n) = yh(i,n+6)
     enddo
  enddo
  
  goto 500
  

  ! HSE
480 continue
  
  if(xyzswp.ne.xyzgrav) then
     if(myid.eq.0) print*,'ERR(bndry_bot): no bndmxx=8 for xyzswp.ne.xyzgrav'
     call MPI_FINALIZE(ierr)
     stop 'ERR(bndry_top): no bndmxx=8 for xyzswp.ne.xyzgrav'
  endif
  
  do i = 1, 4
     u    (i) = -u    (9-i)
     !u(i) = 0.d0
     ut   (i) =  ut   (9-i)
     utt  (i) =  utt  (9-i)
     utbt (i) =  utbt (9-i)
     uttp (i) =  uttp (9-i)
     utlt (i) =  utlt (9-i)
     utrt (i) =  utrt (9-i)
     dx   (i) =  dx   (9-i)
     ugrid(i) = -ugrid(9-i)
  enddo
  do n = 1, qn
     do  i = 1, 4
        xn(i,n) = xn(5,n)
     enddo
  enddo
  do i = 1, 4
     xl(5-i) =        xl(6-i) - dx(5-i)
     xr(5-i) =        xr(6-i) - dx(6-i)
     x (5-i) = 0.5d00 * (xl(5-i) + xr(5-i))
  enddo
  
  
  !hse extrapolation
  
  if(xyzswp.eq.1) tguess = temp(1,j,k)
  if(xyzswp.eq.2) tguess = temp(j,1,k)
  if(xyzswp.eq.3) tguess = temp(j,k,1)
  do n=1,qn
     if(xyzswp.eq.1) xxxinput(n) = xnuc(1,j,k,n)
     if(xyzswp.eq.2) xxxinput(n) = xnuc(j,1,k,n)
     if(xyzswp.eq.3) xxxinput(n) = xnuc(j,k,1,n)
  enddo
  call composition(XXcomp,YYcomp,ZZcomp,imu,abar,zbar)

  five_fourth = 5.d0/4.d0
  
  if(mstates.eq.2) then !source balance mode
     !print*,gravcon
     if((igrav.eq.1).or.(igrav.eq.6))then
        do i=4,1,-1           
           rho(i)  = rho(i+1)
           p  (i)  = p(i+1) - (dx(i)*gravcon*rho(i) + dx(i+1)*gravcon*rho(i+1))/2.d0
           inputz = 3
           call geos(inputz,rho(i),tguess,eint,p(i),gmc,gme,cv,cp,  &
                enx,delta,XXcomp,YYcomp,ZZcomp,imu,abar,zbar,errorkey)
           gamc(i) = gmc
           game(i) = gme
           e   (i) = eint
           !print*,'DBG(bndry): i,p,rho = ', i,p(i),rho(i)
        enddo
     else
        call MPI_FINALIZE(ierr)
        stop 'ERR(bndr_top): igrav.ne.1.and.mstates.eq.2 not supported yet!'
     endif
  else
     do i=4,1,-1
        if(igrav.eq.6) then
           y = x(i)/onelu 
           grav(i) = g0/(y**five_fourth) 
        endif        
        if(igrav.eq.7) then
           grav(i) =  0.d0
        endif
        
        cs2       =  gamc(i+1)*p(i+1)/rho(i+1)
        delx      =  (dx(i+1)+dx(i))/2.d0
        delp      =  rho(i+1)*grav(i)*delx
        rho (i)   =  rho(i+1) - (delp)/cs2
        delp      =  (rho(i)+rho(i+1))*0.5d0*delx*grav(i)
        rho (i)   =  rho(i+1) - (delp)/cs2
        p   (i)   =  p(i+1) - delp
        inputz = 3
        call geos(inputz,rho(i),tguess,eint,p(i),gmc,gme,cv,cp,  &
             enx,delta,XXcomp,YYcomp,ZZcomp,imu,abar,zbar,errorkey)
        gamc(i) = gmc
        game(i) = gme
        e   (i) = eint
     enddo     


     
  endif
  
  goto 600
  
  
  !     SET GRID ZONING: 1 - 4
500 continue
  
  do i = 1, 4
     xl(5-i) =        xl(6-i) - dx(5-i)
     xr(5-i) =        xr(6-i) - dx(6-i)
     x (5-i) = 0.5d00 * (xl(5-i) + xr(5-i))
  enddo
  
  


600 continue  



  return
  
end subroutine bndry_ends_bot

