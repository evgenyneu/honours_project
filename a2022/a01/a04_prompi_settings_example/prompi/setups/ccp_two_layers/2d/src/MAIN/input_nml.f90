!
!
!
subroutine input_nml

  use mod_rans_data, only: irans
  use mod_basestate_data, only: basepoints
  implicit none
  include 'dimen.inc'
  include 'char.inc'
  include 'float.inc'
  include 'intgrs.inc'
  include 'gravity.inc'
  include 'grd.inc'
  include 'mpicom.inc'
  include 'mpif.h'


  namelist /prompi_inlist/ runname, nend, tmax, twallmax, twallout, irstrt, nrstrt, trstrt, &
       nout, tout, itstp, idbgdmp, nsdim, xmin, xmax, ymin, ymax, zmin, zmax, &
       nx, ny, nz, igeomx, igeomy, igeomz, &
       bndmnx, bndmxx, bndmny, bndmxy, bndmnz, bndmxz, &
       ntiles, &
       mstates, isinit, idamp, xdamp, itdiff, xtdiff, ftdiff, iopac, &
       igrav, gravcon, xyzgrav, ieos, ixnuc, iburn, cfl, dtini, tcc, xcc, dtmin, dtmax, &
       nriem, cvisc, small, smlrho, smallp, smalle, smallu, smallx, &
       igrid, igodu, isteep, iflat, epsiln, omg1, omg2, &
       irans, basepoints, &
       rhoin,uin,utin,uttin,ein,pin,gamcin,gamein,irestart_eos,irestart_type, &
       irecl_float

  ! ** set default namelist values **

  runname = 'prompi_run'  !Name for this run..........................
  nend    = 1             !Number of timesteps for this run ..........
  tmax    = 1.d0          !Maximum time for this run (in sec) ........
  twallmax = 1.d99        !Max wall clock time for run
  twallout = 1.d99        !Wall clock time interval for IO
  irstrt  = 0             !Start new sequence, if IRSTRT = 0 .........
  nrstrt  = 10000000      !Store restart file after NRSTRT timesteps .
  trstrt  = 1.d99         !Store restart file after TRSTRT seconds ...
  nout    = 10000000      !Print model after NOUT timesteps ..........
  tout    = 1.d99         !Print model after TOUT seconds ............
  itstp   = 1             !Print timestep information, if ITSTP.ne.0..
  idbgdmp = 10000000      !perform debugging data dump................


  nsdim   = 1             !Dimensionality of the problem ......
  xmin    = 0.d0          !Minimum extent of x-grid (cm) ......
  xmax    = 1.d0          !Maximum extent of x-grid (cm) ......
  ymin    = 0.d0          !Minimum extent of y-grid (cm or rad)
  ymax    = 1.d0          !Maximum extent of y-grid (cm or rad)
  zmin    = 0.d0          !Minimum extent of z-grid (cm or rad)
  zmax    = 1.d0          !Maximum extent of z-grid (cm or rad)
  nx      = 100           !Number of grid points in x-direction
  ny      = 100           !Number of grid points in y-direction
  nz      = 100           !Number of grid points in z-direction
  igeomx  = 0             !Geometry of grid in x-direction ....
  igeomy  = 0             !Geometry of grid in y-direction ....
  igeomz  = 0             !Geometry of grid in z-direction ....

  bndmnx  = 1             !Minimum x boundary condition
  bndmxx  = 1             !Maximun x boundary condition
  bndmny  = 1             !Minimum y boundary condition
  bndmxy  = 1             !Maximun y boundary condition
  bndmnz  = 1             !Minimum z boundary condition
  bndmxz  = 1             !Maximun z boundary condition
  ntiles(1) = 1           ! number of subdomains in x-direction
  ntiles(2) = 1           ! number of subdomains in y-direction
  ntiles(3) = 1           ! number of subdomains in z-direction

  mstates   = 0           !use modified states for gravity..........
  isinit    = 1           !set initial model to use:(see starinit.f)
  idamp     = 0           !damps motion (1=unif.2=outer.3=inner)....
  xdamp     = 1.d7        !damping scale height (idamp.eq.1:exp)....
  itdiff    = 0           !thermal diffusion (on/off:1/0)...........
  xtdiff    = 1.d5        !outer radius for thermal diffusion.......
  ftdiff    = 1.d0        !driving factor for thermal diffusion.....
  igrav     = 0           !gravity setting..........................
  gravcon   = 0.0         !default constant gravity value for igrav = 1 case
  xyzgrav   = 1           !gravity direction........................
  iopac     = 3           !opacity type(1=opal,2=opac,3=analytic)...
  ieos      = 1           !EOS(1=ig+rp,2=jrbeos,3=heos).............
  ixnuc     = 1           !Set Species(1=H:2=burnf).................
  iburn     = 0           !Nuclear burning:(1=msburn:2=burnf).......

  irans     = 0           !Reynolds Averaging (0=no;1=yes)..........

  basepoints = 10000      !Number of zones for basestate data.......

  irestart_eos  = 2       !Default to calling eos3d(2) after reading restart data
  irestart_type = 1       ! 1=bindata, 2=blockdat
  irecl_float   = 1       ! 4=gfortan/ubuntu; 1=intel/stampede


  ! FLOW IN boundary conditions
  rhoin    = 1.d0
  uin      = 0.d0
  utin     = 0.d0
  uttin    = 0.d0
  ein      = 1.d0
  pin      = 1.d0
  gamcin   = 1.d0
  gamein   = 1.d0


  ! Definitions of small values
  small     = 1.d-9       !Cut-off value ............................
  smlrho    = 1.d-9       !Cut-off value for density ................
  smallp    = 1.d-10      !Cut-off value for pressure ...............
  smalle    = 1.d-10      !Cut-off value for energy .................
  smallu    = 1.d-9       !Cut-off value for velocity ...............
  smallx    = 1.d-10      !Cut-off value for abundances .............


  ! PPM Hydro parameters: leave these fixed
  cfl       = 0.8         !Courant factor ...........................
  dtini     = 1.d-4       !Initial size of the timestep (in sec).....
  tcc       = 1.05        !Maximum temperature change per timestep ..
  xcc       = 1.10        !Maximum abundance change per timestep ....
  dtmin     = 1.d-20      !Minimum size of timestep (in sec) ........
  dtmax     = 3.d5        !Maximum size of timestep (in sec) ........
  nriem     = 7           !Number of iterations in Riemann solver ...
  cvisc     = 0.0d0       !Artificial viscosity constant ............
  igrid     = 0           !Grid motion: IGRID=1=lagrangian...........
  igodu     = 0           !PPM parameter: if igodu=1, use Godumov mtd
  isteep    = 0           !PPM parameter: if isteep=1, then steepen..
  iflat     = 0           !PPM parameter: if iflat=1, then flaten....
  epsiln    = 0.33        !PPM parameter used to detect shocked zones
  omg1      = 0.75        !PPM dissipation parameter omega1 .........
  omg2      = 10.0        !PPM dissipation parameter omega2 .........


  if(myid.eq.0) print*,'MSG(input): opening inlist_prompi'

  ! Read namelist values from file

  open(1, file='inlist_prompi', action='read', delim='quote', iostat=ierr)
  read(1, nml=prompi_inlist, iostat=ierr)
  close(1)

  ! Check conditions on geometry flags

  if(myid.eq.0) write(*,*)'MSG(input): Finished reading param.d.'


  ! Write out parameters

  if(myid.eq.0) then
     write(6, nml=prompi_inlist)
  endif


  flush(6)
  call MPI_BARRIER(MPI_COMM_WORLD, ierr)


  !call MPI_FINALIZE(ierr)
  !stop 'DBG(input_nml): stop'


  return




end subroutine input_nml
