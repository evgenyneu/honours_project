!
!
!
subroutine bndry_ends_top(j,k)
  
  
  implicit none
  
  include 'dimen.inc'
  include 'vold.inc'
  include 'vnew.inc'
  include 'mesh.inc'
  include 'float.inc'
  include 'intgrs.inc'
  include 'comp.inc'
  include 'hydro.inc'
  include 'grd.inc'
  include 'physcs.inc'
  include 'core.inc'
  include 'gravity.inc'
  include 'bndry.inc'
  include 'constants.inc'  
  include 'mpif.h'
  include 'mpicom.inc'
  
  integer*4 i, j, k, n
  integer*4 ipp, jpp, kpp
  integer*4 nzn54
  integer*4 ntop, nbot, nlft, nrgt
  integer*4 ibndmn, ibndmx, ibndbt
  integer*4 ibndtp, ibndlt, ibndrt 
  
  real*8 yrbot, ylbot, xrbot, xlbot
  real*8 yltop, yrtop, xltop, xrtop
  real*8 zrlft, zllft, yrlft, yllft
  real*8 zlrgt, zrrgt, ylrgt, yrrgt
  
  integer*4 inputz, errorkey
  real*8 cs2
  real*8 eint,pp,dd,tt,cv,cp,gme,gmc
  real*8 XXcomp,YYcomp,ZZcomp,imu,abar,zbar
  real*8 enx, delta
  real*8 tguess
  
  real*8 y_int,dy_i,xps
  real*8 yh(4,6+qn)
  real*8 xh(4),yhd(4),scrch(4),xhbndry(4),yhbndry(4),xpos(4)
  integer*4 ip,itmp
  
  real*8 delx, delp
  real*8 five_fourth,y
  
  nbot = 1
  nlft = 1
  if(xyzswp.eq.1) ntop = ny
  if(xyzswp.eq.2) ntop = nx
  if(xyzswp.eq.3) ntop = nx
  if(xyzswp.eq.1) nrgt = nz
  if(xyzswp.eq.2) nrgt = nz
  if(xyzswp.eq.3) nrgt = ny
  ibndmn = idnint(bndmin)
  ibndmx = idnint(bndmax)
  ibndbt = idnint(bndbot)
  ibndtp = idnint(bndtop)
  ibndlt = idnint(bndlft)
  ibndrt = idnint(bndrgt)
  

  go to (510, 520, 530, 540, 550, 560, 570, 580), ibndmx


!     REFLECT
510   continue
!
      nzn54 = nzn5 + nzn4
      do 515 i = nzn5, nzn8
         rho  (i) =  rho  (nzn54-i)
         u    (i) = -u    (nzn54-i)
!         if(xyzswp.eq.1.and.igrav.eq.1) then
!            u(i) = u(i) + 0.5d0*gravcon*dt
!         endif
         ut   (i) =  ut   (nzn54-i)
         utt  (i) =  utt  (nzn54-i)
         utbt (i) =  utbt (nzn54-i)
         uttp (i) =  uttp (nzn54-i)
         utlt (i) =  utlt (nzn54-i)
         utrt (i) =  utrt (nzn54-i)
         e    (i) =  e    (nzn54-i)
         gamc (i) =  gamc (nzn54-i)
         game (i) =  game (nzn54-i)
         p    (i) =  p    (nzn54-i)
         dx   (i) =  dx   (nzn54-i)
         ugrid(i) = -ugrid(nzn54-i)
 515  continue
!
      do 516 n = 1, qn
      do 516 i = nzn5, nzn8
         xn(i,n) = xn(nzn54-i,n)
516   continue
!
      go to 600
!
!
!     FLOW OUT
520   continue
!
      do 525 i = nzn5, nzn8
         rho  (i) = rho  (nzn4)
!cc   u    (i) = u    (nzn4)
         u    (i) = dmax1( u(nzn4), 0.d00)
         ut   (i) = ut   (nzn4)
         utt  (i) = utt  (nzn4)
         utbt (i) = utbt (nzn4)
         uttp (i) = uttp (nzn4)
         utlt (i) = utlt (nzn4)
         utrt (i) = utrt (nzn4)
         e    (i) = e    (nzn4)
         gamc (i) = gamc (nzn4)
         game (i) = game (nzn4)
         p    (i) = p    (nzn4)
         dx   (i) = dx   (nzn4)
         ugrid(i) = ugrid(nzn4)
 525  continue
!
      do 526 n = 1, qn
         do 526 i = nzn5, nzn8
            xn(i,n) = xn(nzn4,n)
 526     continue
!
         go to 600
!
!
!     FLOW IN
 530     continue
!
         do 535 i = nzn5, nzn8
            rho  (i) = rhoin
            u    (i) = uin
            ut   (i) = utin
            utt  (i) = uttin
            utbt (i) = utin
            uttp (i) = utin
            utlt (i) = uttin
            utrt (i) = uttin
            e    (i) = ein
            p    (i) = pin
            gamc (i) = gamcin
            game (i) = gamein
            dx   (i) = dx(nzn4)
            ugrid(i) = 0.d00
 535     continue
!
         do 536 n = 1, qn
            do 536 i = nzn5, nzn8
               xn(i,n) = xnin(n)
 536        continue
!
            go to 600
!
!
!     PERIODIC
 540  continue
!
      do 545 i = nzn5, nzn8
         rho  (i) = rho (i-nzn)
         u    (i) = u   (i-nzn)
         ut   (i) = ut  (i-nzn)
         utt  (i) = utt (i-nzn)
         utbt (i) = utbt(i-nzn)
         uttp (i) = uttp(i-nzn)
         utlt (i) = utlt(i-nzn)
         utrt (i) = utrt(i-nzn)
         e    (i) = e   (i-nzn)
         p    (i) = p   (i-nzn)
         gamc (i) = gamc(i-nzn)
         game (i) = game(i-nzn)
         dx   (i) = dx  (i-nzn)
         ugrid(i) = 0.d00
545   continue
!
      do 546 n = 1, qn
      do 546 i = nzn5, nzn8
         xn(i,n) = xn(i-nzn,n)
 546  continue
!
      go to 600
!
!
!     SPECIAL
 550  continue
      print*,'ERR(bndry): SPECIAL BNDRY NOT READY.myid=',myid
      call MPI_FINALIZE(ierr)
      stop 'ERR(bndry): SPECIAL BNDRY NOT READY'
!
      goto 600




!     INTERNAL BOUNDARY: DUE TO DOMAIN DECOMPOSITION

 560  continue
      
      if(xyzswp.eq.1) then
         do i=nzn5,nzn8
            ipp = i - nzn4
            ugrid(i) = 0.d00            
            rho  (i) = rho_mxx  (ipp, j, k)
            u    (i) = u_mxx    (ipp, j, k)
            ut   (i) = ut_mxx   (ipp, j, k)
            utt  (i) = utt_mxx  (ipp, j, k)
            e    (i) = e_mxx    (ipp, j, k)
            p    (i) = p_mxx    (ipp, j, k)
            gamc (i) = gamc_mxx (ipp, j, k)
            game (i) = game_mxx (ipp, j, k)
            dx   (i) = dx_mxx   (ipp      )

            do n = 1, qn
               xn(i,n) = xn_mxx(ipp, j, k, n)
            enddo
         enddo

      elseif(xyzswp.eq.2) then
         do i=nzn5,nzn8
            ipp = i - nzn4
            ugrid(i) = 0.d00            
            rho  (i) = rho_mxy  (j, ipp, k)
            u    (i) = u_mxy    (j, ipp, k)
            ut   (i) = ut_mxy   (j, ipp, k)
            utt  (i) = utt_mxy  (j, ipp, k)
            e    (i) = e_mxy    (j, ipp, k)
            p    (i) = p_mxy    (j, ipp, k)
            gamc (i) = gamc_mxy (j, ipp, k)
            game (i) = game_mxy (j, ipp, k)
            dx   (i) = dx_mxy   (ipp      )
            do n = 1, qn
               xn(i,n) = xn_mxy(j, ipp, k, n)
            enddo
         enddo

      elseif(xyzswp.eq.3) then
         do i=nzn5,nzn8
            ipp = i - nzn4
            ugrid(i) = 0.d00            
            rho  (i) = rho_mxz  (j, k, ipp)
            u    (i) = u_mxz    (j, k, ipp)
            ut   (i) = ut_mxz   (j, k, ipp)
            utt  (i) = utt_mxz  (j, k, ipp)
            e    (i) = e_mxz    (j, k, ipp)
            p    (i) = p_mxz    (j, k, ipp)
            gamc (i) = gamc_mxz (j, k, ipp)
            game (i) = game_mxz (j, k, ipp)
            dx   (i) = dx_mxz   (ipp      )
            do n = 1, qn
               xn(i,n) = xn_mxz(j, k, ipp, n)
            enddo
         enddo
      endif

      goto 600



570   continue
!     REFLECT (with extrapolation to "ghostzones")
!     Author: Miroslav Mocak (last update 07/19/2012)
!
!     get data for extrapolation
      xh(1) = xzn(nx-3)  
      xh(2) = xzn(nx-2) 
      xh(3) = xzn(nx-1)  
      xh(4) = xzn(nx-0)
!     get data for extrapolation
      do i = 1,4
         yh(i,1) = rho  (nzn+i) 
         yh(i,2) = u    (nzn+i) 
         yh(i,3) = e    (nzn+i) 
         yh(i,4) = gamc (nzn+i)
         yh(i,5) = game (nzn+i)
         yh(i,6) = p    (nzn+i)
         do n = 1, qn
            yh(i,n+6) = xn(nzn+i,n)
         enddo
      enddo
!     get position of "ghostzones" in the outer boundary 
      do i = 1,4 
         xhbndry(i) = 9-i
         yhbndry(i) = xzn(nx+i-4)
      enddo
      do i = 1,4
         xps = 5-i
         call polint (xhbndry,yhbndry,3,xps,y_int,dy_i)
         xpos(i) = y_int
      enddo
!     extrapolate data to "ghostzones"
      do ip = 1,6+qn
         do i = 1,4
            yhd(i) = yh(i,ip) 
         enddo
         itmp = 0
         do i = nzn5,nzn8 
            itmp = itmp + 1
            xps = xpos(itmp)
            call polint (xh,yhd,2,xps,y_int,dy_i)
            scrch(itmp) =  y_int
         enddo
         do i = 1,4
            yh(i,ip) = scrch(i)
         enddo
      enddo
!     map extrapolated data to the outer boundary 
      itmp = 0
      do i = nzn5,nzn8 
         itmp = itmp + 1
         rho(i)  =  yh(itmp,1)
         u  (i)  = -yh(itmp,2)
         e    (i) = yh(itmp,3)
         gamc (i) = yh(itmp,4)
         game (i) = yh(itmp,5)
         p    (i) = yh(itmp,6)
      enddo
      nzn54 = nzn5 + nzn4
      do i = nzn5, nzn8
         ut   (i) =  ut   (nzn54-i)
         utt  (i) =  utt  (nzn54-i)
         utbt (i) =  utbt (nzn54-i)
         uttp (i) =  uttp (nzn54-i)
         utlt (i) =  utlt (nzn54-i)
         utrt (i) =  utrt (nzn54-i)
         dx   (i) =  dx   (nzn54-i)
         ugrid(i) = -ugrid(nzn54-i)
      enddo
!     map extrapolated composition to the outer boundary
      do n = 1, qn
         itmp = 0
         do i = nzn5, nzn8
            itmp = itmp + 1
            xn(i,n) = yh(itmp,n+6)
         enddo
      enddo
      
      go to 600



! HSE BOUNDARY
580   continue
      
      if(xyzswp.ne.xyzgrav) then
         if(myid.eq.0) print*,'ERR(bndry_top): no bndmxx=8 for xyzswp.ne.1'
         call MPI_FINALIZE(ierr)
         stop 'ERR(bndry_top): no bndmxx=8 for xyzswp.ne.1'
      endif
      
      nzn54 = nzn5 + nzn4
      do i = nzn5, nzn8
         u    (i) = -u    (nzn54-i)
         ut   (i) =  ut   (nzn54-i)
         utt  (i) =  utt  (nzn54-i)
         utbt (i) =  utbt (nzn54-i)
         uttp (i) =  uttp (nzn54-i)
         utlt (i) =  utlt (nzn54-i)
         utrt (i) =  utrt (nzn54-i)
         dx   (i) =  dx   (nzn54-i)
         ugrid(i) = -ugrid(nzn54-i)
      enddo
      do n = 1, qn
         do i = nzn5, nzn8
            xn(i,n) = xn(nzn4,n)
         enddo
      enddo
      do i = nzn5, nzn8
         xl(i) =      xl(i-1) + dx(i-1)
         xr(i) =      xr(i-1) + dx(i)
         x(i)  = 0.5d00 * (xl(i) + xr(i))
      enddo

      

      !hse extrapolation
      
      if(xyzswp.eq.1) tguess = temp(qx,j,k)
      if(xyzswp.eq.2) tguess = temp(j,qy,k)
      if(xyzswp.eq.3) tguess = temp(j,k,qz)
      do n=1,qn
         if(xyzswp.eq.1) xxxinput(n) = xnuc(qx,j,k,n)
         if(xyzswp.eq.2) xxxinput(n) = xnuc(j,qy,k,n)
         if(xyzswp.eq.3) xxxinput(n) = xnuc(j,k,qz,n)
      enddo
      call composition(XXcomp,YYcomp,ZZcomp,imu,abar,zbar)

      five_fourth = 5.d0/4.d0
      
      if(mstates.eq.2) then ! source term balanced
         if((igrav.eq.1).or.(igrav.eq.6)) then
            do i=nzn5,nzn8
               rho(i) = rho(i-1)
               p  (i) = p(i-1) + 0.5d0*(rho(i)*gravcon*dx(i) + rho(i-1)*gravcon*dx(i-1))
               call geos(inputz,rho(i),tguess,eint,p(i),gmc,gme,cv,cp,  &
                    enx,delta,XXcomp,YYcomp,ZZcomp,imu,abar,zbar,errorkey)
               gamc(i) = gmc
               game(i) = gme
               e   (i) = eint
            enddo
         else
            call MPI_FINALIZE(ierr)
            stop 'ERR(bndry_top): igrav.ne.1.and.mstates.eq.2 not ready'
         endif
      else
         do i=nzn5,nzn8
            if(igrav.eq.6) then
               y = x(i)/onelu 
               grav(i) = g0/(y**five_fourth) 
            endif
            if(igrav.eq.7) then
               grav(i) =  0.d0
            endif
        
            cs2       = gamc(i-1)*p(i-1)/rho(i-1)
            delx      = (dx(i-1)+dx(i))/2.d0
            delp      = rho(i-1)*grav(i)*delx
            rho (i)   = rho(i-1) + delp/cs2
            delp      = (rho(i)+rho(i-1))*0.5d0*delx*grav(i) 
            rho (i)   = rho(i-1) + delp/cs2
            p   (i)   = p(i-1) + delp
            inputz    = 3
            call geos(inputz,rho(i),tguess,eint,p(i),gmc,gme,cv,cp,  &
                 enx,delta,XXcomp,YYcomp,ZZcomp,imu,abar,zbar,errorkey)
            gamc(i) = gmc
            game(i) = gme
            e   (i) = eint
         enddo
      endif
      
      goto 700


!     SET GRID ZONES: nzn5-nzn8
 600  continue
      
      do i = nzn5, nzn8
         xl(i) =      xl(i-1) + dx(i-1)
         xr(i) =      xr(i-1) + dx(i)
         x(i)  = 0.5d00 * (xl(i) + xr(i))
      enddo



700   continue      






      return

      end
