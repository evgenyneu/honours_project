## Report structure

* General introduction about stars and models

* 1D models and what's the science problem? (convection and boundary)

* 2D/3D models how they solve the problem.

* Euler equations in detail

* Read about Flash simulation as well, very similar to PROMPI.

