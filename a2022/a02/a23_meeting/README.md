* Search nasa.ads for PROMPI

citations(bibcode:2007ApJ...667..448M) full:"PROMPI"

Convective boundary: important for stellar structure (temperature gradient) -> changes evolution -> luminosity -> chemical output, mixing irreversible can't un-mix.

Despite problems with simulations we see evidence:

1) Matches Kolmogorov

2) Reaches steady state

3) Looks like turbulence


## Motivations with 2D

* Boundaries with higher resolution look thinner.

* Velocities near boundaries are smaller, things are happening on small scale -> need smaller zones.

## Implicit codes

more computationally costly, but not limited by CFL, can affect physics (like gravity pressure waves, but they are small and might not matter).
