## Simulations data

Table with list of simulations:

* [simulations.csv](a2022/a02/a09_simulations_table/data/simulations.csv)

* [Google sheet](https://docs.google.com/spreadsheets/d/1znFdEr-fjMITI_81TypJiXud7J0XanSaDjH4hkPg9xI/edit?usp=sharing)

