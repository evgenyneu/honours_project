* High/low resolution (refer to code comparison, justifies 264).

* Quantify boundary thickness with numbers.

* Plot distribution of gradient. Discuss the gaussian/non-gaussian shape.

* Plot boundary thickness vs luminosity.

* Kinetic energy 2d/3d.

* Plot horizontal/radial velocities on same plot 2d/3d. - Near boundary 3D becomes 2D (does not move radially).


