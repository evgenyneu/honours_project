from thylacine.reader import get_variable_data
import matplotlib.pyplot as plt
from code_dope.python.plot.plot import save_plot


def plot():
    data_path = (
        'data/a2020/a09/a17/'
        '2020_09_17_09_59_26_192x192x1_2000s_time_1.00_luminosity'
        '/ccptwo.2D.00200.bindata')

    data = get_variable_data(data_path=data_path, variable='0001')
    data = data[:, :, 0]  # Convert to 2D array

    fig, ax = plt.subplots(1, 1)
    ax.set_title(f"Mass fraction of first element")
    pos = ax.imshow(data, cmap='Greys')
    fig.colorbar(pos, ax=ax)
    ax.set_xlabel('y')
    ax.set_ylabel('x')
    ax.invert_yaxis()
    plt.tight_layout()
    save_plot(plt=fig)


if __name__ == '__main__':
    plot()
    print('We are done!')
