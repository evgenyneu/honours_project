# Installing HDF5

I want to learn how to install and use HDF5 (on my Mac laptop).


## Install HDF5 with Homebrew (gfortran)

* `brew install hdf5`


```
gfortran hdf5demo.f90 -lhdf5_fortran -I/usr/local/Cellar/hdf5/1.12.0_1/include -L/usr/local/Cellar/hdf5/1.12.0_1/lib
```

## With HDF5 binary (ifort)

* Downloaded hdf5-1.12.0-Std-osx1013_64-clang.tar.gz from [here](https://www.hdfgroup.org/downloads/hdf5/?https%3A%2F%2Fwww.hdfgroup.org%2Fdownloads%2Fhdf5%2F)

* Extracted into `/Users/evgenii/Downloads/hdf5`.

* `cd a2020/a09/a30_install_hdf5/code/hdf5demo`.

* Build:

```
ifort hdf5demo.f90 -lhdf5_fortran -I/Users/evgenii/Downloads/hdf5/include/static -L/Users/evgenii/Downloads/hdf5/lib -Wl,-rpath,/Users/evgenii/Downloads/hdf5/lib
```


## Compiling prompi2hdf5

I want to compile Simon's program [prompi2hdf5.f90](a2020/a09/a30_install_hdf5/code/prompi2dhf5/prompi2hdf5.f90).

```
gfortran prompi2hdf5.f90 -lhdf5_fortran -I/usr/local/Cellar/hdf5/1.12.0_1/include -L/usr/local/Cellar/hdf5/1.12.0_1/lib
```

Alternatively, user `make` to compile with [Makefile](a2020/a09/a30_install_hdf5/code/prompi2dhf5/Makefile).


## Using Visit locally

### Generate files for Visit

* Copy contents of data/a2020/a09/a17/2020_09_17_09_59_26_192x192x1_2000s_time_1.00_luminosity into a new directory.

* Copy [param.txt](a2020/a09/a30_install_hdf5/code/prompi2dhf5/param.txt).

* Run `./prompi2hdf5` in that directory.

It will create `.h5` and `.xmf` files.

### Open simulation in Visit

* Open Visit version 2.13 on Mac.

* Click "Open", change directory and select *.xmf files.

* Select Plots > Add > Pseudocolor > 0001.

<img src='a2020/a09/a30_install_hdf5/images/visit_screen.png' alt="Visit scren" />

### Creating density difference variable

* Click `Controls > Expressions` and set:

<img src='a2020/a09/a30_install_hdf5/images/density_difference_variable.png' width="500" alt="Desity difference" />

* Plot pseudocolor `density_difference` variable.


### Rotating plot

* Operator Attributes top menu > Transforms > Transform

* `Linear` tab, change

```
(1 0)
(0 1)
```

matrix to

```
(0 1)
(1 0)
```

and click "Apply".


### Transforming colors to see details

* Plot attributes > pseudocolor
* Set Skew to 50.


<img src='a2020/a09/a30_install_hdf5/images/color_transform.png' width="300" alt="Desity difference" />
