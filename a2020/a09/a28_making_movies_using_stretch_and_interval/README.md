# Making movies

I want to play with plotting settings and make movies that show more details.

Code: [i_want_movies.py](a2020/a09/a28_making_movies_using_stretch_and_interval/code/i_want_movies.py)


### Run

```
python python a2020/a09/a28_making_movies_using_stretch_and_interval/code/i_want_movies.py
```

Data saved to: data/a2020/a09/a17/2020_09_17_09_59_26_192x192x1_2000s_time_1.00_luminosity/2020_09_28_movies
