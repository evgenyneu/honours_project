# Make movies from PROMPI data files.

from thylacine.reader import get_variable_data, get_time
import matplotlib.pyplot as plt
import pathlib
import os
import inspect
from tqdm import tqdm
from matplotlib.animation import FFMpegWriter
import numpy as np
from astropy.visualization import ImageNormalize, LinearStretch


def this_dir():
    """
    Returns
    -------
    str
        Directory of the current code file

    """
    frame = inspect.stack()[1]
    module = inspect.getmodule(frame[0])
    codefile = module.__file__
    return os.path.dirname(codefile)


def create_dir(dir):
    """Creates a directory if it does not exist.

    Parameters
    ----------
    dir : str
        Directory path, can be nested directories.

    """

    if not os.path.exists(dir):
        os.makedirs(dir)


def data_files(data_dir):
    """Returns the list of paths to PROMPI data time in the given directory.

    Parameters
    ----------
    data_dir : str
        Path to directory.

    Returns
    -------
    list of str
        Paths to PROMPI data files.

    """

    paths = [str(i) for i in pathlib.Path(data_dir).glob('*.bindata')]
    return sorted(paths)


def prepare_for_animation(title):
    """Prepares for animation by creating matplotlib plot object
    and the data objects that will be updated later at each frame

    Returns
    -------
    (fig, ax, image, text)
        fig
            Matplotlib's figure object.
        ax
            Matplotlib's axis
        image
            The object returned by imshow and containing the plotted data.
        text
            The object returned by Matplotlib's text() function and containing
            the current time text.

    """

    # Create the plot
    # -------

    fig, ax = plt.subplots(1, 1)
    ax.set_title(title)
    data = np.zeros(shape=(1, 1))
    image = ax.imshow(data, cmap='Greys')
    fig.colorbar(image, ax=ax)
    ax.set_xlabel('y')
    ax.set_ylabel('x')

    # Show time
    # -------

    text = ax.text(
        0.05, 0.95,
        f'',
        horizontalalignment='left',
        verticalalignment='top',
        transform=ax.transAxes,
        bbox=dict(facecolor='white', alpha=0.8, edgecolor='0.7', boxstyle='round'))

    fig.tight_layout()

    return (fig, ax, image, text)


def init_plot(fig, ax, image, data):
    """Method for initializing the plot from the data. It is called once
    before the first frame.

    Parameters
    ----------
    fig
        Matplotlib's figure.
    ax
        Matplotlib's axis.
    image : type
        The object returned by imshow and containing the plotted data.
    data : numpy.ndarray
        Variable data from PROMPI file.

    Returns
    -------
    type
        Description of returned object.

    """
    image.set_extent([0, data.shape[0] - 1, data.shape[1] - 1, 0])
    ax.invert_yaxis()


def get_color_limits(current, previous, color_frames=10):
    """Update the color limits if they are wider than those from
    the previous frames. The point is to prevent using different
    color limits for each frame to avoid jumping of the valus on
    the color bar. Use the widest color range from last
    color_frames frames.

    Parameters
    ----------
    current : tuple
        Current color limits (vmin, vmax)
    previous : dict
        Dictionary containing the previous color limits.
        Keys: 'vmin', 'vmax'.
        Values: lists of float numbers. Size of the list if not larger than
        color_frames.
    color_frames : int
        The maximum number of frames to keep for previous limits.

    Returns
    -------
    type
        Description of returned object.

    """
    vmin = previous['vmin']
    vmax = previous['vmax']

    # Add current color limits to the previous arrays
    # ------

    vmin.append(current[0])
    vmax.append(current[1])

    # Do not keep more than color_frames values in the `previous` arrays
    # ------

    if len(vmin) > color_frames:
        vmin.pop(0)

    if len(vmax) > color_frames:
        vmax.pop(0)

    # Return lower and upper boundaries for the color range.
    # The low boundary is the minimum of last color_frames values.
    # The upper boundary is the maximum of last color_frames values.
    return min(vmin), max(vmax)


def set_color_limits(image, data, interval, clim, previous_clim):
    """Set to color limits.

    Parameters
    ----------
    image
        The image object returned by Matplotlib's imshow functions.
    data : numpy.ndarray
        Variable data from PROMPI file.
    interval
        An interval passed to ImageNormalize object, like PercentileInterval.
        It is used to exclude extreme values and see more details in the plots.
    clim : dict
        Fixed color limits for the plot supplied by the user.
        If None, the limits are automatically determined for each frame
        form the data.
    previous_clim : type
        Color limits used for the previous frames of the plot.

    """
    vmin = None
    vmax = None

    # Get current color limits
    # ---------

    if clim is not None:
        # Apply the color bar limits supplied by the user
        vmin = clim['vmin']
        vmax = clim['vmax']
    else:
        if interval is None:
            # Set the color limits based on the data range
            vmin = data.min()
            vmax = data.max()
        else:
            # Set the color limits based on the literval
            limits = interval.get_limits(data)
            vmin = limits[0]
            vmax = limits[1]

    # Get updated color limits based on previous frames to prevent
    # limits from changing too frequently
    (vmin, vmax) = get_color_limits(current=(vmin, vmax),
                                    previous=previous_clim)

    if vmin is not None:
        image.set_clim(vmin=vmin, vmax=vmax)


def animate(data_path, image, text, variable, data,
            interval, stretch, clim, previous_clim):
    """Function called at each animation frame to update it.

    Parameters
    ----------
    data_path : str
        Path to PROMPI data file.
    image
        The object returned by imshow and containing the plotted data.
    text
        The object returned by Matplotlib's text() function and containing
        the current time text.
    variable : str
        Variable to plot from PROMPI data outout, for example 'energy'.
    data: numpy.ndarray
        Data to be shown.
    interval
        An interval passed to ImageNormalize object, like PercentileInterval.
        It is used to exclude extreme values and see more details in the plots.
    stretch
        A stretch passed to ImageNormalize object, like PowerStretch.
        This is used to transform the data and highlight specific regions.
    clim : dict
        Fixed color limits for the plot supplied by the user.
        If None, the limits are automatically determined for each frame
        form the data.
    previous_clim : dict
        Color limits used for the previous frames of the plot.

    """

    if interval is not None or stretch is not None:
        # Set interval to exlude extreme values and see more details.
        # The stretch is used to highlight specific regions of the data.

        if stretch is None:
            stretch = LinearStretch()

        norm = ImageNormalize(data, interval=interval, stretch=stretch)
        image.set_norm(norm)

    set_color_limits(image=image, data=data, interval=interval, clim=clim,
                     previous_clim=previous_clim)

    image.set_data(data)

    # Update time
    # -------

    time = get_time(data_path)
    text.set_text(f't = {int(time)} s')

    return image, text, variable


def load_data(data_path, variable, change):
    """Loads data for a single variable from PROMPI data file.

    Parameters
    ----------
    data_path : str
        Path to PROMPI data file.
    variable : str
        Name of PROMPI variable ('pressure', 'energy' etc.).
    change : str
        change: str
            Indicates if change in data needs to be shown. Expected values:
            * None: data is shown as it is.
            * 'absolute': Calculate difference between the average
                along the x values and the value: avg(x) - value
            * 'relative': Calculate relative difference between the average
                along the x values and the value: 1 - value / avg(x)

    Returns
    -------
    numpy.ndarray
        A 2D array containing data for the variable.

    """

    if variable == 'speed':
        # Load x, y components of velocity
        velx = get_variable_data(data_path=data_path, variable='velx')[:, :, 0]
        vely = get_variable_data(data_path=data_path, variable='vely')[:, :, 0]

        # Calculate speed
        data = np.sqrt(velx**2 + vely**2)
    else:
        data = get_variable_data(data_path=data_path, variable=variable)
        data = data[:, :, 0]  # Convert to 2D array

    # Calculate change from the mean if needed
    # --------

    if change == 'absolute':
        # Calculate average along x axis and subtract it from each value.
        data -= np.array([data.mean(axis=1)]).transpose()
    elif change == 'relative':
        # Calculate average along x axis
        mean = np.array([data.mean(axis=1)]).transpose()

        # Subtract average from each value to calculate absolute perturbation
        data -= mean

        # Divide the absolute perturbation by the average to get relative
        # change
        data /= mean

        # If some numbers were divided by zeros, replace NaN with zero.
        data = np.nan_to_num(data)
    elif change is None:
        pass
    else:
        raise ValueError(f'Unexpected value of "change" argument: {change}')

    return data


def variable_movie(title, data_dir, movie_dir, movie_name, max_frames,
                   frame_rate, variable, clim, silent,
                   interval, stretch, change):
    """Make plots for all data files for single variable and create a movie.

    Parameters
    ----------
    data_dir : str
        Directory containing PROMPI data files.
    movie_dir : str
        Directory where the movie will be saved
    movie_name : str
        Name of the output movie file.
    max_frames : int
        Maximum number of frames in the movie. Each frame is a plot of one
        data file.
    frame_rate : int
        The frame rate of the movie, in frames per second.
    variable : str
        Variable to plot from PROMPI data outout, for example 'energy'.
    clim : dict
        Fixed color limits for the plot supplied by the user.
        If None, the limits are automatically determined for each frame
        form the data.
    silent : bool
        If True, do not show any output (used in unit tests).
    interval
        An interval passed to ImageNormalize object, like PercentileInterval.
        It is used to exclude extreme values and see more details in the plots.
    stretch
        A stretch passed to ImageNormalize object, like PowerStretch.
        This is used to transform the data and highlight specific regions.
    change: str
        Indicates if change in data needs to be shown. Expected values:
        * None: data is shown as it is.
        * 'absolute': Calculate difference between the average
            along the x values and the value: avg(x) - value
        * 'relative': Calculate relative difference between the average
            along the x values and the value: 1 - value / avg(x)

    """

    if not silent:
        print(f"Creating movie from PROMPI output for '{variable}'...")

    # Create the ffmpeg writer
    writer = FFMpegWriter(fps=frame_rate,
                          metadata=dict(artist='Evgenii Neumerzhitckii'),
                          bitrate=1800)

    # Create Matplotlib's plot object and its data
    fig, ax, image, text = prepare_for_animation(title=title)

    # Get the paths of PROMPI data files
    # -------

    create_dir(movie_dir)
    movie_path = os.path.join(movie_dir, movie_name)
    data_paths = data_files(data_dir)
    data_paths = data_paths[:max_frames]
    previous_clim = dict(vmin=[], vmax=[])

    # Save the movie to disk
    with writer.saving(fig, movie_path, dpi=300):
        # Iterate over the data files and add them to the movie
        for i, data_path in enumerate(tqdm(data_paths, disable=silent)):
            data = load_data(data_path=data_path, variable=variable,
                             change=change)

            if i == 0:
                init_plot(fig=fig, ax=ax, image=image, data=data)

            animate(data_path=data_path, image=image, text=text,
                    variable=variable, data=data,
                    interval=interval, stretch=stretch,
                    clim=clim, previous_clim=previous_clim)

            writer.grab_frame()

    plt.close(fig)

    if not silent:
        print('Saved movie to')
        print(movie_path)


def many_variables(title, variables, data_dir, movie_dir, max_frames,
                   frame_rate, silent):
    """Make movies for multiple variables from PROMPI data files.

    Parameters
    ----------
    title : str
        Plot title.
    variables : list of dict
        Dictionary containing the name of the variable

        Examples:
        [{"name": "density"}]

        Specify color limits:
        [{'name': "0001", 'clim': {'vmin': 0, 'vmax': 1}}]
    data_dir : str
        Directory containing PROMPI data files.
    movie_dir : str
        Directory where the movie will be saved
    movie_name : str
        Name of the output movie file.
    max_frames : int
        Maximum number of frames in the movie. Each frame is a plot of one
        data file.
    frame_rate : int
        The frame rate of the movie, in frames per second.
    silent : bool
        If True, do not show any output (used in unit tests).

    """

    for i, variable in enumerate(variables):
        variable_name = variable['name']
        title_with_variable = f"{title} {variable_name}".strip()
        clim = variable.get('clim')
        interval = variable.get('interval')
        stretch = variable.get('stretch')
        change = variable.get('change')

        variable_movie(title=title_with_variable, data_dir=data_dir,
                       movie_dir=movie_dir,
                       movie_name=f'{i+1:03}_{variable_name}.mp4',
                       max_frames=max_frames,
                       frame_rate=frame_rate, variable=variable_name,
                       clim=clim, silent=silent,
                       interval=interval,
                       stretch=stretch,
                       change=change)
