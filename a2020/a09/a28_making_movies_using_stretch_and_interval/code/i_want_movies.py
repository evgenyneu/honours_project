import os

from astropy.visualization import (
    ImageNormalize, LinearStretch, PercentileInterval, AsinhStretch,
    SinhStretch)

from make_movie import many_variables, this_dir

if __name__ == '__main__':
    movie_dir = os.path.join(this_dir(), 'movies')

    data_dir = (
        'data/a2020/a09/a17/'
        '2020_09_17_09_59_26_192x192x1_2000s_time_1.00_luminosity')

    variables = [
        {
            'name': "density",
            'interval': PercentileInterval(92),
            'stretch': AsinhStretch(a=0.49),
            'change': 'relative'
        },
        {
            'name': "speed",
            'interval': PercentileInterval(93),
            'stretch': AsinhStretch(a=0.35),
            'change': 'relative'
        },
        {
            'name': "velx",
            'interval': PercentileInterval(90)
        },
        {
            'name': "energy",
            'interval': PercentileInterval(90),
            'stretch': AsinhStretch(a=0.40),
            'change': 'relative'
        },
        {
            'name': "press",
            'interval': PercentileInterval(85),
            'stretch': AsinhStretch(a=0.46),
            'change': 'relative'
        },
        {
            'name': "temp",
            'interval': PercentileInterval(90),
            'stretch': AsinhStretch(a=0.17),
            'change': 'relative'
        },
        {'name': "enuc1"},
        {
            'name': "0001",
            'interval': PercentileInterval(95),
            'stretch': SinhStretch(a=0.03)
        },
        {
            'name': "0002",
            'interval': PercentileInterval(85),
            'stretch': AsinhStretch(a=0.005)
        }
    ]

    many_variables(title="",
                   variables=variables, data_dir=data_dir, movie_dir=movie_dir,
                   max_frames=10_000, frame_rate=5, silent=False)

    print('We are done!')
