## Using `irecl_float = 1` option.

Yesterday I compiled code with `irecl_float = 1` option (see [commit](https://github.com/evgenyneu/PROMPI/commit/64d95988adc48a0740aefc74e4d014e956f1ed8a)) in order to see if it removed empty data blocks from the output file.

### Result

* The data file generated with `irecl_float = 1` is four times smaller and does not contain blocks with zeros. This is what we want.

* The plots look identical:

![Four byte](a2020/a09/a17_reading_prompi_output/code/plots/a010_irecl_float_1_4_byte_record_length.png)

![One byte](a2020/a09/a17_reading_prompi_output/code/plots/a010_irecl_float_1_irecl_float_1_length.png)

Code: [a010_irecl_float_1.py](a2020/a09/a17_reading_prompi_output/code/a010_irecl_float_1.py).


## Run PROMPI

I want to run PROMPI for 2000 sec with new `irecl_float = 1` setting, since the previous run had output files four times larger than they need to be

[PROMPI code](https://github.com/evgenyneu/PROMPI/commit/2e965dd95eea8c4fcbf804407f76ec121fd3d2dd)

Run script:

```
. slurm_jobs/2020/09/17/jobs/010_2000s_run_1x_luminosity.sh
```

## Run ransX

I want to learn how to run ransX to make plots for the simulation I just ran.

Create Python 2.7 environment

```
conda create --name ransX python=2.7
```

Install Python packages

```
pip install -r requirements.txt
```

with [requirements.txt](a2020/a09/a17_reading_prompi_output/requirements.txt).


### Activate environment


```
conda activate ransX
```

### Running ransX

See [running_ransx.md](running_ransx.md).
