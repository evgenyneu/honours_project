import os
import numpy as np
import matplotlib.pyplot as plt
import ransX.PROMPI_data as prd
from code_dope.python.plot.plot import save_plot


def plot_variable_fraction(file_path, var_name, plot_title, plot_suffix,
                           record_length):
    """Plot a variable from PROMPI data file.

    Parameters
    ----------
    file_path : str
        Path to data file.
    var_name : str
        A variable name, e.g. '0002'
    plot_title : str
        Plot title.
    plot_suffix : str
        A suffix to be added to the file name of the plot image.
    record_length : type
        The length of the record in bytes: 4 for default (containing blanks)
        and 1 for the new one.
    """

    block2D = prd.PROMPI_bindata(file_path, [var_name], record_length)

    # Time in sec
    time_2D = block2D.datadict['time']

    # Fraction of the second element
    x0002_2D = block2D.datadict[var_name]

    # Convert to 2D array
    x00022D = np.asarray(x0002_2D[:, :, 0])

    # Plot
    # --------

    fig, ax = plt.subplots(1, 1)
    ax.set_title(f"{plot_title} {round(time_2D, 1)} s")
    pos = ax.imshow(x00022D, cmap="YlOrBr")
    fig.colorbar(pos, ax=ax)
    ax.set_xlabel('y')
    ax.set_ylabel('x')
    plt.tight_layout()
    save_plot(plt=fig, suffix=plot_suffix)


def plot():
    """Plot two PROMPI datafiles made using the default decord length
    and irecl_float_1=1 settings.

    """

    data_dir = 'data/a2020/a09/a17'

    data_path = os.path.join(data_dir, 'default_record_length',
                             'ccptwo.2D.00005.bindata')

    plot_variable_fraction(file_path=data_path, var_name='0002',
                           plot_title='Mass fraction, four byte record length',
                           plot_suffix='4_byte_record_length', record_length=4)

    # Plot irecl_float_1=1
    # ---------

    data_path = os.path.join(data_dir, 'irecl_float_1',
                             'ccptwo.2D.00005.bindata')

    plot_variable_fraction(file_path=data_path, var_name='0002',
                           plot_title='Mass fraction, with irecl_float_1=1',
                           plot_suffix='irecl_float_1_length', record_length=1)


if __name__ == '__main__':
    plot()
    print('We are done!')
