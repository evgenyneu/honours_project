# Running ransX

I attempted to run ransX.


## Running `ransX_single.py`

* Changed `datafile` and `toplot` parameters in [PARAMS/param.single](a2020/a09/a17_reading_prompi_output/ransx/a01_first_attempt/param.single).

* Ran `python ransX_single.py`

* Five plots were created in RESULT directory: x0001.png, x0002.png, ux.png, dd.png, hse3_ccptwo_256cubed_evolved.png:

![x0002](a2020/a09/a17_reading_prompi_output/ransx/a01_first_attempt/plots/x0002.png)

* The program crashed, the `x0003` variable is hardcoded but we don't have in our output:

```
UTILS/PROMPI/PROMPI_single.py", line 541, in PlotNucEnergyGen
   xhe4 = self.data['x0003']
KeyError: 'x0003'
```


##  Running `ransX_tseries.py`

* Changed `datadir`, `dataout` and `trange` parameters in [PARAMS/param.tseries](a2020/a09/a17_reading_prompi_output/ransx/a01_first_attempt/param.tseries).

* Ran `python ransX_tseries.py`:

```
('Number of snapshots: ', 401)
('Available time range:', 0.0, 1999.997)
('Restrict data to time range:', 0.0, 2000.0)
('Number of time averaged snapshots: ', 395)
('Averaged time range: ', 15.001, 1989.997)
('nx', 192)
```

* The program created an .npy file at `/Users/evgenii/Downloads/tseries_2020_09_17_09_59_26_192x192x1_2000s_time_1.00_luminosity.npy`.


## Running `ransX.py`

* Changed `eht_data` parameter in [PARAMS/param.ransx](a2020/a09/a17_reading_prompi_output/ransx/a01_first_attempt/param.ransx).

* For the individual plots I want to see in param.ransx, I changed setting to `True`.

* The program generated the plots in the RESULTS dir:

![ccptwo_tke_eq](a2020/a09/a17_reading_prompi_output/ransx/a01_first_attempt/plots/ccptwo_tke_eq.png)
