## Running PROMPI on Magnus

I want to run PROMPI on Magnus for the first time. Before that I make [code changes](https://github.com/evgenyneu/PROMPI/commit/2395e577fe7ddae562ba6565223bc6c95c97ea15):

* Reduce luminosity by half.

* Output every 5 sec.


## Compile and run

```
./slurm_jobs/2020/09/15/compile_and_run.sh /scratch/ew6/evgenyneu/PROMPI/setups/ccp_two_layers/2d/src
```


### Run separately

Compile:

```
sbatch ~/slurm_jobs/2020/09/15/compile_prompi.sh /scratch/ew6/evgenyneu/PROMPI/setups/ccp_two_layers/2d/src
```

Run:

```
sbatch ~/slurm_jobs/2020/09/15/run_prompi.sh /scratch/ew6/evgenyneu/PROMPI/setups/ccp_two_layers/2d/src
```

The output is created at:

* /scratch/ew6/evgenyneu/prompi_output/

* /group/ew6/evgenyneu/prompi_output/
