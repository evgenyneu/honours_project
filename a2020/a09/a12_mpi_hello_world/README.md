## Change grid resolution

Previous grid resolution was wrong (based on 16 cores per node while Magnus has 24). Fixing the resolution using [Simon's Wiki](https://stellarmeme.net/wiki/doku.php?id=installing_prompi):

* [inlist_prompi](a2020/a09/a12_mpi_hello_world/magnus/inlist_prompi).

* [src/DIMEN/dimen.inc](a2020/a09/a12_mpi_hello_world/magnus/dimen.inc).

* [evgenii.job](a2020/a09/a12_mpi_hello_world/magnus/evgenii.job).


### Compile

```
ssh pawsey
cd /scratch/ew6/evgenyneu/PROMPI/setups/ccp_two_layers/2d/src
make clean
make prompi
```


## MPI Hello World

* Created a [Hello World MPI program](https://github.com/evgenyneu/fortran_mpi_hello_world) on Magnus to get used to compiling, running code and working with jobs.

* Compiled and ran it on my Mac, Ubuntu and Magnus.


### Pawsey accounting

Show SU (service units) left

```
pawseyAccountBalance -p ew6 -users
```

Show previous jobs

```
sacct
sacct -j [job id]
```
