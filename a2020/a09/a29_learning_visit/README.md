# Learning Visit

I want to install and run Visit locally on my Mac (macOS 10.15.6)


## Download Visit

* Open [download page](https://wci.llnl.gov/simulation/computer-codes/visit/executables).

* [Download](https://github.com/visit-dav/visit/releases/download/v2.13.1/VisIt-2.13.1-10.12.dmg) version VisIt 2.13.1 for Mac OS X 10.12 - Intel 64 bit Darwin 10.12.


## Convert from PROMPI to Visit

I want to learn how to use [Simon's program](a2020/a09/a29_learning_visit/code/prompi2visit/prompi2hdf5-cartesian2D.f90).


### Download hdf5 binary

* Download hdf5-1.12.0-Std-osx1013_64-clang.tar.gz from [here](https://www.hdfgroup.org/downloads/hdf5/?https%3A%2F%2Fwww.hdfgroup.org%2Fdownloads%2Fhdf5%2F)

* Run `./HDF5-1.12.0-Darwin.sh` to extract the files.


### Build prompi2visit

cd to `a2020/a09/a29_learning_visit/code/prompi2visit`

and run:

```
ifort prompi2hdf5-cartesian2D.f90 -I/Users/evgenii/Downloads/hdf5/include/shared /Users/evgenii/Downloads/hdf5/lib/libhdf5_fortran.200.0.0.dylib
```

### Run hdf5 tests (optional)

Run tests to make sure hdf5 is working:

* Intall cmake `brew install cmake`.

* Create a directory and copy share/examples into it

* Run tests:

```
ctest -S HDF5_Examples.cmake,INSTALLDIR=/Users/evgenii/Downloads/hdf5 -C Release -V -O test.log
```

where `/Users/evgenii/Downloads/hdf5` is install directory


### Download hdf5 library (did not work)

Simon's code uses HDF5 library to write data into a binary file with specific format.

* Download from hdf5-1.12.0.zip from [here](https://www.hdfgroup.org/downloads/hdf5/source-code/).

* Run `./configure`, exited with errors
