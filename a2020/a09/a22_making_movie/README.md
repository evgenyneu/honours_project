# Making a movie from PROMPI output

## Creating individual images

Code: [make_movie.py](a2020/a09/a22_making_movie/code/make_movie.py)

Run time: 6 m 26 s.


## Without creating individual images

Movie files: data/a2020/a09/a17/2020_09_17_09_59_26_192x192x1_2000s_time_1.00_luminosity/2020_09_22_movies

Code: [make_movie_animator.py](a2020/a09/a22_making_movie/code/make_movie_animator.py)

Run time: 1 m 10 s.

Run:

```
python a2020/a09/a22_making_movie/code/make_movie_animator.py
```
