from thylacine.reader import get_variable_data, get_time
import matplotlib.pyplot as plt
import pathlib
import os
import inspect
from tqdm import tqdm
from matplotlib.animation import FFMpegWriter
import numpy as np


def this_dir():
    """
    Returns
    -------
    str
        Directory of the current code file

    """
    frame = inspect.stack()[1]
    module = inspect.getmodule(frame[0])
    codefile = module.__file__
    return os.path.dirname(codefile)


def create_dir(dir):
    """Creates a directory if it does not exist.

    Parameters
    ----------
    dir : str
        Directory path, can be nested directories.

    """

    if not os.path.exists(dir):
        os.makedirs(dir)


def data_files(data_dir):
    """Returns the list of paths to PROMPI data time in the given directory.

    Parameters
    ----------
    data_dir : str
        Path to directory.

    Returns
    -------
    list of str
        Paths to PROMPI data files.

    """

    paths = [str(i) for i in pathlib.Path(data_dir).glob('*.bindata')]
    return sorted(paths)


def prepare_for_animation(title):
    """Prepares for animation by creating matplotlib plot object
    and the data objects that will be updated later at each frame

    Returns
    -------
    (fig, ax, image, text)
        fig
            Matplotlib's figure object.
        ax
            Matplotlib's axis
        image
            The object returned by imshow and containing the plotted data.
        text
            The object returned by Matplotlib's text() function and containing
            the current time text.

    """
    fig, ax = plt.subplots(1, 1)
    ax.set_title(title)
    image = ax.imshow(np.zeros(shape=(1, 1)), cmap='Greys')
    fig.colorbar(image, ax=ax)
    ax.set_xlabel('y')
    ax.set_ylabel('x')

    # Show time
    # -------

    text = ax.text(
        0.05, 0.95,
        f'',
        horizontalalignment='left',
        verticalalignment='top',
        transform=ax.transAxes,
        bbox=dict(facecolor='white', alpha=0.8, edgecolor='0.7', boxstyle='round'))

    fig.tight_layout()

    return (fig, ax, image, text)


def init_plot(fig, ax, image, data, clim):
    """Method for initializing the plot from the data. It is called once
    before the first frame.

    Parameters
    ----------
    fig
        Matplotlib's figure.
    ax
        Matplotlib's axis.
    image : type
        The object returned by imshow and containing the plotted data.
    data : numpy.ndarray
        Variable data from PROMPI file.
    clim : dict
        Color limits for the plot. If None, the limits are automatically
        determined for each frame form the data.

    Returns
    -------
    type
        Description of returned object.

    """
    image.set_extent([0, data.shape[0] - 1, data.shape[1] - 1, 0])

    if clim is not None:
        image.set_clim(vmin=clim['vmin'], vmax=clim['vmax'])

    ax.invert_yaxis()


def animate(data_path, image, text, variable, data, clim):
    """Function called at each animation frame to update it.

    Parameters
    ----------
    data_path : str
        Path to PROMPI data file.
    image
        The object returned by imshow and containing the plotted data.
    text
        The object returned by Matplotlib's text() function and containing
        the current time text.
    variable : str
        Variable to plot from PROMPI data outout, for example 'energy'.
    data: numpy.ndarray
        Data to be shown.
    clim : dict
        Color limits for the plot. If None, the limits are automatically
        determined for each frame form the data.

    """
    # Update data
    # -------

    if clim is None:
        image.set_clim(vmin=data.min(), vmax=data.max())

    image.set_data(data)

    # Update time
    # -------

    time = get_time(data_path)
    text.set_text(f't = {int(time)} s')

    return image, text, variable


def variable_movie(title, data_dir, movie_dir, movie_name, max_frames,
                   frame_rate, variable, clim, silent):
    """Make plots for all data files for single variable and create a movie.

    Parameters
    ----------
    data_dir : str
        Directory containing PROMPI data files.
    movie_dir : str
        Directory where the movie will be saved
    movie_name : str
        Name of the output movie file.
    max_frames : int
        Maximum number of frames in the movie. Each frame is a plot of one
        data file.
    frame_rate : int
        The frame rate of the movie, in frames per second.
    variable : str
        Variable to plot from PROMPI data outout, for example 'energy'.
    clim : dict
        Color limits for the plot. If None, the limits are automatically
        determined for each frame form the data.
    silent : bool
        If True, do not show any output (used in unit tests).

    """

    if not silent:
        print(f"Creating movie from PROMPI output for '{variable}'...")

    # Create the ffmpeg writer
    writer = FFMpegWriter(fps=frame_rate,
                          metadata=dict(artist='Evgenii Neumerzhitckii'),
                          bitrate=1800)

    # Create Matplotlib's plot object and its data
    fig, ax, image, text = prepare_for_animation(title=title)

    # Get the paths of PROMPI data files
    # -------

    create_dir(movie_dir)
    movie_path = os.path.join(movie_dir, movie_name)
    data_paths = data_files(data_dir)
    data_paths = data_paths[:max_frames]

    # Save the movie to disk
    with writer.saving(fig, movie_path, dpi=300):
        # Iterate over the data files and add them to the movie
        for i, data_path in enumerate(tqdm(data_paths, disable=silent)):
            data = get_variable_data(data_path=data_path, variable=variable)
            data = data[:, :, 0]  # Convert to 2D array

            if i == 0:
                init_plot(fig, ax, image, data, clim)

            animate(data_path=data_path, image=image, text=text,
                    variable=variable, data=data, clim=clim)

            writer.grab_frame()

    plt.close(fig)

    if not silent:
        print('Saved movie to')
        print(movie_path)


def many_variables(title, variables, data_dir, movie_dir, max_frames,
                   frame_rate, silent):
    """Make movies for multiple variables from PROMPI data files.

    Parameters
    ----------
    title : str
        Plot title.
    variables : list of dict
        Dictionary containing the name of the variable

        Examples:
        [{"name": "density"}]

        Specify color limits:
        [{'name': "0001", 'clim': {'vmin': 0, 'vmax': 1}}]
    data_dir : str
        Directory containing PROMPI data files.
    movie_dir : str
        Directory where the movie will be saved
    movie_name : str
        Name of the output movie file.
    max_frames : int
        Maximum number of frames in the movie. Each frame is a plot of one
        data file.
    frame_rate : int
        The frame rate of the movie, in frames per second.
    silent : bool
        If True, do not show any output (used in unit tests).

    """

    for i, variable in enumerate(variables):
        variable_name = variable['name']
        title_with_variable = f"{title} {variable_name}".strip()
        clim = variable.get('clim')

        variable_movie(title=title_with_variable, data_dir=data_dir,
                       movie_dir=movie_dir,
                       movie_name=f'{i+1:03}_{variable_name}.mp4',
                       max_frames=max_frames,
                       frame_rate=frame_rate, variable=variable_name,
                       clim=clim, silent=silent)


if __name__ == '__main__':
    movie_dir = os.path.join(this_dir(), 'movies')

    data_dir = (
        'data/a2020/a09/a17/'
        '2020_09_17_09_59_26_192x192x1_2000s_time_1.00_luminosity')

    variables = [
        {'name': "density"},
        {'name': "velx"},
        {'name': "vely"},
        {'name': "energy"},
        {'name': "press"},
        {'name': "temp"},
        {'name': "gam1"},
        {'name': "gam2"},
        {'name': "enuc1"},
        {'name': "enuc2"},
        {'name': "0001", 'clim': {'vmin': 0, 'vmax': 1}},
        {'name': "0002", 'clim': {'vmin': 0, 'vmax': 1}}
    ]

    many_variables(title="",
                   variables=variables, data_dir=data_dir, movie_dir=movie_dir,
                   max_frames=10_000, frame_rate=5, silent=False)

    print('We are done!')
