from make_movie_animator import (
    data_files, this_dir, variable_movie, many_variables)

import os
import shutil

DATA_DIR = (
    'data/a2020/a09/a17/'
    '2020_09_17_09_59_26_192x192x1_2000s_time_1.00_luminosity')


def test_data_files():
    result = data_files(DATA_DIR)

    assert len(result) == 401

    assert result[0] == os.path.join(DATA_DIR, 'ccptwo.2D.00001.bindata')
    assert result[1] == os.path.join(DATA_DIR, 'ccptwo.2D.00002.bindata')
    assert result[400] == os.path.join(DATA_DIR, 'ccptwo.2D.00401.bindata')


def test_variable_movie():
    movie_dir = os.path.join(this_dir(), 'test_movies')

    if os.path.exists(movie_dir):
        shutil.rmtree(movie_dir)

    movie_name = 'test_movie.mp4'

    variable_movie(title="Test title", data_dir=DATA_DIR, movie_dir=movie_dir,
                   movie_name=movie_name,
                   max_frames=3, frame_rate=5, variable='0001', clim=None,
                   silent=True)

    expected_path = os.path.join(movie_dir, movie_name)
    assert os.path.isfile(expected_path)
    shutil.rmtree(movie_dir)


def test_many_variables():
    movie_dir = os.path.join(this_dir(), 'test_movies')

    if os.path.exists(movie_dir):
        shutil.rmtree(movie_dir)

    variables = [
        {'name': "temp"},
        {'name': "0001", 'clim': {'vmin': 0, 'vmax': 1}}
    ]

    many_variables(title="hello", variables=variables,
                   data_dir=DATA_DIR, movie_dir=movie_dir,
                   max_frames=3, frame_rate=5, silent=True)

    assert os.path.isfile(os.path.join(movie_dir, '001_temp.mp4'))
    assert os.path.isfile(os.path.join(movie_dir, '002_0001.mp4'))
    shutil.rmtree(movie_dir)
