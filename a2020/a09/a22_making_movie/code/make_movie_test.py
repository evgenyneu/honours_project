from make_movie import (
    data_files, plot, this_dir, plot_and_make_movie, lets_gooooooo)

import os
import shutil

DATA_DIR = (
    'data/a2020/a09/a17/'
    '2020_09_17_09_59_26_192x192x1_2000s_time_1.00_luminosity')


def test_data_files():
    result = data_files(DATA_DIR)

    assert len(result) == 401

    assert result[0] == os.path.join(DATA_DIR, 'ccptwo.2D.00001.bindata')
    assert result[1] == os.path.join(DATA_DIR, 'ccptwo.2D.00002.bindata')
    assert result[400] == os.path.join(DATA_DIR, 'ccptwo.2D.00401.bindata')


def test_plot():
    data_path = os.path.join(DATA_DIR, 'ccptwo.2D.00123.bindata')
    plot_dir = os.path.join(this_dir(), 'plot_test')

    if os.path.exists(plot_dir):
        shutil.rmtree(plot_dir)

    plot(data_path=data_path, plot_dir=plot_dir, variable='0001')

    expected_path = os.path.join(plot_dir, 'ccptwo.2D.00123.bindata.png')
    assert os.path.isfile(expected_path)
    shutil.rmtree(plot_dir)


def test_plot_and_make_movie():
    names = [
        'ccptwo.2D.00101.bindata',
        'ccptwo.2D.00102.bindata',
        'ccptwo.2D.00103.bindata'
    ]

    data_paths = [os.path.join(DATA_DIR, name) for name in names]
    movie_dir = os.path.join(this_dir(), 'test_movie')

    if os.path.exists(movie_dir):
        shutil.rmtree(movie_dir)

    plot_and_make_movie(data_paths=data_paths, variable='0001',
                        movie_dir=movie_dir, movie_name='test_movie.mp4',
                        frame_rate=10, silent=True)

    expected_path = os.path.join(movie_dir, 'test_movie.mp4')
    assert os.path.isfile(expected_path)
    shutil.rmtree(movie_dir)


def test_lets_gooooooo():
    movie_dir = os.path.join(this_dir(), 'test_movies')

    if os.path.exists(movie_dir):
        shutil.rmtree(movie_dir)

    movie_name = 'test_movie.mp4'

    lets_gooooooo(data_dir=DATA_DIR, movie_dir=movie_dir, movie_name=movie_name,
                  max_frames=3, frame_rate=5, silent=True)

    expected_path = os.path.join(movie_dir, movie_name)
    assert os.path.isfile(expected_path)
    shutil.rmtree(movie_dir)
