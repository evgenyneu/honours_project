## Running `calcX_evol.py`

Today I [continue](a2020/a09/a17_reading_prompi_output/running_ransx.md) learning how to run ransX programs.

## Initial run `calcX_evol.py` cancelled: took too long

* I changed in `eht_data`, `dataout`, `tkeevol` parameters in [PARAMS/param.evol](a2020/a09/a18_running_ransx/ransx_param/param.evol.initial).

* Ran `python calcX_evol.py`.

It was taking very long time (long than 30 min) to calculate, so I cancelled that.


## Run `ransX_tseries.py` for shorter time range

I want to generate the .npy file or shorter time interval, so I can run `python calcX_evol.py`.

* Changed `dataout` and `trange` in [PARAMS/param.tseries](a2020/a09/a18_running_ransx/ransx_param/param.tseries.to_500s).

* Ran `python ransX_tseries.py`.

It generated smaller file at `/Users/evgenii/Downloads/tseries_2020_09_17_09_59_26_192x192x1_2000s_time_1.00_luminosity_to_500s.npy`.


## Run `calcX_evol.py` for shorter time range.

* I changed in `eht_data`, `dataout`, parameters in [PARAMS/param.evol](a2020/a09/a18_running_ransx/ransx_param/param.evol.to_500s) to use shorter time interval.

* Ran `python calcX_evol.py`.

It created `/Users/evgenii/Downloads/tseries_2020_09_17_09_59_26_192x192x1_2000s_time_1.00_luminosity_to_500s_evol.npy` output file.


## Run `ransX_evol.py`

Finally, I ran `python ransX_evol.py` and it created the plot of turbulence kinetic energy:

<img src='a2020/a09/a18_running_ransx/plots/ransx/ccptwo128_cosma_tke_evol.png' width="500" alt="TKE" />


This looks similar to 2D TKE from Miro's plot:

<img src='a2020/a09/a18_running_ransx/plots/ransx/tke_miro.png' width="500" alt="Miro's TKE" />


And from Simon's plot:

<img src='a2020/a09/a18_running_ransx/plots/ransx/tke_simon.png' width="500" alt="Simon's TKE" />


## Run `ransX_resEvol.py`

* I changed `eht_data` and `eht_res` parameters in [PARAMS/param.evol.res](a2020/a09/a18_running_ransx/ransx_param/param.evol.res).

* I ran `python ransX_resEvol.py`.

The program produced the following plot

<img src='a2020/a09/a18_running_ransx/plots/ransx/ccptwo_tavg300sEvolStudyR512_tke_evol_res.png' width="500" alt="Evol res TKE plot" />


## Run `ransX_resEvol.py` for 2000s time range.

* I changed [parameters](a2020/a09/a18_running_ransx/ransx_param/2000s).

* Ran ransX_tseries.py, calcX_evol.py and ransX_resEvol.py.

* `calcX_evol.py` took 30 min to complete.

Result looks like Simon's:

<img src='a2020/a09/a18_running_ransx/plots/ransx/ccptwo_tavg300sEvolStudyR512_tke_evol_res_2000s.png' width="500" alt="Evol res TKE plot 2000s" />

## Misc

* [Calculating turbulence kinetic energy](https://github.com/evgenyneu/ransX/blob/b12f673b13449dbb49517ef8bb0637c0ad1f3013/EQUATIONS/TurbulentKineticEnergyCalculation.py#L67)
