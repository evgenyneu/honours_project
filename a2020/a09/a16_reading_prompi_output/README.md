## Improving job running script

Improve [job running scripts](https://gitlab.com/evgenyneu/pawsey_home_backup/-/tree/master/slurm_jobs/2020/09/16):

* Removed hardcoded paths.

* Store README.md in the output.

```
. slurm_jobs/2020/09/16/jobs/010_short_test.sh
```

## Output size problem

I tried to run [Miro's protting](a2020/a09/a16_reading_prompi_output/code/miro/show_2Dvs3D_x0002.py) code and noticed zero values for variables (velx and 0002). After examining the binary output files in a [HEX editor](https://ridiculousfish.com/hexfiend/), I found that after each variable block of 192 x 192 x 1 values, there are extra extra blocks of the same lengths populated with zeros. Consequently, the file is four times larger than it needs to be.

This is probably cased by [this code](https://github.com/evgenyneu/PROMPI/blob/a85041f0eb5c0e054595870a4a60989da27001ca/root/src/IO/write_binary_bindata.f90#L28) in write_binary_bindata.f90 file:

```Fortran
irecl = irecl_float*nxglobal*nyglobal*nzglobal !  RECORD LENGTH (NOTE: SYSTEM DEPENDENT)
open(ntlocal, file=fn_bindata, form='unformatted',access='direct',recl = irecl)
```

The `recl` argument of `open` function specifies the record length and is different between compilers. In gfortran, this is length in bytes, while in ifort, this is length in 4 byte chunks. This is why we see those blocks with zeros. This is explaned [here](https://stackoverflow.com/a/32687019/297131).

### Two solutions

* Add `-assume byterecl` compiler flag.

* Set `irecl_float` to `1` instead of `4`.

### Bindata test

I want to run PROMPI with and without `-assume byterecl` flag and test the size of the binary.

Default settings:

```
. slurm_jobs/2020/09/16/jobs/020_record_length_test.sh
```

With `-assume byterecl` compile flag:

```
. slurm_jobs/2020/09/16/jobs/030_record_length_test_byte_rl.sh
```

### Use `irecl_float = 1` setting

I compile without `-assume byterecl` flag but use `irecl_float = 1` setting. See [commit](https://github.com/evgenyneu/PROMPI/commit/64d95988adc48a0740aefc74e4d014e956f1ed8a).

```
. slurm_jobs/2020/09/16/jobs/040_irecl_float_1.sh
```

### Test results

After using `-assume byterecl` compiler flag, the .bitdata files are four times smaller and don't have sections with zeros:

* [ccptwo.2D.00005.bindata four bytes record length](a2020/a09/a16_reading_prompi_output/data/default_record_length/ccptwo.2D.00005.bindata): Four byte record length (previous).

* [ccptwo.2D.00005.bindata](a2020/a09/a16_reading_prompi_output/data/byte_record_length/ccptwo.2D.00005.bindata): One byte record length after using `-assume byterecl` compiler flag.

The images from both data files look identical:

![Four byte](a2020/a09/a16_reading_prompi_output/code/plots/read_prompi_4_byte_record_length.png)
![One byte](a2020/a09/a16_reading_prompi_output/code/plots/read_prompi_1_byte_record_length.png)

Plotting code: [read_prompi.py](a2020/a09/a16_reading_prompi_output/code/read_prompi.py).
