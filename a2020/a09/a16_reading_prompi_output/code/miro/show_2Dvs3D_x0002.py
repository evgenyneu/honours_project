import PROMPI_data as prd
import numpy as np

dataloc2D = 'a2020/a09/a16_reading_prompi_output/data/ccptwo.2D.00200.bindata'
dat = ['velx', '0002']
block2D = prd.PROMPI_bindata(dataloc2D, dat)

nx_2D = block2D.datadict['qqx']
ny_2D = block2D.datadict['qqy']
nz_2D = block2D.datadict['qqz']

xzn0_2D = block2D.datadict['xzn0']
yzn0_2D = block2D.datadict['yzn0']
zzn0_2D = block2D.datadict['zzn0']

time_2D = block2D.datadict['time']

velx_2D = block2D.datadict['velx']
x0002_2D = block2D.datadict['0002']

print(np.unique(velx_2D.flatten()))
print(np.unique(x0002_2D.flatten()))

print 'We are done!'
