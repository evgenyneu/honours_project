!..include file dimen.inc
!..MUST precede dimensioned arrays
!
!
!..spatial grid
      integer*4      qqq, qq, qx, qy, qz
      integer*4      qqx, qqy, qqz

      parameter( qqq  = 18   )
      parameter( qqx  = 8   )
      parameter( qqy  = 8   )
      parameter( qqz  = 1   )

      parameter( qq   =  18  )
      parameter( qx   =  8  )
      parameter( qy   =  8    )
      parameter( qz   =  1    )


!..composition array
      integer*4 qn
      parameter( qn     = 2  )

!..some io stuff
      integer*4 qdat

      parameter( qdat = 13 + qn )
