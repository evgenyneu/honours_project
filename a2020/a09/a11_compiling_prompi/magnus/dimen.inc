!..include file dimen.inc
!..MUST precede dimensioned arrays
!
!
!..spatial grid
      integer*4      qqq, qq, qx, qy, qz
      integer*4      qqx, qqy, qqz

      parameter( qqq  = 266   )
      parameter( qqx  = 256   )
      parameter( qqy  = 256   )
      parameter( qqz  = 1   )

      parameter( qq   =  42  )
      parameter( qx   =  32  )
      parameter( qy   =  32   )
      parameter( qz   =  1    )


!..composition array
      integer*4 qn
      parameter( qn     = 2  )

!..some io stuff
      integer*4 qdat

      parameter( qdat = 13 + qn )
