## Setup SSH forwarding

I don't want to type Github to push/pull from https://github.com/evgenyneu/PROMPI repository.

Add to `ForwardAgent yes` to `pawsey` hosst in `.ssh/config` hile.

```
Host pawsey
  ForwardAgent yes
```

## Compiling PROMPI on Magnus

I want to compile PROMPI on Magnus computer. First, I make the following configuration changes to https://github.com/evgenyneu/PROMPI, adopted from [Simon's Wiki](https://stellarmeme.net/wiki/doku.php?id=installing_prompi):

* Create `magnus-1` and `magnus-2` directories in `root/sites` of  repository.

* Add [makefile.local](a2020/a09/a11_compiling_prompi/magnus/makefile.local) to `magnus-1` and `magnus-2 directories.

* Comment `SITE` variable in `/setups/ccp_two_layers/2d/src/makefile`.

* Add to clean in the makefile:

```
clean:
	-rm ${lobjects} lib*.a ../prompi.x
	find ${PROMPI_SRC}/../../ -name "*".o -exec rm \{} \;
	find ${PROMPI_SRC}/../../ -name "*".mod -exec rm \{} \;
```

### Download the repository

```
cd $MYSCRATCH
git clone git@github.com:evgenyneu/PROMPI.git
cd PROMPI
git checkout evgenii_honours
```

### Set `SITE` and `SPROMPI_SRC` environmental variables

Add to `~/.bash_profile`:

```
# PROMPI (hydrodynamic simulation https://github.com/evgenyneu/PROMPI)
export SITE=$HOST
export PROMPI_SRC=$MYSCRATCH/PROMPI/root/src
module swap PrgEnv-cray PrgEnv-intel
```

### Compile

```
cd $MYSCRATCH/PROMPI/setups/ccp_two_layers/2d/src
make clean
make prompi
```

This will make an executable `setups/ccp_two_layers/2d/prompi.x`.


## Compile locally on my Mac

Install Open MPI

```
brew install open-mpi
```

Add [makefile.local](a2020/a09/a11_compiling_prompi/local/makefile.local) to `root/sites/evgenii.mac` directory.

Add to ~/.bash_profile`:

```
# PROMPI (hydrodynamic simulation https://github.com/evgenyneu/PROMPI)
export SITE=evgenii.mac
export PROMPI_SRC=/Users/evgenii/Documents/honours_project/PROMPI/root/src
```

## Running PROMPI

### Run locally

Change the number of grid points:

* Change `nx`, `ny` to `8` in [inlist_prompi](a2020/a09/a11_compiling_prompi/local/inlist_prompi).

* Change `qqq`, `qqx`, `qqy` , `qq`, `qx`, `qy` in [src/DIMEN/dimen.inc](a2020/a09/a11_compiling_prompi/local/dimen.inc).

Run the simulation:

```
mpiexec -n 1 ./prompi.x
```

The program created a bunch of executables, which can be removed with script:

```
#!/bin/sh

#
# Deletes output files from PROMPI hydro symulation
#

rm *.header
rm *.ranshead
rm *.ransdat
rm *.bindata
rm *.00000_no_time8_function
rm run_complete_file

```

### Run on Magnus


* In in [inlist_prompi](a2020/a09/a11_compiling_prompi/magnus/inlist_prompi) change the number of grid points:

```
nx      = 256
ny      = 256
nz      = 1

ntiles(1) = 8
ntiles(2) = 8
ntiles(3) = 1
```

`ntiles` is number of tiles in x, y, z. Each tile is assigned to each processor core. So one core will handle `256/8=32` grid points.

* In [src/DIMEN/dimen.inc](a2020/a09/a11_compiling_prompi/magnus/dimen.inc):

```
parameter( qqq  = 266   )  !This should be qqx+10
parameter( qqx  = 256   )  !These are the full domain dimensions
parameter( qqy  = 256   )
parameter( qqz  = 1     )

parameter( qq   =  42   ) !This should be qx+10
parameter( qx   =  32   ) !These are the dimensions for each processor
parameter( qy   =  32   )
parameter( qz   =  1    )
```

### Magnus job file

Add to [evgenii.job](a2020/a09/a11_compiling_prompi/magnus/evgenii.job) `setups/ccp_two_layers/2d/` directory.
