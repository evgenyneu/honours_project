## Download PROMPI

Go to pawsey:

```
ssh pawsey
```

Open scratch directory:

```
cd $MYSCRATCH
```

Get Github access token at https://github.com/settings/tokens, check "repo" permissions.

Clone PROMPI and checkout ransX branch, enter Github token for username and blank for password:

```
git clone https://github.com/evgenyneu/PROMPI.git
cd PROMPI
git checkout ransX
```
