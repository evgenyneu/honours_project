# Twiddling with knobs in plots

I want to experiment with plotting settings and see if I can show more details. This can be done by with two knobs in Matplotlib:

1. **Interval**: Restricting the range of values using classes like `PercentileInterval`. This is useful for excluding extreme values and make the plot focus on majority of the data.

2. **Stretch**: Transforming the values with classes like `PowerStretch` or `LogStretch`. Can be handy for revealing details.

## Example code

```python
# Show majority 80% of value, exclude 20% of the extreme values.
interval = PercentileInterval(80)

# The values are squared
stretch = PowerStretch(2)

# Glue stretch and interval together
norm = ImageNormalize(data, interval=interval, stretch=stretch)

# Plot the data
pos = ax.imshow(data, norm=norm)
```

## Plots with interactive knobs

![Plot and sliders](a2020/a09/a23_playing_with_plot_settings/code/plots/plot_and_sliders.png)

Code: [plot_and_sliders.ipynb](a2020/a09/a23_playing_with_plot_settings/code/jupyter/plot_and_sliders.ipynb)

## References

[Astropy Docs: Image stretching and normalization](https://docs.astropy.org/en/stable/visualization/normalization.html)
