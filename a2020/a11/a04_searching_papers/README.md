
# Searching papers

I want to find relevant papers cited by [Meakin, Arnett 2007](https://ui.adsabs.harvard.edu/abs/2007ApJ...667..448M/abstract)


### Search term

```
abs: ( convect*  AND simulation) AND citations(title: "Turbulent Convection in Stellar Interiors. I. Hydrodynamic Simulation" author: "Meakin, C")
```

### Found two relevant Papers

* [Takashi+ 2019	One-, Two-, and Three-dimensional Simulations of Oxygen-shell Burning Just before the Core Collapse of Massive Stars](https://ui.adsabs.harvard.edu/abs/2019ApJ...881...16Y/abstract).

* [Emmanouil+ 2014	Characterizing the Convective Velocity Fields in Massive Stars](https://ui.adsabs.harvard.edu/abs/2014ApJ...795...92C/abstract).
