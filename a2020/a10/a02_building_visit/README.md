# Build visit locally

I want to find out why VisItXdmf database plugin can't open files produced by prompi2hdf5. In order to do this I want to build Visit locally on my Mac.

* [Download](https://wci.llnl.gov/simulation/computer-codes/visit/source) build_script.

* Read [build instructions](https://visit-sphinx-github-user-manual.readthedocs.io/en/develop/gui_manual/Building/index.html).

Build

```
./build_visit3_1_3 --makeflags -j2 --hdf5 --silo
```

## Building Qt

Qt [failed to build](https://gitlab.com/evgenyneu/honours_project/-/tree/master/a2020/a10/a01_using_visit_on_magnus#building-qt).

* Change `QMAKE_MACOSX_DEPLOYMENT_TARGET` from 10.10 to 10.13:

```
QMAKE_MACOSX_DEPLOYMENT_TARGET = 10.13
```

* Make [this change](https://stackoverflow.com/a/59076895/297131) to `qtbase/src/platformsupport/fontdatabases/mac/qfontengine_coretext.mm` file.

* Go to `qt-everywhere-src-5.10.1` directory, run

```
./configure
make
```

### assimp library error

```
../../../3rdparty/assimp/code/BlenderModifier.cpp:268:14: error: no member named 'bind1st' in namespace 'std'
        std::bind1st(std::plus< unsigned int >(),out.mNumMeshes));
        ~~~~~^
```

[Solution](https://github.com/Homebrew/homebrew-core/issues/27095#issuecomment-384956096) is:

```
./configure -no-assimp
make
```


### Another error in `osxbtledeviceinquiry.mm`

```
qt osx/osxbtledeviceinquiry.mm:142:9: error: conversion from 'ObjCScopedPointer<CBCentralManager>' to 'bool' is ambiguous
```

Apply [the fix](https://github.com/macports/macports-ports/pull/8154/files):

Change

```
- (void)dealloc
{
    if (manager.data()) {
        [manager setDelegate:nil];
```

to

```
- (void)dealloc
{
    if (manager.data()) {
        [manager setDelegate:nil];
```

Also in:

* osx/osxbtperipheralmanager.mm:316:9
* osx/osxbtperipheralmanager.mm:381:5
* osx/osxbtperipheralmanager.mm:381:5
* qbluetoothlocaldevice_osx.mm:152:12
