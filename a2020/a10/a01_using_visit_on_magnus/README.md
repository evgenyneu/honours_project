## Compiling HDF5 demo Fortran program on Pawsey

```
ssh pawsey
module load cray-hdf5
cd /home/evgenyneu/code/a2020/a10/a01/hdf5demo
ftn -I${HDF5_DIR}/include -L${HDF5_DIR}/lib hdf5demo.f90 -lhdf5_fortran
```

## Compiling prompi2hdf5 on Pawsey

```
ssh pawsey
module load cray-hdf5
cd /home/evgenyneu/code/a2020/a10/a01/prompi2dhf5
make -f Makefile.pawsey
```

## Converting PROMPI files to HDF5 on Pawsey

### Extract PRIMPI output into a temporary directory:

```
mkdir -p /group/ew6/evgenyneu/tmp/2020_09_17_09_59_26_192x192x1_2000s_time_1.00_luminosity
cd /group/ew6/evgenyneu/prompi_output
tar -xf 2020_09_17_09_59_26_192x192x1_2000s_time_1.00_luminosity.tar -C /group/ew6/evgenyneu/tmp/2020_09_17_09_59_26_192x192x1_2000s_time_1.00_luminosity
```

## Convert files to HDF5

```
cd /group/ew6/evgenyneu/tmp/2020_09_17_09_59_26_192x192x1_2000s_time_1.00_luminosity
```

Copy parameter prompi2dhf5 file:

```
cp /home/evgenyneu/code/a2020/a10/a01/prompi2dhf5/param.txt ./
```

Run prompi2dhf5:

```
salloc -p debugq --nodes=1
srun --export=all -N 1 -n 1 /home/evgenyneu/code/a2020/a10/a01/prompi2dhf5/build/prompi2hdf5
exit
```

## View PROMPI output with Visit on Pawsey


### Use https://remotevis.pawsey.org.au/

* Go to https://remotevis.pawsey.org.au/

* `New Session` > `Visit` (version 3.1 patch 2).

* `Sources` > `Open`.

* Set path to `/group/ew6/evgenyneu/tmp/2020_09_17_09_59_26_192x192x1_2000s_time_1.00_luminosity`.

* Select *.xmf files.

```
VisIt could not read from the file "/group/ew6/evgenyneu/tmp/2020_09_17_09_59_26_192x192x1_2000s_time_1.00_luminosity/ccptwo.2D.*.xmf database".

The generated error message was:

There was an error opening /group/ew6/evgenyneu/tmp/2020_09_17_09_59_26_192x192x1_2000s_time_1.00_luminosity/ccptwo.2D.*.xmf database. It may be an invalid file.
VisIt tried using the following file format readers to open the file: VisItXdmf, Silo
```


### Use https://zeus.pawsey.org.au:3443/

* Uses Visit version 2.12.2.

* Repeat the steps form above.

Visit works well.

<img src='a2020/a10/a01_using_visit_on_magnus/images/visit_on_zeus.jpg' width="700" alt="Visit on Zeus" />



## Why Visit 3.1 can't open our HDMF5 files?

I want to fix prompi2hdf5 program to make Visit 3.1 work.


### Download Visit 3.1

* Open [download page](https://wci.llnl.gov/simulation/computer-codes/visit/executables).

* [Download](https://github.com/visit-dav/visit/releases/download/v3.1.2/visit3.1.2.darwin-x86_64-10_14.dmg) version VisIt 3.1.2 for macOS Mojave 10.14.6 - Intel 64 bit.

Works well locally.


### Debug Visit 3.1 on Pawsey

* Open https://remotevis.pawsey.org.au and launch Visit.

* `Sources` > `Open`, use `/group/ew6/evgenyneu/tmp/2020_10_01_prompi` path.

Got the same error as above.

The problem is that Visit on Pawsey uses different Database plugin VisItXdmf while a different plugin `Xdmf` is used locally.


### Debug VisItXdmf plugin

* Download Visit 3.1.2 [source code](https://wci.llnl.gov/simulation/computer-codes/visit/source).

* `cd src/databases/VisItXdmf`

* Add [this code](a2020/a10/a01_using_visit_on_magnus/code/add_to_cmakelists.txt) to top of `src/databases/VisItXdmf/CMakeLists.txt`.


Run

```
cmake -DHDF5_INCLUDE_DIR=/Users/evgenii/Downloads/visit3.1.2/src/common/plugin -DVISIT_SOURCE_DIR=/Users/evgenii/Downloads/visit3.1.2/src .
```

A Makefile will be created. Make file ran with errors.


## Build Visit locally

* [Download](https://wci.llnl.gov/simulation/computer-codes/visit/source) and run `build_visit`.

* Error build Qt


## Building Qt

From Qt directory run

```
./configure
make
```

Go error

```
io/qfilesystemengine_unix.cpp:1457:9: error: 'futimens' is only available on macOS 10.13 or newer
      [-Werror,-Wunguarded-availability-new]
    if (futimens(fd, ts) == -1) {
        ^~~~~~~~
/Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX10.15.sdk/usr/include/sys/stat.h:396:9: note:
      'futimens' has been marked as being introduced in macOS 10.13 here, but the deployment target is macOS 10.10.0
int     futimens(int __fd, const struct timespec __times[2]) __API_AVAILABLE(macosx(10.13), ios(11.0), tvos(11.0), watchos(4.0));
        ^
io/qfilesystemengine_unix.cpp:1457:9: note: enclose 'futimens' in a __builtin_available check to silence this warning
    if (futimens(fd, ts) == -1) {

```

Todo: try settings `QMAKE_MACOSX_DEPLOYMENT_TARGET` to 10.13
