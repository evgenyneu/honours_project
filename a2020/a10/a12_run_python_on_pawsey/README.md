# Run Python on Pawsey

I want to learn how to run Python scripts on Pawsey.


## Hello World

Run on login node:

```
cd /scratch/ew6/evgenyneu/logbook
python a2020/a10/a12_run_python_on_pawsey/code/hello_world.py
```

Run on compute node:

```
cd /scratch/ew6/evgenyneu/logbook
salloc --partition=debugq --nodes=1 --ntasks=1 --time=00:05:00
srun --export=all -N 1 -n 1 python a2020/a10/a12_run_python_on_pawsey/code/hello_world.py
exit
```

## Making a Docker container

I want to make a Docker container that includes additional Python libraries. Later I will use this container to run my Python code on Magnus.

Go to work director:

```
cd a2020/a10/a12_run_python_on_pawsey/docker
```

Make a Docker image, based on [Pawsey](https://support.pawsey.org.au/documentation/display/US/Installing+Python+modules+on+supercomputers) image with additional [Python libraries](a2020/a10/a12_run_python_on_pawsey/docker/requirements.txt).

```
docker build --tag pawsey .
```

Run the image:

```
docker run -ti --name pawsey -v /Users/evgenii/Documents/honours_project/logbook/a2020/a10/a12_run_python_on_pawsey/code:/hostcode pawsey
```

* **-ti**: show interactive session for the container.

* **-v**: Map local directory `/Users/evgenii/...` to `/hostcode` inside the container
