# Continuing building Visit locally

## New day, new build error

```
include/mbgl/util/optional.hpp:3:10: fatal error: 'experimental/optional' file not found
#include <experimental/optional>
         ^~~~~~~~~~~~~~~~~~~~~~~
```

### Solution

* Replace `#include <experimental/optional>` with `#include <optional>`.
* Replace `std::experimental::optional` with `std::optional`.

### Same error in

* `include/mapbox/geometry/feature.hpp:11:10`

* `qt/src/sqlite3.cpp:128:23`

* `src/csscolorparser/csscolorparser.hpp:30:10`

* `qtlocation/src/3rdparty/mapbox-gl-native/platform/default/sqlite3.cpp`

* `geojsonvt/6.3.0/include/mapbox/geojsonvt/types.hpp:93:18`


## Another error

```
deps/boost/1.62.0/include/boost/get_pointer.hpp:48:23: error: redefinition of 'get_pointer' as different kind of symbol
template<class T> T * get_pointer(std::auto_ptr<T> const& p)
```


### Solution

Comment:

```
// template<class T> T * get_pointer(std::auto_ptr<T> const& p)
// {
//     return p.get();
// }
```

## Error

Can not find `auto_ptr` in `qtlocation/src/3rdparty/mapbox-gl-native/deps/boost/1.62.0/include/boost/smart_ptr/scoped_ptr.hpp`

### Solution

Comment out blocks `#ifndef BOOST_NO_AUTO_PTR`.

## Error


```
src/mbgl/gl/state.hpp:35:38: error: invalid operands to binary expression ('const typename VertexAttribute::Type'
      (aka 'const optional<mbgl::gl::AttributeBinding>') and 'const typename VertexAttribute::Type')
        return dirty || currentValue != value;
```


## Other errors

Solution: commented the lines that give errors until them compile. I hope this won't cause problems at runtime.


## Building Visit again

```
./build_visit3_1_3 --makeflags -j2 --hdf5 --silo
```

It runs the following for Qt:

```
CFLAGS=-fno-common -fexceptions -O2 CXXFLAGS=-fno-common -fexceptions -O2 ./configure -prefix /Users/evgenii/Documents/honours_project/visit/third_party/qt/5.10.1/i386-apple-darwin19_clang -platform macx-clang -make libs -make tools -no-separate-debug-info  -no-dbus -no-sql-db2 -no-sql-ibase -no-sql-mysql -no-sql-oci -no-sql-odbc -no-sql-psql -no-sql-sqlite -no-sql-sqlite2 -no-sql-tds -no-libjpeg -opensource -confirm-license -skip 3d -skip canvas3d -skip charts -skip connectivity -skip datavis3d -skip doc -skip gamepad -skip graphicaleffects -skip location -skip multimedia -skip networkauth -skip purchasing -skip quickcontrols -skip quickcontrols2 -skip remoteobjects -skip scxml -skip sensors -skip serialport -skip speech -skip wayland -nomake examples -nomake tests -no-qml-debug -qt-zlib -qt-libpng
```

Errors [log](a2020/a10/a03_building_visit/logs/build_visit.log.zip).

## Build Visit without Qt

I want to try building Visit without building Qt but using Qt installed into `/usr/local/Qt-5.10.1` (which I installed by using `make install` in Qt).


```
./build_visit3_1_3 --makeflags -j2 --hdf5 --silo --system-qt
```

Failed to build HDF5. Giving up.
