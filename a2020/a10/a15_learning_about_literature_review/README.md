# Learning about honours literature review (continue...)

Continue watching [Advanced searching skills video](https://lms.monash.edu/mod/book/view.php?id=7076608&chapterid=689988).


## Subject headings

* Subject headings are article headings used to group articles of similar types in databases.

* Unique to each database.

* Try using subject heading in search queries.


## Develop search plan and test it

Include more spelling variants, use truncation operators, phrase searching.



## Coping with the literature

* Search for recent articles.

* Primary literature (no literature reviews).

* Do title, abstract, Ctrl-F and decide if article is relevant.


## Making notes

* Bibliographic details and page numbers.

* Paraphrase and summarise content (separate those).

* Thoughts/comments.

* Connections to my research and other articles.

* Organise information in mind maps and/or spreadsheet.



## Writing the literature review

[Source](https://lms.monash.edu/mod/book/view.php?id=7076608&chapterid=689990).

* Make a bullet-point outline.

* Inverted pyramid: from general to specific.

### Paragraph structure

* Only one main topic per paragraph.

* Start with a **topic sentence**, the main topic of the paragraph.

* Add **supporting statements** that include evidence from literature.

* End with **conclusive sentence**: what evidence means and its relevance to my project.

### Avoid summarising papers

* Organise literature into topics.

* Identify topic sentences in each paragraph.

* Compare different models, theories, results.



## Preliminary search

* Opened Scopus through [guides.lib.monash.edu/subject-databases](https://guides.lib.monash.edu/subject-databases).
