# Preliminary search

I'm doing preliminary search to find papers on [my topic](a2020/a10/a14_learning_about_literature_review/README.md).



## Searching on Scopus

### First search

* ( TITLE-ABS-KEY ( "fluid dynamic*"  OR  hydrodynamic* )  AND  TITLE-ABS-KEY ( convect* )  AND  TITLE-ABS-KEY ( boundar* ) )  AND  PUBYEAR  >  2009

* [Search results](https://www.scopus.com/results/results.uri?numberOfFields=2&src=s&clickedLink=&edit=t&editSaveSearch=&origin=searchbasic&authorTab=&affiliationTab=&advancedTab=&scint=1&menu=search&tablin=&searchterm1=%22fluid+dynamic*%22+or+hydrodynamic*&field1=TITLE_ABS_KEY&connector=AND&searchterm2=convect*&field2=TITLE_ABS_KEY&connectors=AND&searchTerms=boundar*&fields=TITLE_ABS_KEY&dateType=Publication_Date_Type&yearFrom=2010&yearTo=Present&loadDate=7&documenttype=All&accessTypes=All&resetFormLink=&st1=%22fluid+dynamic*%22+or+hydrodynamic*&st2=convect*&sot=b&sdt=b&sl=105&s=%28TITLE-ABS-KEY%28%22fluid+dynamic*%22+or+hydrodynamic*%29+AND+TITLE-ABS-KEY%28convect*%29AND+TITLE-ABS-KEY%28boundar*%29%29&sid=a130e2318886b33999aa811273b12277&searchId=a130e2318886b33999aa811273b12277&txGid=9518d3a1fe12c8fa566154ad97d3b362&sort=r-f&originationType=b&rr=)

* 2700 documents, nothing relevant.


### Added "stellar"

* ( TITLE-ABS-KEY ( "fluid dynamic*"  OR  hydrodynamic* )  AND  TITLE-ABS-KEY ( convect* )  AND  TITLE-ABS-KEY ( boundar* )  AND  TITLE-ABS-KEY ( stellar ) )  AND  PUBYEAR  >  2009  

* [Search results](https://www.scopus.com/results/results.uri?numberOfFields=3&src=s&clickedLink=&edit=t&editSaveSearch=&origin=searchbasic&authorTab=&affiliationTab=&advancedTab=&scint=1&menu=search&tablin=&searchterm1=%22fluid+dynamic*%22+or+hydrodynamic*&field1=TITLE_ABS_KEY&connector=AND&searchterm2=convect*&field2=TITLE_ABS_KEY&connectors=AND&searchTerms=boundar*&fields=TITLE_ABS_KEY&connectors=AND&searchTerms=stellar&fields=TITLE_ABS_KEY&dateType=Publication_Date_Type&yearFrom=2010&yearTo=Present&loadDate=7&documenttype=All&accessTypes=All&resetFormLink=&st1=%22fluid+dynamic*%22+or+hydrodynamic*&st2=convect*&sot=b&sdt=b&sl=133&s=%28TITLE-ABS-KEY%28%22fluid+dynamic*%22+or+hydrodynamic*%29+AND+TITLE-ABS-KEY%28convect*%29+AND+TITLE-ABS-KEY%28boundar*%29+AND+TITLE-ABS-KEY%28stellar%29%29&sid=9b40d6bd832fcbfed93bcb2e7f777e21&searchId=9b40d6bd832fcbfed93bcb2e7f777e21&txGid=93b5165b739361bb366e28182615aec7&sort=r-f&originationType=b&rr=)

* 56 documents, pretty good.


### Including "2D AND 3D"

* ( TITLE-ABS-KEY ( turbulen* )  AND  TITLE-ABS-KEY ( convect* )  AND  TITLE-ABS-KEY ( solar  OR  stellar )  AND  TITLE-ABS-KEY ( 2d  AND  3d ) )  AND  PUBYEAR  >  1994

* [Search results](https://www.scopus.com/results/results.uri?numberOfFields=3&src=s&clickedLink=&edit=t&editSaveSearch=&origin=searchbasic&authorTab=&affiliationTab=&advancedTab=&scint=1&menu=search&tablin=&searchterm1=Turbulen*&field1=TITLE_ABS_KEY&connector=AND&searchterm2=Convect*&field2=TITLE_ABS_KEY&connectors=AND&searchTerms=solar+OR+stellar&fields=TITLE_ABS_KEY&connectors=AND&searchTerms=2D+AND+3D&fields=TITLE_ABS_KEY&dateType=Publication_Date_Type&yearFrom=1995&yearTo=Present&loadDate=7&documenttype=All&accessTypes=All&resetFormLink=&st1=Turbulen*&st2=Convect*&sot=b&sdt=b&sl=119&s=%28TITLE-ABS-KEY%28Turbulen*%29+AND+TITLE-ABS-KEY%28Convect*%29+AND+TITLE-ABS-KEY%28solar+OR+stellar%29+AND+TITLE-ABS-KEY%282D+AND+3D%29%29&sid=0a3539cce6483d96ab53c7a0458cd0a4&searchId=0a3539cce6483d96ab53c7a0458cd0a4&txGid=aa00e57e4fba044744dc810460df30d7&sort=r-f&originationType=b&rr=)

* 22 Results, very relevant.


### Removing "turbulen*" and adding "simulation"

* ( TITLE-ABS-KEY ( convect* )  AND  TITLE-ABS-KEY ( solar  OR  stellar )  AND  TITLE-ABS-KEY ( 2d  AND  3d )  AND  TITLE-ABS-KEY ( simulation ) )  AND  PUBYEAR  >  1998

* [Search results](https://www.scopus.com/results/results.uri?numberOfFields=3&src=s&clickedLink=&edit=t&editSaveSearch=&origin=searchbasic&authorTab=&affiliationTab=&advancedTab=&scint=1&menu=search&tablin=&searchterm1=Convect*&field1=TITLE_ABS_KEY&connector=AND&searchterm2=solar+OR+stellar&field2=TITLE_ABS_KEY&connectors=AND&searchTerms=2D+AND+3D&fields=TITLE_ABS_KEY&connectors=AND&searchTerms=simulation&fields=TITLE_ABS_KEY&dateType=Publication_Date_Type&yearFrom=1999&yearTo=Present&loadDate=7&documenttype=All&accessTypes=All&resetFormLink=&st1=Convect*&st2=solar+OR+stellar&sot=b&sdt=b&sl=120&s=%28TITLE-ABS-KEY%28Convect*%29+AND+TITLE-ABS-KEY%28solar+OR+stellar%29+AND+TITLE-ABS-KEY%282D+AND+3D%29+AND+TITLE-ABS-KEY%28simulation%29%29&sid=dd3da9fdeb78efdd5286f0d880b9e6f1&searchId=dd3da9fdeb78efdd5286f0d880b9e6f1&txGid=ad4fd7829ce530d309304f234a7e59cb&sort=r-f&originationType=b&rr=)

* 62 documents, very relevant.


## Search on Web of Science Core Collection

Accessed through [guides.lib.monash.edu/subject-databases](https://guides.lib.monash.edu/subject-databases).
