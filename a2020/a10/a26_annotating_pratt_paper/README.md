* Annotated [Pratt+ 2020](literature/2D_vs_3D/pratt_2020_comparison_2d_3d_convection) paper.

* Added comments in the [papers spreadsheet](https://docs.google.com/spreadsheets/d/1IrMU7_ZvJB2HpVx5Lv4mY8tBsg7UJGgrz01VODhiRwM/edit?usp=sharing).
