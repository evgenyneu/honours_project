import time
from tqdm import tqdm


def sleepy_loop(iterations, pause_sec):
    """Simulates computation by doing `iterations` iteration,
    pausing after each one.

    Parameters
    ----------
    iterations : int
        Number of iterations

    pause_sec : float
        Amount of time in second to pause after each iterations.
    """

    for i in tqdm(range(iterations)):
        time.sleep(pause_sec)
