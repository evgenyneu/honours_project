from another_module import sleepy_loop


if __name__ == '__main__':
    sleepy_loop(iterations=10, pause_sec=0.3)

    print('We are done!')
