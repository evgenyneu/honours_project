from thylacine.reader import get_variable_data, get_time
import matplotlib.pyplot as plt
import pathlib
import shutil
from shutil import copyfile
import os
import inspect
import subprocess
from datetime import datetime
from tqdm import tqdm


def delete_dir_if_exists(dir):
    """Creates a directory if exists exist.

    Parameters
    ----------
    dir : str
        Directory path, can be nested directories.

    """
    if os.path.exists(dir):
        shutil.rmtree(dir)


def create_dir(dir):
    """Creates a directory if it does not exist.

    Parameters
    ----------
    dir : str
        Directory path, can be nested directories.

    """

    if not os.path.exists(dir):
        os.makedirs(dir)


def this_dir():
    """
    Returns
    -------
    str
        Directory of the current code file

    """
    frame = inspect.stack()[1]
    module = inspect.getmodule(frame[0])
    codefile = module.__file__
    return os.path.dirname(codefile)


def save_plot(data_path, plot_dir):
    """Saves a plot to an image

    Parameters
    ----------
    data_path : str
        Path to the PROMPI data file
    plot_dir : str
        Directory where the plot image will be saved to.

    """
    create_dir(plot_dir)
    image_name = f"{os.path.basename(data_path)}.png"
    image_path = os.path.join(plot_dir, image_name)
    plt.savefig(image_path, dpi=300)


def plot(data_path, plot_dir, variable):
    """Plot variable from PROMPI data file

    Parameters
    ----------
    data_path : str
        Path to PROMPI data file.
    plot_dir : str
        Directory where the plot image will be saved.
    variable : str
        Variable to be plotted, for example: 'density', 'energy'.

    """
    data = get_variable_data(data_path=data_path, variable=variable)
    data = data[:, :, 0]  # Convert to 2D array
    fig, ax = plt.subplots(1, 1)
    ax.set_title(f"Mass fraction of first element")
    pos = ax.imshow(data, cmap='Greys', vmin=0, vmax=1)
    fig.colorbar(pos, ax=ax)
    ax.set_xlabel('y')
    ax.set_ylabel('x')
    ax.invert_yaxis()

    # Show time
    # -------

    time = get_time(data_path)

    ax.text(
        0.05, 0.95,
        f't = {int(time)} s',
        horizontalalignment='left',
        verticalalignment='top',
        transform=ax.transAxes,
        bbox=dict(facecolor='white', alpha=0.8, edgecolor='0.7', boxstyle='round'))

    # Save plot
    # -------

    fig.tight_layout()
    save_plot(data_path=data_path, plot_dir=plot_dir)
    plt.close(fig)


def data_files(data_dir):
    """Returns the list of paths to PROMPI data time in the given directory.

    Parameters
    ----------
    data_dir : str
        Path to directory.

    Returns
    -------
    list of str
        Paths to PROMPI data files.

    """

    paths = [str(i) for i in pathlib.Path(data_dir).glob('*.bindata')]
    return sorted(paths)


def make_movie(plot_dir, movie_dir, movie_name, frame_rate):
    """
    Creates a movie .mp4 file from individual images.

    Parameters
    ----------
    plot_dir : str
        Path to directory containing individual images for the movie.
    movie_dir : str
        The output directory of the movie
    frame_rate : int
        The frame rate of the movie, in frames per second.

    """

    src_movie = os.path.join(plot_dir, movie_name)

    if os.path.exists(src_movie):
        os.remove(src_movie)

    command = (f"ffmpeg -framerate {frame_rate} -pattern_type glob -i '*.png' "
               f"-c:v libx264 -pix_fmt yuv420p {movie_name}")

    subprocess.call(command, cwd=plot_dir, shell=True)

    # Copy movie to output directory
    # ----------

    dest_movie = os.path.join(movie_dir, movie_name)
    create_dir(movie_dir)

    if os.path.exists(dest_movie):
        os.remove(dest_movie)

    copyfile(src_movie, dest_movie)


def plot_and_make_movie(data_paths, variable, movie_dir, movie_name,
                        frame_rate, silent):
    """Make plots and create a movie from them

    Parameters
    ----------
    data_paths : list of str
        Paths to PROMPI data files.
    variable : str
        Variable to be plotted, for example: 'density', 'energy'.
    movie_dir : str
        Directory where the movie will be saved.
    movie_name : str
        Name of the movie file.
    frame_rate : int
        The frame rate of the movie, in frames per second.
    silent : bool
        If True, do not show any output (used in unit tests).

    """
    plot_dir = os.path.join(this_dir(), 'plots_tmp')

    delete_dir_if_exists(plot_dir)

    if not silent:
        print("Creating plot images...")

    for data_path in tqdm(data_paths, disable=silent):
        plot(data_path=data_path, plot_dir=plot_dir, variable=variable)

    if not silent:
        print("Making a movie from images...")

    make_movie(plot_dir=plot_dir, movie_dir=movie_dir, movie_name=movie_name,
               frame_rate=frame_rate)

    shutil.rmtree(plot_dir)


def lets_gooooooo(data_dir, movie_dir, movie_name, max_frames, frame_rate,
                  silent=False):
    """Make plots for all data files and create a movie.

    Parameters
    ----------
    data_dir : str
        Directory containing PROMPI data files.
    movie_dir : str
        Directory where the movie will be saved
    movie_name : str
        Name of the output movie file.
    max_frames : int
        Maximum number of frames in the movie. Each frame is a plot of one
        data file.
    frame_rate : int
        The frame rate of the movie, in frames per second.
    silent : bool
        If True, do not show any output (used in unit tests).

    """

    data_paths = data_files(data_dir)
    data_paths = data_paths[:max_frames]

    plot_and_make_movie(data_paths=data_paths, variable='0001',
                        movie_dir=movie_dir, movie_name=movie_name,
                        frame_rate=frame_rate,
                        silent=silent)

    movie_path = os.path.join(movie_dir, movie_name)

    if not silent:
        print('Saved movie to')
        print(movie_path)


if __name__ == '__main__':
    movie_name = f'sim_{datetime.now():%Y-%m-%d_%H-%M-%S}.mp4'

    lets_gooooooo(data_dir='/data', movie_dir='/data/movies',
                  movie_name=movie_name, max_frames=30, frame_rate=5)

    print('We are done!')
