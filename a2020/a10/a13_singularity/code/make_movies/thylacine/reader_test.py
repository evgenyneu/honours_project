from reader import (
    get_variable_data, get_variable_data_2d, get_variable_data_1d,
    get_variable_names, get_variable_count, get_grid_size,
    get_time, locate_epochs)

from test_data import DATA_PATH

from timeit import default_timer as timer
import numpy as np
from pytest import approx
import os


def test_get_variable_data():
    data = get_variable_data(data_path=DATA_PATH, variable='0001')

    assert data.shape == (192, 192, 1)
    assert data[0, 0, 0] == approx(0.87162155, rel=1e-7)
    assert data[190, 188, 0] == approx(4.851705e-09, rel=1e-7)
    assert data[20, 188, 0] == approx(0.8098655, rel=1e-7)


def test_get_variable_data_performance():
    variables = [
        "density", "velx", "vely", "energy", "press", "temp",
        "gam1", "gam2", "enuc1", "enuc2", "0001", "0002"]

    runtimes = []

    for variable_name in variables:
        start = timer()
        get_variable_data(data_path=DATA_PATH, variable=variable_name)
        end = timer()
        runtimes.append(end - start)

    assert np.median(runtimes) < 0.002


def test_get_variable_data_2d():
    data = get_variable_data_2d(data_path=DATA_PATH, variable='0001', zindex=0)

    assert data.shape == (192, 192)
    assert data[0, 0] == approx(0.87162155, rel=1e-7)
    assert data[190, 188] == approx(4.851705e-09, rel=1e-7)
    assert data[20, 188] == approx(0.8098655, rel=1e-7)


def test_get_variable_data_2d_performance():
    variables = [
        "density", "velx", "vely", "energy", "press", "temp",
        "gam1", "gam2", "enuc1", "enuc2", "0001", "0002"]

    runtimes = []

    for variable_name in variables:
        start = timer()
        get_variable_data_2d(data_path=DATA_PATH, variable=variable_name, zindex=0)
        end = timer()
        runtimes.append(end - start)

    assert np.median(runtimes) < 0.002


def test_get_variable_data_1d():
    data = get_variable_data_1d(data_path=DATA_PATH, variable='0001')

    assert data.shape == (192, )
    assert data[0] == approx(0.87162155, rel=1e-7)
    assert data[20] == approx(0.8060047, rel=1e-7)
    assert data[190] == approx(3.5187768e-09, rel=1e-7)


def test_get_variable_names():
    names = get_variable_names(DATA_PATH)

    assert len(names) == 12

    assert names[0] == 'density'
    assert names[1] == 'velx'
    assert names[11] == '0002'


def test_get_get_variable_count():
    result = get_variable_count(DATA_PATH)
    assert result == 12


def test_get_grid_size():
    x, y, z = get_grid_size(DATA_PATH)

    assert x == 192
    assert y == 192
    assert z == 1


def test_get_time():
    result = get_time(DATA_PATH)

    assert result == 995.41163


def test_locate_epochs():
    result = locate_epochs(os.path.dirname(DATA_PATH))
    assert len(result) == 401
    assert result[0] == 1
    assert result[400] == 401
