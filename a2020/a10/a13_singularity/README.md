# Learning Singularity

My goal is to run my Python code on Pawsey. Proble is, my code uses Python libraries that are not installed on Pawsey. So I need to install custom Python libraries on Pawsey. But I can't just install Python libraries with pip or anaconda on Pawsey ([explained here](https://support.pawsey.org.au/documentation/display/US/Installing+Python+modules+on+supercomputers)). Instead

* I need to create a Docker image,

* install all Python libraries there,

* launch this container using Singularity on Pawsey and run my Python code inside it.


## The plan

1. Create a Docker image containing Python libraries I need. I did this [yesterday](a2020/a10/a12_run_python_on_pawsey).

2. Upload the image to [Docker Hub](https://hub.docker.com/).

3. Create a Singularity image `image.sif` from the Docker image into:

```
singularity pull image.sif docker://myaccount/repository:my_tag
```

4. Launch the Singularity container from the image `image.sif`, and run my Python script `my_script.py` inside the container.

```
singularity exec image.sif python my_script.py
```


## Create Docker image and upload to Docker Hub

### Create the image

* Open my directory containing the [Dockerfile](a2020/a10/a13_singularity/code/docker/Dockerfile):

```
cd a2020/a10/a13_singularity/code/docker
```

* Create docker image with repository `hello_pawsey` and tag `1.0`:

```
docker build --tag hello_pawsey:1.0 .
```

See the image with `docker images`.


### Run Docker container locally (optional)

* Launch the image and run my hello.py script.

```
docker run --name hello_pawsey -v /Users/evgenii/Documents/honours_project/logbook/a2020/a10/a13_singularity/code:/hostcode hello_pawsey:1.0 python hello.py
```

Shows the output from hello.py:

```
100%|██████████| 10/10 [00:03<00:00,  3.31it/s]
We are done!
```

The stoped container is shown by `docker ps -a`. Remove the container with `docker rm hello_pawsey`.


### Upload image to Docker Hub

* At [hub.docker.com](https://hub.docker.com) created new repository named `evgenyneu/hello_pawsey`: Repositories > Create Repository.

* Copy the image using to the new name `evgenyneu/hello_pawsey`:

```
docker tag hello_pawsey:1.0 evgenyneu/hello_pawsey:1.0
```

* In Account Settings > Security create a new access token and login to Docker from command line, needs to be done only once:


```
docker login --username evgenyneu
```

* Upload the image to DockerHub

```
docker push evgenyneu/hello_pawsey:1.0
```

## Create a Singularity image from the Docker image

```
singularity pull --dir ~/Downloads docker://evgenyneu/hello_pawsey:1.0
```

Created `~/Downloads/hello_pawsey_1.0.sif` file.


## Run Python script inside Singularity container

* Open directory with Python script:

```
cd /Users/evgenii/Documents/honours_project/logbook/a2020/a10/a13_singularity/code
```

* Run `hello.py` inside Singularity container:

```
singularity exec ~/Downloads/hello_pawsey_1.0.sif python hello.py
```

Output:

```
100% 10/10 [00:03<00:00,  3.28it/s]
We are done!
```

## On Pawsey: create a Singularity image from the Docker image

```
ssh pawsey
salloc --partition=debugq --nodes=1 --ntasks=1 --time=00:05:00
module load singularity
singularity pull -F --dir /scratch/ew6/evgenyneu/singularity docker://evgenyneu/hello_pawsey:1.0
exit
```

Image created at: `/scratch/ew6/evgenyneu/singularity/hello_pawsey_1.0.sif`

## On Pawsey: run Python script inside Singularity container

* Open directory with Python script:

```
ssh pawsey
cd /scratch/ew6/evgenyneu/logbook/a2020/a10/a13_singularity/code
```

* Run `hello.py` inside Singularity container:

```
salloc --partition=debugq --nodes=1 --ntasks=1 --time=00:05:00
module load singularity
singularity exec /scratch/ew6/evgenyneu/singularity/hello_pawsey_1.0.sif python hello.py
exit
```

Output:

```
100%|███████████████████████████| 10/10 [00:03<00:00,  3.33it/s]
We are done!
```

## Making Docker image with ffmpeg

### Make Docker image

I need ffmpeg in order to make movies of simulation.

* Open my directory containing the [Dockerfile](a2020/a10/a13_singularity/code/docker_1.1/Dockerfile):

```
cd a2020/a10/a13_singularity/code/docker_1.1
```

* Create docker image with repository `hello_pawsey` and tag `1.1`:

```
docker build --tag hello_pawsey:1.1 .
```

* Check ffmpeg is installed:

```
docker run -ti --rm --name hello_pawsey hello_pawsey:1.1 ffmpeg -version
```

### Upload image to Docker Hub


```
docker tag hello_pawsey:1.1 evgenyneu/hello_pawsey:1.1
docker push evgenyneu/hello_pawsey:1.1
```


## Create a Singularity image from the Docker image

```
singularity pull --dir ~/Downloads docker://evgenyneu/hello_pawsey:1.1
```

Created `~/Downloads/hello_pawsey_1.1.sif` file.


## Make movies from PROMPI simulation using Singularity container

* Open directory with Python code:

```
cd /Users/evgenii/Documents/honours_project/logbook/a2020/a10/a13_singularity/code/make_movies
```

* Run `make_movie.py` inside Singularity container:

```
singularity exec -B /Users/evgenii/Documents/honours_project/logbook/data/a2020/a09/a17/2020_09_17_09_59_26_192x192x1_2000s_time_1.00_luminosity:/data ~/Downloads/hello_pawsey_1.1.sif python make_movie.py
```


## On Pawsey: create a Singularity image from the Docker image

```
ssh pawsey
salloc --partition=debugq --nodes=1 --ntasks=1 --time=00:05:00
module load singularity
singularity pull -F --dir /scratch/ew6/evgenyneu/singularity docker://evgenyneu/hello_pawsey:1.1
exit
```

Image created at: `/scratch/ew6/evgenyneu/singularity/hello_pawsey_1.1.sif`



## On Pawsey: run Python script to make movies under the Singularity container

* Open directory with Python script:

```
ssh pawsey
cd /scratch/ew6/evgenyneu/logbook/a2020/a10/a13_singularity/code/make_movies
```

* Run `make_movie.py` inside Singularity container:

```
salloc --partition=debugq --nodes=1 --ntasks=1 --time=00:05:00
module load singularity
singularity exec -B /scratch/ew6/evgenyneu/prompi_output/2020_09_17_09_59_26_192x192x1_2000s_time_1.00_luminosity:/data /scratch/ew6/evgenyneu/singularity/hello_pawsey_1.1.sif python make_movie.py
exit
```

Created movie at `/scratch/ew6/evgenyneu/prompi_output/2020_09_17_09_59_26_192x192x1_2000s_time_1.00_luminosity/movies/sim_2020-10-13_14-10-16.mp4`.
