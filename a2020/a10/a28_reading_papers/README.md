# Reading, annotating and summarising papers

* [Convective heat transport (Anders, Brown 2017)](https://ui.adsabs.harvard.edu/abs/2017PhRvF...2h3501A/abstract).

* [Overshooting (Dintrans 2009)](https://ui.adsabs.harvard.edu/abs/2009CoAst.158...45D/abstract).

* [Turbulent Convection (Meakin, Arnett 2007)](https://ui.adsabs.harvard.edu/abs/2007ApJ...667..448M/abstract).
