# Learning about honours literature review

Reading ["Writing a literature review"](https://lms.monash.edu/mod/book/view.php?id=7076608) article on Moodle.

## Aim of literature review

* For physics honours, use [narrative review type](https://www.sciencedirect.com/topics/psychology/narrative-review): background + relevant research finding.

* **Aim**: provide background and justify my honours project by addressing the gaps in the literature.



## My research question

* **Topic**: Hydrodynamics.

* **Question**: Are 2D hydro simulations sufficient for understanding convective boundaries?

* **Aim**: In this project I want to determine if 2D hydro simulations are sufficient for understanding convective boundaries.

<img src="a2020/a10/a14_learning_about_literature_review/images/good_research_question.png" width=400 alt="Good research question">

[Source](https://lms.monash.edu/mod/book/view.php?id=7076608&chapterid=689987)

**Problem**: my question can be answered with yes/no


## Finding literature

### Identify main concepts:

* Hydrodynamics

* Hydrodynamical simulations

* 2D hydrodynamics simulations

* 3D hydrodynamics simulations

* Convective boundaries


### Find relevant search terms

* Fluid dynamics

* Turbulence

* High Reynolds number

* Convective boundaries

* Boundary layers


### Preliminary search

* [Google Scholar](https://scholar.google.com)

* [Scopus](https://www.scopus.com)

* Web of Science Core Collection

Accessed through [guides.lib.monash.edu/subject-databases](https://guides.lib.monash.edu/subject-databases)

<img src="a2020/a10/a14_learning_about_literature_review/images/monash_library_subject_databases.png" width="200" alt="Monash Library subject databases">

Example of search on Scopus:

<img src="a2020/a10/a14_learning_about_literature_review/images/search_example.png" width="600" alt="Scopus search  example">
