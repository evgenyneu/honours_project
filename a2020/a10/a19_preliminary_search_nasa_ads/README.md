# Preliminary search on NASA ADS

Following Simon's advice, I want to try NASA ADS to search for papers.


### First search

* abs: ( ("fluid dynamic*"  OR  hydrodynamic*) AND convect* AND boundar*) AND year:2009-2025

* [Search results](https://ui.adsabs.harvard.edu/search/q=abs%3A%20(%20(%22fluid%20dynamic*%22%20%20OR%20%20hydrodynamic*)%20AND%20convect*%20AND%20boundar*)%20AND%20year%3A2009-2025&sort=date%20desc%2C%20bibcode%20desc&p_=0)

* 450 documents.


### Added "stellar"

* abs: ( ("fluid dynamic*"  OR  hydrodynamic*) AND convect* AND boundar* AND stellar) AND year:2009-2025

* [Search results](https://ui.adsabs.harvard.edu/search/q=abs%3A%20(%20(%22fluid%20dynamic*%22%20%20OR%20%20hydrodynamic*)%20AND%20convect*%20AND%20boundar*%20AND%20stellar)%20AND%20year%3A2009-2025&sort=date%20desc%2C%20bibcode%20desc&p_=0)

* 131 documents.


### Including "2D AND 3D", remove bounda*

* abs: ( turbulen* AND convect* AND (solar OR stellar) AND (2D AND 3D)) AND year:1994-2025

* [Search results](https://ui.adsabs.harvard.edu/search/q=abs%3A%20(%20turbulen*%20AND%20convect*%20AND%20(solar%20OR%20stellar)%20AND%20(2D%20AND%203D))%20AND%20year%3A1994-2025&sort=date%20desc%2C%20bibcode%20desc&p_=0)

* 55 Results.


### Removing "turbulen*" and adding "simulation"

* abs: ( convect* AND (solar OR stellar) AND (2D AND 3D) AND simulation) AND year:1998-2025

* [Search results](https://ui.adsabs.harvard.edu/search/q=abs%3A%20(%20convect*%20AND%20(solar%20OR%20stellar)%20AND%20(2D%20AND%203D)%20AND%20simulation)%20AND%20year%3A1998-2025&sort=date%20desc%2C%20bibcode%20desc&p_=0)

* 172 documents.

* Filter by "refereed", results almost identical to Scopus.


## Preliminary search

* abs: ( convect* AND (solar OR stellar) AND (2D AND 3D) AND simulation) AND year:1998-2025 and -abs: supernova

* [Search results](https://ui.adsabs.harvard.edu/search/filter_property_fq_property=AND&filter_property_fq_property=property%3A%22refereed%22&fq=%7B!type%3Daqp%20v%3D%24fq_property%7D&fq_property=(property%3A%22refereed%22)&q=abs%3A%20(%20convect*%20AND%20(solar%20OR%20stellar)%20AND%20(2D%20AND%203D)%20AND%20simulation)%20AND%20year%3A1998-2025%20and%20-abs%3A%20supernova&sort=date%20desc%2C%20bibcode%20desc&p_=0)

### Papers

* [Comparison of 2D and 3D compressible convection in a pre-main sequence star (Pratt+ 2020)](https://ui.adsabs.harvard.edu/abs/2020A%26A...638A..15P/abstract)

* [Convective heat transport in stratified atmospheres at low and high Mach number (Anders, Brown 2017)](https://ui.adsabs.harvard.edu/abs/2017PhRvF...2h3501A/abstract)

* [A New Model for Mixing by Double-diffusive Convection (Semi-convection). III. Thermal and Compositional Transport through Non-layered ODDC (Moll+ 2016)](https://ui.adsabs.harvard.edu/abs/2016ApJ...823...33M/abstract)

* [2D or Not 2D: The Effect of Dimensionality on the Dynamics of Fingering Convection at Low Prandtl Number ( Garaud, Brummell 2015)](https://ui.adsabs.harvard.edu/abs/2015ApJ...815...42G/abstract)

* [Radial Stellar Pulsation and Three-dimensional Convection. III. Comparison of Two-dimensional and Three-dimensional Convection Effects on Radial Pulsation ( Geroux, Deupree 2014)](https://ui.adsabs.harvard.edu/abs/2014ApJ...783..107G/abstract)

* [Overshooting (Dintrans 2009)](https://ui.adsabs.harvard.edu/abs/2009CoAst.158...45D/abstract)

* [Generation and Propagation of Internal Gravity Waves: Comparison between Two- and Three-Dimensional Models at Low Resolution (Kiraga+ 2005)](https://ui.adsabs.harvard.edu/abs/2005AcA....55..205K/abstract)

* [Turbulent convection: is 2D a good proxy of 3D? (Canuto 2000)](https://ui.adsabs.harvard.edu/abs/2000A%26A...357..177C/abstract)
