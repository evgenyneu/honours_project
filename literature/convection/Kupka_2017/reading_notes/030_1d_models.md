# 3 One dimensional modelling (p. 47)

1D models of turbulent flows:

* Performs horizontal averaging of basic fields: $`\rho`$, $`\mu = \rho u`$, $`e = \rho E`$.

* Ensemble averaging: averaging over many slightly different initial conditions (realisations).

**Quasi-ergodic hypothesis**: time average of single realisation is equal to an average over many realisations.

## 3.2 Phenomenological approach (p. 49)

**Phenomenological models** that involve concepts (like rising bubbles) that are neither derived directly from Navier-Stokes equations nor confirmed by experiment or numerical simulations.

Example: MLT, which has fictitious bubbles that are not observed anywhere.

### 3.2.1 Models and physics (p. 49)

**Viscosity** (thickness) is resistance of fluid to deformation, created by electrostatic forces between molecules. Honey is more viscous (thick) than water.

**Turbulence** makes fluids more viscous, (i.e. increases their thickness).

**Turbulent viscosity** $`\nu_{\text{turb}}`$ increases effective viscosity:

$`\nu_{\text{eff}} = \nu + \nu_{\text{turb}}`$,

where $`\nu`$ is kinematic viscosity.


### 3.2.2 Mixing length treatment (p. 50)

See mixing length theory (MLT) is described in convection_mixing_length_theory.md.


### 3.2.3 A more recent, parameter-less sibling (p. 54)

* Model of convective flux by Pasetto et al. (2014).

* Claims to have no free parameters.

#### Problems

* Like MLT it invokes convective elements.

* Considers flow without turbulence, which contradicts LES.


### 3.2.4 Non-local mixing length models (p. 55)

* Gough (1977a, b) and Unno (1967): two second order equations, but includes idea of bubbles and the mixing length.

* Grossman et al. (1993) generalisation of MLT but provides a non-closed system of equation impossible to solve.


### 3.2.5 Further models specifically designed to calculate overshooting (p. 56)

Overshooting models involve problematic approximations and assumptions and can only be applied to special cases.



## 3.3 More general models of turbulent convection (p. 58)

Theories that don't involve fictitious blobs.

### 3.3.1 Summary of methods

**Reynolds decomposition** or **Reynolds splitting**: decompose a quantity $`A_i`$ (for example, velocity, temperature) into an average $`\overline{A_i}`$ and fluctuations $`A_i'`$ around it:

$`A_i = \overline{A_i} + A_i'`$.

Decomposed quantities are substituted into Navier-Stokes equations.

#### Closure models

1. **One-point closure models**: use averages of variables evaluated at specific point in space and time. Example: MLT

2. **Two-point closure models**: consider correlations (products?) of functions evaluated at different points in space, such as velocity differences. Main tool for studying homogeneous turbulence. Mathematically complex.


### 3.3.2 Non-local models of turbulent convection (p. 61)

Incomprehensible.


### 3.3.3 Non-local Reynolds stress models (p. 62)

Incomprehensible.


### 3.3.4 Rotating convection and two remarks (p. 68)

Unintelligible.
