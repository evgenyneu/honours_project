# 5 Multidimensional modelling: numerical methods (p. 87)

## 5.1 Stellar convection modelling: the numerical problem


### Problems of numerical modelling

* Smallest spatial scales are too expensive, so we use scales as small as we can afford.

* Time steps are too small to analyse **slow convection** flows because we need to include sound speed in Courant condition. Thus calculation takes forever. Solution:
    * Modify Navier Stokes equations equations to ignore or reduce sound speed (sec 4.3-4.7).
    * Use different numerical methods.

* Modelling large portion of star requires **spherical coordinates** that lead to division by zero at the center and very small grids cells near the poles.


## 5.2 Methods for general hydrodynamics (p. 88)

Numerical methods for solving Euler part of Navier Stokes equation equations are not trivial because they produce **discontinuities** and **artificial diffusivity**.

### 5.2.1 Numerical conservation methods: a few basic issues (p. 89)

* The numerical methods need to conserve mass, momentum and energy.

* But anaelastic and low Mach number approximation are not conservative.

* Schemes use **conserved** (density, momentum, energy) or **natural** (density, velocity, pressure, temperature) variables.


### 5.2.2 Stability, dissipation (p. 91)

* The Euler equation is solved explicit method (implicit don't work) which requires small time steps from Courant condition.

* Efficient implicit methods are used for diffusive terms, such as radiative diffusivity.


Simon's note: implicit codes are MUSIC code (Exeter university), Maestro.



### 5.2.3 Classical finite difference schemes (p. 92)

Scheme have stability problems that are fixed by adding artificial diffusive terms to equations.


### 5.2.4 Riemann solvers (p. 92)

We want to find value of $`u`$ state at next time step $`n+1`$:

$`u^{n+1}_i = u^{n}_i - \Delta t \frac{F_{i+1/2} - F_{i-1/2}}{\Delta x}`$.

For this we need to estimate the fluxes $`F_{i+1/2}`$ and $`F_{i-1/2}`$

![grid](images/050_numerical_methods/grid_flux.png)

The fluxes $`F_{i+1/2}`$ and $`F_{i-1/2}`$ can be found in two ways

* Exact solution by solving non-linear equations, can be difficult.

* Approximate solutions using a Riemann solver such as Kurganov-Tadmor, HLL, HLLE, HLLC etc.

#### What is the problem with numerical solutions?

* Methods create diffusion and/or unwanted oscillations near sharp edges:

![Problems with solutions](images/050_numerical_methods/problems_with_solutions.png)

### 5.2.5 Classic higher order schemes using Riemann solvers (p. 97)

Instead of using discrete values of $`u`$ state for the left and right $`x`$ grid points:

![piecewise state](images/050_numerical_methods/piecewise_state.png)

we use interpolation ([MUSCL scheme](https://en.wikipedia.org/wiki/MUSCL_scheme)) to find left and right values $`u_{i+1/2}^L`$, $`u_{i+1/2}^R`$:

![piecewise state](images/050_numerical_methods/interpolated_state.png)

* The interpolation uses a [slope limiter](https://en.wikipedia.org/wiki/Flux_limiter), such as minmod, to reduce oscillations near discontinuities. Also called, **TVD scheme** (Total Variation Diminishing).

* The method produces high accuracy in smooth regions.

* Instead of linear, a quadratic interpolation can be used (PPM, Piecewise Parabolic Method).

* Once the interpolated values $`u_{i+1/2}^L`$, $`u_{i+1/2}^R`$ are found, we can then use them with an approximate Riemann solver, such as Kurganov-Tadmor, to find the fluxes $`F_{i+1/2}`$ and $`F_{i-1/2}`$ between the grid points.


### 5.2.6 ENO (essentially non-oscillatory) schemes (p. 100)

The problem: slope limiters are 1st near smooth extrema.

The goal: ENO schemes want to achieve higher order accuracy both in smooth and sharp regions.

Suppose we want to find value of v in between the data points. Interpolating with a parabola points will produce an undershoot, which will produce the oscillations:

![ENO undershoot](images/050_numerical_methods/001_eno_undershoot.png)

Instead we draw two more parabolas, one to the left, one to the right and **choose the smoothest** one (which is a line here):

![ENO undershoot](images/050_numerical_methods/002_eno_three_parabolas.png)


### 5.2.8 Spectral methods (p. 105)

Incomprehensible.


## 5.3 Direct low Mach number modelling (p. 107)

Very general and non-specific.


## 5.4 Sphericity and resolution (p. 109)

Very general.


## 5.5 Time marching (p. 112)

Incomprehensible.
