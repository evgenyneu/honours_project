# 4. Multidimensional modelling: the equations (p. 69)

Navier-Stokes equations are harder to solve:

* Near the surface convective velocity is close to sound speed

* But in interior, convective velocity are much smaller than sound speed. This leads to times steps that are much smaller than convective time scales.

* This requires very long computation time.


## 4.1 Conservation laws (p. 69)

Conservation of mass, momentum and energy.

### 4.1.1 The 1D case (p. 69)

The change in amount of stuff $`v = x(x, t)`$ at a point in space is equal to change in flux $`-f(v)`$ (flux is amount of stuff that moved in and out of the two boundaries of that point):

$`\partial_t v + \partial_x f(v) = 0`$


### 4.1.2 The 3D case (p. 70)

Same for 3D: change is stuff $`v = v(\vec{x}, t)`$ over time $`\partial_t v`$ is equal to divergence of stuff $`-\text{div} \ \vec{f}(v)`$:

$`\partial_t v + \text{div} \ \vec{f}(v) = 0`$,

where $`\vec{f} = (f_1, f_2, f_3)`$ is flux in direction x, y, z.

In hydro we have five conserved quantities: $`\vec{v} = (\rho, \mu_1, \mu_2, \mu_3, e)`$ which are densities for mass, x, y, z components of momentum and energy.


## 4.2 Compressible flow: the Euler and Navier–Stokes equations  (p. 71)

Use Navier–Stokes equations.

### 4.2.2 Radiative heating  (p. 72)

* We need to compute radiative heating rate $`q_\text{rad}`$.

* We can use diffusion approximation and get $`q_\text{rad}= -K_\text{rad} \Delta T`$, where $`K_\text{rad}`$ is a constant radiative conductivity.

### 4.2.3 Viscosity  (p. 73)

The tensor viscosity $`\pi`$ is

![Tensor viscocity](images/040_multidimensional_modelling/tensor_viscocity.png)

where,

* $`\eta`$ is **dynamic viscosity**, $`\eta = \nu \rho`$, $`\nu`$ is kinematic viscocity,
* $`\zeta`$ is **second** or **bulk viscosity**, a function of temperature, density and internal energy.

#### Neglecting viscosity

* $`\zeta = 0`$ almost always in astrophysics.

* $`\pi = 0`$ because viscosity acts on length scales orders of magnitude smaller than affordable grid size.

Navier-Stokes equations become Euler equations. But viscosity is an important feature of turbulence and some numerical methods include artificial viscosity or have numerical diffusivity built-in by design.


## 4.3 Equations for low-Mach-number flows  (p. 74)


### 4.3.1 The Boussinesq approximation (p. 74)

#### Assumptions

* Convection layer is very thin.

* Flow speed are much smaller than sound speed (i.e. Mach number is small).

* Horizontal variation of quantities is very small.

Continuity equation reduces to

$`\text{div} \ \vec{u} = 0`$

(i.e. same as in incompressible flow).


### 4.3.2 The anelastic approximation (p. 76)

#### Assumptions

* No sound waves.

* Assumes deviations from average density $`\overline{\rho}`$ at each height are very small.

* The Mach number is very small.

Continuity equation reduces to

$`\text{div} \ \overline{\rho} \vec{u} = 0`$

#### Notes

* There are many different implementations of anelastic approximation (p. 78).

* Boussinesq approximation is a simplified version of anelastic approximation.



### 4.3.5 The anelastic approximation: tests (p. 80)

* Anelastic approximation  differs (heat flux, velocities) from full Navier Stokes equations by 30% if vertical temperature differs by 30%.

* Energy is not conserved.

Verdict: it's not clear when anelastic approximation can be applied, so it's not very useful.


## 4.4 The pseudo incompressible and the low Mach number model (p. 83)

### Assumptions

* Mach number is small.

* Horizontal pressure fluctuations are small.

Verdict: of limited use since it's unclear when it is accurate.


## 4.5 The reduced speed of sound technique (p. 85)

Speed of sound is reduced that allows larger time steps.
