# 6 Modelling: limitations, tasks, and chances (p. 128)

## 6.1 Requirements for astrophysical convection modelling (p. 129)

* Agree with observations.

* Low computation cost.

* No free parameters (debatable).


### 6.1.1 The issue of parameter freeness (p. 129)

All models have internal parameters, not just MLT:

* Choice of the grid, resolution.

* The choice of numerical discretization method.

* Numerical viscosity (built into numerical scheme or added).

* Parameters of the boundary conditions. For example, adding energy flow from the bottom boundary of the box.



#### Requirements of internal model parameters

* Model should be universal and describe any type of star without changing the parameters.

* Model should be robust: small changes to parameter should not change the outcomes.

* Parameter calibration should be done based on math and physics, and not on observed data. Parameters should not be tuned in order to fit observations (like $`\alpha`$ in MLT).
