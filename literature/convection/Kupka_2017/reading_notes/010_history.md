## 1.1 History

### 1.1.1 The beginning

* Solar granulation (Herschel 1801).
* Solar atmosphere is radiative rather than convective (Schwarzschild 1906).
* Convection zone below solar surface (Unsöld 1930).
* Mixing length theory (MLT) invented by Prandtl (1925), Biermann (1932).


### 1.1.2 Classical modelling

* Realistic models (Navier–Stokes) were computationally expensive.

* MLT was invented by Prandtl (1925), improved by Böhm-Vitense (1958) and still dominates.

* Disadvantages
	* uncertainties from physical approximations and assumptions.
	* not universal, can be applied to limited range of parameters.


### 1.1.3 Non-local models

* **Local models**: at each point solve and equation.

* **Non-local**: have depth variable, solve ODE. Needed to model overshoot and pulsating stars.

**Overshoot**: overlap of convective area with a stable one.

#### Disadvantages of realistic models

* Computational complex.

* Still require additional assumptions (actual models in use today), can not just rely on first principles (on Navier-Stokes Equations).

* Hard to test (although astro-seismology is helping).

* Classical models can be just as good (parameter tweaking).

* Require teams (one person is not enough).


#### Research directions

1. Surface, solar granulation.

1. Interiors spherical shells in non-Cartesian coordinates.


#### Semi-convection

Occurs when star's region is stable according to Ledoux criterion (molecular weight/chemical composition) by unstable according to Schwarzschild criterion (temperature gradient).

* In early development: no MLT-like model exist, numerical simulation are recent.
