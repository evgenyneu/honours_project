# 2 What is the difficulty with convection?

# 2.1 The hydrodynamical equations and some solution strategies


## 2.1.1 Hydro equations

* Continuity (conservation of mass)

* Navier-Stokes m a = F (conservation of momentum).

* Conservation of energy.


## 2.1.2 Solution strategies

* Direct numerical simulations (DNS): do not assume any additional properties.

* Use volume averages (e.g. LES - large eddy simulations), assume viscocity is the same if used higher resolution.

* Alternatives to NSE (Navier-stokes equations):

	* Lattice Boltzmann Methods (LBM)

	* Smooth particle hydrodynamics (SPH)



# 2.2 Spatial grids for numerical simulations of stellar convection


## 2.2.1 Constructing a grid for simulating the entire solar convection zone


### Resolution scales

* Colmogorov scale
	* $`l_d`$=1 cm through most of convection zone.
	* $`l_d`$=1 m just below the surface.

* Boundary layer between convection and radiation zone:
	* $`\delta_{\text{min}}`$ = 30 m: at bottom of convection
	* $`\delta`$ = 30 km: at surface.

* $`H_p`$ = 150 km: pressure scale height at solar surface and down to about 20 $`H_p`$. $`H_p`$ = 50,000 km at bottom of Solar convection zone.

* h=3 km, higest resolution acheived in solar granule simulation.


### How expensive is hydro for entire Sun or its convection zone?

Table 0: Total grid points and memory needed for single time snapshot.

| Resolution | Description | Total grid points | Memory needed [bytes] |
|:----------:|:------:|:-----------:|:---------------------:|
| $`l_d=`$ 1 cm  | Kolmogorov scale| $`10^{32}`$ 		   |      $`10^{34}`$        |
| $`\delta_{\text{min}}`$ = 30 m | Bottom boundary | $`10^{22}`$    |      $`10^{24}`$        |
| $`\delta`$ = 15 km | Top boundary | $`10^{14}`$   |      $`10^{16}`$        |
| $`H_p`$ = 150 km | Pressure scale height | $`10^{13}`$ | $`10^{15}`$ |

* Maximum supercomputer memory available today is about $`10^{15}`$ byte = 1 Petabyte (PB).

* Minimum memory required to hold five variables is 80 bytes, but most codes require 5x-10x more memory per cell.

#### Conclusion

* Impossible with supercomputers of today. $`H_p`$ = 150 km is barely possible in terms of memory, but simulations require large number of time steps.

* Thus, simulations are limited to small boxes or spherical shells.


## 2.2.2 Computing grids for affordable problems

* Modern 3D simulations have between 100 and 500 grid cells per spatial direction, suitable for available computer memory.

* But there are other costs apart from memory.

### Box-in-a-star

* Close to a surface, includes photosphere.
* Can use Cartesian coordinates.
* Horizontal boundary can be periodic.
* Challange: suitable vertical boundary conditions.
* Box, can be inside the star.
* Can be shell-in-a-star with curved geometry.

### Entire star

* Used for AGBs (excluding cores) and supernovaue.
* Other types: simulations with mapped grids and interpolation between grids, optimized grid geometry



## 2.3 Hydrodynamical simulations and the problem of time scales

### 2.3.1 General principles

* We want to know $`\rho`$, $`u`$ and $`E`$ values at different times.

* Set initial conditions: from approximations, 1D stellar structure or atmosphere models, or scaled from previous a simulation.

* Simulation stages:
	* First, run for relaxation time: $`t_{\text{rel}}`$, to achive stationary state,
	* Next, run the numerical study for $`t_{\text{stat}}`$.

* Reassuring results: different initial conditions, resolutions, numerical methods resulted in similar granulation pattern.

* Conclusion: numerical simulations may have some meaning.



### 2.3.2 Time steps and time resolution in numerical simulations of stellar convection

Time step is limited by:


$`\Delta t_{\text{adv}} \leq C \ \frac{min(\Delta x, \Delta y, \Delta z)}{max(|u| + c_s)}`$, (1)

where C ~ 1 (depends on numerical method), c_s is sound speed, |u| is flow speed. This is derived from  von Neumann stability analysis of NSE advection terms.

* Reason: convection changes the state of the grid cell on order of $`\Delta t_{\text{adv}}`$ by more than several percent.

* Example: $`\Delta t < 1 \ s`$ on surface of conviction zone (h ~ 15 km, sound speed 10 km/s)



### 2.3.3 Implications from $`\Delta t_{\text{adv}}`$ for performing LES of stellar convection zones

* $`\Delta t_{\text{adv}}`$ is smallest near the surface because (see Eq. 1):
	* Higher spatial resolution is required to resolve vertical structure.
	* Flow speed u is higher near the surface.

* Surface condition $`\Delta t_{\text{adv}} < 1`$ s also limits the entire solar convection zone.

* To increase timestep, the surface layer is excluded in models of lower parts of convection zone.



### 2.3.4 Duration of numerical simulations of stellar convection

* $`t_{\text{rel}}`$: relaxation time, from about $`10^5`$ to $`10^6`$ time steps for current LES of stellar convection.

* $`t_{\text{stat}}`$: time needed to measure quantities of interest (time to get converged statistical averages). Depends on quantity we want to measure. For temperature T in Sun, $`t_{\text{stat}} \approx t_{\text{conv}} \approx 1`$ h or $`10^5`$ time steps. For stellar oscillations $`10^6`$ - $`10^8`$ time steps (close to computational limit).

#### Time scales

* $`t_{\text{nuc}}`$: Nuclear time scale. Largest.

* $`t_{\text{conv}}`$ convective turn over times scale: about 1 hour, and from 1000 to 10,000 $`\Delta t_{\text{adv}}`$, and $`10^5`$ time steps. See Eq. 15 on p.29. Approximate (a bit arbitrary): $`t_{\text{conv, loc}} \approx u_x/(2H_p)`$, evaluated a bit below supeadiabatic peak.

* $`t_{KH}`$: Kelvin–Helmholtz time scale, time needed to radiate away the gravitational energy of a star, Eq. 17 p. 29.

* $`t_{\text{therm}}`$: times scale on which thermal equilibrium is reached (energy production = loss). $`t_{KH} \approx t_{\text{therm}}`$.

* $`t_{\text{rad,diff}} = h^2 / \chi`$, where h is vertical travel distance, $`\chi`$ is radiative diffusivity (speed of heat transfer in specific material). Irrelevant, since radiative flux is negligible in solar convective zone.

* $`t_{\text{ac}}`$: sound crossing time (Eq. 16 p.29). $`t_{\text{ac}} = 1`$h for Sun (sound wave travel from surface to the center).

* $`t_{\text{hyd}}`$: hydrostatic time scale, relevant when star is not in hydrostatic equilibrium (formation, supernova). Usually not important.


#### Note

* Relaxation requires model to reach thermal equilibrium, therefore $`t_{\text{therm}}`$ is most important in simulation of convection.

* $`t_{\text{therm}}`$ can be very long, thus suitable initial conditions are needed to reduce $`t_{\text{rel}}`$ to about $`t_{\text{rel}} \approx 2 t_{\text{conv}} \approx 2`$ h, otherwise $`t_{\text{rel}} = t_{\text{therm}} \approx 100`$ h.



### 2.3.5 Implications from $`t_{\text{rel}}`$ and $`t_{\text{stat}}`$ and summary on computational costs (p.35)

Total number of time steps: $`N_t = \frac{t_{\text{rel}} + t_{\text{stat}}}{\Delta t}`$ are from $`10^6`$ to $`10^8`$.

Techniques of grid refinement (ANTARES) can reduce $`N_t`$ by at most 100x with parallel computation.


## 2.4 Effective resolution and consequences of insufficient resolution (p. 37)

* Insufficient spatial resolution significantly influences the measurements. For example, cooling rate and flow velocities differ by factor of 4 between fine and coarse resolution grids.

* Choice of resolution depends on the area of investigation.

* Stellar spectra is well reproduced with moderate resolution LES, but...

* ...for the bottom convection boundary current resolution is insufficient. No realistic 3D LES model of overshooting of bottom convection zone exists because it requires at least 10-100 times more memory than is currently available.



### Peclet Number

Pe = [convective]/[conductive] heat transport.

* $`Pe = \frac{U L}{\chi}`$ (U velocity, L length, $`\chi`$ thermal diffusivity).
* Pe = $`10^5-10^6`$ at bottom of convection zone.
* Pe = $`10`$ at top of convection zone.
* Best 3D LES produces $`\text{Pe}_{\text{eff}} \sim 150`$.
* Conclusion: $`\text{Pe}_{\text{eff}}`$ is sutable for surface, but 3-4 orders of magnitude smaller than necassary at the bottom of convection zone.

Consequence: currently MESA uses model of convection based on LES that extrapolates low Pe=10 of atmosphere into the Pe=$`10^6`$ of interior. This procedure is not valid.




## 2.5 Reducing dimensions: 2D simulations as an alternative to 3D ones? (p. 40)

* Changing from 3D (1000x1000x1000 grid) to 2D gives 30x increase in spatial resolution. But time step needs to be reduced by 30 times as well (Eq. 11 p.25).

* Such increase of resolution can be achieved in 3D by grid refinement (ANTARES code).

* Alternatively, one can increase number of time steps by 1000, if spatial 2D resolution is left unchanged.

### Price to pay for 2D

* Rotation effects will differ in 2D.

* Magnetic fields can't be modelled in 2D at all.

* Turbulence is a 3D phenomenon:

  * It acts differently in 2D. In 3D the energy transfer goes form larger scales to smaller ones (Kolmogorov energy cascade), and in 2D this is the other way around (from smaller to larger).

  * Important phenomenon is vortex stretching: larger eddies are stretched along direction perpendicular to their plane of rotation into smaller eddies rotating faster. This is only possible in 3D.

* Turbulence in small scale structures is different in 2D.

* Much stronger shock fronts in 2D.

* Larger overshooting zones in 2D.


## Computable problems and alternatives to non-computable problems (p. 42)

### 2.6.1 Modelling inherent problems versus the problem of scales

Two types of problems:

1. Uncertainties in initial state: equation of state, opacities, nuclear reaction rate, magnetic field etc.

2. Limited computing power


### 2.6.2 A list of examples: doable and undoable problems for LES of stellar convection (p. 43)

See table p.44.

* Only low resolution (h=15 km) small surface box or entire convection zone (800 points vertically) excluding surface are computable.

* Most problems are incomputable, even in principle.


### 2.6.3 The continued necessity of 1D models of stellar convection

* Moore's law (number of CPU transistors doubling every two years) appears to slow down.

* Increasing number of cores is not 100% efficient.

* 3D can't replace 1D stellar models for next few decades.
