# Pesky numbers in fluid dynamics

* **Nusselt number** Nu = [convective] / [conductive] heat transfer. Nu=1 is Pure conduction, Nu=10, laminar flow, Nu=100..1000 is turbulent flow.

* **Rayleigh number** Ra = time scales thermal transport via [diffusion] / [convection]. Ra < crit: no flow, conduction only. Ra > crit: by convection. Related to Nusselt number. Used when mass density is non-uniform (Rayleigh-Bénard convection).

* **Prandtl number**: Pr = [Momentum diffusivity (kinematic viscosity)] / [Thermal diffusivity]. Low number (mercury): conduction is more important. High numbers (oil): convection is better for energy transfer.

## Measures of turbulence

* **Reynolds number**: Re = [Inertial forces] / [Viscous forces]. Higher numbers -> more turbulence.

* **Peclet number**: Pe = rate of [advection] / [thermal diffusion].
