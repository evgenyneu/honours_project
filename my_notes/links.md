# Links

* [Relevant papers for lit review](https://docs.google.com/spreadsheets/d/1IrMU7_ZvJB2HpVx5Lv4mY8tBsg7UJGgrz01VODhiRwM/edit?usp=sharing).

* [Simon's Project Wiki](https://stellarmeme.net/wiki/doku.php?id=evgenii_s_honours_project_2020b)

* [Pawsey supercomputer documentation](https://support.pawsey.org.au/documentation/display/US/Supercomputing+Documentation)

* [RansX data](https://github.com/mmicromegas/ransX/tree/ransX_development ) analysis tool from Miro.

* [Notes](http://zingale.github.io/hydro1d/) with examples on computational Hydrodynamics.

* [Visit visualisation tool](https://wci.llnl.gov/simulation/computer-codes/visit/).

* [YT Python visualisation](https://yt-project.org/).
