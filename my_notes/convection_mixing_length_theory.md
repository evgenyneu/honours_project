Notes based on Carroll and Ostlie Ch. 11 Interiors of Stars, chapter "Energy transport and thermodynamics".

## Condition for stellar convection

$`|\frac{dT}{dr} |_{\text{act}} > |\frac{dT}{dr} |_{\text{ad}}`$

Aka **superadiabatic** condition.


#### Adiabatic temperature gradient $`\frac{dT}{dr} \Big{|}_{\text{ad}}`$

This is amount $`dT`$ the temperature of a bubble changes as it raises by distance $`dr`$. Here we assume adiabatic process when heat is not exchanged between the bubble and surroundings.

![Adiabatic temperture gradient](images/eq_89_adiabatic_temperature_gradient.png)

* $`\gamma \equiv \frac{C_P}{C_V}`$,
* $`C_P`$, $`C_V`$: specific hears at constant pressure and volume,
* $`\mu`$: mean molecular weight,
* $`m_H`$: mass of a proton (same as hydrogen nucleus),
* $`k`$: Boltzmann constant,
* $`G`$: gravitational constant,
* $`r`$: distance from the center of the star,
* $`M_r`$: mass of a star inside radius $`r`$,
* $`g`$: acceleration due to gravity.


#### Actual temperature gradient $`\frac{dT}{dr} \Big{|}_{\text{act}}`$

Is the temperature of the material surrounding the raising bubble. It is often calculated using the radiative gradient formula:

![Convective temperature gradient](images/eq_68_radiative_temperature_gradient.png)

## Mixing length theory (MLT)

**Mixing length theory** calculates the *convective flux* $`F_C`$ inside a star. $`F_C`$ is the amount of energy in Joules carried by bubbles raising inside a star through square meter per second. $`F_C`$ is a function of four types of variables:

$`F_C = F(\alpha, \beta, [\text{thermodynamic quantities}], [\text{temperature gradients}])`$


### 1. Mixing length parameter $`\alpha`$

* $`\alpha`$ is a free parameter chosen to match solar luminosity and temperature (or radius) at present age, $`0.5 <\alpha < 3`$.

* $`\alpha 2. \frac{l}{H_p}`$, where

    * $`l`$ is **mixing length**. It is the length a bubble raises before it dissipates into surroundings.

    * $`H_P`$ is **pressure scale height**, the length $`r`$ over which pressure $`P`$ decreases by $`e = 2.718`$:

      $`P = P_0 e^{-r/H_P}`$

    * $`H_P \approx R_{\odot} / 10`$ in the Sun.


### 2. Parameter $`\beta`$

* Another free parameter $`0 < \beta < 1`$ that is chosen to match model to observations.

* Used for estimating the average speed of the bubble raising over the mixing length $`l`$.


### 3. Thermodynamic quantities

* Density $`\rho`$.

* Temperature $`T`$.

* Mean molecular weight $`\mu`$.

* Acceleration due to gravity $`g`$.


### 4. Temperature gradient

$`\frac{dT}{dr} \Big{|}_{\text{ad}} - \frac{dT}{dr} \Big{|}_{\text{act}}`$ (See described above).

### Full scary version from C&O on p.418:

![Convective flux](images/eq_99_convective_flux_mixing_length.png)

### Why MLT sucks?

* It is built upon a fictitious concept of a raising bubble inside a star.

* Raising bubbles "are not observed in convective flows **anywhere** (Sun, Earth atmosphere and oceans, laboratory experiments of convection, or numerical simulations of convection in these objects)" (Kupka 2017 p. 49).

* Such theory is called "phenomenological", which means a theory that uses concepts not seen in nature or numerical simulations. Such theory is not based on first principles such as Navier-Stokes equations.

* Parameters $`\alpha`$ and $`\beta`$ are chosen to fit the model to observations. These parameters vary between stars and may even vary throughout regions of the same star.

* The model based on mixing length theory is not universal. It can't be used with $`\alpha`$ and $`\beta`$ chosen for one star to describe other stars, or even the same star at different stages of evolution.

* Convective energy flux $`F_C=0`$ outside the regions where $`\Big{|}\frac{dT}{dr} \Big{|}_{\text{act}} > \Big{|}\frac{dT}{dr} \Big{|}_{\text{ad}}`$. Thus, it can not model overshooting of matter form convective region.

### MLT is still widely used in stellar models because:

* Tradition: many stellar evolution and atmosphere models in use are built using MLT models.

* Easy to implement into codes and calibrate the $`\alpha`$ parameter.

* Full hydro models or full stellar convection zones are too computationally expensive.

* More complex phenomenological models are harder to calibrate to observations than MLT.


### MLT has been falsified

* Can not correctly predict temperature, pressure, sound speed, kinetic energy, up and down flows in convective regions.

* Disagreement with astroseismology (oscillation frequencies) and spectroscopy (line profiles, chemical abundances).


#### Falsification papers

* [Rosenthal et al. 1999](https://arxiv.org/abs/astro-ph/9803206): looks at solar oscillations in the atmosphere. Finds significant difference in high frequency oscillations between 3D and 1D models. Turbulent pressure provides additional support agains gravity.

* Could not find anything conclusive.


Aside: MLT gives speed of Solar convection of about 50 m/s.
