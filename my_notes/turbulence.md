# Turbulence is...

... no clear definition, no math criterion. When flow is:

* Random,

* Swirls on many scales

* Diffusive (mixes, spreads out particles, heat, momentum)

* High Re number.

* Dissipative (transfers energy from large to small eddies and then into heat)

Turbulence is generated when parts of fluid pass a solid surface (**Wall tubulence**) or other parts that move at different velocities (**free turbulence**.)
