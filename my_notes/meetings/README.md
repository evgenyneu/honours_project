



## 2020/10/19

* New daily.

* ABC.

* Read lit review assessment rubric.

* NASA ADS.

* Try Arxiv for fluid dynamics papers.

* Journal of Fluid Dynamics.

* Review for turbulence boundary layer.




## 2020/10/13

* The end of politicians book.

* Look at past lit reviews on Moodle.

* Read more general topic of fluid dynamics.

* Read about turbulence.

* High Reynolds number flows.

* Convective boundaries, boundary layers in fluid dynamics.

* Monash library.


## 2020/09/27

### Rerun the simulation

* Rerun 192x192x1, drop heating to 1% at 80s (BURN/volheat):

```
!     CCP LUMINOSITY update 7/March/2020

       lum = 1.2082151d-4
       lum = lum
       if(myid.eq.0) print*, 'VOLHEAT: Lum factor = 1.0'
       if(time.gt.80.0) lum = lum*0.01  !Drop heating to 1% for rest of
run. SWC 15/Sep/20
       if(time.gt.80.0.and.myid.eq.0) print*,'VOLHEAT: Dropping heating
to 1%***'
```

* Print out every second.



### Login to Pawsey visualisation (From Simon's wiki):

  * New version remote vis: https://remotevis.pawsey.org.au/

  * Old version remote vis: https://zeus.pawsey.org.au:3443/


### Install Visit locally

* Controls > Expressions > New: sqrt(sqr(velx) + sqr(vely))

* Use Simon's Fortran program [prompi2hdf5-cartesian2D.f90](a2020/a09/a28_making_movies_using_stretch_and_interval/code/prompi2hdf5-cartesian2D.f90) to convert from PROMPI to Visit format. Run with

```
h5fc -o prompi2hdf5_2D prompi2hdf5-cartesian2D.f90
```

* Check Visit plug, somewhere in PROMPI repository.

* Simon's Visit version is  2.13.1.


## 2020/09/22

## param.tseries

* In param.tseries start from 4 turnover to ignore the start.

* tavg: make it 3-4 turnover time.


## Movies

* In movies, narrow the color range, use log scale.

* Density/pressure plot: [horizontal average] - [value]. Perturbation plot. Color scale 1-2%.

* Plot speed.


## 2020/09/08

Reynolds average

https://ui.adsabs.harvard.edu/abs/1984JCoPh..54..174C/abstract

## 2020/08/03

* Ask Bernhard about part time.

* 25 page literature review.


### Read about

* Fluid dynamics.

* Navier Stokes equations.

* Fluid instability.

* Boundary layer problems.

* Reynolds number.
