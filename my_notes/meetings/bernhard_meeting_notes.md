## 2021/02/18

Honours orientation.

Lit review due: June 25.


## 2020/09/08

* Ok to use part of literature review, but it needs to fit well. Improve it using feedback.


## 2020/08/25

* Identify key seminal (classic) papers, study in details.

* Read few lit review, few main papers, then branch out.


## 2020/08/18

* Lit review due June 2021
* Can contain my project work.


## 2020/08/08

* Can work on project till March.

* Aim to have solid draft of thesis by Feb 2021.


### Literature review

* Limit: 25 pages
